using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Obs
{
	/// <summary>
	/// Link の概要の説明です。
	/// </summary>
	public class Link : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Button ButtonOK;
		protected System.Web.UI.WebControls.Button ButtonCancel;
		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.WebControls.Label LabelNone;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;

		#region Web フォーム デザイナで生成されたコード 
		//[SRC-03-06-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-06-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Link);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-06-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ｷｬﾝｾﾙﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
				this.ButtonCancel.Attributes["onclick"] = "self.window.close();";
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.ButtonOK);
					Common.SetText(sql, this.mType, this.ButtonDelete);
					Common.SetText(sql, this.mType, this.ButtonAdd);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelNone);
					this.DataGrid1.Columns[1].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGrid1.Columns[2].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGrid1.Columns[1].FooterText = Common.GetText(sql, this.mType, "Comment1");
					this.DataGrid1.Columns[2].FooterText = Common.GetText(sql, this.mType, "Comment2");
					#endregion
				}
				DataTable table = (DataTable)Common.GetSession(SessionID.Obs_Edit_Link);
				this.DataGrid1.DataSource = table;
				this.DataGrid1.DataBind();
				for(int i = 0; i < this.DataGrid1.Items.Count; i++)
				{
					TextBox LinkName = this.DataGrid1.Items[i].FindControl("TextBoxLinkName") as TextBox;
					LinkName.Text=HttpUtility.HtmlDecode(LinkName.Text);
				}
				//ﾘﾝｸが0件の場合
				if(table.Rows.Count == 0)
				{
					this.LabelNone.Visible = true;
					this.DataGrid1.Visible = false;
					this.ButtonDelete.Enabled = false;//行削除ﾎﾞﾀﾝを無効に
				}
				else
				{
					this.LabelNone.Visible = false;
					this.DataGrid1.Visible = true;
					this.ButtonDelete.Enabled = true;
				}
			}
		}
		#endregion

		/// <summary>
		/// ﾘﾝｸ追加ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonAdd_Click(object sender, System.EventArgs e)
		//[SRC-03-06-004]private void ButtonAdd_Click
		private void ButtonAdd_Click(object sender, System.EventArgs e)
		{
			this.AddRow();
			this.LabelNone.Visible = false;
			this.DataGrid1.Visible = true;
			this.ButtonDelete.Enabled = true;
		}
		#endregion

		/// <summary>
		/// ﾘﾝｸ削除ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonDelete_Click(object sender, System.EventArgs e)
		//[SRC-03-06-005]private void ButtonDelete_Click
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			DataTable table = new DataTable();
			table.Columns.Add("LinkName");
			table.Columns.Add("LinkUrl");
			table.Columns.Add("Checked");

			int selected = Convert.ToInt32(Request.Form["gn"]);
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				if(i != selected)
				{
					TextBox LinkName = this.DataGrid1.Items[i].FindControl("TextBoxLinkName") as TextBox;
					TextBox LinkUrl = this.DataGrid1.Items[i].FindControl("TextBoxLinkUrl") as TextBox;
					DataRow row = table.NewRow();
					row["LinkName"] = LinkName.Text.Trim();
					row["LinkUrl"] = LinkUrl.Text.Trim();
					row["Checked"] = false;
					table.Rows.Add(row);
				}
			}
			//ﾘﾝｸが0件の場合
			if(table.Rows.Count == 0)
			{
				this.LabelNone.Visible = true;
				this.DataGrid1.Visible = false;
				this.ButtonDelete.Enabled = false;//行削除ﾎﾞﾀﾝを無効に
			}
			else
			{
				this.LabelNone.Visible = false;
				this.DataGrid1.Visible = true;
				this.ButtonDelete.Enabled = true;
			}

			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();
		}
		#endregion

		/// <summary>
		/// OKﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonOK_Click(object sender, System.EventArgs e)
		//[SRC-03-06-006]private void ButtonOK_Click
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			//入力ﾃﾞｰﾀﾁｪｯｸ
			if(this.DataCheck() == false) return;

			DataTable table = new DataTable();
			table.Columns.Add("LinkName");
			table.Columns.Add("LinkUrl");
			table.Columns.Add("Checked");

			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox LinkName = this.DataGrid1.Items[i].FindControl("TextBoxLinkName") as TextBox;
				TextBox LinkUrl = this.DataGrid1.Items[i].FindControl("TextBoxLinkUrl") as TextBox;
				DataRow row = table.NewRow();
				row["LinkName"] = HttpUtility.HtmlEncode(LinkName.Text.Trim());
				row["LinkUrl"] = LinkUrl.Text.Trim();
				table.Rows.Add(row);
			}
			Common.SetSession(SessionID.Obs_Edit_Link, table);
			Common.ScriptStart();
			Common.ScriptOutput("self.opener.document.Form1.ButtonRefresh.click();");
			Common.ScriptOutput("window.close();");
			Common.ScriptEnd();
		}
		#endregion



		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 1行追加
		/// </summary>
		/// <param name="LinkName"></param>
		/// <param name="URL"></param>
		#region private void AddRow()
		//[SRC-03-06-007]private void AddRow
		private void AddRow()
		{
			DataTable table = new DataTable();
			table.Columns.Add("LinkName");
			table.Columns.Add("LinkUrl");
			table.Columns.Add("Checked");

			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox LinkName = this.DataGrid1.Items[i].FindControl("TextBoxLinkName") as TextBox;
				TextBox LinkUrl = this.DataGrid1.Items[i].FindControl("TextBoxLinkUrl") as TextBox;
				DataRow row = table.NewRow();
				row["LinkName"] = LinkName.Text.Trim();
				row["LinkUrl"] = LinkUrl.Text.Trim();
				row["Checked"] = false;
				table.Rows.Add(row);
			}
			if(true)
			{
				DataRow row = table.NewRow();
				row["LinkName"] = string.Empty;
				row["LinkUrl"] = string.Empty;
//				row["Checked"] = true;
				table.Rows.Add(row);
			}

			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();
		}
		#endregion

		/// <summary>
		/// 入力ﾃﾞｰﾀﾁｪｯｸ
		/// </summary>
		/// <returns></returns>
		#region private bool DataCheck()
		//[SRC-03-06-008]private bool DataCheck
		private bool DataCheck()
		{
			//ﾘﾝｸ名、URLの入力が空でないかﾁｪｯｸ
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox tbLinkName = this.DataGrid1.Items[i].FindControl("TextBoxLinkName") as TextBox;
				TextBox tbUrl = this.DataGrid1.Items[i].FindControl("TextBoxLinkUrl") as TextBox;
				if(Common.CnvToDB(tbLinkName.Text) == string.Empty)
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertLinkName"));
					return false;
				}
				if(Common.CnvToDB(tbUrl.Text) == string.Empty)
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertURL"));
					return false;
				}
			}
			return true;
		}
		#endregion
        




































	}
}
