using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;

namespace EPSNet.Obs
{
	/// <summary>
	/// Lib の概要の説明です。
	/// </summary>
	public class Lib : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelTitle1;
		protected System.Web.UI.WebControls.Label LabelFileName;
		protected System.Web.UI.WebControls.Label LabelComment;
		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Label LabelNone;
		protected System.Web.UI.WebControls.Label LabelTitle2;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Button ButtonOK;
		protected System.Web.UI.WebControls.Button ButtonCancel;
		protected System.Web.UI.WebControls.TextBox TextBoxComment1;
		protected System.Web.UI.WebControls.Label LabelCommentLen;
		protected System.Web.UI.WebControls.Button ButtonAddHide;
		protected System.Web.UI.HtmlControls.HtmlInputFile File1;

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			this.ButtonAddHide.Click += new System.EventHandler(this.ButtonAddHide_Click);
			this.DataGrid1.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_DeleteCommand);
			this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================

		private const int COL_CommentLen = 3;
		private const int COL_ServerPath = 4;
		private const int COL_ServerName = 5;
		private const int COL_UserPath = 6;
		private const int COL_FullPath = 7;

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Lib);

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		
		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-05-001]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			this.ButtonAdd.Enabled = true;
			if(this.IsPostBack == false)
			{
				//ｷｬﾝｾﾙﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
				this.ButtonCancel.Attributes["onclick"] = "self.close();";
				//添付ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
				//this.ButtonAdd.Attributes["onclick"] = "DisableButton();document.Form1.ButtonAddHide.click();";
				this.ButtonAdd.Attributes["onclick"] = "Upload();";
				//OKﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
				this.ButtonOK.Attributes["onclick"] = "return funcOK('" + Common.GetText(this.mType, "ConfirmOK") + "');";

				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.ButtonOK);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					Common.SetText(sql, this.mType, this.ButtonAdd);

					Common.SetText(sql, this.mType, this.LabelTitle1);
					Common.SetText(sql, this.mType, this.LabelTitle2);
					Common.SetText(sql, this.mType, this.LabelFileName);
					Common.SetText(sql, this.mType, this.LabelComment);
					Common.SetText(sql, this.mType, this.LabelNone);
					Common.SetText(sql, this.mType, this.LabelCommentLen);

					//削除ボタン名称変更
					ButtonColumn bc = this.DataGrid1.Columns[1] as ButtonColumn;
					bc.Text = Common.GetText(this.mType, "ButtonDelete");
					#endregion
				}
				//ﾌｧｲﾙ一覧作成
				DataTable table = (DataTable)Common.GetSession(SessionID.Obs_Edit_Lib);
				this.DataGrid1.DataSource = table;
				this.DataGrid1.DataBind();
				for(int i = 0; i < this.DataGrid1.Items.Count; i++)
				{
					TextBox Comment = this.DataGrid1.Items[i].FindControl("TextBoxComment2") as TextBox;

				}
				if(table.Rows.Count == 0)
				{
					this.LabelNone.Visible = true;
					this.DataGrid1.Visible = false;
				}
				else
				{
					this.LabelNone.Visible = false;
					this.DataGrid1.Visible = true;
				}
			}
		}
		#endregion		

		/// <summary>
		/// OKﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonOK_Click(object sender, System.EventArgs e)
		//[SRC-03-05-002]private void ButtonOK_Click
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			DataTable table = new DataTable();
			table.Columns.Add("UserPath");
			table.Columns.Add("ServerPath");
			table.Columns.Add("ServerName");
			table.Columns.Add("FullPath");
			table.Columns.Add("Comment");
			table.Columns.Add("CommentDisp");

			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox Comment = this.DataGrid1.Items[i].FindControl("TextBoxComment2") as TextBox;
				DataRow row = table.NewRow();
				row["ServerPath"]  = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerPath].Text);
				row["ServerName"]  = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerName].Text);
				row["Comment"]     = Comment.Text;
				row["CommentDisp"] = HttpUtility.HtmlEncode(Comment.Text);
				row["UserPath"]    = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_UserPath].Text);
				row["FullPath"]    = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_FullPath].Text);
				table.Rows.Add(row);
			}
			Common.SetSession(SessionID.Obs_Edit_Lib, table);
			Common.ScriptStart();
			Common.ScriptOutput("self.opener.document.Form1.ButtonRefresh.click();");
			Common.ScriptOutput("window.close();");
			Common.ScriptEnd();
		}
		#endregion			

		/// <summary>
		/// 添付ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonAdd_Click(object sender, System.EventArgs e)
		//[SRC-03-05-003]private void ButtonAddHide_Click
		private void ButtonAddHide_Click(object sender, System.EventArgs e)
		{
			try
			{
				//ﾌｧｲﾙｻｲｽﾞが0はｴﾗｰ
				if(this.File1.PostedFile.InputStream.Length == 0)
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertUpload"));
					return;
				}
				string filePath = Common.GetCommonLibTmp();
				filePath = Path.Combine(filePath, Path.GetFileName(this.File1.PostedFile.FileName));				
				int i = 1;
				DataTable table = (DataTable)Common.GetSession(SessionID.Obs_Edit_Lib);
				//ｻｰﾊﾞ上でのﾌｧｲﾙ名重複を避ける
				while(this.IsExistFileName(Path.GetFileName(filePath)) == true)
				{
					filePath = Common.GetCommonLibTmp();
					filePath = Path.Combine(filePath, Path.GetFileNameWithoutExtension(this.File1.PostedFile.FileName) + i.ToString() + Path.GetExtension(this.File1.PostedFile.FileName));
					i++;
				}
				//ﾃﾞｨﾚｸﾄﾘがなければ作成
				if(Directory.Exists(Path.GetDirectoryName(filePath)) == false) Directory.CreateDirectory(Path.GetDirectoryName(filePath));
				this.File1.PostedFile.SaveAs(filePath);
				//ﾌｧｲﾙ一覧に追加
				this.AddRow(Path.GetFileName(this.File1.PostedFile.FileName), string.Empty, Path.GetFileName(filePath), this.TextBoxComment1.Text);
				this.TextBoxComment1.Text = string.Empty;

				this.LabelNone.Visible = false;
				this.DataGrid1.Visible = true;

			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}

		private void ButtonAdd_Click(object sender, System.EventArgs e)
		{

		}
		#endregion

		/// <summary>
		/// 削除ﾎﾞﾀﾝｸﾘｯｸｲﾍﾞﾝﾄ
		/// </summary>
		//[SRC-03-05-004]private void DataGrid1_DeleteCommand
		#region private void DataGrid1_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		private void DataGrid1_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataTable table = new DataTable();
			table.Columns.Add("UserPath");
			table.Columns.Add("ServerPath");
			table.Columns.Add("ServerName");
			table.Columns.Add("FullPath");
			table.Columns.Add("Comment");

			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				if(i != e.Item.ItemIndex)
				{
					TextBox Comment = this.DataGrid1.Items[i].FindControl("TextBoxComment2") as TextBox;
					DataRow row = table.NewRow();
					row["ServerPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerPath].Text);
					row["ServerName"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerName].Text);
					row["Comment"] = Common.CnvToDB(Comment.Text);
					row["UserPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_UserPath].Text);
					row["FullPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_FullPath].Text);
					table.Rows.Add(row);
				}
			}
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();
			if(table.Rows.Count == 0)
			{
				this.DataGrid1.Visible = false;
				this.LabelNone.Visible = true;
			}
			//PostBackが発生すると、File1のﾃﾞｰﾀがｸﾘｱされてしまうので、ｺﾒﾝﾄもｸﾘｱする
			this.TextBoxComment1.Text = string.Empty;
		}
		#endregion

		/// <summary>
		/// DataGrid1_ItemDataBound
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		#region private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		//[SRC-03-05-005]private void DataGrid1_ItemDataBound
		private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1) return;

			e.Item.Cells[COL_CommentLen].Text = this.LabelCommentLen.Text;

			//ﾌｧｲﾙが存在しなければ赤文字でﾘﾝｸは張らない
			HyperLink hl = e.Item.Cells[0].Controls[0] as HyperLink;
			if(hl.NavigateUrl == string.Empty)
			{
				e.Item.Cells[0].ForeColor = Color.Red;
			}

//			string path = Server.MapPath(string.Empty);
//			path = Path.Combine(path, Common.CnvFromDB(e.Item.Cells[COL_ServerPath].Text));
//			path = Path.Combine(path, Common.CnvFromDB(e.Item.Cells[COL_ServerName].Text));
//
//			if(File.Exists(path) == true)
//			{
//				//row["UserPath"] = this.DataGrid1.Items[i].Cells[COL_UserPath].Text;
//				//row["FullPath"] = Path.Combine(row["ServerPath"].ToString(), row["ServerName"].ToString());
//			}
//			else
//			{
//				//row["UserPath"] = "<font color=red>" + this.DataGrid1.Items[i].Cells[COL_UserPath].Text + "</font>";
//				//row["FullPath"] = string.Empty;
//			}

		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 1行追加
		/// </summary>
		/// <param name="userPath"></param>
		/// <param name="serverPath"></param>
		/// <param name="serverName"></param>
		/// <param name="comment"></param>
		#region private void AddRow(string userPath, string serverPath, string serverName, string comment)
		//[SRC-03-05-006]private void AddRow
		private void AddRow(string userPath, string serverPath, string serverName, string comment)
		{
			DataTable table = new DataTable();
			table.Columns.Add("UserPath");
			table.Columns.Add("ServerPath");
			table.Columns.Add("ServerName");
			table.Columns.Add("FullPath");
			table.Columns.Add("Comment");
			//既存のﾃﾞｰﾀを取得
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox Comment = this.DataGrid1.Items[i].FindControl("TextBoxComment2") as TextBox;
				DataRow row = table.NewRow();
				row["ServerPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerPath].Text);
				row["ServerName"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_ServerName].Text);
				row["Comment"] = Common.CnvToDB(Comment.Text);
				row["UserPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_UserPath].Text);
				row["FullPath"] = Common.CnvToDB(this.DataGrid1.Items[i].Cells[COL_FullPath].Text);
				table.Rows.Add(row);
			}
			//新規ﾃﾞｰﾀを追加
			{
				DataRow row = table.NewRow();
				row["UserPath"] = Common.CnvToDB(userPath);
				row["ServerPath"] = Common.CnvToDB(serverPath);
				row["ServerName"] = Common.CnvToDB(serverName);
				row["Comment"] = Common.CnvToDB(comment);
				row["FullPath"] = "../Common/GetFile.aspx?ServerName=" + Common.CnvToDB(serverName);
				table.Rows.Add(row);
			}

			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();
		}
		#endregion

		/// <summary>
		/// 指定されたﾌｧｲﾙ名が重複するかﾁｪｯｸ
		/// </summary>
		/// <param name="ServerName"></param>
		/// <returns></returns>
		#region private bool IsExistFileName(string ServerName)
		//[SRC-03-05-007]private bool IsExistFileName
		private bool IsExistFileName(string ServerName)
		{
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				string serverName = this.DataGrid1.Items[i].Cells[COL_ServerName].Text.ToUpper();
				if(serverName == ServerName.ToUpper()) return true;
			}
			return false;
		}
		#endregion










































	}
}
