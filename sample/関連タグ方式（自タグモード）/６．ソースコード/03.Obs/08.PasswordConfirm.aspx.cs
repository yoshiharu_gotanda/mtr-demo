using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Obs
{
	/// <summary>
	/// PasswordConfirm の概要の説明です。
	/// </summary>
	public class PasswordConfirm : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelPassword;
		protected System.Web.UI.WebControls.Button ButtonOK;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.TextBox TextBoxPassword;
		protected System.Web.UI.WebControls.Label LabelComment;
	
		#region Web フォーム デザイナで生成されたコード
		//[SRC-03-08-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-08-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(PasswordConfirm);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-08-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(this.IsPostBack == false)
				{
					using(CSql sql = new CSql())
					{
						sql.Open();
						//多国語対応
						#region
						Common.SetText(sql, this.mType, this.ButtonOK);
						Common.SetText(sql, this.mType, this.LabelTitle);
						Common.SetText(sql, this.mType, this.LabelComment);
						Common.SetText(sql, this.mType, this.LabelPassword);
						Common.SetText(sql, this.mType, this.TextBoxPassword);
						#endregion
					}
					//ﾊﾟｽﾜｰﾄﾞ設定済みであればﾍﾟｰｼﾞ遷移
					if(Convert.ToBoolean(Common.GetSession(SessionID.Obs_PasswordConfirm_Valid)) == true)
					{
						this.RedirectPage();
					}
					//ﾊﾟｽﾜｰﾄﾞが設定されていなければﾍﾟｰｼﾞ遷移
					if(this.GetPassword() == string.Empty)
					{
						this.RedirectPage();
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 確認ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonOK_Click(object sender, System.EventArgs e)
		//[SRC-03-08-004]private void ButtonOK_Click
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				//ﾊﾟｽﾜｰﾄﾞ一致ﾁｪｯｸ処理
				if(this.GetPassword() == this.CnvToAsciiString(this.TextBoxPassword.Text))
				{
					//ﾊﾟｽﾜｰﾄﾞﾁｪｯｸ済みﾌﾗｸﾞｾｯﾄ
					Common.SetSession(SessionID.Obs_PasswordConfirm_Valid, true);
					//ﾍﾟｰｼﾞ遷移
					this.RedirectPage();
				}
				else
				{
					//ｴﾗｰﾒｯｾｰｼﾞ表示
					Common.Alert(this, Common.GetText(this.mType, "AlertPasswordError"));
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion




		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// M_DefectのPasswordを取得する
		/// </summary>
		/// <returns></returns>
		#region private string GetPassword()
		//[SRC-03-08-005]private string GetPassword
		private string GetPassword()
		{
			string pass = string.Empty;
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				cmdTxt = "SELECT Password FROM M_Defect";
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true && dr["Password"] != DBNull.Value)
				{
					pass = dr["Password"].ToString();
				}
				dr.Close();
			}
			return pass;
		}
		#endregion

		/// <summary>
		/// 文字列をASCIIｺｰﾄﾞの文字列に変換する
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
		#region private string CnvToAsciiString(string src)
		//[SRC-03-08-006]private string CnvToAsciiString
		private string CnvToAsciiString(string src)
		{
			string ret = string.Empty;
			for(int i = 0; i < src.Length; i++)
			{
				int asc = (int)src[i];
				ret += asc.ToString("x");
			}
			return ret;
		}
		#endregion


		/// <summary>
		/// ﾓｰﾄﾞ別にそれぞれﾏｽﾀﾍﾟｰｼﾞへ遷移する
		/// </summary>
		#region private void RedirectPage()
		//[SRC-03-08-007]private void RedirectPage
		private void RedirectPage()
		{
			string mode = Request.QueryString["mode"];
			Common.ScriptStart();
			if(mode == "1")			Common.ScriptOutput("window.open(\"../Master/Item/ListFrame.aspx\", \"ObsMain\");");
			else if(mode == "2")	Common.ScriptOutput("window.open(\"../Master/Template/ListFrame.aspx\", \"ObsMain\");");
			else if(mode == "3")	Common.ScriptOutput("window.open(\"../Master/Numbering/ListFrame.aspx\", \"ObsMain\");");
			Common.ScriptEnd();
		}
		#endregion



























	}
}
