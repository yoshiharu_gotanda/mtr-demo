using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace EPSNet.Obs
{
	/// <summary>
	/// Frame の概要の説明です。
	/// </summary>
	public class Frame : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード
		//[SRC-03-04-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-04-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		
		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-04-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			//LoginID取得
			string id = Request.QueryString["UserName"];
			if(id == null && Common.LoginID != null) id = Common.LoginID;

			if(id == null)
			{
				//ｴﾗｰﾍﾟｰｼﾞへ遷移
				Response.Write("<script>");
				Response.Write("document.location='../Common/Message.aspx'");
				Response.Write("</script>");
			}
			else
			{
				Common.SetSession(SessionID.LoginID, id);
				Common.GetLanguage();
				//障害管理者ﾊﾟｽﾜｰﾄﾞｸﾘｱ
				Common.SetSession(SessionID.Obs_PasswordConfirm_Valid, null);

				Response.Write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\">");
				Response.Write("<html>");
				Response.Write("<head>");
				Response.Write("</head>");
				Response.Write("<frameset cols='200,*'>");
				//No.117 対応
				//ﾃﾞﾌｫﾙﾄでｶﾃｺﾞﾘﾂﾘｰ、障害ﾂﾘｰを表示
				Response.Write("<frame name='menu' src='Menu.aspx'>");
				//Response.Write("<frame name='ObsMain' src=''>");
				Response.Write("<frame name='ObsMain' src='./Tree/Frame.aspx'>");
				Response.Write("</frameset>");
				Response.Write("</html>");
			}
		}
		#endregion
	}
}
