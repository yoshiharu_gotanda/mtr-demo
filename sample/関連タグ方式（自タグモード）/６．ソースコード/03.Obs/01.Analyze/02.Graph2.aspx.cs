using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Drawing.Imaging;

namespace EPSNet.Obs.Analyze
{
	/// <summary>
	/// Graph2 の概要の説明です。
	/// </summary>
	public class Graph2 : System.Web.UI.Page
	{

		#region Web フォーム デザイナで生成されたコード
		//[SRC-03-01-02-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-01-02-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected System.Web.UI.WebControls.Button ButtonClose;

			
		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================
		
		private Type mType = typeof(Graph1);

		
		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-01-02-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				this.ButtonClose.Attributes["onclick"] = "self.window.close(); return false;";

				using(CSql sql = new CSql())
				{
					sql.Open();

					//多国語対応
					#region

					Common.SetText(sql, this.mType, this.ButtonClose);

					#endregion


				}
			}
		}
		#endregion



		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================



		///================================================================
		/// ﾌﾟﾛﾃｸﾄ関数
		///================================================================



















































	}
}
