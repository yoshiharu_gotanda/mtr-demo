using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;


namespace EPSNet.Obs.Analyze
{
	/// <summary>
	/// Graph1 の概要の説明です。
	/// </summary>
	public class Graph1 : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード
		//[SRC-03-01-01-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-01-01-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected System.Web.UI.WebControls.Button ButtonClose;
	
	
		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================
		
		private Type mType = typeof(Graph1);

		
		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-01-01-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				this.ButtonClose.Attributes["onclick"] = "window.close(); return false;";
				using(CSql sql = new CSql())
				{
					sql.Open();

					//多国語対応
					#region

					Common.SetText(sql, this.mType, this.ButtonClose);

					#endregion
				}
			}
		}
		#endregion



		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================





		
		///================================================================
		/// ﾌﾟﾛﾃｸﾄ関数
		///================================================================

		/// <summary>
		/// 集計結果ﾀｸﾞ作成
		/// </summary>
		/// <returns>作成したﾀｸﾞ文字列</returns>
		#region protected string TableTag()
		//[SRC-03-01-01-004]protected string TableTag
		protected string TableTag()
		{
			try
			{
				string ItemID = Request.QueryString["id"];

				string id = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Kind);		//集計種別
				string item = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Item);	//集計項目
				string unit = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Unit);	//集計単位
				string from = (string)Common.GetSession(SessionID.Obs_Analyze_Main_From);	//集計期間From
				string to = (string)Common.GetSession(SessionID.Obs_Analyze_Main_To);		//集計期間To

				StringBuilder sb = new StringBuilder();

				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					sql.Open();

					//集計対象の年月日等一覧を取得
					ArrayList alSpan;
					#region
					if(unit == "m")
					{
						//月単位
						alSpan = Common.GetRangeYM(from, to);
					}
					else if(unit == "d")
					{
						//日単位
						alSpan = Common.GetRangeYMD(from, to);
					}
					else
					{
						//時単位
						alSpan = new ArrayList();
						for(int i = 0; i < 24; i++)
						{
							alSpan.Add(from.Replace("/", string.Empty) + i.ToString("00"));
						}
					}
					#endregion

					//集計結果取得
					ArrayList alCode;
					ArrayList alText;
					ArrayList alCount;
					Common.Analyze(sql, id, ItemID, alSpan, out alCode, out alText, out alCount);
					//期間内の最大件数を取得する
					int max = 0;
					for(int i = 0; i < alCount.Count; i++)
					{
						int itmp = Convert.ToInt32(alCount[i]);
						if(itmp > max) max = itmp;
					}
					if(max == 0) max = 1;//0割を防ぐため

					//ﾀｲﾄﾙ
					sb.Append("<div align=center><h2>");
					sb.Append(HttpUtility.HtmlEncode(alText[0].ToString()));
					sb.Append("</h2></div>");
					sb.Append("<br>");

					sb.Append("<table border=1 width='100%'>");
					//ﾍｯﾀﾞ行
					sb.Append("<tr>");
					sb.Append("<td width=100><b>");
					sb.Append(Common.GetText(sql, this.mType, "Col1"));
					sb.Append("</b></td>");
					sb.Append("<td width=99%><b>");
					sb.Append(Common.GetText(sql, this.mType, "Col2"));
					sb.Append("</b></td>");
					sb.Append("</tr>\r\n");
					//ﾃﾞｰﾀ行
					for(int i = 0; i < alSpan.Count; i++)
					{
						//行開始
						sb.Append("<tr>");
						//期間ﾀｸﾞ出力
						string tmp = alSpan[i].ToString();
						tmp = Common.DateTimeConvert(sql, tmp);
						if(unit == "m")
						{
							sb.Append("<td width=100 nowrap>");
							sb.Append(tmp);
							sb.Append("</td>");
						}
						else if(unit == "d")
						{
							sb.Append("<td width=100 nowrap>");
							sb.Append(tmp);
							sb.Append("</td>");
						}
						else
						{
							sb.Append("<td width=100 nowrap>");
							sb.Append(tmp);
							sb.Append("</td>");
						}
						sb.Append("<td>");
						sb.Append("<table width=100% border=0><tr>");
						//ｸﾞﾗﾌﾀｸﾞ
						sb.Append("<td nowrap bgcolor=blue width=\"" + (100 * Convert.ToInt32(alCount[i]) / max).ToString() + "%\">");
						sb.Append("&nbsp;");
						sb.Append("</td>");
						//件数ﾀｸﾞ出力
						sb.Append("<td width=\"99%\">(");
						sb.Append(alCount[i].ToString());
						sb.Append(")</td>");
						sb.Append("</tr></table>");
						sb.Append("</td>");
						//行終了
						sb.Append("</tr>\r\n");
					}
				}
				sb.Append("</table>");
				return sb.ToString();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
				return string.Empty;
			}
		}
		#endregion
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
}
