using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;
using System.IO;

namespace EPSNet.Obs
{
	/// <summary>
	/// Detail の概要の説明です。
	/// </summary>
	public class Detail : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード 
		//[SRC-03-01-001]override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//[SRC-03-01-002]private void InitializeComponent
		private void InitializeComponent()
		{    
			this.ButtonRcg.Click += new System.EventHandler(this.ButtonRcg_Click);
			this.ButtonCSV.Click += new System.EventHandler(this.ButtonCSV_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected System.Web.UI.WebControls.Button ButtonCSV;
		protected System.Web.UI.WebControls.Button ButtonClose;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Button ButtonHistory;
		protected System.Web.UI.WebControls.Button ButtonEdit;
		protected System.Web.UI.WebControls.Button ButtonRcg;

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Detail);
		

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//[SRC-03-01-003]private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(this.IsPostBack == false)
				{
					string id = Request.QueryString["id"];
					string mode = Request.QueryString["mode"];
					if(mode == "Rcg")
					{
						//承認画面から開かれた場合、編集ﾎﾞﾀﾝ非表示
						this.ButtonEdit.Visible = false;
					}
					else
					{
						//障害画面から開かれた場合、承認ﾎﾞﾀﾝ非表示
						this.ButtonRcg.Visible = false;
					}
					//承認履歴ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定(ｻｰﾊﾞｰにいかないようにreturn falseをする)
					this.ButtonHistory.Attributes["onclick"] = "OpenHistory('" + id + "');return false;";
					//閉じるﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
					this.ButtonClose.Attributes["onclick"] = "window.close();";
					//編集ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定(ｻｰﾊﾞｰにいかないようにreturn falseをする)
					this.ButtonEdit.Attributes["onclick"] = "document.location='./Edit.aspx?id=" + id + "';return false;";

					//更新権限がなければ更新不可
					if(Common.IsObsEditable(id) == false)
					{
						this.ButtonEdit.Visible = false;
					}

					using(CSql sql = new CSql())
					{
						sql.Open();
						//多国語対応
						#region
						Common.SetText(sql, this.mType, this.ButtonEdit);
						Common.SetText(sql, this.mType, this.ButtonCSV);
						Common.SetText(sql, this.mType, this.ButtonHistory);
						Common.SetText(sql, this.mType, this.ButtonClose);
						Common.SetText(sql, this.mType, this.ButtonRcg);
						Common.SetText(sql, this.mType, this.LabelTitle);

						//承認ﾎﾞﾀﾝ確認ﾒｯｾｰｼﾞ
						this.ButtonRcg.Attributes["onclick"] = "return confirm('" + Common.GetText(sql, this.mType, "ConfirmRcg") + "')";

						#endregion
						
						//障害詳細一覧作成
						this.CreateList(sql, id);
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// CSV出力ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonCSV_Click(object sender, System.EventArgs e)
		//[SRC-03-01-004]private void ButtonCSV_Click
		private void ButtonCSV_Click(object sender, System.EventArgs e)
		{
			Response.AddHeader("Content-Disposition","attachment;filename=" + HttpUtility.UrlEncode("Detail.csv"));
			Response.ContentType = "application/octet-stream";
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				Response.Write("\"" + Common.CSVCnv(this.DataGrid1.Items[i].Cells[0].Text) + "\",\"" + Common.CSVCnv(this.DataGrid1.Items[i].Cells[2].Text) + "\"\r\n");
			}
			Response.End();
		}
		#endregion
		
		/// <summary>
		/// 承認ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonRcg_Click(object sender, System.EventArgs e)
		//[SRC-03-01-005]private void ButtonRcg_Click
		private void ButtonRcg_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Request.QueryString["id"];
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					sql.BeginTrans();
					try
					{
						cmdTxt = "UPDATE T_ObsMgr_RcgUsr SET RcgDateTime=GETDATE(),UpdDt=@UpdDt WHERE ObsMgrID=@ObsMgrID AND RcgDateTime IS NULL ";
						sql.AddParam("@ObsMgrID", id);
						sql.AddParam("@UpdDt", DateTime.Now.ToString("yyyyMMddHHmmss"));
						sql.CommandTrans(cmdTxt);
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ｽｸﾘﾌﾟﾄ出力開始
				Common.ScriptStart();
				//承認完了画面へ
				Common.ScriptOutput("self.location='RcgFinish.aspx?id=" + id + "';");
				//ｽｸﾘﾌﾟﾄ出力終了
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 一覧作成
		/// </summary>
		#region private void CreateList(CSql sql, string ObsMgrID)
		//[SRC-03-01-006]private void CreateList
		private void CreateList(CSql sql, string ObsMgrID)
		{
			DataTable table = new DataTable();
			table.Columns.Add("Title");
			table.Columns.Add("Detail");
			table.Columns.Add("CSV");

			string cmdTxt;
			SqlDataReader dr;
			string cont = string.Empty;//内容
			string contCSV = string.Empty;//内容
			using(CSql sql2 = new CSql())
			{
				SqlDataReader dr2;
				sql2.Open();

				//障害情報取得
				cmdTxt = "SELECT O.MgrNo, O.Sbj, O.Cnt, MU.FullName, M.Lang0" + Common.LangID.ToString() + " AS Lang, S.Text" + Common.LangID.ToString() + " AS Status FROM T_ObsMgr2 AS O";
				cmdTxt += " LEFT JOIN M_Prj_Typ AS M ON M.TypCD=O.PrjID";
				cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=O.Status";
				cmdTxt += " LEFT JOIN M_users AS MU ON O.EntUsrID=MU.UserID";
				cmdTxt += " WHERE O.ObsMgrID=@ObsMgrID";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true)
				{
					//管理番号
					this.AddRow(table, Common.GetText(sql2, this.mType, "Title1"), HttpUtility.HtmlEncode(dr["MgrNo"].ToString()), dr["MgrNo"]);
					//件名
					this.AddRow(table, Common.GetText(sql2, this.mType, "Title2"), HttpUtility.HtmlEncode(dr["Sbj"].ToString()), dr["Sbj"]);
					//ｽﾃｰﾀｽ
					this.AddRow(table, Common.GetText(sql2, this.mType, "Title4"), HttpUtility.HtmlEncode(dr["Status"].ToString()), dr["Status"]);
					//起票者
					this.AddRow(table, Common.GetText(sql2, this.mType, "Title8"), HttpUtility.HtmlEncode(dr["FullName"].ToString()), dr["FullName"]);
					//ﾌﾟﾛｼﾞｪｸﾄ名
					this.AddRow(table, HttpUtility.HtmlEncode(Common.GetNumberingItemName(sql2)), HttpUtility.HtmlEncode(dr["Lang"].ToString()), dr["Lang"]);
					//内容
					cont = HttpUtility.HtmlEncode(dr["Cnt"].ToString()).Replace("\r\n", "<br>");
					contCSV = HttpUtility.HtmlEncode(dr["Cnt"].ToString());
				}
				dr.Close();
				//可変項目情報取得
				#region

				//ﾃﾝﾌﾟﾚｰﾄID取得
				string TemplateID = string.Empty;
				#region
				cmdTxt = "SELECT P.TemplateID FROM T_ObsMgr2 AS O";
				cmdTxt += " LEFT JOIN M_Prj_Typ AS P ON P.TypCD=O.PrjID";
				cmdTxt += " WHERE O.ObsMgrID=@ObsMgrID";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true)
				{
					TemplateID = dr["TemplateID"].ToString();
				}
				dr.Close();
				#endregion

				//ﾌﾟﾛｼﾞｪｸﾄのﾃﾝﾌﾟﾚｰﾄから使用されている項目一覧を取得する
				#region
				switch(Common.LangID)
				{
					case 1: cmdTxt = "SELECT I.ItemID, I.Japanese AS Text, I.DataType FROM M_ObsTemplateItem AS T"; break;
					case 2: cmdTxt = "SELECT I.ItemID, I.English AS Text, I.DataType FROM M_ObsTemplateItem AS T"; break;
				}
				cmdTxt += " LEFT JOIN M_ObsItem AS I ON I.ItemID=T.ItemID WHERE T.TemplateID=@TemplateID ORDER BY T.SortID";
				sql.AddParam("@TemplateID", TemplateID);
				dr = sql.Read(cmdTxt);
				#endregion

				while(dr.Read() == true)
				{
					string title = HttpUtility.HtmlEncode(dr["Text"].ToString());
					int type = Convert.ToInt32(dr["DataType"]);
					string itemID = dr["ItemID"].ToString();
					switch(type)
					{
						case 1:	//ﾃｷｽﾄ型
						{
							#region
							cmdTxt = "SELECT Data1 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							string str = string.Empty;
							if(dr2.Read() == true && dr2["Data1"] != DBNull.Value) str = dr2["Data1"].ToString(); dr2.Close();
							this.AddRow(table, title, HttpUtility.HtmlEncode(str.ToString()), str);
							break;
							#endregion
						}
						case 2:	//ﾘｽﾄ型
						{
							#region
							string selectedValue = string.Empty;
							cmdTxt = "SELECT Data2 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true)
							{
								selectedValue = dr2["Data2"].ToString();
							}
							dr2.Close();

							cmdTxt = "SELECT Text" + Common.LangID.ToString() +" AS Text, ListID FROM M_ObsItemList";
							cmdTxt += " WHERE ItemID=@ItemID AND ListID=@ListID AND DisabledFlag=0";
							sql2.AddParam("@ItemID", itemID);
							sql2.AddParam("@ListID", selectedValue);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true)
							{
								this.AddRow(table, title, HttpUtility.HtmlEncode(dr2["Text"].ToString()), dr2["Text"]);
							}
							else
							{
								this.AddRow(table, title, string.Empty, string.Empty);
							}
							dr2.Close();
							break;
							#endregion
						}
						case 3:	//数値型
						{
							#region
							cmdTxt = "SELECT Data3 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							string str = string.Empty;
							if(dr2.Read() == true && dr2["Data3"] != DBNull.Value) str = dr2["Data3"].ToString(); dr2.Close();
							this.AddRow(table, title, str, str);
							break;
							#endregion
						}								
						case 4:	//整数型
						{
							#region
							cmdTxt = "SELECT Data4 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							string str = string.Empty;
							if(dr2.Read() == true && dr2["Data4"] != DBNull.Value) str = dr2["Data4"].ToString(); dr2.Close();
							this.AddRow(table, title, str, str);
							break;
							#endregion
						}
						case 5:	//日付型
						{
							#region
							cmdTxt = "SELECT Data5 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							string str = string.Empty;
							if(dr2.Read() == true && dr2["Data5"] != DBNull.Value) str = Convert.ToDateTime(dr2["Data5"]).ToString("yyyy/MM/dd"); dr2.Close();
							this.AddRow(table, title, str, str);
							break;
							#endregion
						}								
						case 6:	//ﾕｰｻﾞ型
						{
							#region
							string selectedValue = string.Empty;
							cmdTxt = "SELECT Data6 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true) selectedValue = dr2["Data6"].ToString(); dr2.Close();

							cmdTxt = "SELECT UserID, FullName FROM M_users WHERE UserID=@UserID";
							sql2.AddParam("@UserID", selectedValue);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true)
							{
								this.AddRow(table, title, HttpUtility.HtmlEncode(dr2["FullName"].ToString()), dr2["FullName"]);
							}
							else
							{
								this.AddRow(table, title, "", "");
							}
							dr2.Close();
							break;
							#endregion
						}								
						case 7:	//部門型
						{
							#region
							string selectedValue = string.Empty;
							cmdTxt = "SELECT Data7 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
							sql2.AddParam("@ObsMgrID", ObsMgrID); sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true) selectedValue = dr2["Data7"].ToString(); dr2.Close();

							cmdTxt = "SELECT Sect, SectName FROM M_SECT WHERE Sect=@Sect";
							sql2.AddParam("@Sect", selectedValue);
							dr2 = sql2.Read(cmdTxt);
							if(dr2.Read() == true)
							{
								this.AddRow(table, title, HttpUtility.HtmlEncode(dr2["SectName"].ToString()), dr2["SectName"]);
							}
							else
							{
								this.AddRow(table, title, "", "");
							}
							dr2.Close();
							break;
							#endregion
						}
					}
				}
				dr.Close();
			
				#endregion
				//承認状態
				string tmp = Common.GetRcgStatusString(sql, ObsMgrID);
				this.InsertRow(table, Common.GetText(sql2, this.mType, "Title3"), tmp, tmp, 2);
				//内容
				this.AddRow(table, Common.GetText(sql2, this.mType, "Title5"), cont, contCSV);
				//添付ﾌｧｲﾙ
				#region
				//添付ﾌｧｲﾙの絶対ﾊﾟｽを取得
				string filePath = Common.GetCommonLib(ObsMgrID);

				cmdTxt = "SELECT UserPath, ServerPath, ServerName, Comment FROM T_ObsMgr_Lib WHERE ObsMgrID=@ObsMgrID ORDER BY UpdDt";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				dr = sql.Read(cmdTxt);
				StringBuilder sb = new StringBuilder();
				StringBuilder sbCsv = new StringBuilder();
				while(dr.Read() == true)
				{
					if(sbCsv.ToString() != string.Empty) sbCsv.Append(",");
					sbCsv.Append(dr["UserPath"]);

					//ﾌｧｲﾙ名出力
					string fileName = Path.Combine(filePath, Common.CnvFromDB(dr["ServerName"].ToString()));
					if(File.Exists(fileName) == true)
					{
						//ﾌｧｲﾙが存在したら、ﾘﾝｸを設定
						sb.Append("<a href=\"../Common/GetFile.aspx?ObsMgrID=" + ObsMgrID + "&ServerName=" + Common.CnvFromDB(dr["ServerName"].ToString()) + "\" target=_blank>");
						sb.Append(HttpUtility.HtmlEncode(dr["UserPath"].ToString()));
						sb.Append("</a>");
					}
					else
					{
						//ﾌｧｲﾙが存在しない場合、ﾘﾝｸは張らず赤表示
						sb.Append("<FONT Color=red>");
						sb.Append(HttpUtility.HtmlEncode(dr["UserPath"].ToString()));
						sb.Append("</FONT>");
					}
					//ｺﾒﾝﾄがあれば表示
					if(dr["Comment"] != DBNull.Value && dr["Comment"].ToString() != string.Empty)
					{
						sb.Append("<br>");
						//sb.Append(Common.CnvToDB("&nbsp;"));
						sb.Append("&nbsp;");
						sb.Append(HttpUtility.HtmlEncode(dr["Comment"].ToString()));
					}
					sb.Append("<br>");
				}
				dr.Close();
				this.AddRow(table, Common.GetText(sql2, this.mType, "Title6"), sb.ToString(), sbCsv.ToString());
				#endregion
				//URLﾘﾝｸ
				#region
				cmdTxt = "SELECT LinkName, LinkUrl FROM T_ObsMgrLink WHERE ObsMgrID=@ObsMgrID ORDER BY LinkID";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				dr = sql.Read(cmdTxt);
				sb = new StringBuilder();
				sbCsv = new StringBuilder();
				while(dr.Read() == true)
				{
					if(sbCsv.ToString() != string.Empty) sbCsv.Append(",");
					sbCsv.Append("[" + dr["LinkName"].ToString() + "]" + dr["LinkUrl"].ToString());

					sb.Append("<a href=\"" + HttpUtility.HtmlEncode(dr["LinkUrl"].ToString()) + "\" target=_blank>");
					sb.Append(HttpUtility.HtmlEncode(dr["LinkName"].ToString()));
					sb.Append("</a>");
					sb.Append("<br>");
				}
				dr.Close();
				this.AddRow(table, Common.GetText(sql2, this.mType, "Title7"), sb.ToString(), sbCsv);
				#endregion

			}
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();
		}
		#endregion

		/// <summary>
		/// 指定DataTableに指定内容の行を追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="title"></param>
		/// <param name="detail"></param>
		#region private void AddRow(DataTable table, string title, object detail, object csv)
		//[SRC-03-01-007]private void AddRow
		private void AddRow(DataTable table, string title, object detail, object csv)
		{
			this.InsertRow(table, title, detail, csv, table.Rows.Count);
		}
		#endregion

		/// <summary>
		/// 指定DataTableに指定内容の行を追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="title"></param>
		/// <param name="detail"></param>
		/// <param name="index"></param>
		#region private void InsertRow(DataTable table, string title, object detail, object csv, int index)
		//[SRC-03-01-008]private void InsertRow
		private void InsertRow(DataTable table, string title, object detail, object csv, int index)
		{
			DataRow row = table.NewRow();
			row["Title"] = title;
			row["Detail"] = detail.ToString();
			row["CSV"] = Common.CnvFromDB(csv.ToString());
			table.Rows.InsertAt(row, index);
		}
		#endregion









	}
}
