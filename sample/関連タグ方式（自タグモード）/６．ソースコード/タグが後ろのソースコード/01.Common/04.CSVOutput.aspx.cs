using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace EPSNet
{
	/// <summary>
	/// CSVOutput の概要の説明です。
	/// </summary>
    //public class CSVOutput : System.Web.UI.Page【SRC-01-04-001】
    public class CSVOutput : System.Web.UI.Page
	{
        //private void Page_Load【SRC-01-04-002】
        private void Page_Load(object sender, System.EventArgs e)
		{
		//	Response.Clear();
		//	Response.ClearHeaders();
		//	Response.ClearContent();

			Response.ContentType = "application/octet-stream";
			
			Response.AddHeader("Content-Disposition","attachment;filename=" + HttpUtility.UrlEncode(Common.GetSession(SessionID.Common_CSVFileName).ToString()));
			
			Response.Write(HttpUtility.HtmlDecode(Common.GetSession(SessionID.Common_CSVData).ToString()));
			
			Response.End();

			Response.Clear();
			Response.ClearHeaders();
			Response.ClearContent();
		}

		#region Web フォーム デザイナで生成されたコード 
        //override protected void OnInit【SRC-01-04-003】
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
        //private void InitializeComponent【SRC-01-04-004】
        private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
