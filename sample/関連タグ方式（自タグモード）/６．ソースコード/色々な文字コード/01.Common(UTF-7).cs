using System+ADs-

using System.Web+ADs-
using System.Web.UI.WebControls+ADs-
using System.Data+ADs-
using System.Data.SqlClient+ADs-
using System.Collections+ADs-
using System.IO+ADs-
using System.Text+ADs-
using System.Net+ADs-

namespace EPSNet
+AHs-
	/// +ADw-summary+AD4-
	/// Common +MG5pgomBMG6KrGYOMGcwWTAC-
	/// +ADw-/summary+AD4-
	public class Common
	+AHs-
		/// +ADw-summary+AD4-
		/// +/3r/nf99/4T/l/94/4A-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-001+AD4-static Common
		+ACM-region static Common()
		static Common()
		+AHs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/5v/eP+e/3L/nf+V/3D/e/+eMG5Pf3UoigCKnjCSU9Zflw-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-002+AD4-public static void GetLanguage
        +ACM-region public static void GetLanguage()
		public static void GetLanguage()
		+AHs-
			using(CSql sql +AD0- new CSql())
			+AHs-
				string cmdTxt+ADs-
				SqlDataReader dr+ADs-
				cmdTxt +AD0- +ACI-SELECT LanguageID FROM m+AF8-users WHERE UserID+AD0AQA-UserID+ACIAOw-
				sql.AddParam(+ACIAQA-UserID+ACI-, Common.LoginID)+ADs-
				sql.Open()+ADs-
				dr +AD0- sql.Read(cmdTxt)+ADs-
				if(dr.Read() +AD0APQ- true)	Common.SetSession(SessionID.LanguageID, dr+ADwAIg-LanguageID+ACIAXQ-)+ADs-
				else					Common.SetSession(SessionID.LanguageID, 1)+ADs-	//+MOYw/DC2YMVYMTBMiYswZDBLMIkwajBEWDRUCDBvZeJbmlAk-
				dr.Close()+ADs-
			+AH0-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/37/b/98/27/nf9x/3j/fv99dSg-
		/// +ADw-/summary+AD4-
		+ACM-region

		/// +ADw-summary+AD4-
		/// +/37/b/98/27/nWDFWDH/eP+Y/3E-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-003+AD4-public static void CreateSession
        public static void CreateSession()
		+AHs-
			if(System.Web.HttpContext.Current.Session+AFsAIg-CSession+ACIAXQ- +AD0APQ- null)
				System.Web.HttpContext.Current.Session+AFsAIg-CSession+ACIAXQ- +AD0- new CSession()+ADs-
		+AH0-

		/// +ADw-summary+AD4-
		/// +/37/b/98/27/nWDFWDFm+I+8MH8-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-id+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-setValue+ACIAPgA8-/param+AD4-
        //+ADw-SRC-01-01-004+AD4-public static void SetSession
        public static void SetSession(SessionID id, object setValue)
		+AHs-
			CSession session +AD0- (CSession)System.Web.HttpContext.Current.Session+AFsAIg-CSession+ACIAXQA7-
			session.Set(id, setValue)+ADs-
		+AH0-

		/// +ADw-summary+AD4-
		/// +/37/b/98/27/nWDFWDGKrY+8MH8-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-id+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-005+AD4-public static object GetSession
        public static object GetSession(SessionID id)
		+AHs-
			CSession session +AD0- (CSession)System.Web.HttpContext.Current.Session+AFsAIg-CSession+ACIAXQA7-
			return session.Get(id)+ADs-
		+AH0-

		+ACM-endregion

        //+ADw-SRC-01-01-006+AD4-public static string ConvertToBit
        +ACM-region public static string ConvertToBit(bool src)
		public static string ConvertToBit(bool src)
		+AHs-
			if(src +AD0APQ- true)	return +ACI-1+ACIAOw-
			else			return +ACI-0+ACIAOw-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +i2ZUSv+A/57/cv9x/5v/eP+eiGh5Og-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-page+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-Message+ACIAPgA8-/param+AD4-
        //+ADw-SRC-01-01-007+AD4-public static void Alert
        +ACM-region public static void Alert(System.Web.UI.Page page, string Message)
		public static void Alert(System.Web.UI.Page page, string Message)
		+AHs-
			//Message +AD0- Message.Replace(+ACI-'+ACI-, +ACIAXA-'+ACI-)+ADs-
			//Message +AD0- Message.Replace(+ACIAPA-/+ACI-, +ACIAPA-' +- '/+ACI-)+ADs-
			//Message +AD0- Message.Replace(+ACIAXA-r+ACI-, +ACIAXABc-r+ACI-)+ADs-
			//Message +AD0- Message.Replace(+ACIAXA-n+ACI-, +ACIAXABc-n+ACI-)+ADs-

			Message +AD0- Message.Replace(+ACI-'+ACI-, +ACIAXABc-'+ACI-).Replace(+ACIAPA-/+ACI-, +ACIAPA-' +- '/+ACI-).Replace(+ACIAXABc-r+ACI-, +ACIAXABcAFwAXA-r+ACI-)+ADs-
			page.RegisterStartupScript(+ACI-alert+ACI-, +ACIAPA-script+AD4-alert('+ACI- +- Message +- +ACI-')+ADsAPA-/script+AD4AIg-)+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/33/eP+Y/4z/n/+EUfpSmw-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-page+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-script+ACIAPgA8-/param+AD4-
        //+ADw-SRC-01-01-008+AD4-public static void OutputScript
        +ACM-region public static void OutputScript(System.Web.UI.Page page, string script)
		public static void OutputScript(System.Web.UI.Page page, string script)
		+AHs-
			script +AD0- script.Replace(+ACI-'+ACI-, string.Empty)+ADs-
			script +AD0- script.Replace(+ACIAXA-r+ACI-, +ACIAXABc-r+ACI-)+ADs-
			script +AD0- script.Replace(+ACIAXA-n+ACI-, +ACIAXABc-n+ACI-)+ADs-

			page.RegisterStartupScript(+ACI-script+ACI-, +ACIAPA-script+AD4AIg-  +- script +- +ACIAPA-/script+AD4AIg-)+ADs-
		+AH0-
		+ACM-endregion


        /// +ADw-summary+AD4-
		/// +ZYdbV1IXTi0wbjAM-'+MA0wkjAM-''+MA0wa1kJY9swWTCL-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-src+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-009+AD4-public static string SQCnv(string src)
        +ACM-region public static string SQCnv(string src)
		public static string SQCnv(string src)
		+AHs-
			return src.Trim().Replace(+ACI-'+ACI-, +ACI-''+ACI-)+ADs-
		+AH0-
		+ACM-endregion
        //+ADw-SRC-01-01-010+AD4-public static string SQCnv(object src)
        +ACM-region public static string SQCnv(object src)
		public static string SQCnv(object src)
		+AHs-
			return src.ToString().Trim().Replace(+ACI-'+ACI-, +ACI-''+ACI-)+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +ZYdbV1IXMJI-DB+aDx9DXUoMGv/ev+d/4r/nv9w/4QwWTCL-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-src+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-011+AD4-public static string CnvToDB
        +ACM-region public static string CnvToDB(string src)
		public static string CnvToDB(string src)
		+AHs-
			src +AD0- HttpUtility.HtmlDecode(src)+ADs-
			src +AD0- src.Trim()+ADs-
			//src +AD0- HttpUtility.HtmlEncode(src)+ADs-
			return src+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +ZYdbV1IXMJI-DB+aDx9DXUoMEswif96/53/iv+e/3D/hDBZMIs-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-src+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-012+AD4-public static string CnvFromDB
        +ACM-region public static string CnvFromDB(string src)
		public static string CnvFromDB(string src)
		+AHs-
			src +AD0- src.Trim()+ADs-
			src +AD0- HttpUtility.HtmlDecode(src)+ADs-
			return src+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// bool+V4swkg-0+MEs-1+MG5lh1tXMGtZCWPbMFkwiw-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-src+ACIAPg-true/false+ADw-/param+AD4-
		/// +ADw-returns+AD4-true:1 false:0+ADw-/returns+AD4-
        //+ADw-SRC-01-01-013+AD4-public static string ConvertToString
        +ACM-region public static string ConvertToString(bool src)
		public static string ConvertToString(bool src)
		+AHs-
			if(src +AD0APQ- true)	return +ACI-1+ACIAOw-
			else			return +ACI-0+ACIAOw-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/33/g/9w/4D/fU4Aiac-DropDownList+MJJPXGIQMFkwiw-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-sql+ACIAPgA8-/param+AD4-
        //+ADw-SRC-01-01-014+AD4-public static void CreateStatusDropDownList
        +ACM-region public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		+AHs-
			string cmdTxt+ADs-
			SqlDataReader dr+ADs-
			cmdTxt +AD0- +ACI-SELECT Text+ACI- +- Common.LangID.ToString() +- +ACI- AS Text, StatusID FROM M+AF8-ObsStatus ORDER BY SortID+ACIAOw-
			dr +AD0- sql.Read(cmdTxt)+ADs-
			ddl.Items.Clear()+ADs-
			while(dr.Read() +AD0APQ- true)
			+AHs-
				ddl.Items.Add(new ListItem(dr+AFsAIg-Text+ACIAXQ-.ToString(), dr+AFsAIg-StatusID+ACIAXQ-.ToString()))+ADs-
			+AH0-
			dr.Close()+ADs-
			if(ddl.Items.Count +AD4- 0) ddl.SelectedIndex +AD0- 0+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// DropDownList+MG5QJDCSii1bmjBZMIs-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-ddl+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-Value+ACIAPgA8-/param+AD4-
        //+ADw-SRC-01-01-015+AD4-public static void SetValue
        +ACM-region public static void SetValue(DropDownList ddl, string Value)
		public static void SetValue(DropDownList ddl, string Value)
		+AHs-
			for(int i +AD0- 0+ADs- i +ADw- ddl.Items.Count+ADs- i+-+-)
			+AHs-
				if(ddl.Items+AFs-i+AF0-.Value +AD0APQ- Value)
				+AHs-
					ddl.SelectedIndex +AD0- i+ADs-
					return+ADs-
				+AH0-
			+AH0-
		+AH0-
		+ACM-endregion
        //+ADw-SRC-01-01-016+AD4-public static void SetText
        +ACM-region public static void SetText(DropDownList ddl, string Text)
		public static void SetText(DropDownList ddl, string Text)
		+AHs-
			for(int i +AD0- 0+ADs- i +ADw- ddl.Items.Count+ADs- i+-+-)
			+AHs-
				if(ddl.Items+AFs-i+AF0-.Text +AD0APQ- Text)
				+AHs-
					ddl.SelectedIndex +AD0- i+ADs-
					return+ADs-
				+AH0-
			+AH0-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +e6F0BnVqU/cwknUfYhAwWTCL-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-productName+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-format+ACIAPgA8-/param+AD4-
		/// +ADw-param name+AD0AIg-seqNum+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-017+AD4-public static string CreateMngNo
        +ACM-region public static string CreateMngNo(string productName, string format, int seqNum)
		public static string CreateMngNo(string productName, string format, int seqNum)
		+AHs-
			//+e6F0BnVqU/d1H2IQ-
			//+ACU-n:1+MEswiVnLMH4wi4HqUtVjoXVqMFUwjDBfdWpT9zBrWQlj2zBVMIwwiw-
			string mgrNo +AD0- format.Replace(+ACIAJQ-n+ACI-, seqNum.ToString())+ADs-
			//+ACU-0+AFtlcFtXAF3/Tg-:+YwdbmjBVMIwwXwBbZXBbVwBdMGc-0+inAwgQ-
			for(int i +AD0- 0+ADs- i +ADwAPQ- 9+ADs- i+-+-)
			+AHs-
				mgrNo +AD0- mgrNo.Replace(+ACIAJQ-0+ACI- +- i.ToString() +- +ACI-n+ACI-, seqNum.ToString().PadLeft(i, '0'))+ADs-
			+AH0-
			//+ACU-p:+Uh1nH1AkMGcwb4j9VMFUDTBrMGowizABUAtSJTBuVA1STTBrWQlj2zBVMIwwiw-
			mgrNo +AD0- mgrNo.Replace(+ACIAJQ-p+ACI-, productName)+ADs-
			//+ACU-d:+ZeVO2DBMMAw-yymmdd+MA0wbmb4Xw8wZ1kJY9swVTCMMIs-
			mgrNo +AD0- mgrNo.Replace(+ACIAJQ-d+ACI-, DateTime.Now.ToString(+ACI-yyyyMMdd+ACI-))+ADs-
			//+ACU-t:+ZkKVkzBMMAw-hhmmss+MA0wbmb4Xw8wZ1kJY9swVTCMMIs-
			mgrNo +AD0- mgrNo.Replace(+ACIAJQ-t+ACI-, DateTime.Now.ToString(+ACI-HHmmss+ACI-))+ADs-

			return mgrNo+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/5v/eP+e/3L/nQ-ID+ZYdbV1IXMJJT1l+X-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-018+AD4-public static string LoginID
        +ACM-region public static string LoginID
		public static string LoginID
		+AHs-
			get
			+AHs-
				if(Common.GetSession(SessionID.LoginID) +AD0APQ- null)	return null+ADs-
				else												return Common.GetSession(SessionID.LoginID).ToString()+ADs-
			+AH0-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/5v/eP+e/3L/nf+V/3D/e/+eMG4-LanguageID
		/// +ADw-/summary+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-019+AD4-public static int LangID
        +ACM-region public static int LangID
		public static int LangID
		+AHs-
			get
			+AHs-
				return Convert.ToInt32(GetSession(SessionID.LanguageID))+ADs-
			+AH0-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +/4P/nv9w/4BXi1QNTgCJpw-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-020+AD4-public static string+AFs-,+AF0- DataTypes
        +ACM-region public static string+AFs-,+AF0- DataTypes
		public static string+AFs-,+AF0- DataTypes
		+AHs-
			get
			+AHs-
				//+/4P/nv9w/4BXi1QNU9Zflw-
				string+AFs-,+AF0- types +AD0- new string+AFs-7,2+AF0AOw-
				using(CSql sql +AD0- new CSql())
				+AHs-
					sql.Open()+ADs-
					for(int i +AD0- 1+ADs- i +ADwAPQ- types.GetLength(0)+ADs- i+-+-)
					+AHs-
						types+AFs-i - 1, 0+AF0- +AD0- Common.GetText(sql, typeof(Common), +ACI-DataType+ACI- +- i.ToString())+ADs-
						types+AFs-i - 1, 1+AF0- +AD0- i.ToString()+ADs-
					+AH0-
				+AH0-
				return types+ADs-
			+AH0-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +VvpbmpgFdu5UDVPWX5c-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-021+AD4-public static string+AFsAXQ- GetFixItemColName
        +ACM-region public static string+AFsAXQ- GetFixItemColName(CSql sql)
		public static string+AFsAXQ- GetFixItemColName(CSql sql)
		+AHs-
			string+AFsAXQ- names +AD0- new string+AFs-8+AF0AOw-
			for(int i +AD0- 1+ADs- i +ADwAPQ- names.Length+ADs- i+-+-)
			+AHs-
				names+AFs-i - 1+AF0- +AD0- Common.GetText(sql, typeof(Common), +ACI-ItemColName+ACI- +- i.ToString())+ADs-
			+AH0-
			return names+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +VvpbmpgFdu6KrGYOU9Zflw-
		/// +ADw-/summary+AD4-
        //+ADw-SRC-01-01-022+AD4-public static string+AFsAXQ- GetFixItemColExplain
        +ACM-region public static string+AFsAXQ- GetFixItemColExplain(CSql sql)
		public static string+AFsAXQ- GetFixItemColExplain(CSql sql)
		+AHs-
			string+AFsAXQ- explain +AD0- new string+AFs-8+AF0AOw-
			for(int i +AD0- 1+ADs- i +ADwAPQ- explain.Length+ADs- i+-+-)
			+AHs-
				explain+AFs-i - 1+AF0- +AD0- Common.GetText(sql, typeof(Common), +ACI-ItemColExplain+ACI- +- i.ToString())+ADs-
			+AH0-
			return explain+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +Y6F1alv+jGGYBXbuVA1T1l+X-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-sql+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-023+AD4-public static string GetNumberingItemName
        +ACM-region public static string GetNumberingItemName()
		public static string GetNumberingItemName()
		+AHs-
			using(CSql sql +AD0- new CSql())
			+AHs-
				sql.Open()+ADs-
				return GetNumberingItemName(sql)+ADs-
			+AH0-
		+AH0-
		public static string GetNumberingItemName(CSql sql)
		+AHs-
			string cmdTxt +AD0- string.Empty+ADs-
			SqlDataReader dr+ADs-
			string name +AD0- string.Empty+ADs-
			//+Y6F1alv+jGGYBXbu-
			switch(Common.LangID)
			+AHs-
				case 1: cmdTxt +AD0- +ACI-SELECT Japanese FROM M+AF8-ObsItem WHERE ItemID+AD0-0+ACIAOw- break+ADs-
				case 2: cmdTxt +AD0- +ACI-SELECT English FROM M+AF8-ObsItem WHERE ItemID+AD0-0+ACIAOw- break+ADs-
			+AH0-
			dr +AD0- sql.Read(cmdTxt)+ADs-
			if(dr.Read() +AD0APQ- true)	name +AD0- dr+AFs-0+AF0-.ToString()+ADs-
			dr.Close()+ADs-
			return name+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +ZYdbV1IXTi0wSzCJACIAJQBbACoAKgAqAF0AIjBuX2JfDzBuZYdbV1IXMJJT1l+XMFkwiw-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-str+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4APA-/returns+AD4-
        //+ADw-SRC-01-01-024+AD4-public static ArrayList GetTag
        +ACM-region public static ArrayList GetTag(string str)
		public static ArrayList GetTag(string str)
		+AHs-
			ArrayList al +AD0- new ArrayList()+ADs-
			string+AFsAXQ- strs +AD0- str.Split('+ACU-')+ADs-
			for(int i +AD0- 0+ADs- i +ADw- strs.Length+ADs- i+-+-)
			+AHs-
				if(strs+AFs-i+AF0-.Length +AD4- 3 +ACYAJg- strs+AFs-i+AF0AWw-0+AF0- +AD0APQ- '+AFs-')
				+AHs-
					int index +AD0- strs+AFs-i+AF0-.IndexOf('+AF0-')+ADs-
					if(index +AD4- 0)
					+AHs-
						al.Add(+ACIAJQAi- +- strs+AFs-i+AF0-.Substring(0, index +- 1))+ADs-
					+AH0-
				+AH0-
			+AH0-
			return al+ADs-
		+AH0-
		+ACM-endregion

		/// +ADw-summary+AD4-
		/// +ZeVO2P+M/2v/cP+P/2//hFkJY9s-(yyyy/MM/dd)
		/// +Zwgw+2XlMGs-0+MJKI3FuM-
		/// +ADw-/summary+AD4-
		/// +ADw-param name+AD0AIg-strDate+ACIAPgA8-/param+AD4-
		/// +ADw-returns+AD4-string strFormatDate+ADw-/returns+AD4-
        //+ADw-SRC-01-01-025+AD4-public static bool FormatDate
        +ACM-region public static bool FormatDate(string strDate)
		public static string FormatDate(string strDate)
		+AHs-
			string strFormatDate +AD0- strDate+ADs-

			try
			+AHs-
				DateTime dt +AD0- DateTime.ParseExact(strDate, +ACI-yyyy/M/d+ACI-, null)+ADs-
				strFormatDate +AD0- dt.Year.ToString(+ACI-0000+ACI-) +- +ACI-/+ACI- +- dt.Month.ToString(+ACI-00+ACI-) +- +ACI-/+ACI- +- dt.Day.ToString(+ACI-00+ACI-)+ADs-	
				return strFormatDate+ADs-
			+AH0-
			catch
			+AHs-
				return strFormatDate+ADs-
			+AH0-
		+AH0-
		+ACM-endregion








	+AH0-
+AH0-
