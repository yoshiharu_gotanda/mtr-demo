using System;

using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Text;
using System.Net;

namespace EPSNet
{
	/// <summary>
	/// Common $B$N35MW$N@bL@$G$9!#(B
	/// </summary>
	public class Common
	{
		/// <summary>
		/// (I:]=DW8@(B
		/// </summary>
        //<SRC-01-01-001>static Common
		#region static Common()
		static Common()
		{
		}
		#endregion

		/// <summary>
		/// (I[8^2]U0;^$B$N;HMQ8@8l$r<hF@(B
		/// </summary>
        //<SRC-01-01-002>public static void GetLanguage
        #region public static void GetLanguage()
		public static void GetLanguage()
		{
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				cmdTxt = "SELECT LanguageID FROM m_users WHERE UserID=@UserID";
				sql.AddParam("@UserID", Common.LoginID);
				sql.Open();
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true)	Common.SetSession(SessionID.LanguageID, dr<"LanguageID"]);
				else					Common.SetSession(SessionID.LanguageID, 1);	//$B%f!<%6>pJs$,8+$D$+$i$J$$>l9g$O4{DjCM(B
				dr.Close();
			}
		}
		#endregion

		/// <summary>
		/// (I>/<.]18>=$BMQ(B
		/// </summary>
		#region

		/// <summary>
		/// (I>/<.]$B>pJs(I8X1(B
		/// </summary>
        //<SRC-01-01-003>public static void CreateSession
        public static void CreateSession()
		{
			if(System.Web.HttpContext.Current.Session["CSession"] == null)
				System.Web.HttpContext.Current.Session["CSession"] = new CSession();
		}

		/// <summary>
		/// (I>/<.]$B>pJs=q9~$_(B
		/// </summary>
		/// <param name="id"></param>
		/// <param name="setValue"></param>
        //<SRC-01-01-004>public static void SetSession
        public static void SetSession(SessionID id, object setValue)
		{
			CSession session = (CSession)System.Web.HttpContext.Current.Session["CSession"];
			session.Set(id, setValue);
		}

		/// <summary>
		/// (I>/<.]$B>pJsFI9~$_(B
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        //<SRC-01-01-005>public static object GetSession
        public static object GetSession(SessionID id)
		{
			CSession session = (CSession)System.Web.HttpContext.Current.Session["CSession"];
			return session.Get(id);
		}

		#endregion

        //<SRC-01-01-006>public static string ConvertToBit
        #region public static string ConvertToBit(bool src)
		public static string ConvertToBit(bool src)
		{
			if(src == true)	return "1";
			else			return "0";
		}
		#endregion

		/// <summary>
		/// $B7Y9p(I@^21[8^$BI=<((B
		/// </summary>
		/// <param name="page"></param>
		/// <param name="Message"></param>
        //<SRC-01-01-007>public static void Alert
        #region public static void Alert(System.Web.UI.Page page, string Message)
		public static void Alert(System.Web.UI.Page page, string Message)
		{
			//Message = Message.Replace("'", "\'");
			//Message = Message.Replace("</", "<' + '/");
			//Message = Message.Replace("\r", "\\r");
			//Message = Message.Replace("\n", "\\n");

			Message = Message.Replace("'", "\\'").Replace("</", "<' + '/").Replace("\\r", "\\\\r");
			page.RegisterStartupScript("alert", "<script>alert('" + Message + "');</script>");
		}
		#endregion

		/// <summary>
		/// (I=8XL_D$B=PNO(B
		/// </summary>
		/// <param name="page"></param>
		/// <param name="script"></param>
        //<SRC-01-01-008>public static void OutputScript
        #region public static void OutputScript(System.Web.UI.Page page, string script)
		public static void OutputScript(System.Web.UI.Page page, string script)
		{
			script = script.Replace("'", string.Empty);
			script = script.Replace("\r", "\\r");
			script = script.Replace("\n", "\\n");

			page.RegisterStartupScript("script", "<script>"  + script + "</script>");
		}
		#endregion


        /// <summary>
		/// $BJ8;zNsCf$N!V(B'$B!W$r!V(B''$B!W$KJQ49$9$k(B
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //<SRC-01-01-009>public static string SQCnv(string src)
        #region public static string SQCnv(string src)
		public static string SQCnv(string src)
		{
			return src.Trim().Replace("'", "''");
		}
		#endregion
        //<SRC-01-01-010>public static string SQCnv(object src)
        #region public static string SQCnv(object src)
		public static string SQCnv(object src)
		{
			return src.ToString().Trim().Replace("'", "''");
		}
		#endregion

		/// <summary>
		/// $BJ8;zNs$r(BDB$B3JG<MQ$K(I:]J^0D$B$9$k(B
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //<SRC-01-01-011>public static string CnvToDB
        #region public static string CnvToDB(string src)
		public static string CnvToDB(string src)
		{
			src = HttpUtility.HtmlDecode(src);
			src = src.Trim();
			//src = HttpUtility.HtmlEncode(src);
			return src;
		}
		#endregion

		/// <summary>
		/// $BJ8;zNs$r(BDB$B3JG<MQ$+$i(I:]J^0D$B$9$k(B
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //<SRC-01-01-012>public static string CnvFromDB
        #region public static string CnvFromDB(string src)
		public static string CnvFromDB(string src)
		{
			src = src.Trim();
			src = HttpUtility.HtmlDecode(src);
			return src;
		}
		#endregion

		/// <summary>
		/// bool$B7?$r(B0$B$+(B1$B$NJ8;z$KJQ49$9$k(B
		/// </summary>
		/// <param name="src">true/false</param>
		/// <returns>true:1 false:0</returns>
        //<SRC-01-01-013>public static string ConvertToString
        #region public static string ConvertToString(bool src)
		public static string ConvertToString(bool src)
		{
			if(src == true)	return "1";
			else			return "0";
		}
		#endregion

		/// <summary>
		/// (I=C0@=$B0lMw(BDropDownList$B$r:n@.$9$k(B
		/// </summary>
		/// <param name="sql"></param>
        //<SRC-01-01-014>public static void CreateStatusDropDownList
        #region public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT Text" + Common.LangID.ToString() + " AS Text, StatusID FROM M_ObsStatus ORDER BY SortID";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["Text"].ToString(), dr["StatusID"].ToString()));
			}
			dr.Close();
			if(ddl.Items.Count > 0) ddl.SelectedIndex = 0;
		}
		#endregion

		/// <summary>
		/// DropDownList$B$NCM$r@_Dj$9$k(B
		/// </summary>
		/// <param name="ddl"></param>
		/// <param name="Value"></param>
        //<SRC-01-01-015>public static void SetValue
        #region public static void SetValue(DropDownList ddl, string Value)
		public static void SetValue(DropDownList ddl, string Value)
		{
			for(int i = 0; i < ddl.Items.Count; i++)
			{
				if(ddl.Items[i].Value == Value)
				{
					ddl.SelectedIndex = i;
					return;
				}
			}
		}
		#endregion
        //<SRC-01-01-016>public static void SetText
        #region public static void SetText(DropDownList ddl, string Text)
		public static void SetText(DropDownList ddl, string Text)
		{
			for(int i = 0; i < ddl.Items.Count; i++)
			{
				if(ddl.Items[i].Text == Text)
				{
					ddl.SelectedIndex = i;
					return;
				}
			}
		}
		#endregion

		/// <summary>
		/// $B4IM}HV9f$r@8@.$9$k(B
		/// </summary>
		/// <param name="productName"></param>
		/// <param name="format"></param>
		/// <param name="seqNum"></param>
		/// <returns></returns>
        //<SRC-01-01-017>public static string CreateMngNo
        #region public static string CreateMngNo(string productName, string format, int seqNum)
		public static string CreateMngNo(string productName, string format, int seqNum)
		{
			//$B4IM}HV9f@8@.(B
			//%n:1$B$+$i;O$^$k<+F0:NHV$5$l$?HV9f$KJQ49$5$l$k(B
			string mgrNo = format.Replace("%n", seqNum.ToString());
			//%0[$B?t;z(B]$B#n(B:$B;XDj$5$l$?(B[$B?t;z(B]$B$G(B0$B5M$a(B
			for(int i = 0; i <= 9; i++)
			{
				mgrNo = mgrNo.Replace("%0" + i.ToString() + "n", seqNum.ToString().PadLeft(i, '0'));
			}
			//%p:$B=i4|CM$G$O@=IJL>$K$J$k!"8DJL$NL>A0$KJQ49$5$l$k(B
			mgrNo = mgrNo.Replace("%p", productName);
			//%d:$BF|IU$,!V(Byymmdd$B!W$N=q<0$GJQ49$5$l$k(B
			mgrNo = mgrNo.Replace("%d", DateTime.Now.ToString("yyyyMMdd"));
			//%t:$B;~4V$,!V(Bhhmmss$B!W$N=q<0$GJQ49$5$l$k(B
			mgrNo = mgrNo.Replace("%t", DateTime.Now.ToString("HHmmss"));

			return mgrNo;
		}
		#endregion

		/// <summary>
		/// (I[8^2](BID$BJ8;zNs$r<hF@(B
		/// </summary>
        //<SRC-01-01-018>public static string LoginID
        #region public static string LoginID
		public static string LoginID
		{
			get
			{
				if(Common.GetSession(SessionID.LoginID) == null)	return null;
				else												return Common.GetSession(SessionID.LoginID).ToString();
			}
		}
		#endregion

		/// <summary>
		/// (I[8^2]U0;^$B$N(BLanguageID
		/// </summary>
		/// <returns></returns>
        //<SRC-01-01-019>public static int LangID
        #region public static int LangID
		public static int LangID
		{
			get
			{
				return Convert.ToInt32(GetSession(SessionID.LanguageID));
			}
		}
		#endregion

		/// <summary>
		/// (IC^0@$B7?L>0lMw(B
		/// </summary>
        //<SRC-01-01-020>public static string[,] DataTypes
        #region public static string[,] DataTypes
		public static string[,] DataTypes
		{
			get
			{
				//(IC^0@$B7?L><hF@(B
				string[,] types = new string[7,2];
				using(CSql sql = new CSql())
				{
					sql.Open();
					for(int i = 1; i <= types.GetLength(0); i++)
					{
						types[i - 1, 0] = Common.GetText(sql, typeof(Common), "DataType" + i.ToString());
						types[i - 1, 1] = i.ToString();
					}
				}
				return types;
			}
		}
		#endregion

		/// <summary>
		/// $B8GDj9`L\L><hF@(B
		/// </summary>
        //<SRC-01-01-021>public static string[] GetFixItemColName
        #region public static string[] GetFixItemColName(CSql sql)
		public static string[] GetFixItemColName(CSql sql)
		{
			string[] names = new string[8];
			for(int i = 1; i <= names.Length; i++)
			{
				names[i - 1] = Common.GetText(sql, typeof(Common), "ItemColName" + i.ToString());
			}
			return names;
		}
		#endregion

		/// <summary>
		/// $B8GDj9`L\@bL@<hF@(B
		/// </summary>
        //<SRC-01-01-022>public static string[] GetFixItemColExplain
        #region public static string[] GetFixItemColExplain(CSql sql)
		public static string[] GetFixItemColExplain(CSql sql)
		{
			string[] explain = new string[8];
			for(int i = 1; i <= explain.Length; i++)
			{
				explain[i - 1] = Common.GetText(sql, typeof(Common), "ItemColExplain" + i.ToString());
			}
			return explain;
		}
		#endregion

		/// <summary>
		/// $B:NHVBP>]9`L\L><hF@(B
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
        //<SRC-01-01-023>public static string GetNumberingItemName
        #region public static string GetNumberingItemName()
		public static string GetNumberingItemName()
		{
			using(CSql sql = new CSql())
			{
				sql.Open();
				return GetNumberingItemName(sql);
			}
		}
		public static string GetNumberingItemName(CSql sql)
		{
			string cmdTxt = string.Empty;
			SqlDataReader dr;
			string name = string.Empty;
			//$B:NHVBP>]9`L\(B
			switch(Common.LangID)
			{
				case 1: cmdTxt = "SELECT Japanese FROM M_ObsItem WHERE ItemID=0"; break;
				case 2: cmdTxt = "SELECT English FROM M_ObsItem WHERE ItemID=0"; break;
			}
			dr = sql.Read(cmdTxt);
			if(dr.Read() == true)	name = dr[0].ToString();
			dr.Close();
			return name;
		}
		#endregion

		/// <summary>
		/// $BJ8;zNsCf$+$i(B"%[***]"$B$N7A<0$NJ8;zNs$r<hF@$9$k(B
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
        //<SRC-01-01-024>public static ArrayList GetTag
        #region public static ArrayList GetTag(string str)
		public static ArrayList GetTag(string str)
		{
			ArrayList al = new ArrayList();
			string[] strs = str.Split('%');
			for(int i = 0; i < strs.Length; i++)
			{
				if(strs[i].Length > 3 && strs[i][0] == '[')
				{
					int index = strs[i].IndexOf(']');
					if(index > 0)
					{
						al.Add("%" + strs[i].Substring(0, index + 1));
					}
				}
			}
			return al;
		}
		#endregion

		/// <summary>
		/// $BF|IU(IL+0O/D$BJQ49(B(yyyy/MM/dd)
		/// $B7n!&F|$K(B0$B$rJd40(B
		/// </summary>
		/// <param name="strDate"></param>
		/// <returns>string strFormatDate</returns>
        //<SRC-01-01-025>public static bool FormatDate
        #region public static bool FormatDate(string strDate)
		public static string FormatDate(string strDate)
		{
			string strFormatDate = strDate;

			try
			{
				DateTime dt = DateTime.ParseExact(strDate, "yyyy/M/d", null);
				strFormatDate = dt.Year.ToString("0000") + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");	
				return strFormatDate;
			}
			catch
			{
				return strFormatDate;
			}
		}
		#endregion








	}
}
