using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;

namespace EPSNet.Master.Item
{
	/// <summary>
	/// ItemEdit の概要の説明です。
	/// </summary>
    public class Edit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonUpdate;
		protected System.Web.UI.WebControls.TextBox TextBoxItemName;
		protected System.Web.UI.WebControls.TextBox TextBoxHelpMessage;
		protected System.Web.UI.WebControls.Button ButtonAddRow;
		protected System.Web.UI.WebControls.DropDownList DropDownListDataType;
		protected System.Web.UI.HtmlControls.HtmlTableRow TRList2;
		protected System.Web.UI.HtmlControls.HtmlTableRow TRList1;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelItemName;
		protected System.Web.UI.WebControls.Label LabelHelpMessage;
		protected System.Web.UI.WebControls.Label LabelDataType;
		protected System.Web.UI.WebControls.Label LabelList;
		protected System.Web.UI.WebControls.Label LabelHelp1;
		protected System.Web.UI.WebControls.Label LabelHelp2;
		protected System.Web.UI.WebControls.Button ButtonDeleteRow;
		protected System.Web.UI.WebControls.Button ButtonUp;
		protected System.Web.UI.WebControls.Button ButtonDown;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;
		protected System.Web.UI.WebControls.Button ButtonInsert;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.WebControls.Button ButtonCopy;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
			this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.ButtonCopy.Click += new System.EventHandler(this.ButtonCopy_Click);
			this.DropDownListDataType.SelectedIndexChanged += new System.EventHandler(this.DropDownListDataType_SelectedIndexChanged);
			this.ButtonAddRow.Click += new System.EventHandler(this.ButtonAddRow_Click);
			this.ButtonDeleteRow.Click += new System.EventHandler(this.ButtonDeleteRow_Click);
			this.ButtonUp.Click += new System.EventHandler(this.ButtonUp_Click);
			this.ButtonDown.Click += new System.EventHandler(this.ButtonDown_Click);
			this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================

		private const int COL_Select = 0;
		private const int COL_Text = 1;
		private const int COL_Hidden = 2;
		private const int COL_ListID = 3;
		private const int COL_NotUsed = 4;
		private const int COL_Text1 = 5;
		private const int COL_Text2 = 6;


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Edit);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //[SRC-02-01-01-002]private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			//ｷｬﾝｾﾙﾎﾞﾀﾝ動作設定
			this.ButtonDelete.Attributes["onclick"] = "return confirm('" + Common.GetText(this.mType, "ConfirmDelete") + "');";
			this.ButtonDeleteRow.Attributes["onclick"] = "return confirm('" + Common.GetText(this.mType, "ConfirmDelete") + "');";
			this.ButtonCancel.Attributes["onclick"] = "document.location='./ListFrame.aspx';";

			if(this.IsPostBack == false)
			{
				this.TRList1.Visible = this.TRList2.Visible = false;
				string id = Request.QueryString["id"];
				string copy = Request.QueryString["Copy"];

				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelHelp1);
					Common.SetText(sql, this.mType, this.LabelHelp2);
					Common.SetText(sql, this.mType, this.LabelItemName);
					Common.SetText(sql, this.mType, this.LabelHelpMessage);
					Common.SetText(sql, this.mType, this.LabelDataType);
					Common.SetText(sql, this.mType, this.LabelList);
					Common.SetText(sql, this.mType, this.ButtonInsert);
					Common.SetText(sql, this.mType, this.ButtonUpdate);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					Common.SetText(sql, this.mType, this.ButtonDelete);
					Common.SetText(sql, this.mType, this.ButtonCopy);
					Common.SetText(sql, this.mType, this.ButtonAddRow);
					Common.SetText(sql, this.mType, this.ButtonDeleteRow);
					Common.SetText(sql, this.mType, this.ButtonUp);
					Common.SetText(sql, this.mType, this.ButtonDown);
					this.DataGrid1.Columns[1].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGrid1.Columns[2].HeaderText = Common.GetText(sql, this.mType, "Col2");
					ButtonColumn bc = this.DataGrid1.Columns[0] as ButtonColumn;
					bc.Text = Common.GetText(sql, this.mType, "ButtonSelect");
					this.DataGrid1.Columns[1].FooterText = Common.GetText(sql, this.mType, "FooterText");
					#endregion

					//ﾓｰﾄﾞによる機能ﾎﾞﾀﾝ設定
					if(id == null || copy != null)
					{
						//追加ﾓｰﾄﾞ
						this.ButtonInsert.Visible = true;
						this.ButtonUpdate.Visible = false;
						this.ButtonDelete.Visible = false;
						this.ButtonCopy.Visible = false;
					}
					else
					{
						//更新ﾓｰﾄﾞ
						this.ButtonInsert.Visible = false;
						this.ButtonUpdate.Visible = true;
						this.ButtonDelete.Visible = true;
						this.ButtonCopy.Visible = true;
					}

					//ﾃﾞｰﾀ型名取得
					string[,] types = Common.DataTypes;
					this.DropDownListDataType.Items.Clear();
					for(int i = 0; i < types.GetLength(0); i++)
					{
						this.DropDownListDataType.Items.Add(new ListItem(types[i, 0], types[i, 1]));
					}
					//更新orｺﾋﾟｰの場合、既存ﾃﾞｰﾀの表示
					if(id != null)
					{
						#region
						string cmdTxt;
						SqlDataReader dr;
						cmdTxt = "SELECT *,  (SELECT COUNT(*) FROM T_ObsMgrData AS D WHERE D.ItemID=M.ItemID) AS CNT1,";
						cmdTxt += " (SELECT COUNT(*) FROM M_ObsTemplateItem AS T WHERE T.ItemID=M.ItemID) AS CNT2 FROM M_ObsItem AS M WHERE ItemID='" + id + "'";
						dr = sql.Read(cmdTxt);
						if(dr.Read() == true)
						{
							switch(Common.LangID)
							{
								case 1:
								{
									this.TextBoxItemName.Text = Common.CnvFromDB(dr["Japanese"].ToString());
									this.TextBoxHelpMessage.Text = Common.CnvFromDB(dr["Tips"].ToString());
									break;
								}
								case 2:
								{
									this.TextBoxItemName.Text = Common.CnvFromDB(dr["English"].ToString());
									this.TextBoxHelpMessage.Text = Common.CnvFromDB(dr["Tips2"].ToString());
									break;
								}
							}
							//ｺﾋﾟｰの場合
							if(copy != null)
							{
								//ｱｲﾃﾑ名に「コピー〜」を付加
								this.TextBoxItemName.Text = Common.GetText(this.mType, "CopyString") + this.TextBoxItemName.Text;
							}
							//ﾃﾞｰﾀ型選択
							this.DropDownListDataType.SelectedIndex = Convert.ToInt32(dr["DataType"]) - 1;
							//ﾘｽﾄ型の場合
							if(this.DropDownListDataType.SelectedIndex == 1)
							{
								//ﾘｽﾄ内容作成
								this.CreateItemList();
								//ﾘｽﾄ表示
								this.TRList1.Visible = this.TRList2.Visible = true;
							}
							//ｺﾋﾟｰの場合は変更可能
							if(copy == null)
							{
								//実ﾃﾞｰﾀが存在するorﾃﾝﾌﾟﾚｰﾄに使用されている項目の場合は削除不可
								if(Convert.ToInt32(dr["CNT1"]) != 0 || Convert.ToInt32(dr["CNT2"]) != 0)
								{
									this.ButtonDelete.Enabled = false;
									this.DropDownListDataType.Enabled = false;
								}
							}
							//採番対象項目の場合
							if(id == "0")
							{
								this.ButtonDelete.Enabled = false;
								this.ButtonCopy.Enabled = false;
								this.DropDownListDataType.Enabled = false;
							}
						}
						dr.Close();
						#endregion
					}
				}
			}
		}
		#endregion

		/// <summary>
		/// 追加ﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-003]private void ButtonInsert_Click
        #region private void ButtonInsert_Click(object sender, System.EventArgs e)
		private void ButtonInsert_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Request.QueryString["id"];
				string copy = Request.QueryString["Copy"];
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();

					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}
					
					sql.BeginTrans();
					try
					{
						#region 追加処理
						//項目名重複ﾁｪｯｸ
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "SELECT * FROM M_ObsItem WHERE Japanese=@Name";
								break;
							case 2:
								cmdTxt = "SELECT * FROM M_ObsItem WHERE English=@Name";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxItemName.Text));
						dr = sql.ReadTrans(cmdTxt);
						bool exist = false;
						if(dr.Read() == true) exist = true;
						dr.Close();
						if(exist == true)
						{
							sql.CommitTrans();
							Common.Alert(this, Common.GetText(sql, this.mType, "AlertSameItemName"));
							return;
						}
						//ItemID最大値＋１取得
						cmdTxt = "SELECT MAX(ItemID) AS MAX FROM M_ObsItem";
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true && dr["MAX"] != DBNull.Value)	id = (Convert.ToInt32(dr["MAX"]) + 1).ToString();
						else												id = "0";
						dr.Close();

						cmdTxt = "INSERT INTO M_ObsItem(ItemID, Japanese,English,Tips,Tips2,DataType)";
						cmdTxt += " VALUES(@ItemID, @Name, @Name, @Tips, @Tips, @DataType)";
						sql.AddParam("@ItemID", id);
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@Tips", Common.CnvToDB(this.TextBoxHelpMessage.Text));
						sql.AddParam("@DataType", (this.DropDownListDataType.SelectedIndex + 1).ToString());
						sql.CommandTrans(cmdTxt);

						if(this.DropDownListDataType.SelectedIndex == 1)
						{
							for(int i = 0; i < this.DataGrid1.Items.Count; i++)
							{
								TextBox tb = this.DataGrid1.Items[i].FindControl("TextBoxDispText") as TextBox;
								CheckBox cb = this.DataGrid1.Items[i].FindControl("CheckBoxHidden") as CheckBox;

								cmdTxt = "INSERT INTO M_ObsItemList(ItemID, ListID, Text1, Text2, SortID, DisabledFlag)";
								cmdTxt += " VALUES(@ItemID, @ListID, @Text, @Text, @SortID, @DisabledFlag)";
								sql.AddParam("@ItemID", id);
								sql.AddParam("@ListID", i.ToString());
								sql.AddParam("@Text", Common.CnvToDB(tb.Text));
								sql.AddParam("@SortID", i.ToString());
								sql.AddParam("@DisabledFlag", Common.ConvertToBit(cb.Checked));

								sql.CommandTrans(cmdTxt);
							}
						}
						#endregion
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 更新ﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-004]private void ButtonUpdate_Click
        #region private void ButtonUpdate_Click(object sender, System.EventArgs e)
		private void ButtonUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Request.QueryString["id"];
				string copy = Request.QueryString["Copy"];
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();

					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}

					//更新処理
					sql.BeginTrans();
					try
					{
						#region
						//項目名重複ﾁｪｯｸ
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "SELECT * FROM M_ObsItem WHERE Japanese=@Name AND ItemID<>@ItemID";
								break;
							case 2:
								cmdTxt = "SELECT * FROM M_ObsItem WHERE English=@Name AND ItemID<>@ItemID";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@ItemID", id);
						dr = sql.ReadTrans(cmdTxt);
						bool exist = false;
						if(dr.Read() == true) exist = true;
						dr.Close();
						if(exist == true)
						{
							sql.CommitTrans();
							Common.Alert(this, Common.GetText(sql, this.mType, "AlertSameItemName"));
							return;
						}
						//項目追加
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "UPDATE M_ObsItem SET Japanese=@Name, Tips=@Tips, DataType=@DataType WHERE ItemID=@ItemID";
								break;
							case 2:
								cmdTxt = "UPDATE M_ObsItem SET English=@Name, Tips2=@Tips, DataType=@DataType WHERE ItemID=@ItemID";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@Tips", Common.CnvToDB(this.TextBoxHelpMessage.Text));
						sql.AddParam("@DataType", (this.DropDownListDataType.SelectedIndex + 1).ToString());
						sql.AddParam("@ItemID", id);
						sql.CommandTrans(cmdTxt);

						//ﾘｽﾄ型の登録済み項目を削除
						cmdTxt = "DELETE FROM M_ObsItemList WHERE ItemID=@ItemID";
						sql.AddParam("@ItemID", id);
						sql.CommandTrans(cmdTxt);

						//ﾃﾞｰﾀﾀｲﾌﾟがﾘｽﾄ型の場合、項目を登録し直す
						if(this.DropDownListDataType.SelectedIndex == 1)	//ﾃﾞｰﾀﾀｲﾌﾟがﾘｽﾄの場合
						{
							int startListID = -1;	//新規追加されるﾘｽﾄの開始ListID(現在の最大値+1)

							//現在登録済みのListIDの最大値を取得
							for(int i = 0; i < this.DataGrid1.Items.Count; i++)
							{
								DataGridItem item = this.DataGrid1.Items[i];
								int ListID = Convert.ToInt32(item.Cells[COL_ListID].Text);
								if(ListID > startListID) startListID = ListID;
							}
							startListID++;

							//登録済みのものと新規追加されたﾘｽﾄ項目をDBに登録する
							for(int i = 0; i < this.DataGrid1.Items.Count; i++)
							{
								DataGridItem item = this.DataGrid1.Items[i];
								TextBox tb = item.FindControl("TextBoxDispText") as TextBox;
								CheckBox cb = item.FindControl("CheckBoxHidden") as CheckBox;
								string ListID = item.Cells[COL_ListID].Text;

								cmdTxt = "INSERT INTO M_ObsItemList(ItemID, ListID, Text1, Text2, SortID, DisabledFlag)";
								cmdTxt += " VALUES(@ItemID, @ListID, @Text1, @Text2, @SortID, @DisabledFlag)";
								//新規・既存の共通SQLﾊﾟﾗﾒｰﾀ
								sql.AddParam("@ItemID", id);
								sql.AddParam("@SortID", i.ToString());
								sql.AddParam("@DisabledFlag", Common.ConvertToBit(cb.Checked));

								if(ListID == "-1")	//新規ﾘｽﾄ項目
								{
									//SQLﾊﾟﾗﾒｰﾀ設定(日本語・英語とも同じ値をｾｯﾄ)
									sql.AddParam("@Text1", Common.CnvToDB(tb.Text));
									sql.AddParam("@Text2", Common.CnvToDB(tb.Text));
									sql.AddParam("@ListID", startListID.ToString());
									startListID++;
								}
								else				//既存ﾘｽﾄ項目
								{
									//SQLﾊﾟﾗﾒｰﾀ設定(言語設定により、日本語・英語の設定ﾃﾞｰﾀを変える)
									if(Common.LangID == 1)	//日本語
									{
										string ss = Common.CnvToDB(item.Cells[COL_Text2].Text);
										sql.AddParam("@Text1", Common.CnvToDB(tb.Text));					//日本語ﾌｨｰﾙﾄﾞには入力値
										sql.AddParam("@Text2", Common.CnvToDB(item.Cells[COL_Text2].Text));	//英語ﾌｨｰﾙﾄﾞには既存値
									}
									else					//英語
									{
										string ss = Common.CnvToDB(item.Cells[COL_Text1].Text);
										sql.AddParam("@Text1", Common.CnvToDB(item.Cells[COL_Text1].Text));	//日本語ﾌｨｰﾙﾄﾞには既存値
										sql.AddParam("@Text2", Common.CnvToDB(tb.Text));					//英語ﾌｨｰﾙﾄﾞには入力値
									}
									sql.AddParam("@ListID", ListID);
								}
								sql.CommandTrans(cmdTxt);
							}
						}
						#endregion

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 削除ﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-005]private void ButtonDelete_Click
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			string id = Request.QueryString["id"];
			if(id == null) return;

			try
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//項目ﾏｽﾀから削除
						cmdTxt = "DELETE FROM M_ObsItem WHERE ItemID=@id";
						sql.AddParam("@id", id);
						sql.CommandTrans(cmdTxt);

						//ﾘｽﾄ項目ﾏｽﾀから削除
						cmdTxt = "DELETE FROM M_ObsItemList WHERE ItemID=@id";
						sql.AddParam("@id", id);
						sql.CommandTrans(cmdTxt);

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ｺﾋﾟｰﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-006]private void ButtonCopy_Click
        #region private void ButtonCopy_Click(object sender, System.EventArgs e)
		private void ButtonCopy_Click(object sender, System.EventArgs e)
		{
			string id = Request.QueryString["id"];
			if(id == null) return;

			//自分自身をｺﾋﾟｰﾓｰﾄﾞで表示し直す
			Response.Redirect("./Edit.aspx?id=" + id + "&Copy=1");
		}
		#endregion

		/// <summary>
		/// 1行追加ﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-007]private void ButtonAddRow_Click
        #region private void ButtonAddRow_Click(object sender, System.EventArgs e)
		private void ButtonAddRow_Click(object sender, System.EventArgs e)
		{
			DataTable table = this.CreateEmptyTable();

			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				DataGridItem item = this.DataGrid1.Items[i];
				TextBox tb = item.FindControl("TextBoxDispText") as TextBox;
				CheckBox cb = item.FindControl("CheckBoxHidden") as CheckBox;
				DataRow row = table.NewRow();
				row["Text"] = tb.Text;
				row["Hidden"] = cb.Checked;
				row["ListID"] = Convert.ToInt32(item.Cells[COL_ListID].Text);
				row["NotUsed"] = Convert.ToBoolean(item.Cells[COL_NotUsed].Text);
				row["Text1"] = item.Cells[COL_Text1].Text;
				row["Text2"] = item.Cells[COL_Text2].Text;
				table.Rows.Add(row);
			}
			if(true)
			{
				DataRow row = table.NewRow();
				row["Text"] = string.Empty;
				row["Hidden"] = false;
				row["ListID"] = -1;
				row["NotUsed"] = true;
				row["Text1"] = string.Empty;
				row["Text2"] = string.Empty;
				table.Rows.Add(row);
			}
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();

			this.DataGrid1.SelectedIndex = this.DataGrid1.Items.Count - 1;
			this.DataGrid1_SelectedIndexChanged(null, null);
		}
		#endregion

		/// <summary>
		/// 1行削除ﾎﾞﾀﾝ
		/// </summary>
        //[SRC-02-01-01-008]private void ButtonDeleteRow_Click
        #region private void ButtonDeleteRow_Click(object sender, System.EventArgs e)
		private void ButtonDeleteRow_Click(object sender, System.EventArgs e)
		{
			int index = this.DataGrid1.SelectedIndex;

			DataTable table = this.CreateEmptyTable();
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				if(i == index) continue;
				DataGridItem item = this.DataGrid1.Items[i];
				TextBox tb = item.FindControl("TextBoxDispText") as TextBox;
				CheckBox cb = item.FindControl("CheckBoxHidden") as CheckBox;
				DataRow row = table.NewRow();
				row["Text"] = tb.Text;
				row["Hidden"] = cb.Checked;
				row["ListID"] = Convert.ToInt32(item.Cells[COL_ListID].Text);
				row["NotUsed"] = Convert.ToBoolean(item.Cells[COL_NotUsed].Text);
				row["Text1"] = item.Cells[COL_Text1].Text;
				row["Text2"] = item.Cells[COL_Text2].Text;
				table.Rows.Add(row);
			}
			if(table.Rows.Count > 0)
			{
				this.DataGrid1.DataSource = table;
			}
			else
			{
				this.DataGrid1.DataSource = null;
			}
			this.DataGrid1.DataBind();
			this.DataGrid1.SelectedIndex = -1;
			this.DataGrid1_SelectedIndexChanged(null, null);
		}
		#endregion

		/// <summary>
		/// 上へ移動
		/// </summary>
        //[SRC-02-01-01-009]private void ButtonUp_Click
        #region private void ButtonUp_Click(object sender, System.EventArgs e)
		private void ButtonUp_Click(object sender, System.EventArgs e)
		{
			int index = this.DataGrid1.SelectedIndex;
			//一つ上と交換
			TextBox tbSrc = this.DataGrid1.Items[index].FindControl("TextBoxDispText") as TextBox;
			TextBox tbDst = this.DataGrid1.Items[index - 1].FindControl("TextBoxDispText") as TextBox;
			CheckBox cbSrc = this.DataGrid1.Items[index].FindControl("CheckBoxHidden") as CheckBox;
			CheckBox cbDst = this.DataGrid1.Items[index - 1].FindControl("CheckBoxHidden") as CheckBox;
			string sortSrc = this.DataGrid1.Items[index].Cells[COL_ListID].Text;
			string sortDst = this.DataGrid1.Items[index - 1].Cells[COL_ListID].Text;

			string str = tbDst.Text;
			bool hidden = cbDst.Checked;
			bool enabled = cbDst.Enabled;
			string sort = sortDst;

			tbDst.Text = tbSrc.Text;
			cbDst.Checked = cbSrc.Checked;
			cbDst.Enabled = cbSrc.Enabled;
			this.DataGrid1.Items[index - 1].Cells[COL_ListID].Text = sortSrc;

			tbSrc.Text = str;
			cbSrc.Checked = hidden;
			cbSrc.Enabled = enabled;
			this.DataGrid1.Items[index].Cells[COL_ListID].Text = sort;

			string NotUsedSrc = this.DataGrid1.Items[index].Cells[COL_NotUsed].Text;
			this.DataGrid1.Items[index].Cells[COL_NotUsed].Text = this.DataGrid1.Items[index - 1].Cells[COL_NotUsed].Text;
			this.DataGrid1.Items[index - 1].Cells[COL_NotUsed].Text = NotUsedSrc;

			string Text1Src = this.DataGrid1.Items[index].Cells[COL_Text1].Text;
			this.DataGrid1.Items[index].Cells[COL_Text1].Text = this.DataGrid1.Items[index - 1].Cells[COL_Text1].Text;
			this.DataGrid1.Items[index - 1].Cells[COL_Text1].Text = Text1Src;

			string Text2Src = this.DataGrid1.Items[index].Cells[COL_Text2].Text;
			this.DataGrid1.Items[index].Cells[COL_Text2].Text = this.DataGrid1.Items[index - 1].Cells[COL_Text2].Text;
			this.DataGrid1.Items[index - 1].Cells[COL_Text2].Text = Text2Src;

			this.DataGrid1.SelectedIndex--;
			this.DataGrid1_SelectedIndexChanged(null, null);
		}
		#endregion

		/// <summary>
		/// 下へ移動
		/// </summary>
        //[SRC-02-01-01-010]private void ButtonDown_Click
        #region private void ButtonDown_Click(object sender, System.EventArgs e)
		private void ButtonDown_Click(object sender, System.EventArgs e)
		{
			int index = this.DataGrid1.SelectedIndex;

			//一つ下と交換
			TextBox tbSrc = this.DataGrid1.Items[index].FindControl("TextBoxDispText") as TextBox;
			TextBox tbDst = this.DataGrid1.Items[index + 1].FindControl("TextBoxDispText") as TextBox;
			CheckBox cbSrc = this.DataGrid1.Items[index].FindControl("CheckBoxHidden") as CheckBox;
			CheckBox cbDst = this.DataGrid1.Items[index + 1].FindControl("CheckBoxHidden") as CheckBox;
			string sortSrc = this.DataGrid1.Items[index].Cells[COL_ListID].Text;
			string sortDst = this.DataGrid1.Items[index + 1].Cells[COL_ListID].Text;

			string str = tbDst.Text;
			bool hidden = cbDst.Checked;
			string sort = sortDst;
			bool enabled = cbDst.Enabled;

			tbDst.Text = tbSrc.Text;
			cbDst.Checked = cbSrc.Checked;
			cbDst.Enabled = cbSrc.Enabled;
			this.DataGrid1.Items[index + 1].Cells[COL_ListID].Text = sortSrc;

			tbSrc.Text = str;
			cbSrc.Checked = hidden;
			cbSrc.Enabled = enabled;
			this.DataGrid1.Items[index].Cells[COL_ListID].Text = sort;

			string NotUsedSrc = this.DataGrid1.Items[index].Cells[COL_NotUsed].Text;
			this.DataGrid1.Items[index].Cells[COL_NotUsed].Text = this.DataGrid1.Items[index + 1].Cells[COL_NotUsed].Text;
			this.DataGrid1.Items[index + 1].Cells[COL_NotUsed].Text = NotUsedSrc;

			string Text1Src = this.DataGrid1.Items[index].Cells[COL_Text1].Text;
			this.DataGrid1.Items[index].Cells[COL_Text1].Text = this.DataGrid1.Items[index + 1].Cells[COL_Text1].Text;
			this.DataGrid1.Items[index + 1].Cells[COL_Text1].Text = Text1Src;

			string Text2Src = this.DataGrid1.Items[index].Cells[COL_Text2].Text;
			this.DataGrid1.Items[index].Cells[COL_Text2].Text = this.DataGrid1.Items[index + 1].Cells[COL_Text2].Text;
			this.DataGrid1.Items[index + 1].Cells[COL_Text2].Text = Text2Src;

			this.DataGrid1.SelectedIndex++;
			this.DataGrid1_SelectedIndexChanged(null, null);
		}
		#endregion

		/// <summary>
		/// 選択行ﾁｪﾝｼﾞｲﾍﾞﾝﾄ
		/// </summary>
        //[SRC-02-01-01-011]private void DataGrid1_SelectedIndexChanged
        #region private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//未選択
			if(this.DataGrid1.SelectedIndex == -1)
			{
				this.ButtonDeleteRow.Enabled = this.ButtonUp.Enabled = this.ButtonDown.Enabled = false;
				return;
			}
			this.ButtonDeleteRow.Enabled = this.ButtonUp.Enabled = this.ButtonDown.Enabled = true;
			//先頭行選択
			if(this.DataGrid1.SelectedIndex == 0)
			{
				this.ButtonUp.Enabled = false;
			}
			//最終行選択
			if(this.DataGrid1.SelectedIndex == this.DataGrid1.Items.Count - 1)
			{
				this.ButtonDown.Enabled = false;
			}
			//最後の１ﾚｺｰﾄﾞの場合削除不可
			if(this.DataGrid1.Items.Count == 1)
			{
				this.ButtonDeleteRow.Enabled = false;
			}
			string id = Request.QueryString["id"];
			//実ﾃﾞｰﾀで使用されているﾘｽﾄ項目の場合は削除不可
			if(id != null)
			{
				bool NotUsed = Convert.ToBoolean(this.DataGrid1.SelectedItem.Cells[COL_NotUsed].Text);
				if(NotUsed == false)
				{
					this.ButtonDeleteRow.Enabled = false;
				}
			}
		}
		#endregion

		/// <summary>
		/// DataGrid1_ItemDataBound
		/// </summary>
        //[SRC-02-01-01-012]private void DataGrid1_ItemDataBound
        #region private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.SelectedItem)
			{
				//TDｸﾘｯｸで行選択
				LinkButton lb = e.Item.Cells[COL_Select].Controls[0] as LinkButton;
				string name = "LinkButton" + e.Item.ItemIndex.ToString();
				lb.Attributes["name"] = name;
				e.Item.Cells[0].Attributes["Class"] = "Hand";
				e.Item.Cells[0].Attributes["onclick"] = name + ".click();";
			}
		}
		#endregion

		/// <summary>
		/// ﾃﾞｰﾀﾀｲﾌﾟ変更ｲﾍﾞﾝﾄ
		/// </summary>
        //[SRC-02-01-01-013]private void DropDownListDataType_SelectedIndexChanged
        #region private void DropDownListDataType_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DropDownListDataType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.DropDownListDataType.SelectedIndex == 1)
			{
				this.TRList1.Visible = this.TRList2.Visible = true;
				if(this.DataGrid1.Items.Count == 0)
				{
					this.ButtonAddRow_Click(null, null);
				}
			}
			else
			{
				this.TRList1.Visible = this.TRList2.Visible = false;
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 空ﾃｰﾌﾞﾙ作成
		/// </summary>
		/// <returns></returns>
        //[SRC-02-01-01-014]private DataTable CreateEmptyTable
        #region private DataTable CreateEmptyTable()
		private DataTable CreateEmptyTable()
		{
			DataTable table = new DataTable();
			table.Columns.Add("Text", typeof(string));
			table.Columns.Add("Hidden", typeof(bool));
			table.Columns.Add("ListID", typeof(int));
			table.Columns.Add("NotUsed", typeof(bool));
			table.Columns.Add("Text1", typeof(string));
			table.Columns.Add("Text2", typeof(string));
			return table;
		}
		#endregion

		/// <summary>
		/// DBに登録済みのﾘｽﾄ一覧表示
		/// </summary>
        //[SRC-02-01-01-015]private void CreateItemList
        #region private void CreateItemList()
		private void CreateItemList()
		{
			try
			{
				string id = Request.QueryString["id"];
				DataTable table = this.CreateEmptyTable();

				if(id != null)
				{
					using(CSql sql = new CSql())
					{
						string cmdTxt;
						SqlDataReader dr;

						cmdTxt = "SELECT M_ObsItemList.*,";
						cmdTxt += " (SELECT COUNT(*) FROM T_ObsMgrData AS DATA WHERE DATA.ItemID=M_ObsItemList.ItemID AND DATA.Data2=M_ObsItemList.ListID) AS CNT";
						cmdTxt += " FROM M_ObsItemList";
						cmdTxt += " WHERE M_ObsItemList.ItemID=@ItemID ORDER BY M_ObsItemList.SortID";
						sql.AddParam("@ItemID", id);
						sql.Open();
						dr = sql.Read(cmdTxt);
						string copy = Request.QueryString["Copy"];
						while(dr.Read() == true)
						{
							DataRow row = table.NewRow();
							row["Text"] = dr["Text" + Common.LangID.ToString()];
							row["Hidden"] = Convert.ToBoolean(dr["DisabledFlag"]);
							row["ListID"] = Convert.ToInt32(dr["ListID"]);
							if(copy == null)
							{
								if(Convert.ToInt32(dr["CNT"]) == 0)		row["NotUsed"] = true;
								else									row["NotUsed"] = false;
							}
							else
							{
								row["NotUsed"] = true;
							}
							row["Text1"] = dr["Text1"];
							row["Text2"] = dr["Text2"];
							table.Rows.Add(row);
						}
						dr.Close();
					}
				}
				this.DataGrid1.DataSource = table;
				this.DataGrid1.DataBind();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 入力ﾁｪｯｸ
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
        //[SRC-02-01-01-016]private bool InputCheck
        #region private bool InputCheck(CSql sql)
		private bool InputCheck(CSql sql)
		{
			if(Common.CnvToDB(this.TextBoxItemName.Text) == string.Empty)
			{
				Common.Alert(this, Common.GetText(sql, this.mType, "AlertItemInput"));
				return false;
			}
			//ﾘｽﾄ型の場合、ﾘｽﾄｱｲﾃﾑをﾁｪｯｸ
			if(this.DropDownListDataType.SelectedValue == "2")
			{
				for(int i = 0; i < this.DataGrid1.Items.Count; i++)
				{
					TextBox tb = this.DataGrid1.Items[i].FindControl("TextBoxDispText") as TextBox;
					if(Common.CnvToDB(tb.Text) == string.Empty)
					{
						Common.Alert(this, Common.GetText(sql, this.mType, "AlertListItemInput"));
						return false;
					}
				}
			}

			//同一ﾘｽﾄ存在ﾁｪｯｸ
			ArrayList al = new ArrayList();
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				TextBox tb = this.DataGrid1.Items[i].FindControl("TextBoxDispText") as TextBox;
				//string str = tb.Text.Trim().ToUpper();
				string str = tb.Text.Trim();
				int index = al.IndexOf(str);
				if(index >= 0)
				{
					Common.Alert(this, Common.GetText(sql, this.mType, "SameText") + "\\n(" + str + ")");
					return false;
				}
				al.Add(str);
			}
			return true;
		}
		#endregion














































	}
}
