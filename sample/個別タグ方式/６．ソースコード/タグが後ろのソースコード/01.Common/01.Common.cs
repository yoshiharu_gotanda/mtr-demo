using System;

using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Text;
using System.Net;

namespace EPSNet
{
	/// <summary>
	/// Common の概要の説明です。
	/// </summary>
	public class Common
	{
		/// <summary>
		/// ｺﾝｽﾄﾗｸﾀ
		/// </summary>
        //static Common【SRC-01-01-001】
		#region static Common()
		static Common()
		{
		}
		#endregion

		/// <summary>
		/// ﾛｸﾞｲﾝﾕｰｻﾞの使用言語を取得
		/// </summary>
        //public static void GetLanguage【SRC-01-01-002】
        #region public static void GetLanguage()
		public static void GetLanguage()
		{
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				cmdTxt = "SELECT LanguageID FROM m_users WHERE UserID=@UserID";
				sql.AddParam("@UserID", Common.LoginID);
				sql.Open();
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true)	Common.SetSession(SessionID.LanguageID, dr【"LanguageID"]);
				else					Common.SetSession(SessionID.LanguageID, 1);	//ユーザ情報が見つからない場合は既定値
				dr.Close();
			}
		}
		#endregion

		/// <summary>
		/// ｾｯｼｮﾝｱｸｾｽ用
		/// </summary>
		#region

		/// <summary>
		/// ｾｯｼｮﾝ情報ｸﾘｱ
		/// </summary>
        //public static void CreateSession【SRC-01-01-003】
        public static void CreateSession()
		{
			if(System.Web.HttpContext.Current.Session["CSession"] == null)
				System.Web.HttpContext.Current.Session["CSession"] = new CSession();
		}

		/// <summary>
		/// ｾｯｼｮﾝ情報書込み
		/// </summary>
		/// <param name="id"></param>
		/// <param name="setValue"></param>
        //public static void SetSession【SRC-01-01-004】
        public static void SetSession(SessionID id, object setValue)
		{
			CSession session = (CSession)System.Web.HttpContext.Current.Session["CSession"];
			session.Set(id, setValue);
		}

		/// <summary>
		/// ｾｯｼｮﾝ情報読込み
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        //public static object GetSession【SRC-01-01-005】
        public static object GetSession(SessionID id)
		{
			CSession session = (CSession)System.Web.HttpContext.Current.Session["CSession"];
			return session.Get(id);
		}

		#endregion

        //public static string ConvertToBit【SRC-01-01-006】
        #region public static string ConvertToBit(bool src)
		public static string ConvertToBit(bool src)
		{
			if(src == true)	return "1";
			else			return "0";
		}
		#endregion

		/// <summary>
		/// 警告ﾀﾞｲｱﾛｸﾞ表示
		/// </summary>
		/// <param name="page"></param>
		/// <param name="Message"></param>
        //public static void Alert【SRC-01-01-007】
        #region public static void Alert(System.Web.UI.Page page, string Message)
		public static void Alert(System.Web.UI.Page page, string Message)
		{
			//Message = Message.Replace("'", "\'");
			//Message = Message.Replace("</", "<' + '/");
			//Message = Message.Replace("\r", "\\r");
			//Message = Message.Replace("\n", "\\n");

			Message = Message.Replace("'", "\\'").Replace("</", "<' + '/").Replace("\\r", "\\\\r");
			page.RegisterStartupScript("alert", "<script>alert('" + Message + "');</script>");
		}
		#endregion

		/// <summary>
		/// ｽｸﾘﾌﾟﾄ出力
		/// </summary>
		/// <param name="page"></param>
		/// <param name="script"></param>
        //public static void OutputScript【SRC-01-01-008】
        #region public static void OutputScript(System.Web.UI.Page page, string script)
		public static void OutputScript(System.Web.UI.Page page, string script)
		{
			script = script.Replace("'", string.Empty);
			script = script.Replace("\r", "\\r");
			script = script.Replace("\n", "\\n");

			page.RegisterStartupScript("script", "<script>"  + script + "</script>");
		}
		#endregion


        /// <summary>
		/// 文字列中の「'」を「''」に変換する
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //public static string SQCnv(string src)【SRC-01-01-009】
        #region public static string SQCnv(string src)
		public static string SQCnv(string src)
		{
			return src.Trim().Replace("'", "''");
		}
		#endregion
        //public static string SQCnv(object src)【SRC-01-01-010】
        #region public static string SQCnv(object src)
		public static string SQCnv(object src)
		{
			return src.ToString().Trim().Replace("'", "''");
		}
		#endregion

		/// <summary>
		/// 文字列をDB格納用にｺﾝﾊﾞｰﾄする
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //public static string CnvToDB【SRC-01-01-011】
        #region public static string CnvToDB(string src)
		public static string CnvToDB(string src)
		{
			src = HttpUtility.HtmlDecode(src);
			src = src.Trim();
			//src = HttpUtility.HtmlEncode(src);
			return src;
		}
		#endregion

		/// <summary>
		/// 文字列をDB格納用からｺﾝﾊﾞｰﾄする
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //public static string CnvFromDB【SRC-01-01-012】
        #region public static string CnvFromDB(string src)
		public static string CnvFromDB(string src)
		{
			src = src.Trim();
			src = HttpUtility.HtmlDecode(src);
			return src;
		}
		#endregion

		/// <summary>
		/// bool型を0か1の文字に変換する
		/// </summary>
		/// <param name="src">true/false</param>
		/// <returns>true:1 false:0</returns>
        //public static string ConvertToString【SRC-01-01-013】
        #region public static string ConvertToString(bool src)
		public static string ConvertToString(bool src)
		{
			if(src == true)	return "1";
			else			return "0";
		}
		#endregion

		/// <summary>
		/// ｽﾃｰﾀｽ一覧DropDownListを作成する
		/// </summary>
		/// <param name="sql"></param>
        //public static void CreateStatusDropDownList【SRC-01-01-014】
        #region public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		public static void CreateStatusDropDownList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT Text" + Common.LangID.ToString() + " AS Text, StatusID FROM M_ObsStatus ORDER BY SortID";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["Text"].ToString(), dr["StatusID"].ToString()));
			}
			dr.Close();
			if(ddl.Items.Count > 0) ddl.SelectedIndex = 0;
		}
		#endregion

		/// <summary>
		/// DropDownListの値を設定する
		/// </summary>
		/// <param name="ddl"></param>
		/// <param name="Value"></param>
        //public static void SetValue【SRC-01-01-015】
        #region public static void SetValue(DropDownList ddl, string Value)
		public static void SetValue(DropDownList ddl, string Value)
		{
			for(int i = 0; i < ddl.Items.Count; i++)
			{
				if(ddl.Items[i].Value == Value)
				{
					ddl.SelectedIndex = i;
					return;
				}
			}
		}
		#endregion
        //public static void SetText【SRC-01-01-016】
        #region public static void SetText(DropDownList ddl, string Text)
		public static void SetText(DropDownList ddl, string Text)
		{
			for(int i = 0; i < ddl.Items.Count; i++)
			{
				if(ddl.Items[i].Text == Text)
				{
					ddl.SelectedIndex = i;
					return;
				}
			}
		}
		#endregion

		/// <summary>
		/// 管理番号を生成する
		/// </summary>
		/// <param name="productName"></param>
		/// <param name="format"></param>
		/// <param name="seqNum"></param>
		/// <returns></returns>
        //public static string CreateMngNo【SRC-01-01-017】
        #region public static string CreateMngNo(string productName, string format, int seqNum)
		public static string CreateMngNo(string productName, string format, int seqNum)
		{
			//管理番号生成
			//%n:1から始まる自動採番された番号に変換される
			string mgrNo = format.Replace("%n", seqNum.ToString());
			//%0[数字]ｎ:指定された[数字]で0詰め
			for(int i = 0; i <= 9; i++)
			{
				mgrNo = mgrNo.Replace("%0" + i.ToString() + "n", seqNum.ToString().PadLeft(i, '0'));
			}
			//%p:初期値では製品名になる、個別の名前に変換される
			mgrNo = mgrNo.Replace("%p", productName);
			//%d:日付が「yymmdd」の書式で変換される
			mgrNo = mgrNo.Replace("%d", DateTime.Now.ToString("yyyyMMdd"));
			//%t:時間が「hhmmss」の書式で変換される
			mgrNo = mgrNo.Replace("%t", DateTime.Now.ToString("HHmmss"));

			return mgrNo;
		}
		#endregion

		/// <summary>
		/// ﾛｸﾞｲﾝID文字列を取得
		/// </summary>
        //public static string LoginID【SRC-01-01-018】
        #region public static string LoginID
		public static string LoginID
		{
			get
			{
				if(Common.GetSession(SessionID.LoginID) == null)	return null;
				else												return Common.GetSession(SessionID.LoginID).ToString();
			}
		}
		#endregion

		/// <summary>
		/// ﾛｸﾞｲﾝﾕｰｻﾞのLanguageID
		/// </summary>
		/// <returns></returns>
        //public static int LangID【SRC-01-01-019】
        #region public static int LangID
		public static int LangID
		{
			get
			{
				return Convert.ToInt32(GetSession(SessionID.LanguageID));
			}
		}
		#endregion

		/// <summary>
		/// ﾃﾞｰﾀ型名一覧
		/// </summary>
        //public static string[,] DataTypes【SRC-01-01-020】
        #region public static string[,] DataTypes
		public static string[,] DataTypes
		{
			get
			{
				//ﾃﾞｰﾀ型名取得
				string[,] types = new string[7,2];
				using(CSql sql = new CSql())
				{
					sql.Open();
					for(int i = 1; i <= types.GetLength(0); i++)
					{
						types[i - 1, 0] = Common.GetText(sql, typeof(Common), "DataType" + i.ToString());
						types[i - 1, 1] = i.ToString();
					}
				}
				return types;
			}
		}
		#endregion

		/// <summary>
		/// 固定項目名取得
		/// </summary>
        //public static string[] GetFixItemColName【SRC-01-01-021】
        #region public static string[] GetFixItemColName(CSql sql)
		public static string[] GetFixItemColName(CSql sql)
		{
			string[] names = new string[8];
			for(int i = 1; i <= names.Length; i++)
			{
				names[i - 1] = Common.GetText(sql, typeof(Common), "ItemColName" + i.ToString());
			}
			return names;
		}
		#endregion

		/// <summary>
		/// 固定項目説明取得
		/// </summary>
        //public static string[] GetFixItemColExplain【SRC-01-01-022】
        #region public static string[] GetFixItemColExplain(CSql sql)
		public static string[] GetFixItemColExplain(CSql sql)
		{
			string[] explain = new string[8];
			for(int i = 1; i <= explain.Length; i++)
			{
				explain[i - 1] = Common.GetText(sql, typeof(Common), "ItemColExplain" + i.ToString());
			}
			return explain;
		}
		#endregion

		/// <summary>
		/// 採番対象項目名取得
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
        //public static string GetNumberingItemName【SRC-01-01-023】
        #region public static string GetNumberingItemName()
		public static string GetNumberingItemName()
		{
			using(CSql sql = new CSql())
			{
				sql.Open();
				return GetNumberingItemName(sql);
			}
		}
		public static string GetNumberingItemName(CSql sql)
		{
			string cmdTxt = string.Empty;
			SqlDataReader dr;
			string name = string.Empty;
			//採番対象項目
			switch(Common.LangID)
			{
				case 1: cmdTxt = "SELECT Japanese FROM M_ObsItem WHERE ItemID=0"; break;
				case 2: cmdTxt = "SELECT English FROM M_ObsItem WHERE ItemID=0"; break;
			}
			dr = sql.Read(cmdTxt);
			if(dr.Read() == true)	name = dr[0].ToString();
			dr.Close();
			return name;
		}
		#endregion

		/// <summary>
		/// 文字列中から"%[***]"の形式の文字列を取得する
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
        //public static ArrayList GetTag【SRC-01-01-024】
        #region public static ArrayList GetTag(string str)
		public static ArrayList GetTag(string str)
		{
			ArrayList al = new ArrayList();
			string[] strs = str.Split('%');
			for(int i = 0; i < strs.Length; i++)
			{
				if(strs[i].Length > 3 && strs[i][0] == '[')
				{
					int index = strs[i].IndexOf(']');
					if(index > 0)
					{
						al.Add("%" + strs[i].Substring(0, index + 1));
					}
				}
			}
			return al;
		}
		#endregion

		/// <summary>
		/// 日付ﾌｫｰﾏｯﾄ変換(yyyy/MM/dd)
		/// 月・日に0を補完
		/// </summary>
		/// <param name="strDate"></param>
		/// <returns>string strFormatDate</returns>
        //public static bool FormatDate【SRC-01-01-025】
        #region public static bool FormatDate(string strDate)
		public static string FormatDate(string strDate)
		{
			string strFormatDate = strDate;

			try
			{
				DateTime dt = DateTime.ParseExact(strDate, "yyyy/M/d", null);
				strFormatDate = dt.Year.ToString("0000") + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");	
				return strFormatDate;
			}
			catch
			{
				return strFormatDate;
			}
		}
		#endregion








	}
}
