using System;

using System.Collections;

namespace EPSNet
{
    //public enum SessionID�ySRC-01-02-001�z
    public enum SessionID
	{
		LoginID = 0,
		LanguageID,
		Master_Item_List_SelectedIndex,
		Master_Template_List_SelectedIndex,
		Master_Numbering_List_SelectedIndex,
		Numbering_AutoSendMail,
		Numbering_AutoSendMailTmp,
		Numbering_SendMailUser,
		Obs_Tree_CatTree_SelCatID,	//�I�𒆂̶ú��ID
		Obs_Tree_CatTree_Mode,		//0:�ʏ� 1:�ړ�Ӱ��
		Obs_Tree_ObsTree_SelObsID,	//�I�𒆂̏�QID
		Obs_Tree_ObsTree_Mode,		//0:�ʏ� 1:�ړ�Ӱ��
		Obs_Edit_Link,				//��Q�ݸ���
		Obs_Edit_Lib,				//��Q�Y�ţ�ُ��
		Obs_Edit_RcgUser,			//��Q�ҏW��ʁA���F��ID
		Obs_Analyze_Main_Kind,		//���͏���-�W�v���
		Obs_Analyze_Main_Item,		//���͏���-�W�v����(��ϋ�؂�)
		Obs_Analyze_Main_Unit,		//���͏���-�W�v�P��
		Obs_Analyze_Main_From,		//���͏���-�W�v����From
		Obs_Analyze_Main_To,		//���͏���-�W�v����To
		Common_CSVFileName,			//CSV̧�ٖ�
		Common_CSVData,				//CSV̧�قɏo�͂���÷���ް�
		Obs_PasswordConfirm_Valid,	//��QϽ��Ǘ��߽ܰ�ޓ��͍ς��׸�(true/false)
		
	}
	/// <summary>
	/// CSession �̊T�v�̐����ł��B
	/// </summary>
    //public class CSession�ySRC-01-02-002�z
    public class CSession
	{
		public Hashtable ht = new Hashtable();

		public void Set(SessionID id, object Value)
		{
			if(ht.ContainsKey(id) == false)
			{
				ht.Add(id, Value);
			}
			else
			{
				ht[id] = Value;
			}
		}

		public object Get(SessionID id)
		{
			return ht[id];
		}
	}

	//����Ұّ��M(�̔ԑΏۍ����߰��)�p�׽
    //CAutoSendMail�ySRC-01-02-003�z
    public class CAutoSendMail
	{
		public bool Check1;				//���F��
		public bool Check2;				//�ǉ��E�X�V��
		public bool Check3;				//���F�ҁi���F�ς݁j
		public ArrayList alUser;		//�o�^հ��
		public ArrayList alAddress;		//Ұٱ��ڽ
		public CAutoSendMail()
		{
			this.alUser = new ArrayList();
			this.alAddress = new ArrayList();
		}
	}
}
