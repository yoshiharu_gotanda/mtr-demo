using System;
using System.Data.SqlClient;

using System.Data;
using System.Configuration;
using System.Collections;

namespace EPSNet
{
	/// <summary>
	/// Sql の概要の説明です。
	/// </summary>
    //public enum SessionID【SRC-01-03-001】
    public class CSql : IDisposable
	{
		private const int PK_ERROR = 2627;

		private SqlConnection con;
		private SqlTransaction mTransaction;

        //public CSql【SRC-01-03-002】
        #region public CSql()
		public CSql()
		{
			//Web.Configから情報取得
			string server = ConfigurationSettings.AppSettings["Server"];
			string dataBase = ConfigurationSettings.AppSettings["DataBase"];
			string pwd = ConfigurationSettings.AppSettings["PWD"];
			if(pwd == null)
			{
				//Web.ConfigにSAパスワードが定義されていない場合、固定値を使用する
				pwd = "eps";
			}
			string connectionString = "DATA SOURCE=" + server + ";";
			connectionString += "INITIAL CATALOG=" + dataBase + ";";
			connectionString += "PERSIST SECURITY INFO=FALSE;";
			connectionString += "UID=SA;";
			connectionString += "PWD=" + pwd + ";";
			this.con = new SqlConnection(connectionString);
		}
		#endregion

        //public void Open【SRC-01-03-003】
        #region public void Open()
		public void Open()
		{
			con.Open();
		}
		#endregion

        //public void Close【SRC-01-03-004】
        #region public void Close()
		public void Close()
		{
			con.Close();
		}
		#endregion

        //public void Dispose【SRC-01-03-005】
        #region public void Dispose()
		public void Dispose()
		{
			con.Dispose();
			con = null;
		}
		#endregion

        //public SqlDataReader Read【SRC-01-03-006】
        #region public SqlDataReader Read(string cmdTxt)
		public SqlDataReader Read(string cmdTxt)
		{
			using(SqlCommand com = new SqlCommand(cmdTxt, con))
			{
				if(this.mParams.Count > 0)
				{
					for(int i = 0; i < this.mParams.Count; i++)
					{
						CParam p = (CParam)this.mParams[i];
						com.Parameters.Add(p.ParamName, p.Value);
					}
					this.ClearParam();
				}
				return com.ExecuteReader();
			}
		}
		#endregion

        //public SqlDataReader ReadTrans【SRC-01-03-007】
        #region public SqlDataReader ReadTrans(string cmdTxt)
		public SqlDataReader ReadTrans(string cmdTxt)
		{
			using(SqlCommand com = new SqlCommand(cmdTxt, con, this.mTransaction))
			{
				if(this.mParams.Count > 0)
				{
					for(int i = 0; i < this.mParams.Count; i++)
					{
						CParam p = (CParam)this.mParams[i];
						com.Parameters.Add(p.ParamName, p.Value);
					}
					this.ClearParam();
				}
				return com.ExecuteReader();
			}
		}
		#endregion

        //public int Command【SRC-01-03-008】
        #region public int Command(string cmdTxt)
		public int Command(string cmdTxt)
		{
			try
			{
				using(SqlCommand com = new SqlCommand(cmdTxt, con))
				{
					if(this.mParams.Count > 0)
					{
						for(int i = 0; i < this.mParams.Count; i++)
						{
							CParam p = (CParam)this.mParams[i];
							com.Parameters.Add(p.ParamName, p.Value);
						}
					}
					return  com.ExecuteNonQuery();
				}
			}
			catch(SqlException ex)
			{
				throw new Exception(ex.Number.ToString());
			}
			finally
			{
				this.ClearParam();
			}
		}
		#endregion

        //public int CommandTrans【SRC-01-03-009】
        #region public int CommandTrans(string cmdTxt)
		public int CommandTrans(string cmdTxt)
		{
			try
			{
				using(SqlCommand com = new SqlCommand(cmdTxt, con, this.mTransaction))
				{
					if(this.mParams.Count > 0)
					{
						for(int i = 0; i < this.mParams.Count; i++)
						{
							CParam p = (CParam)this.mParams[i];
							com.Parameters.Add(p.ParamName, p.Value);
						}
					}
					return  com.ExecuteNonQuery();
				}
			}
			catch(SqlException ex)
			{
				switch(ex.Number)
				{
					case PK_ERROR:
					{
						throw new Exception("入力されたデータは既に登録されています");
					}
				}
				throw new Exception(ex.Message);
			}
			finally
			{
				this.ClearParam();
			}
		}
		#endregion

        //public void BeginTrans【SRC-01-03-010】
        #region public void BeginTrans()
		public void BeginTrans()
		{
			this.mTransaction = this.con.BeginTransaction();
		}
		#endregion

        //public void CommitTrans【SRC-01-03-011】
        #region public void CommitTrans()
		public void CommitTrans()
		{
			this.mTransaction.Commit();
		}
		#endregion

        //public void RollbackTrans【SRC-01-03-012】
        #region public void RollbackTrans()
		public void RollbackTrans()
		{
			this.mTransaction.Rollback();
		}
		#endregion

















	}
}
