using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;


namespace EPSNet.Master.Numbering
{
	/// <summary>
	/// Edit の概要の説明です。
	/// </summary>
	public class Edit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonUpdate;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;
		protected System.Web.UI.WebControls.Label LabelItemName;
		protected System.Web.UI.WebControls.TextBox TextBoxItemName;
		protected System.Web.UI.WebControls.Label LabelHelp1;
		protected System.Web.UI.WebControls.Label LabelHelp2;
		protected System.Web.UI.WebControls.Label LabelTemplate;
		protected System.Web.UI.WebControls.DropDownList DropDownListTemplate;
		protected System.Web.UI.WebControls.Label LabelUnit;
		protected System.Web.UI.WebControls.DropDownList DropDownListUnit;
		protected System.Web.UI.WebControls.Label LabelStyle;
		protected System.Web.UI.WebControls.CheckBox CheckBoxReNumbering;
		protected System.Web.UI.WebControls.Label LabelAutoSendMail;
		protected System.Web.UI.WebControls.Label LabelMailTitle;
		protected System.Web.UI.WebControls.DropDownList DropDownListMailTitle;
		protected System.Web.UI.WebControls.TextBox TextBoxMailTitle;
		protected System.Web.UI.WebControls.Label LabelHelp3;
		protected System.Web.UI.WebControls.Label LabelMailSender;
		protected System.Web.UI.WebControls.Label LabelExplain;
		protected System.Web.UI.WebControls.Label LabelNumbering;
		protected System.Web.UI.WebControls.Label LabelEdit;
		protected System.Web.UI.WebControls.TextBox TextBoxExplain;
		protected System.Web.UI.WebControls.TextBox TextBoxFormat;
		protected System.Web.UI.WebControls.CheckBox CheckBoxAutoMail;
		protected System.Web.UI.WebControls.LinkButton LinkButtonMail;
		protected System.Web.UI.WebControls.Button ButtonInsert;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.WebControls.Button ButtonCopy;
		protected System.Web.UI.WebControls.Label LabelHelp4;

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
			this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
			this.DropDownListTemplate.SelectedIndexChanged += new System.EventHandler(this.DropDownListTemplate_SelectedIndexChanged);
			this.DropDownListUnit.SelectedIndexChanged += new System.EventHandler(this.DropDownListUnit_SelectedIndexChanged);
			this.LinkButtonMail.Click += new System.EventHandler(this.LinkButton1_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.ButtonCopy.Click += new System.EventHandler(this.ButtonCopy_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Edit);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		
		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-02-01-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				this.ButtonDelete.Attributes["onclick"] = "return confirm('" + Common.GetText(this.mType, "ConfirmDelete") + "');";
				this.DropDownListMailTitle.Attributes["onchange"] = "TitleChanged();";
				this.ButtonCancel.Attributes["onclick"] = "document.location='./ListFrame.aspx'";

				if(this.IsPostBack == false)
				{
					string id = Request.QueryString["id"];
					string copy = Request.QueryString["Copy"];

					using(CSql sql = new CSql())
					{
						string cmdTxt;
						SqlDataReader dr;
						sql.Open();
						//多国語対応
						#region
						this.LabelNumbering.Text = HttpUtility.HtmlEncode(Common.GetNumberingItemName(sql));
						Common.SetText(sql, this.mType, this.ButtonInsert);
						Common.SetText(sql, this.mType, this.ButtonUpdate);
						Common.SetText(sql, this.mType, this.ButtonCancel);
						Common.SetText(sql, this.mType, this.ButtonDelete);
						Common.SetText(sql, this.mType, this.ButtonCopy);
						Common.SetText(sql, this.mType, this.LinkButtonMail);
						Common.SetText(sql, this.mType, this.LabelHelp1);
						Common.SetText(sql, this.mType, this.LabelHelp2);
						Common.SetText(sql, this.mType, this.LabelHelp3);
						Common.SetText(sql, this.mType, this.LabelHelp4);
						Common.SetText(sql, this.mType, this.LabelItemName);
						Common.SetText(sql, this.mType, this.LabelTemplate);
						Common.SetText(sql, this.mType, this.LabelUnit);
						Common.SetText(sql, this.mType, this.LabelStyle);
						Common.SetText(sql, this.mType, this.LabelAutoSendMail);
						Common.SetText(sql, this.mType, this.LabelMailTitle);
						Common.SetText(sql, this.mType, this.LabelMailSender);
						Common.SetText(sql, this.mType, this.LabelExplain);
						Common.SetText(sql, this.mType, this.CheckBoxReNumbering);
						Common.SetText(sql, this.mType, this.CheckBoxAutoMail);
						Common.SetText(sql, this.mType, this.LabelEdit);						
						#endregion

						//ﾓｰﾄﾞによる機能ﾎﾞﾀﾝ設定
						if(id == null || copy != null)
						{
							//追加ﾓｰﾄﾞ
							this.ButtonInsert.Visible = true;
							this.ButtonUpdate.Visible = false;
							this.ButtonDelete.Visible = false;
							this.ButtonCopy.Visible = false;
						}
						else
						{
							//更新ﾓｰﾄﾞ
							this.ButtonInsert.Visible = false;
							this.ButtonUpdate.Visible = true;
							this.ButtonDelete.Visible = true;
							this.ButtonCopy.Visible = true;

							//障害登録されているﾌﾟﾛｼﾞｪｸﾄは削除不可
							cmdTxt = "SELECT * FROM T_ObsMgr2 WHERE PrjID=@TypCD";
							sql.AddParam("@TypCD", id);
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								this.ButtonDelete.Enabled = false;
							}
							dr.Close();
						}
						//ﾃﾝﾌﾟﾚｰﾄﾘｽﾄ作成
						this.CreateTemplateList(sql);
						//採番単位ﾘｽﾄ作成
						this.CreateNumberingUnitList(sql);
						//ﾒｰﾙﾀｲﾄﾙﾘｽﾄ作成
						this.CreateMailTitle(sql);
						//ﾃﾞｰﾀ表示
						#region
						//更新の場合、既存ﾃﾞｰﾀの表示
						if(id != null)	//更新・ｺﾋﾟｰ
						{
							CAutoSendMail mail = new CAutoSendMail();
							CAutoSendMail mail2 = new CAutoSendMail();
							int send = 0;

							cmdTxt = "SELECT * FROM M_Prj_Typ WHERE TypCD=@TypCD";
							sql.AddParam("@TypCD", id);
							dr = sql.Read(cmdTxt);
							//ｺﾋﾟｰの場合
							if(copy != null)
							{
								//採番項目名に「コピー〜」を付加
								this.TextBoxItemName.Text = Common.GetText(this.mType, "CopyString") + this.TextBoxItemName.Text;
							}
							string title = string.Empty;
							if(dr.Read() == true)
							{
								//項目名
								this.TextBoxItemName.Text = Common.CnvFromDB(dr["Lang0" + Common.LangID.ToString()].ToString());
								if(copy != null) this.TextBoxItemName.Text = Common.GetText(this.mType, "CopyString") + this.TextBoxItemName.Text;
								//ﾃﾝﾌﾟﾚｰﾄ設定
								Common.SetValue(this.DropDownListTemplate, dr["TemplateID"].ToString());
								//管理番号の採番単位
								Common.SetValue(this.DropDownListUnit, dr["Unit"].ToString());
								//管理番号の採番ﾌｫｰﾏｯﾄ
								this.TextBoxFormat.Text = Common.CnvFromDB(dr["Format"].ToString());
								//自動ﾒｰﾙ送信有無
								if(dr["AutoMail"].ToString() == "1")	this.CheckBoxAutoMail.Checked = true;
								//ﾒｰﾙﾀｲﾄﾙ
								title = Common.CnvFromDB(dr["Title"].ToString());
								//説明
								this.TextBoxExplain.Text = Common.CnvFromDB(dr["Exp"].ToString());
								if(dr["Send"] != DBNull.Value) send = Convert.ToInt32(dr["Send"]);
							}
							dr.Close();
							//ﾒｰﾙﾀｲﾄﾙﾘｽﾄ作成
							this.CreateMailTitle(sql);
							//ﾒｰﾙﾀｲﾄﾙ(IDを項目名に変換する)
							for(int i = 1; i < this.DropDownListMailTitle.Items.Count; i++)
							{
								title = title.Replace("[" + this.DropDownListMailTitle.Items[i].Value + "]", "[" + this.DropDownListMailTitle.Items[i].Text + "]");
							}
							this.TextBoxMailTitle.Text = title;


							//自動メール送信情報取得
							if(send >= 1024)
							{
								mail.Check3 = mail2.Check3 = true;
								send -= 1024;
							}
							if(send >= 8)
							{
								mail.Check2 = mail2.Check2 = true;
								send -= 8;
							}
							if(send >= 2)
							{
								mail.Check1 = mail2.Check1 = true;
							}
							//送信者情報取得
							cmdTxt = "SELECT UserID, Address FROM T_AutoMailAddress WHERE CD=@CD";
							sql.AddParam("@CD", id);
							dr = sql.Read(cmdTxt);
							while(dr.Read() == true)
							{
								if(Convert.ToInt32(dr["UserID"]) == 0)
								{
									mail.alAddress.Add(dr["Address"].ToString());
									mail2.alAddress.Add(dr["Address"].ToString());
								}
								else
								{
									mail.alUser.Add(dr["UserID"].ToString());
									mail2.alUser.Add(dr["UserID"].ToString());
								}
							}
							dr.Close();
							//自動ﾒｰﾙ送信情報
							Common.SetSession(SessionID.Numbering_AutoSendMail, mail);
							Common.SetSession(SessionID.Numbering_AutoSendMailTmp, mail2);
						}
						else			//追加
						{
							//全体管理番号の書式を取得
							cmdTxt = "SELECT AllFormat FROM M_Defect";
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								this.TextBoxFormat.Text = dr["AllFormat"].ToString();
							}
							dr.Close();

							//自動ﾒｰﾙ送信情報
							Common.SetSession(SessionID.Numbering_AutoSendMail, new CAutoSendMail());
							Common.SetSession(SessionID.Numbering_AutoSendMailTmp, new CAutoSendMail());
						}
						//採番単位選択によるﾌｫｰﾏｯﾄ入力可否を設定
						if(this.DropDownListUnit.SelectedIndex == 0)	//全体
						{
							this.TextBoxFormat.Enabled = false;
							this.CheckBoxReNumbering.Enabled = false;
						}
						else											//個別
						{
							this.TextBoxFormat.Enabled = true;
							this.CheckBoxReNumbering.Enabled = true;
						}

						#endregion
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 追加ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-002］private void ButtonInsert_Click
        #region private void ButtonInsert_Click(object sender, System.EventArgs e)
		private void ButtonInsert_Click(object sender, System.EventArgs e)
		{
			try
			{
				//管理番号 書式チェック
				//ADD STRAT 問題No.17-------------------------------------------------------------SHIMI 2006/07/14 ADD 
				//☆★☆★まだM_ObsTextにメッセージを追加していません。☆★☆★
				if (!ChkManagementNoFormat()) 
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertManagementNoFormat"));
					return;
				}
				//ADD END 問題No.17\\-------------------------------------------------------------SHIMI 2006/07/14 ADD 

				using(CSql sql = new CSql())
				{
					sql.Open();
					CAutoSendMail mail = (CAutoSendMail)Common.GetSession(SessionID.Numbering_AutoSendMail);
					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}
					string id = Request.QueryString["id"];
					string copy = Request.QueryString["Copy"];
					string cmdTxt;
					SqlDataReader dr;
					sql.BeginTrans();
					try
					{
						//追加処理
						#region
						//名称重複ﾁｪｯｸ
						cmdTxt = "SELECT COUNT(*) AS CNT FROM M_Prj_Typ WHERE Lang0" + Common.LangID.ToString() + "=@Lang";
						sql.AddParam("@Lang", Common.CnvToDB(this.TextBoxItemName.Text));
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							int cnt = Convert.ToInt32(dr["CNT"]);
							if(cnt != 0)
							{
								dr.Close();
								sql.Close();
								sql.Open();
								string msg = Common.GetText(sql, this.mType, "ExistError").Replace("%[Numbering]", Common.GetNumberingItemName(sql));
								Common.Alert(this, msg);
								return;
							}
						}
						dr.Close();

						//TypCDの最大を取得
						string typCD = "P000";
						cmdTxt = "SELECT TOP 1 TypCD FROM M_Prj_Typ ORDER BY TypCD DESC";
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							if(dr["TypCD"] != DBNull.Value)
							{
								typCD = "P" + (Convert.ToInt32(dr["TypCD"].ToString().Substring(1, 3)) + 1).ToString("000");
							}
						}
						dr.Close();

						//追加用SQL文
						cmdTxt = "INSERT INTO M_Prj_Typ(TypCD,Lang01,Lang02,Unit,Format,AutoMail,Title,TemplateID,Exp,Send)";
						cmdTxt += "VALUES(@TypCD,@Lang01,@Lang02,@Unit,@Format,@AutoMail,@Title,@TemplateID,@Exp,@Send)";

						//TypCD設定
						sql.AddParam("@TypCD", typCD);

						//画面から入力されたﾃﾞｰﾀをSQL文に設定
						int send = 0;
						if(mail.Check1 == true) send += 2;
						if(mail.Check2 == true) send += 8;
						if(mail.Check3 == true) send += 1024;
						sql.AddParam("@Lang01", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@Lang02", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@Unit", this.DropDownListUnit.SelectedIndex);
						sql.AddParam("@Format", Common.CnvToDB(this.TextBoxFormat.Text));
						sql.AddParam("@AutoMail", Common.ConvertToString(this.CheckBoxAutoMail.Checked));
						//ﾒｰﾙﾀｲﾄﾙ(項目名をIDに変換する)
						string title = this.TextBoxMailTitle.Text;
						for(int i = 1; i < this.DropDownListMailTitle.Items.Count; i++)
						{
							title = title.Replace("[" + this.DropDownListMailTitle.Items[i].Text + "]", "[" + this.DropDownListMailTitle.Items[i].Value + "]");
						}
						sql.AddParam("@Title", Common.CnvToDB(title));
						sql.AddParam("@TemplateID", this.DropDownListTemplate.SelectedValue);
						sql.AddParam("@Exp", Common.CnvToDB(this.TextBoxExplain.Text));
						sql.AddParam("@Send", send);
						//追加実行
						sql.CommandTrans(cmdTxt);
						#endregion

						//自動ﾃﾞｰﾀ送信者保存
						AutoMailSettings(sql, typCD, mail);

						//管理番号振り直し処理
						if(this.CheckBoxReNumbering.Checked == true)
						{
							RenumberingManageNo(sql, typCD);
						}

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 更新ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-003］private void ButtonUpdate_Click
        #region private void ButtonUpdate_Click(object sender, System.EventArgs e)
		private void ButtonUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				//管理番号 書式チェック
				//ADD STRAT 問題No.17-------------------------------------------------------------SHIMI 2006/07/14 ADD 
				//☆★☆★まだM_ObsTextにメッセージを追加していません。☆★☆★
				if (!ChkManagementNoFormat()) 
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertManagementNoFormat"));
					return;
				}
				//ADD END 問題No.17\\-------------------------------------------------------------SHIMI 2006/07/14 ADD 

				using(CSql sql = new CSql())
				{
					sql.Open();
					CAutoSendMail mail = (CAutoSendMail)Common.GetSession(SessionID.Numbering_AutoSendMail);
					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}
					string id = Request.QueryString["id"];
					string copy = Request.QueryString["Copy"];
					string cmdTxt;
					SqlDataReader dr;
					sql.BeginTrans();
					try
					{
						//更新処理
						#region
						//名称重複ﾁｪｯｸ
						cmdTxt = "SELECT COUNT(*) AS CNT FROM M_Prj_Typ WHERE Lang0" + Common.LangID.ToString() + "=@Lang AND TypCD<>@TypCD";
						sql.AddParam("@Lang", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@TypCD", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							int cnt = Convert.ToInt32(dr["CNT"]);
							if(cnt != 0)
							{
								dr.Close();
								sql.Close();
								sql.Open();
								string msg = Common.GetText(sql, this.mType, "ExistError").Replace("%[Numbering]", Common.GetNumberingItemName(sql));
								Common.Alert(this, msg);
								return;
							}
						}
						dr.Close();

						//更新用SQL文
						cmdTxt = "UPDATE M_Prj_Typ SET";
						cmdTxt += " Lang0" + Common.LangID.ToString() + "=@Lang";
						cmdTxt += ",Unit=@Unit";
						cmdTxt += ",Format=@Format";
						cmdTxt += ",AutoMail=@AutoMail";
						cmdTxt += ",Title=@Title";
						cmdTxt += ",TemplateID=@TemplateID";
						cmdTxt += ",Exp=@Exp";
						cmdTxt += ",Send=@Send";
						cmdTxt += " WHERE TypCD=@TypCD";

						//TypCD設定
						sql.AddParam("@TypCD", id);

						//画面から入力されたﾃﾞｰﾀをSQL文に設定
						int send = 0;
						if(mail.Check1 == true) send += 2;
						if(mail.Check2 == true) send += 8;
						if(mail.Check3 == true) send += 1024;
						sql.AddParam("@Lang", Common.CnvToDB(this.TextBoxItemName.Text));
						sql.AddParam("@Unit", this.DropDownListUnit.SelectedIndex);
						sql.AddParam("@Format", Common.CnvToDB(this.TextBoxFormat.Text));
						sql.AddParam("@AutoMail", Common.ConvertToString(this.CheckBoxAutoMail.Checked));
						//ﾒｰﾙﾀｲﾄﾙ(項目名をIDに変換する)
						string title = this.TextBoxMailTitle.Text;
						for(int i = 1; i < this.DropDownListMailTitle.Items.Count; i++)
						{
							title = title.Replace("[" + this.DropDownListMailTitle.Items[i].Text + "]", "[" + this.DropDownListMailTitle.Items[i].Value + "]");
						}
						sql.AddParam("@Title", Common.CnvToDB(title));
						//sql.AddParam("@Title", Common.CnvToDB(this.TextBoxMailTitle.Text));
						sql.AddParam("@TemplateID", this.DropDownListTemplate.SelectedValue);
						sql.AddParam("@Exp", Common.CnvToDB(this.TextBoxExplain.Text));
						sql.AddParam("@Send", send);
						//更新実行
						sql.CommandTrans(cmdTxt);
						#endregion

						//この採番対象項目を使用している障害について、不要なﾃﾞｰﾀを削除する
						#region
						//この採番対象項目を使用している障害IDを取得
						cmdTxt = "SELECT ObsMgrID FROM T_ObsMgr2 WHERE PrjID=@PrjID";
						sql.AddParam("@PrjID", id);
						ArrayList alObs = new ArrayList();
						dr = sql.ReadTrans(cmdTxt);
						while(dr.Read() == true)
						{
							alObs.Add(dr["ObsMgrID"]);
						}
						dr.Close();
						//不要ﾃﾞｰﾀ削除
						for(int i = 0; i < alObs.Count; i++)
						{
							Common.DeleteNoUseItemData(sql, alObs[i].ToString());
						}
						#endregion

						//自動ﾃﾞｰﾀ送信者保存
						AutoMailSettings(sql, id, mail);

						//管理番号振り直し処理
						if(this.CheckBoxReNumbering.Checked == true)
						{
							RenumberingManageNo(sql, id);
						}

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ｷｬﾝｾﾙﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-004］private void ButtonCancel_Click
        #region private void ButtonCancel_Click(object sender, System.EventArgs e)
		private void ButtonCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./ListFrame.aspx");
		}
		#endregion

		/// <summary>
		/// 削除ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-005］private void ButtonDelete_Click
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			string id = Request.QueryString["id"];
			if(id == null) return;

			try
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					cmdTxt = "DELETE FROM M_Prj_Typ WHERE TypCD=@id";
					sql.AddParam("@id", id);
					sql.Command(cmdTxt);
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ｺﾋﾟｰﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-006］private void ButtonCopy_Click
        #region private void ButtonCopy_Click(object sender, System.EventArgs e)
		private void ButtonCopy_Click(object sender, System.EventArgs e)
		{
			string id = Request.QueryString["id"];
			if(id == null) return;

			//自分自身をｺﾋﾟｰﾓｰﾄﾞで表示し直す
			Response.Redirect("./Edit.aspx?id=" + id + "&Copy=1");
		}
		#endregion

		/// <summary>
		/// 使用ﾃﾝﾌﾟﾚｰﾄ変更ｲﾍﾞﾝﾄ
		/// </summary>
        //［SRC-02-02-01-007］private void DropDownListTemplate_SelectedIndexChangedOnInit
        #region private void DropDownListTemplate_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DropDownListTemplate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			using(CSql sql = new CSql())
			{
				sql.Open();
				//使用しているﾀｸﾞ取得
				ArrayList al = Common.GetTag(this.TextBoxMailTitle.Text.Trim());
				this.CreateMailTitle(sql);
				//使用しているﾀｸﾞが一覧から削除されたら、ﾒｰﾙﾀｲﾄﾙからも削除する
				for(int i = 0; i < al.Count; i++)
				{
					string str = al[i].ToString();
					bool exist = false;
					for(int j = 0; j < this.DropDownListMailTitle.Items.Count; j++)
					{
						if(str == "%[" + this.DropDownListMailTitle.Items[j].Value + "]")
						{
							exist = true;
							break;
						}
					}
					if(exist == false) this.TextBoxMailTitle.Text = this.TextBoxMailTitle.Text.Replace(str, string.Empty);
				}
			}
		}
		#endregion

		/// <summary>
		/// 採番単位変更ｲﾍﾞﾝﾄ
		/// </summary>
        //［SRC-02-02-01-008］private void DropDownListUnit_SelectedIndexChanged
        #region private void DropDownListUnit_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DropDownListUnit_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				if(this.DropDownListUnit.SelectedIndex == 0)	//全体
				{
					//全体管理番号の書式を取得
					cmdTxt = "SELECT AllFormat FROM M_Defect";
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true)
					{
						this.TextBoxFormat.Text = dr["AllFormat"].ToString();
					}
					dr.Close();
					this.TextBoxFormat.Enabled = false;
					this.CheckBoxReNumbering.Checked = false;
					this.CheckBoxReNumbering.Enabled = false;
				}
				else											//個別
				{
					//個別管理番号の書式を取得
					cmdTxt = "SELECT IndividualFormat FROM M_Defect";
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true)
					{
						this.TextBoxFormat.Text = dr["IndividualFormat"].ToString();
					}
					dr.Close();
					this.TextBoxFormat.Enabled = true;
					this.CheckBoxReNumbering.Enabled = true;
				}
			}
		}
		#endregion

		/// <summary>
		/// ﾒｰﾙ送信者設定ﾘﾝｸｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-01-009］private void LinkButton1_Click
        #region private void LinkButton1_Click(object sender, System.EventArgs e)
		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			CAutoSendMail mail = (CAutoSendMail)Common.GetSession(SessionID.Numbering_AutoSendMail);
			CAutoSendMail copy = new CAutoSendMail();
			copy.Check1 = mail.Check1;
			copy.Check2 = mail.Check2;
			copy.Check3 = mail.Check3;
			for(int i = 0; i < mail.alUser.Count; i++)
			{
				copy.alUser.Add(mail.alUser[i]);
			}
			for(int i = 0; i < mail.alAddress.Count; i++)
			{
				copy.alAddress.Add(mail.alAddress[i]);
			}

			Common.SetSession(SessionID.Numbering_AutoSendMailTmp, copy);
			Common.OutputScript(this, "OpenWindow();");
//			this.RegisterClientScriptBlock("script", "<script>OpenWindow();</script>");
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// ﾃﾝﾌﾟﾚｰﾄﾘｽﾄ作成
		/// </summary>
		/// <param name="sql"></param>
        //［SRC-02-02-01-010］private void CreateTemplateList
        #region private void CreateTemplateList(CSql sql)
		private void CreateTemplateList(CSql sql)
		{
			string cmdTxt;
			SqlDataReader dr;
			this.DropDownListTemplate.Items.Clear();
			cmdTxt = "SELECT TemplateID, Text" + Common.LangID.ToString() + " FROM M_ObsTemplate ORDER BY TemplateID";
			dr = sql.Read(cmdTxt);
			while(dr.Read() == true)
			{
				this.DropDownListTemplate.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
			}
			dr.Close();
		}
		#endregion

		/// <summary>
		/// 採番単位ﾘｽﾄ作成
		/// </summary>
		/// <param name="sql"></param>
        //［SRC-02-02-01-011］private void CreateNumberingUnitList
        #region private void CreateNumberingUnitList(CSql sql)
		private void CreateNumberingUnitList(CSql sql)
		{
			this.DropDownListUnit.Items.Clear();
			this.DropDownListUnit.Items.Add(new ListItem(Common.GetText(sql, this.mType, "NumberingUnit1"), "0"));
			this.DropDownListUnit.Items.Add(new ListItem(Common.GetText(sql, this.mType, "NumberingUnit2"), "1"));
		}
		#endregion

		/// <summary>
		/// ﾒｰﾙﾀｲﾄﾙﾘｽﾄ作成
		/// </summary>
		/// <param name="sql"></param>
        //［SRC-02-02-01-012］private void CreateMailTitle
        #region private void CreateMailTitle(CSql sql)
		private void CreateMailTitle(CSql sql)
		{
			this.DropDownListMailTitle.Items.Clear();
			this.DropDownListMailTitle.Items.Add(string.Empty);

			//ADD STRAT 問題No.17--------------------------------------------------------------SHIMI 2006/07/18 ADD 
			string cmdTxt = string.Empty;
			SqlDataReader dr;

			//採番対象項目名のリストへのセット
			switch(Common.LangID)
			{
				case 1: cmdTxt = "SELECT Japanese FROM M_ObsItem WHERE ItemID = 0"; break;
				case 2: cmdTxt = "SELECT English  FROM M_ObsItem WHERE ItemID = 0"; break;
			}
			dr = sql.Read(cmdTxt);
			dr.Read();
			this.DropDownListMailTitle.Items.Add(new ListItem(dr[0].ToString(),"0"));
			dr.Close();
			//ADD END 問題No.17------------------------------------------------------------------SHIMI 2006/07/18 ADD 

			this.DropDownListMailTitle.Items.Add(new ListItem(Common.GetText(sql, this.mType, "MailTitle1"), "F1"));
			this.DropDownListMailTitle.Items.Add(new ListItem(Common.GetText(sql, this.mType, "MailTitle2"), "F2"));

			//DEL STRAT 問題No.17--------------------------------------------------------------SHIMI 2006/07/18 ADD 
			//string cmdTxt = string.Empty;
			//SqlDataReader dr;
			//DEL END 問題No.17------------------------------------------------------------------SHIMI 2006/07/18 ADD 

			//指定ﾃﾝﾌﾟﾚｰﾄで使用している項目を取得
			ArrayList alItemID = new ArrayList();
			ArrayList alItemName = new ArrayList();
			switch(Common.LangID)
			{
				case 1: cmdTxt = "SELECT M.Japanese, M.ItemID FROM M_ObsTemplateItem AS I"; break;
				case 2: cmdTxt = "SELECT M.English, M.ItemID FROM M_ObsTemplateItem AS I"; break;
			}
			cmdTxt += " INNER JOIN M_ObsItem AS M ON M.ItemID = I.ItemID AND (M.DataType=2 OR M.DataType=6 OR M.DataType=7)";
			cmdTxt += " WHERE I.TemplateID=@TemplateID ORDER BY I.SortID";
			sql.AddParam("@TemplateID", this.DropDownListTemplate.SelectedValue);
			dr = sql.Read(cmdTxt);
			//M_ObsItem
			while(dr.Read() == true)
			{
				this.DropDownListMailTitle.Items.Add(new ListItem(dr[0].ToString(), dr["ItemID"].ToString()));
			}
			dr.Close();

			this.DropDownListMailTitle.Items.Add(new ListItem(Common.GetText(sql, this.mType, "MailTitle3"), "F3"));
		}
		#endregion

		/// <summary>
		/// 入力ﾁｪｯｸ
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
        //［SRC-02-02-01-013］private bool InputCheck
        #region private bool InputCheck(CSql sql)
		private bool InputCheck(CSql sql)
		{
			//項目名入力ﾁｪｯｸ
			if(Common.CnvToDB(this.TextBoxItemName.Text) == string.Empty)
			{
				Common.Alert(this, Common.GetText(sql, this.mType, "AlertNameInput"));
				return false;
			}
			//管理番号の書式
			if(Common.CnvToDB(this.TextBoxFormat.Text) == string.Empty)
			{
				Common.Alert(this, Common.GetText(sql, this.mType, "AlertFormatInput"));
				return false;
			}
			//説明ﾊﾞｲﾄ数ﾁｪｯｸ
			int cnt = Common.UserEncode.GetByteCount(this.TextBoxExplain.Text);
			if(cnt > 2000)
			{
				Common.Alert(this, Common.GetText(sql, this.mType, "AlertExplain"));
				return false;
			}
			return true;
		}
		#endregion

		/// <summary>
		/// 自動ﾃﾞｰﾀ送信者保存
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="id"></param>
        //［SRC-02-02-01-014］private void AutoMailSettings
        #region private void AutoMailSettings(CSql sql, string id, CAutoSendMail mail)
		private void AutoMailSettings(CSql sql, string id, CAutoSendMail mail)
		{
			SqlDataReader dr;
			string cmdTxt;

			//最大MailID取得
			int mailId = 0;
			cmdTxt = "SELECT MAX(MailID) AS MailID FROM T_AutoMailAddress";
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true && dr["MailID"] != DBNull.Value) mailId = Convert.ToInt32(dr["MailID"]) + 1;
			dr.Close();
			if(id != null)
			{
				//既存ﾚｺｰﾄﾞ削除
				cmdTxt = "DELETE FROM T_AutoMailAddress WHERE CD=@CD";
				sql.AddParam("@CD", id);
				sql.CommandTrans(cmdTxt);
			}
			//登録ﾕｰｻﾞ
			for(int i = 0; i < mail.alUser.Count; i++)
			{
				cmdTxt = "INSERT INTO T_AutoMailAddress(MailID,ID,CD,Type,UserID,Address,FileID,Send)";
				cmdTxt += "VALUES(@MailID,@ID,@CD,@Type,@UserID,@Address,@FileID,@Send)";
				sql.AddParam("@MailID", mailId);
				sql.AddParam("@ID", 0);
				sql.AddParam("@CD", id);
				sql.AddParam("@Type", "111");
				sql.AddParam("@UserID", mail.alUser[i].ToString());
				sql.AddParam("@Address", string.Empty);
				sql.AddParam("@FileID", 0);
				sql.AddParam("@Send", 0);
				sql.CommandTrans(cmdTxt);
				mailId++;
			}
			//ﾒｰﾙｱﾄﾞﾚｽ
			for(int i = 0; i < mail.alAddress.Count; i++)
			{
				cmdTxt = "INSERT INTO T_AutoMailAddress(MailID,ID,CD,Type,UserID,Address,FileID,Send)";
				cmdTxt += "VALUES(@MailID,@ID,@CD,@Type,@UserID,@Address,@FileID,@Send)";
				sql.AddParam("@MailID", mailId);
				sql.AddParam("@ID", 0);
				sql.AddParam("@CD", id);
				sql.AddParam("@Type", "112");
				sql.AddParam("@UserID", string.Empty);
				sql.AddParam("@Address", mail.alAddress[i].ToString());
				sql.AddParam("@FileID", 0);
				sql.AddParam("@Send", 0);
				sql.CommandTrans(cmdTxt);
				mailId++;
			}
		}
		#endregion

		/// <summary>
		/// 管理番号振り直し
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="id"></param>
        //［SRC-02-02-01-015］private void RenumberingManageNo
        #region private void RenumberingManageNo(CSql sql, string id)
		private void RenumberingManageNo(CSql sql, string id)
		{
			SqlDataReader dr;

			using(CSql sqlRead = new CSql())
			{
				sqlRead.Open();
				int seqNum = 0;
				string cmdTxt;
				if(this.DropDownListUnit.SelectedIndex == 0)
				{
					//全体
					#region
					//全体での管理番号作成のための情報を取得する
					string format = "%n";
					int defectID = 0;
					#region
					cmdTxt = "SELECT ID, SeqNum, AllFormat FROM M_Defect";
					dr = sqlRead.Read(cmdTxt);
					if(dr.Read() == true)
					{
						seqNum = Convert.ToInt32(dr["SeqNum"]) + 1;
						format = dr["AllFormat"].ToString();
						defectID = Convert.ToInt32(dr["ID"]);
					}
					dr.Close();
					#endregion
								
					//管理番号更新
					cmdTxt = "SELECT ObsMgrID FROM T_ObsMgr2 WHERE PrjID=@TypCD ORDER BY EntDt";
					sqlRead.AddParam("@TypCD", id);
					dr = sqlRead.Read(cmdTxt);
					while(dr.Read() == true)
					{
						seqNum++;
						cmdTxt = "UPDATE T_ObsMgr2 SET MgrNo=@MgrNo WHERE ObsMgrID=@ObsMgrID";
						sql.AddParam("@MgrNo", Common.CreateMngNo(this.TextBoxItemName.Text.Trim(), format, seqNum));
						sql.AddParam("@ObsMgrID", dr["ObsMgrID"].ToString());
						sql.CommandTrans(cmdTxt);
					}
					dr.Close();
					//全体でのｼｰｹﾝｽ番号を更新
					cmdTxt = "UPDATE M_Defect SET SeqNum=@SeqNum WHERE ID=@ID";
					sql.AddParam("@SeqNum", seqNum);
					sql.AddParam("@ID", defectID);
					sql.CommandTrans(cmdTxt);
					#endregion
				}
				else
				{
					//個別
					#region
					cmdTxt = "SELECT ObsMgrID FROM T_ObsMgr2 WHERE PrjID=@TypCD ORDER BY EntDt";
					sqlRead.AddParam("@TypCD", id);
					dr = sqlRead.Read(cmdTxt);
					while(dr.Read() == true)
					{
						seqNum++;
						cmdTxt = "UPDATE T_ObsMgr2 SET MgrNo=@MgrNo WHERE ObsMgrID=@ObsMgrID";
						sql.AddParam("@MgrNo", Common.CreateMngNo(this.TextBoxItemName.Text.Trim(), this.TextBoxFormat.Text.Trim(), seqNum));
						sql.AddParam("@ObsMgrID", dr["ObsMgrID"].ToString());
						sql.CommandTrans(cmdTxt);
					}
					dr.Close();
					//seqNum更新
					cmdTxt = "UPDATE M_Prj_Typ SET SeqNum=@SeqNum WHERE TypCD=@TypCD";
					sql.AddParam("@SeqNum", seqNum);
					sql.AddParam("@TypCD", id);
					sql.CommandTrans(cmdTxt);
					#endregion
				}
			}
		}
		#endregion


        //［SRC-02-02-01-016］private bool ChkManagementNoFormat
        #region 採番管理番号　書式チェック
		//ADD STRAT 問題No.14-------------------------------------------------------------SHIMI 2006/07/14 ADD 
		private bool ChkManagementNoFormat()
		{

			if ( this.TextBoxFormat.Text == "" ) 
			{
				return false;
			}

			string strFormat = this.TextBoxFormat.Text;

			int		i;
			string	ch = string.Empty;
			int		max = strFormat.Length;

			for ( i = 0; i < max; i++ ) 
			{
				ch = strFormat.Substring( i, 1 );
				if ( ch == "%" ) 
				{
					i++;
					if ( i < max ) 
					{
						ch = strFormat.Substring( i, 1 );
						if ( ch == "n" ) 
						{
							break;
						} 
						else if ( ch == "0" ) 
						{
							int count = 0;

							for ( i++; i < max; i++ ) 
							{
								ch = strFormat.Substring( i, 1 );
								if ( ch == "n" ) 
								{
									break;
								}

								int j;
								string strIndex = string.Empty;

								for (j = 0; j <=9; j++)
								{
									strIndex = j.ToString();
									
									if (ch == strIndex)
									{
										break;
									}
								}

								if (j <= 9)
								{
									count++;
								} 
								else 
								{
									break;
								}

							}
							if ( i < max && count > 0 && ch == "n" ) 
							{
								break;
							}
						}
					}
				}
			}

			if ( i == max || ch != "n" ) 
			{
				return false;
			}

			return true;

		}
		//ADD END 問題No.14------------------------------------------------------------------SHIMI 2006/07/14 ADD 
		#endregion









	}
}
