using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Master.Numbering
{
	/// <summary>
	/// List の概要の説明です。
	/// </summary>
	public class List : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelNumberingItemName;
		protected System.Web.UI.WebControls.Label LabelTitle2;
		protected System.Web.UI.WebControls.Label LabelTitle1;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;

        #region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
        //［SRC-02-02-02-001］InitializeComponent
        private void InitializeComponent()
		{    
			this.DataGrid1.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_ItemCommand);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================

		private const int COL_TypCD = 3;
		private const int COL_Cnt = 4;


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(List);
		private string mObsCntUnit = string.Empty;


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾌｫｰﾑﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-02-02-002］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ﾃｷｽﾄ取得
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					Common.SetText(sql, this.mType, this.LabelTitle1);
					Common.SetText(sql, this.mType, this.LabelTitle2);
					string num = HttpUtility.HtmlEncode(Common.GetNumberingItemName(sql));
					this.DataGrid1.Columns[0].HeaderText = num;
					this.DataGrid1.Columns[1].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGrid1.Columns[2].HeaderText = Common.GetText(sql, this.mType, "Col3");
					this.LabelNumberingItemName.Text = num;
					//「件」文字列取得
					this.mObsCntUnit = Common.GetText(sql, this.mType, "ObsCntUnit");
				}
				this.CreateList();
				//行が選択された場合
				#region
//				if(Common.GetSession(SessionID.Master_Item_List_SelectedIndex) != null)
//				{
//					int index = -1;
//					index = Convert.ToInt32(Common.GetSession(SessionID.Master_Numbering_List_SelectedIndex));
//					if(this.DataGrid1.Items.Count <= index) return;
//					this.DataGrid1.SelectedIndex = index;
//				}
				#endregion
			}
		}
		#endregion

		/// <summary>
		/// 行選択ｲﾍﾞﾝﾄ
		/// </summary>
        //［SRC-02-02-02-003］private void DataGrid1_SelectedIndexChanged
        #region private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.DataGrid1.SelectedIndex == -1)
			{
				Response.Write("<script>");
				Response.Write("parent.frames['button'].document.Form1.ButtonEdit.disabled=true;");
				Response.Write("parent.frames['button'].document.Form1.ButtonDelete.disabled=true;");
				Response.Write("parent.frames['button'].document.Form1.ButtonCopy.disabled=true;");
				Response.Write("</script>");
			}
			else
			{
				Response.Write("<script>");
				Response.Write("parent.frames['button'].document.Form1.ButtonEdit.disabled=false;");
				Response.Write("parent.frames['button'].document.Form1.ButtonDelete.disabled=false;");
				Response.Write("parent.frames['button'].document.Form1.ButtonCopy.disabled=false;");
				Response.Write("</script>");
			}
		}
		#endregion

		/// <summary>
		/// 項目名ﾘﾝｸｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-02-004]private void DataGrid1_ItemCommand
        #region private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			Common.SetSession(SessionID.Master_Template_List_SelectedIndex, e.Item.ItemIndex.ToString());

			string id = e.Item.Cells[COL_TypCD].Text;
			Response.Write("<script>");
			Response.Write("parent.document.location='./Edit.aspx?id=" + id + "';");
			Response.Write("</script>");
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 項目一覧作成
		/// </summary>
        //［SRC-02-02-02-005］private void CreateList
        #region private void CreateList()
		private void CreateList()
		{
			DataTable table = new DataTable();
			table.Columns.Add("Name");
			table.Columns.Add("TemplateName");
			table.Columns.Add("CntStr");
			table.Columns.Add("TypCD");
			table.Columns.Add("Cnt");
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				//採番対象項目一覧取得
				ArrayList alTypCD = new ArrayList();
				ArrayList alText = new ArrayList();
				ArrayList alTemplateName = new ArrayList();
				ArrayList alObsCount = new ArrayList();

				cmdTxt = "SELECT N.TypCD, N.Lang0" + Common.LangID.ToString() + ", T.Text" + Common.LangID.ToString() + " FROM M_Prj_Typ AS N";
				cmdTxt += " LEFT JOIN M_ObsTemplate AS T ON T.TemplateID=N.TemplateID";
				cmdTxt += " ORDER BY N.TypCD";
				dr = sql.Read(cmdTxt);
				while(dr.Read() == true)
				{
					alTypCD.Add(dr[0]);
					alText.Add(dr[1]);
					alTemplateName.Add(dr[2]);
				}
				dr.Close();
				//件数取得
				for(int i = 0; i < alTypCD.Count; i++)
				{
					int cnt = 0;
					cmdTxt = "SELECT COUNT(*) AS CNT FROM T_ObsMgr2 WHERE PrjID=@TypCD";
					sql.AddParam("@TypCD", alTypCD[i]);
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true && dr[0] != DBNull.Value) cnt = Convert.ToInt32(dr[0]);
					dr.Close();
					//行追加
					this.AddRow(table, alText[i], alTemplateName[i], cnt, alTypCD[i]);
				}
				dr.Close();
			}
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();

			this.DataGrid1.SelectedIndex = -1;
		}
		#endregion

		/// <summary>
		/// 行追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="Name"></param>
		/// <param name="TemplateName"></param>
		/// <param name="Cnt"></param>
		/// <param name="TypCD"></param>
        //［SRC-02-02-02-006］private void AddRow
        #region private void AddRow(DataTable table, object Name, object TemplateName, object Cnt, object TypCD)
		private void AddRow(DataTable table, object Name, object TemplateName, object Cnt, object TypCD)
		{
			DataRow row = table.NewRow();
			row["Name"] = HttpUtility.HtmlEncode(Name.ToString());
			row["TemplateName"] = HttpUtility.HtmlEncode(TemplateName.ToString());
			row["CntStr"] = Cnt.ToString() + this.mObsCntUnit;
			row["TypCD"] = TypCD.ToString();
			row["Cnt"] = Cnt;
			table.Rows.Add(row);
		}
		#endregion

















	}
}
