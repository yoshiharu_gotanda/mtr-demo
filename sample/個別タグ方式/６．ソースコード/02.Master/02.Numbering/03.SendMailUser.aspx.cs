using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Master.Numbering
{
	/// <summary>
	/// SendMailUser の概要の説明です。
	/// </summary>
	public class SendMailUser : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonOK;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelMailSender;
		protected System.Web.UI.WebControls.ListBox ListBoxMailSender;
		protected System.Web.UI.WebControls.Label LabelUser;
		protected System.Web.UI.WebControls.ListBox ListBoxUser;
		protected System.Web.UI.WebControls.Label LabelSect;
		protected System.Web.UI.WebControls.ListBox ListBoxSect;
		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.ListBoxSect.SelectedIndexChanged += new System.EventHandler(this.ListBoxSect_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(SendMailUser);


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄｸﾗｽ
		///================================================================

		/// <summary>
		/// M_Sectの情報格納用
		/// </summary>
        //［SRC-02-02-03-001］private class CSect
        #region private class CSect
		private class CSect
		{
			public string Sect;
			public string SectName;
			public string AbbrName;
			public int SectLevel;
			public string[] INC = new string[4];
			public CSect(object sect, object sectName, object abbrName, object sectLevel, object inc1, object inc2, object inc3, object inc4)
			{
				this.Sect = sect.ToString();
				this.SectName = sectName.ToString();
				this.AbbrName = abbrName.ToString();
				this.SectLevel = Convert.ToInt32(sectLevel) - 1;
				this.INC[0] = inc1.ToString();
				this.INC[1] = inc2.ToString();
				this.INC[2] = inc3.ToString();
				this.INC[3] = inc4.ToString();
			}
		}
		#endregion


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-02-03-002］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					sql.Open();

					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelMailSender);
					Common.SetText(sql, this.mType, this.LabelSect);
					Common.SetText(sql, this.mType, this.LabelUser);
					Common.SetText(sql, this.mType, this.ButtonOK);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					#endregion

					//部署一覧作成
					Common.CreateSectList(this, this.ListBoxSect);

					//ﾕｰｻﾞが削除されていた場合はどうなるのか？

					CAutoSendMail mail = (CAutoSendMail)Common.GetSession(SessionID.Numbering_AutoSendMailTmp);
					Common.SetSession(SessionID.Numbering_SendMailUser, mail.alUser.Clone());

					this.CreateList();
				}
			}
		}
		#endregion

		/// <summary>
		/// 選択部署変更
		/// </summary>
        //［SRC-02-02-03-003］private void ListBoxSect_SelectedIndexChanged
        #region private void ListBoxSect_SelectedIndexChanged(object sender, System.EventArgs e)
		private void ListBoxSect_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.CreateList();
		}
		#endregion

		/// <summary>
		/// 「＜」ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-03-004］private void ButtonAdd_Click
        #region private void ButtonAdd_Click(object sender, System.EventArgs e)
		private void ButtonAdd_Click(object sender, System.EventArgs e)
		{
			ArrayList alSendUsers = (ArrayList)Common.GetSession(SessionID.Numbering_SendMailUser);
			for(int i = 0; i < this.ListBoxUser.Items.Count; i++)
			{
				if(this.ListBoxUser.Items[i].Selected == true)
				{
					string[] users = this.ListBoxUser.Items[i].Value.Split(',');
					for(int j = 0; j < users.Length; j++)
					{
						int index = alSendUsers.IndexOf(users[j]);
						if(index < 0) alSendUsers.Add(users[j]);
					}
				}
			}
			Common.SetSession(SessionID.Numbering_SendMailUser, alSendUsers);
			this.CreateList();				
		}
		#endregion

		/// <summary>
		/// 「＞」ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-03-005］private void ButtonDelete_Click
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			ArrayList alMailSender = (ArrayList)Common.GetSession(SessionID.Numbering_SendMailUser);
			for(int i = 0; i < this.ListBoxMailSender.Items.Count; i++)
			{
				if(this.ListBoxMailSender.Items[i].Selected == true)
				{
					string[] users = this.ListBoxMailSender.Items[i].Value.Split(',');
					for(int j = 0; j < users.Length; j++)
					{
						alMailSender.Remove(users[j]);
					}
				}
			}
			Common.SetSession(SessionID.Numbering_SendMailUser, alMailSender);
			this.CreateList();
		}
		#endregion

		/// <summary>
		/// OKﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-02-02-03-006］private void ButtonOK_Click
        #region private void ButtonOK_Click(object sender, System.EventArgs e)
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			ArrayList al = (ArrayList)Common.GetSession(SessionID.Numbering_SendMailUser);
			CAutoSendMail mail = (CAutoSendMail)Common.GetSession(SessionID.Numbering_AutoSendMailTmp);
			mail.alUser.Clear();
			mail.alUser = (ArrayList)al.Clone();
			Common.SetSession(SessionID.Numbering_AutoSendMailTmp, mail);
			Response.Redirect("./SendMailMenu.aspx");
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

        //［SRC-02-02-03-007］private void CreateList
        #region private void CreateList()
		private void CreateList()
		{
			this.ListBoxUser.Items.Clear();
			this.ListBoxMailSender.Items.Clear();
			
			ArrayList alSendUsers = (ArrayList)Common.GetSession(SessionID.Numbering_SendMailUser);

			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();

				#region ﾕｰｻﾞ一覧作成

				for(int i = 0; i < this.ListBoxSect.Items.Count; i++)
				{
					if(this.ListBoxSect.Items[i].Selected == true)
					{
						string sect = this.ListBoxSect.Items[i].Value;
						cmdTxt = "SELECT UserID, FullName FROM M_users WHERE SECT=@SECT ORDER BY UserID";
						sql.AddParam("@SECT", sect);
						dr = sql.Read(cmdTxt);
						ArrayList alFullName = new ArrayList();
						ArrayList alUserID = new ArrayList();
						while(dr.Read() == true)
						{
							alFullName.Add(dr["FullName"]);
							alUserID.Add(dr["UserID"]);
						}
						dr.Close();
						//部署にﾕｰｻﾞがいなければﾕｰｻﾞ一覧には部署も表示しない
						if(alUserID.Count == 0) continue;
						//部署に属する全ﾕｰｻﾞIDを取得する
						string users = string.Empty;
						for(int j = 0; j < alUserID.Count; j++)
						{
							int index = alSendUsers.IndexOf(alUserID[j].ToString());//送信対象ﾕｰｻﾞに含まれているかﾁｪｯｸ
							if(index < 0)
							{
								if(users != string.Empty) users += ",";
								users += alUserID[j].ToString();
							}
						}
						//全員登録済みのﾕｰｻﾞは部署も表示しない
						if(users == string.Empty) continue;
						//ｾｸｼｮﾝ名表示
						#region
						cmdTxt = "SELECT SectName FROM M_SECT WHERE Sect=@Sect";
						sql.AddParam("@Sect", this.ListBoxSect.Items[i].Value);
						dr = sql.Read(cmdTxt);
						string sectName = string.Empty;
						if(dr.Read() == true) sectName = dr["SectName"].ToString();
						dr.Close();
						#endregion
						if(users != string.Empty)
						{
							this.ListBoxUser.Items.Add(new ListItem(sectName, users));
						}
						//ﾕｰｻﾞ情報追加
						for(int j = 0; j < alUserID.Count; j++)
						{
							int index = alSendUsers.IndexOf(alUserID[j].ToString());//送信対象ﾕｰｻﾞに含まれているかﾁｪｯｸ
							if(index < 0)
							{
								this.ListBoxUser.Items.Add(new ListItem("　" + alFullName[j].ToString(), alUserID[j].ToString()));
							}
						}
					}
				}

				#endregion

				#region 送信ﾕｰｻﾞ一覧作成

				if(alSendUsers.Count > 0)
				{
					cmdTxt = "SELECT U.Sect, U.UserID, U.FullName, S.SectName FROM M_users AS U LEFT JOIN M_SECT AS S ON U.Sect=S.Sect WHERE";
					for(int i = 0; i < alSendUsers.Count; i++)
					{
						if(i != 0)  cmdTxt += " OR";
						cmdTxt += " U.UserID='" + alSendUsers[i].ToString() + "'";
					}
					cmdTxt += " ORDER BY U.SECT, U.UserID";
					dr = sql.Read(cmdTxt);
					ArrayList alFullName = new ArrayList();
					ArrayList alUserID = new ArrayList();
					ArrayList alSect = new ArrayList();
					ArrayList alSectName = new ArrayList();
					while(dr.Read() == true)
					{
						alFullName.Add(dr["FullName"].ToString());
						alUserID.Add(dr["UserID"].ToString());
						alSect.Add(dr["Sect"].ToString());
						alSectName.Add(dr["SectName"].ToString());
					}
					dr.Close();
					string prevSect = string.Empty;
					for(int i = 0; i < alUserID.Count; i++)
					{
						string sect = alSect[i].ToString();
						//部署の切替わり
						if(sect != prevSect)
						{
							string users = string.Empty;
							for(int j = 0; j < alUserID.Count; j++)
							{
								if(sect == alSect[j].ToString())
								{
									if(users != string.Empty) users += ",";
									users += alUserID[j].ToString();
								}
							}
							this.ListBoxMailSender.Items.Add(new ListItem(alSectName[i].ToString(), users));
						}
						this.ListBoxMailSender.Items.Add(new ListItem("　" + alFullName[i].ToString(), alUserID[i].ToString()));
						prevSect = sect;
					}
				}

				#endregion
			}
		}
		#endregion





















































	}
}
