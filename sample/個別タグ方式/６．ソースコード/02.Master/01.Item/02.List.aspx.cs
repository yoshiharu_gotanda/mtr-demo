using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Master.Item
{
	/// <summary>
	/// WebFormKoumokuMaster の概要の説明です。
	/// </summary>
	public class List : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label LabelTitle;
		
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.DataGrid1.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_ItemCommand);
			this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================

		private const int COL_ItemID = 3;
		private const int COL_Used = 4;


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(List);
		private string mNumberingItemName;


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾌｫｰﾑﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-01-02-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ﾃｷｽﾄ取得
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					Common.SetText(sql, this.mType, this.LabelTitle);
					this.DataGrid1.Columns[0].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGrid1.Columns[1].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGrid1.Columns[2].HeaderText = Common.GetText(sql, this.mType, "Col3");
					//(採番対象項目)文字列取得
					this.mNumberingItemName = Common.GetText(sql, this.mType, "NumberingItem");
				}
				this.CreateList();
				//行が選択された場合
				#region
//				if(Common.GetSession(SessionID.Master_Item_List_SelectedIndex) != null)
//				{
//					int index = -1;
//					index = Convert.ToInt32(Common.GetSession(SessionID.Master_Item_List_SelectedIndex));
//					if(this.DataGrid1.Items.Count <= index) return;
//					this.DataGrid1.SelectedIndex = index;
//				}
				#endregion
			}
		}
		#endregion

		/// <summary>
		/// DataGrid1_ItemDataBound
		/// </summary>
        //［SRC-02-01-02-002］private void DataGrid1_ItemDataBound
        #region private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
//			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
//			{
//				//TRｸﾘｯｸで行選択
//				e.Item.Attributes["onclick"] = "Select(" + e.Item.ItemIndex.ToString() + ");";
//			}
		}
		#endregion

		/// <summary>
		/// 項目名ﾘﾝｸｸﾘｯｸ
		/// </summary>
        //［SRC-02-01-02-003］private void DataGrid1_ItemCommand
        #region private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			Common.SetSession(SessionID.Master_Item_List_SelectedIndex, e.Item.ItemIndex.ToString());

			string id = e.Item.Cells[COL_ItemID].Text;
			Common.ScriptStart();
			Common.ScriptOutput("parent.document.location='./Edit.aspx?id=" + id + "';");
			Common.ScriptEnd();
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 項目一覧作成
		/// </summary>
        //［SRC-02-01-02-004］private void CreateList
        #region private void CreateList()
		private void CreateList()
		{
			DataTable table = new DataTable();
			table.Columns.Add("項目名");
			table.Columns.Add("データ型");
			table.Columns.Add("一覧表示");
			table.Columns.Add("ヘルプメッセージ");
			table.Columns.Add("ItemID");
			table.Columns.Add("Used");	//1:実ﾃﾞｰﾀ有りorﾃﾝﾌﾟﾚｰﾄで使用されている項目 0:未使用項目
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				cmdTxt = "SELECT M.*, (SELECT COUNT(*) FROM T_ObsMgrData AS D WHERE D.ItemID=M.ItemID) AS CNT1, (SELECT COUNT(*) FROM M_ObsTemplateItem AS T WHERE T.ItemID=M.ItemID) AS CNT2";
				cmdTxt += " FROM M_ObsItem AS M";
				cmdTxt += " ORDER BY M.ItemID";
				sql.Open();
				//ﾃﾞｰﾀ型名取得
				string[] type= new string[7];
				for(int i = 1; i <= 7; i++)
				{
					type[i - 1] = Common.GetText(sql, "EPSNet.Common", "DataType" + i.ToString());
				}
				dr = sql.Read(cmdTxt);
				while(dr.Read() == true)
				{
					switch(Common.LangID)
					{
						case 1: this.AddRow(table, dr["Japanese"], type[Convert.ToInt32(dr["DataType"]) - 1], dr["Tips"], dr["ItemID"], dr["CNT1"], dr["CNT2"]); break;
						case 2: this.AddRow(table, dr["English"], type[Convert.ToInt32(dr["DataType"]) - 1], dr["Tips2"], dr["ItemID"], dr["CNT1"], dr["CNT2"]); break;
					}
				}
				dr.Close();
			}
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();

			this.DataGrid1.SelectedIndex = -1;
		}
		#endregion

		/// <summary>
		/// 項目一覧への行追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="項目名"></param>
		/// <param name="データ型"></param>
		/// <param name="ヘルプメッセージ"></param>
		/// <param name="ItemID"></param>
		/// <param name="Cnt1"></param>
		/// <param name="Cnt2"></param>
        //［SRC-02-01-02-005］private void AddRow
        #region private void AddRow(DataTable table, object 項目名, object データ型, object ヘルプメッセージ, object ItemID, object cnt1, object cnt2)
		private void AddRow(DataTable table, object 項目名, object データ型, object ヘルプメッセージ, object ItemID, object Cnt1, object Cnt2)
		{
			DataRow row = table.NewRow();
			row["項目名"] = HttpUtility.HtmlEncode(項目名.ToString());
			if(ItemID.ToString() == "0")	row["データ型"] = "(" + this.mNumberingItemName + ")";
			else							row["データ型"] = データ型.ToString();
			row["ヘルプメッセージ"] = HttpUtility.HtmlEncode(ヘルプメッセージ.ToString());
			row["ItemID"] = ItemID.ToString();
			int cnt1 = Convert.ToInt32(Cnt1);
			int cnt2 = Convert.ToInt32(Cnt2);
			if(cnt1 == 0 && cnt2 == 0)	row["Used"] = "0";
			else						row["Used"] = "1";
			table.Rows.Add(row);
		}
		#endregion
































	}
}
