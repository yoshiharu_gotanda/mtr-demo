using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Master.Template
{
	/// <summary>
	/// Detail の概要の説明です。
	/// </summary>
	public class Detail : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonClose;
		protected System.Web.UI.WebControls.Label LabelTemplateName;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelDetail;

	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		//ﾌｫｰﾑﾛｰﾄﾞ
        //［SRC-02-04-01-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				string templateId = Request.QueryString["id"];
				//多国語対応処理
				using(CSql sql = new CSql())
				{
					sql.Open();
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelDetail);
					Common.SetText(sql, this.mType, this.ButtonClose);
					//ﾃﾝﾌﾟﾚｰﾄ名取得
					string cmdTxt;
					SqlDataReader dr;
					cmdTxt = "SELECT Text" + Common.LangID.ToString() + " FROM M_ObsTemplate WHERE TemplateID=@TemplateID";
					sql.AddParam("@TemplateID", templateId);
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true)
					{
						this.LabelTemplateName.Text = dr[0].ToString();
					}
					dr.Close();
				}
				//一覧作成
				this.CreateList();
			}
		}
		#endregion

		private Type mType = typeof(Detail);
		private int mRows;

		/// <summary>
		/// ﾃﾝﾌﾟﾚｰﾄに登録されている項目一覧を作成する
		/// </summary>
        //［SRC-02-04-01-002］private void CreateList
        #region private void CreateList()
		private void CreateList()
		{
			try
			{
				string id = Request.QueryString["id"];
				DataTable table = new DataTable();
				table.Columns.Add("ItemName");
				table.Columns.Add("DataType");
				
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();
					string[] ColNames = Common.GetFixItemColName(sql);			//必須項目名
					string[] ColExplains = Common.GetFixItemColExplain(sql);	//必須項目説明

					this.AddRow(table, ColNames[0] + "(*)", ColExplains[0]);
					this.AddRow(table, ColNames[1] + "(*)", ColExplains[1]);
					this.AddRow(table, ColNames[2] + "(*)", ColExplains[2]);
					//採番対象項目
					string num = Common.GetNumberingItemName(sql);
					this.AddRow(table, num + "(*)", num);
					//必須項目
					this.AddRow(table, ColNames[3] + "(*)", ColNames[3]);
					//設定項目
					switch(Common.LangID)
					{
						case 1: cmdTxt = "SELECT M.DataType, M.Japanese FROM M_ObsTemplateItem AS T"; break;
						case 2: cmdTxt = "SELECT M.DataType, M.English FROM M_ObsTemplateItem AS T"; break;
					}
					cmdTxt += " LEFT JOIN M_ObsItem AS M ON M.ItemID = T.ItemID";
					cmdTxt += " WHERE T.TemplateID=@TemplateID";
					cmdTxt += " ORDER BY T.SortID";
					sql.AddParam("@TemplateID", id);
					dr = sql.Read(cmdTxt);
					string[,] types = Common.DataTypes;
					while(dr.Read() == true)
					{
						this.AddRow(table, dr[1].ToString(), types[Convert.ToInt32(dr["DataType"]) - 1, 0]);
					}
					dr.Close();
					//必須項目
					this.AddRow(table, ColNames[4] + "(*)", ColExplains[4]);
					this.AddRow(table, ColNames[5] + "(*)", ColExplains[5]);
					this.AddRow(table, ColNames[6] + "(*)", ColExplains[6]);
				}
				this.DataGrid1.DataSource = table;
				this.DataGrid1.DataBind();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

        //［SRC-02-04-01-003］private void AddRow
        #region private void AddRow(string ItemName, string DataType)
		private void AddRow(DataTable table, string ItemName, string DataType)
		{
			this.mRows++;

			DataRow row = table.NewRow();
			row["ItemName"] = ItemName;
			row["DataType"] = DataType;
			table.Rows.Add(row);
		}
		#endregion



        //［SRC-02-04-01-004］protected string WindowHeight
        #region protected string WindowHeight
		protected string WindowHeight
		{
			get
			{
				return (this.mRows * 16 + 150).ToString();
			}
		}
		#endregion







































	}
}
