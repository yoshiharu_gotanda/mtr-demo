using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace EPSNet.Master.Status
{
	/// <summary>
	/// ListFrame の概要の説明です。
	/// </summary>
	public class ListFrame : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		
		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-03-04-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			//LoginID取得
			string id = Request.QueryString["UserName"];
			if(id == null && Common.LoginID != null) id = Common.LoginID;

			if(id == null)
			{
				//ｴﾗｰﾍﾟｰｼﾞへ遷移
				Response.Write("<script>");
				Response.Write("document.location='../../Common/Message.aspx'");
				Response.Write("</script>");
			}
			else
			{
				Common.SetSession(SessionID.LoginID, id);
				Common.GetLanguage();

				Response.Write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\">");
				Response.Write("<html>");
				Response.Write("<head>");
				Response.Write("</head>");
				Response.Write("<frameset rows='50,*'>");
				Response.Write("<frame name='button' src='ListButton.aspx' scrolling='no' noresize>");
				Response.Write("<frame name='list' src=''>");
				Response.Write("</frameset>");
				Response.Write("</html>");
			}
		}
		#endregion

	}
}
