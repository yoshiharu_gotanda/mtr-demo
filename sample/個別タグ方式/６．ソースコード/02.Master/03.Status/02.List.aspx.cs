using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Master.Status
{
	/// <summary>
	/// List の概要の説明です。
	/// </summary>
	public class List : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Button ButtonUp;
		protected System.Web.UI.WebControls.Button ButtonDown;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonUp.Click += new System.EventHandler(this.ButtonUp_Click);
			this.ButtonDown.Click += new System.EventHandler(this.ButtonDown_Click);
			this.DataGrid1.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================

		private const int COL_SelectMove = 0;
		private const int COL_StatusName = 1;
		private const int COL_Finished = 2;
		private const int COL_StatusID = 3;
		private const int COL_Up = 4;
		private const int COL_Down = 5;
		private const int COL_SortID = 6;


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(List);

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾌｫｰﾑﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-03-02-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ﾃｷｽﾄ取得
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					Common.SetText(sql, this.mType, this.LabelTitle);
					this.DataGrid1.Columns[COL_StatusName].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGrid1.Columns[COL_Finished].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGrid1.Columns[COL_SelectMove].HeaderText = Common.GetText(sql, this.mType, "Col3");
					Common.SetText(sql, this.mType, this.ButtonUp);
					Common.SetText(sql, this.mType, this.ButtonDown);
				}
				this.CreateList(-1);
			}
		}
		#endregion

		/// <summary>
		/// 項目名ﾘﾝｸｸﾘｯｸ
		/// </summary>
        //［SRC-02-03-02-002］private void DataGrid1_ItemCommand
        #region private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		private void DataGrid1_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					if(e.CommandName == "Detail")
					{
						Common.SetSession(SessionID.Master_Item_List_SelectedIndex, e.Item.ItemIndex.ToString());
						string id = e.Item.Cells[COL_StatusID].Text;
						Common.ScriptStart();
						Common.ScriptOutput("parent.document.location='./Edit.aspx?id=" + id + "';");
						Common.ScriptEnd();
					}
					else if(e.CommandName == "Up")
					{
						#region
						sql.BeginTrans();
						string statusId = e.Item.Cells[COL_StatusID].Text;
						string sortId = e.Item.Cells[COL_SortID].Text;
						//自分の一つ上のSortIDを取得
						cmdTxt = "SELECT SortID FROM M_ObsStatus WHERE SortID<@SortID ORDER BY SortID DESC";
						sql.AddParam("@SortID", sortId);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							int sortIdUp = Convert.ToInt32(dr["SortID"]);
							dr.Close();
							//SortIDを交換する
							//選択されたｽﾃｰﾀｽの一つ上のSortIDを選択されたｽﾃｰﾀｽのSortIDに更新
							cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortID WHERE SortID=@SortIDUp";
							sql.AddParam("@SortID", sortId);
							sql.AddParam("@SortIDUp", sortIdUp);
							sql.CommandTrans(cmdTxt);
							//選択されたｽﾃｰﾀｽのSortIDを一つ上のSortIDに更新
							cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortIDUp WHERE StatusID=@StatusID";
							sql.AddParam("@SortIDUp", sortIdUp);
							sql.AddParam("@StatusID", statusId);
							sql.CommandTrans(cmdTxt);
							sql.CommitTrans();
							this.CreateList(-1);
						}
						else
						{
							dr.Close();
						}
						#endregion
					}
					else if(e.CommandName == "Down")
					{
						#region
						sql.BeginTrans();
						string statusId = e.Item.Cells[COL_StatusID].Text;
						string sortId = e.Item.Cells[COL_SortID].Text;
						//自分の一つ下のSortIDを取得
						cmdTxt = "SELECT SortID FROM M_ObsStatus WHERE SortID>@SortID ORDER BY SortID";
						sql.AddParam("@SortID", sortId);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							int sortIdDown = Convert.ToInt32(dr["SortID"]);
							dr.Close();
							//SortIDを交換する
							//選択されたｽﾃｰﾀｽの一つ下のSortIDを選択されたｽﾃｰﾀｽのSortIDに更新
							cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortID WHERE SortID=@SortIDDown";
							sql.AddParam("@SortID", sortId);
							sql.AddParam("@SortIDDown", sortIdDown);
							sql.CommandTrans(cmdTxt);
							//選択されたｽﾃｰﾀｽのSortIDを一つ下のSortIDに更新
							cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortIDDown WHERE StatusID=@StatusID";
							sql.AddParam("@SortIDDown", sortIdDown);
							sql.AddParam("@StatusID", statusId);
							sql.CommandTrans(cmdTxt);
							sql.CommitTrans();
							this.CreateList(-1);
						}
						else
						{
							dr.Close();
						}
						#endregion
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 上へ移動
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        //［SRC-02-03-02-003］private void ButtonUp_Click
        #region private void ButtonUp_Click(object sender, System.EventArgs e)
		private void ButtonUp_Click(object sender, System.EventArgs e)
		{
			string sel = Request.Form["moveHeader"];
			if(sel == null) return;
			int selectedIndex = Convert.ToInt32(sel);
			//先頭行が選択されていたら上へは移動出来ない
			if(selectedIndex <= 0)
			{
				this.CreateList(selectedIndex);
				return;
			}

			DataTable table = (DataTable)ViewState["DataTable"];
			int dataCnt = table.Rows.Count;
			
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				#region
				sql.BeginTrans();
				try
				{
					string statusId = table.Rows[selectedIndex]["StatusID"].ToString();
					string sortId = table.Rows[selectedIndex]["SortID"].ToString();

					//自分の一つ上のSortIDを取得
					cmdTxt = "SELECT SortID FROM M_ObsStatus WHERE SortID<@SortID ORDER BY SortID DESC";
					sql.AddParam("@SortID", sortId);
					dr = sql.ReadTrans(cmdTxt);
					if(dr.Read() == true)
					{
						int sortIdUp = Convert.ToInt32(dr["SortID"]);
						dr.Close();
						//SortIDを交換する
						//選択されたｽﾃｰﾀｽの一つ上のSortIDを選択されたｽﾃｰﾀｽのSortIDに更新
						cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortID WHERE SortID=@SortIDUp";
						sql.AddParam("@SortID", sortId);
						sql.AddParam("@SortIDUp", sortIdUp);
						sql.CommandTrans(cmdTxt);
						//選択されたｽﾃｰﾀｽのSortIDを一つ上のSortIDに更新
						cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortIDUp WHERE StatusID=@StatusID";
						sql.AddParam("@SortIDUp", sortIdUp);
						sql.AddParam("@StatusID", statusId);
						sql.CommandTrans(cmdTxt);
						sql.CommitTrans();
						//一覧作成
						this.CreateList(selectedIndex - 1);
					}
					else
					{
						dr.Close();
					}
				}
				catch(Exception ex)
				{
					sql.RollbackTrans();
					Common.Alert(this, ex.Message);
				}
				#endregion
			}
		}
		#endregion

		/// <summary>
		/// 下へ移動
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        //［SRC-02-03-02-004］private void ButtonDown_Click
        #region private void ButtonDown_Click(object sender, System.EventArgs e)
		private void ButtonDown_Click(object sender, System.EventArgs e)
		{
			string sel = Request.Form["moveHeader"];
			if(sel == null) return;
			int selectedIndex = Convert.ToInt32(sel);
			DataTable table = (DataTable)ViewState["DataTable"];
			int dataCnt = table.Rows.Count;
			//一番したの行が選択されていたら移動出来ない
			if(selectedIndex >= dataCnt - 1)
			{
				this.CreateList(selectedIndex);
				return;
			}
			
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();

				#region
				sql.BeginTrans();
				try
				{
					string statusId = table.Rows[selectedIndex]["StatusID"].ToString();
					string sortId = table.Rows[selectedIndex]["SortID"].ToString();

					//自分の一つ下のSortIDを取得
					cmdTxt = "SELECT SortID FROM M_ObsStatus WHERE SortID>@SortID ORDER BY SortID";
					sql.AddParam("@SortID", sortId);
					dr = sql.ReadTrans(cmdTxt);
					if(dr.Read() == true)
					{
						int sortIdDown = Convert.ToInt32(dr["SortID"]);
						dr.Close();
						//SortIDを交換する
						//選択されたｽﾃｰﾀｽの一つ下のSortIDを選択されたｽﾃｰﾀｽのSortIDに更新
						cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortID WHERE SortID=@SortIDDown";
						sql.AddParam("@SortID", sortId);
						sql.AddParam("@SortIDDown", sortIdDown);
						sql.CommandTrans(cmdTxt);
						//選択されたｽﾃｰﾀｽのSortIDを一つ下のSortIDに更新
						cmdTxt = "UPDATE M_ObsStatus SET SortID=@SortIDDown WHERE StatusID=@StatusID";
						sql.AddParam("@SortIDDown", sortIdDown);
						sql.AddParam("@StatusID", statusId);
						sql.CommandTrans(cmdTxt);
						sql.CommitTrans();
						this.CreateList(selectedIndex + 1);
					}
					else
					{
						dr.Close();
					}
				}
				catch(Exception ex)
				{
					sql.RollbackTrans();
					Common.Alert(this, ex.Message);
				}
				#endregion
			}
		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 項目一覧作成
		/// </summary>
        //［SRC-02-03-02-005］private void CreateList
        #region private void CreateList(int selectedIndex)
		private void CreateList(int selectedIndex)
		{
			DataTable table = new DataTable();
			table.Columns.Add("ステータス名");
			table.Columns.Add("完了フラグ");
			table.Columns.Add("StatusID");
			table.Columns.Add("SortID");
			table.Columns.Add("移動選択");
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				cmdTxt = "SELECT * FROM M_ObsStatus ORDER BY SortID";
				sql.Open();
				//ﾃﾞｰﾀ型名取得
				string[] type= new string[7];
				for(int i = 1; i <= 7; i++)
				{
					type[i - 1] = Common.GetText(sql, "EPSNet.Common", "DataType" + i.ToString());
				}
				dr = sql.Read(cmdTxt);
				int index = 0;
				while(dr.Read() == true)
				{
					switch(Common.LangID)
					{
						case 1: this.AddRow(table, dr["Text1"], dr["Finished"], dr["StatusID"], dr["SortID"], selectedIndex, index); break;
						case 2: this.AddRow(table, dr["Text2"], dr["Finished"], dr["StatusID"], dr["SortID"], selectedIndex, index); break;
					}
					index++;
				}
				dr.Close();
			}
			ViewState["DataTable"] = table;
			this.DataGrid1.DataSource = table;
			this.DataGrid1.DataBind();

			this.DataGrid1.SelectedIndex = -1;
		}
		#endregion

		/// <summary>
		/// 項目一覧への行追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="項目名"></param>
		/// <param name="データ型"></param>
		/// <param name="ヘルプメッセージ"></param>
		/// <param name="ItemID"></param>
		/// <param name="Cnt1"></param>
		/// <param name="Cnt2"></param>
        //［SRC-02-03-02-006］private void AddRow
        #region private void AddRow(DataTable table, object ステータス名, object 完了フラグ, object StatusID, object SortID, int selectedIndex, int index)
		private void AddRow(DataTable table, object ステータス名, object 完了フラグ, object StatusID, object SortID, int selectedIndex, int index)
		{
			DataRow row = table.NewRow();
			row["ステータス名"] = HttpUtility.HtmlEncode(ステータス名.ToString());
			row["完了フラグ"] = "";
			if(Convert.ToBoolean(完了フラグ) == true)
			{
				if(Common.LangID == 1) row["完了フラグ"] = "●";
				else					row["完了フラグ"] = "*";
			}
			row["StatusID"] = StatusID.ToString();
			row["SortID"] = SortID.ToString();
			if(index == selectedIndex)
			{
				row["移動選択"] = "<INPUT TYPE=\"radio\" NAME=\"moveHeader\" ID=\"moveHeader" + index.ToString() + "\" VALUE=" + index.ToString() + " CHECKED>";
			}
			else
			{
				row["移動選択"] = "<INPUT TYPE=\"radio\" NAME=\"moveHeader\" ID=\"moveHeader" + index.ToString() + "\" VALUE=" + index.ToString() + ">";
			}
			
			table.Rows.Add(row);
		}
		#endregion







	}
}
