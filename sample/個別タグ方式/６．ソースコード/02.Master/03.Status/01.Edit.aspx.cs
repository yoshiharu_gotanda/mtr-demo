using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;

namespace EPSNet.Master.Status
{
	/// <summary>
	/// Edit の概要の説明です。
	/// </summary>
	public class Edit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonInsert;
		protected System.Web.UI.WebControls.Button ButtonUpdate;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelStatusName;
		protected System.Web.UI.WebControls.Label LabelHelp1;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;
		protected System.Web.UI.WebControls.TextBox TextBoxStatusName;
		protected System.Web.UI.WebControls.CheckBox CheckBoxFinished;
	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonInsert.Click += new System.EventHandler(this.ButtonInsert_Click);
			this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// 定数
		///================================================================


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Edit);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-03-01-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ｷｬﾝｾﾙﾎﾞﾀﾝ動作設定
				this.ButtonDelete.Attributes["onclick"] = "return confirm('" + Common.GetText(this.mType, "ConfirmDelete") + "');";
				this.ButtonCancel.Attributes["onclick"] = "document.location='./ListFrame.aspx';";

				string id = Request.QueryString["id"];

				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelHelp1);
					Common.SetText(sql, this.mType, this.LabelStatusName);
					Common.SetText(sql, this.mType, this.CheckBoxFinished);
					Common.SetText(sql, this.mType, this.ButtonInsert);
					Common.SetText(sql, this.mType, this.ButtonUpdate);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					Common.SetText(sql, this.mType, this.ButtonDelete);
					#endregion

					//ﾓｰﾄﾞによる機能ﾎﾞﾀﾝ設定
					if(id == null)
					{
						//追加ﾓｰﾄﾞ
						this.ButtonInsert.Visible = true;
						this.ButtonUpdate.Visible = false;
						this.ButtonDelete.Visible = false;
					}
					else
					{
						//更新ﾓｰﾄﾞ
						this.ButtonInsert.Visible = false;
						this.ButtonUpdate.Visible = true;
						this.ButtonDelete.Visible = true;
					}

					//更新の場合、既存ﾃﾞｰﾀの表示
					if(id != null)
					{
						#region
						string cmdTxt;
						SqlDataReader dr;
						cmdTxt = "SELECT S.*, D.Status AS Exist FROM M_ObsStatus AS S";
						cmdTxt += " LEFT JOIN T_ObsMgr2 AS D ON D.Status=S.StatusID";
						cmdTxt += " WHERE S.StatusID=@ID";
						sql.AddParam("@ID", id);
						dr = sql.Read(cmdTxt);
						if(dr.Read() == true)
						{
							switch(Common.LangID)
							{
								case 1:
								{
									this.TextBoxStatusName.Text = Common.CnvFromDB(dr["Text1"].ToString());
									break;
								}
								case 2:
								{
									this.TextBoxStatusName.Text = Common.CnvFromDB(dr["Text2"].ToString());
									break;
								}
							}
							//既存ﾃﾞｰﾀが存在する場合は削除不可
							if(dr["Exist"] != DBNull.Value) this.ButtonDelete.Enabled = false;

							this.CheckBoxFinished.Checked = Convert.ToBoolean(dr["Finished"]);
						}
						dr.Close();
						#endregion
					}
				}
			}
		}
		#endregion

		/// <summary>
		/// 追加ﾎﾞﾀﾝ
		/// </summary>
        //［SRC-02-03-01-002］private void ButtonInsert_Click
        #region private void ButtonInsert_Click(object sender, System.EventArgs e)
		private void ButtonInsert_Click(object sender, System.EventArgs e)
		{
			try
			{
				//string id;
				//string id = Request.QueryString["id"];
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();

					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}
					
					sql.BeginTrans();
					try
					{
						#region 追加処理
						//項目名重複ﾁｪｯｸ
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "SELECT * FROM M_ObsStatus WHERE Text1=@Name";
								break;
							case 2:
								cmdTxt = "SELECT * FROM M_ObsItem WHERE Text2=@Name";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxStatusName.Text));
						dr = sql.ReadTrans(cmdTxt);
						bool exist = false;
						if(dr.Read() == true) exist = true;
						dr.Close();
						if(exist == true)
						{
							sql.CommitTrans();
							Common.Alert(this, Common.GetText(sql, this.mType, "AlertSameStatusName"));
							return;
						}
						//StatusID最大値+1、SortIDの最大値+1を取得
						int statusId = 0;
						int sortId = 0;
						cmdTxt = "SELECT MAX(StatusID) AS StatusID, MAX(SortID) AS SortID FROM M_ObsStatus";
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							if(dr["StatusID"] != DBNull.Value) statusId = Convert.ToInt32(dr["StatusID"]) + 1;
							if(dr["SortID"] != DBNull.Value) sortId = Convert.ToInt32(dr["SortID"]) + 1;
						}
						dr.Close();

						cmdTxt = "INSERT INTO M_ObsStatus(StatusID, Text1, Text2, Finished, SortID)";
						cmdTxt += " VALUES(@StatusID, @Name, @Name, @Finished, @SortID)";
						sql.AddParam("@StatusID", statusId);
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxStatusName.Text));
						sql.AddParam("@Finished", Common.ConvertToString(this.CheckBoxFinished.Checked));
						sql.AddParam("@SortID", sortId);
						sql.CommandTrans(cmdTxt);
						#endregion

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 更新ﾎﾞﾀﾝ
		/// </summary>
        //［SRC-02-03-01-003］private void ButtonUpdate_Click
        #region private void ButtonUpdate_Click(object sender, System.EventArgs e)
		private void ButtonUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Request.QueryString["id"];
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();

					//入力ﾁｪｯｸ
					if(this.InputCheck(sql) == false)
					{
						return;
					}

					//更新処理
					sql.BeginTrans();
					try
					{
						#region
						//項目名重複ﾁｪｯｸ
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "SELECT * FROM M_ObsStatus WHERE Text1=@Name AND StatusID<>@StatusID";
								break;
							case 2:
								cmdTxt = "SELECT * FROM M_ObsStatus WHERE Text2=@Name AND StatusID<>@StatusID";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxStatusName.Text));
						sql.AddParam("@StatusID", id);
						dr = sql.ReadTrans(cmdTxt);
						bool exist = false;
						if(dr.Read() == true) exist = true;
						dr.Close();
						if(exist == true)
						{
							sql.CommitTrans();
							Common.Alert(this, Common.GetText(sql, this.mType, "AlertSameStatusName"));
							return;
						}
						//項目追加
						switch(Common.LangID)
						{
							case 1:
								cmdTxt = "UPDATE M_ObsStatus SET Text1=@Name, Finished=@Finished WHERE StatusID=@StatusID";
								break;
							case 2:
								cmdTxt = "UPDATE M_ObsStatus SET Text2=@Name, Finished=@Finished WHERE StatusID=@StatusID";
								break;
						}
						sql.AddParam("@Name", Common.CnvToDB(this.TextBoxStatusName.Text));
						sql.AddParam("@Finished", Common.ConvertToString(this.CheckBoxFinished.Checked));
						sql.AddParam("@StatusID", id);
						sql.CommandTrans(cmdTxt);
						#endregion

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 削除ﾎﾞﾀﾝ
		/// </summary>
        //［SRC-02-03-01-004］private class CSect
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			string id = Request.QueryString["id"];
			if(id == null) return;

			try
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//既存ﾃﾞｰﾀがあるかﾁｪｯｸ
						cmdTxt = "SELECT COUNT(*) AS CNT FROM T_ObsMgr2";
						cmdTxt += " WHERE Status=@Status";
						sql.AddParam("@Status", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true && Convert.ToInt32(dr["CNT"]) == 0)
						{
							dr.Close();
							//ｽﾃｰﾀｽﾏｽﾀから削除
							cmdTxt = "DELETE FROM M_ObsStatus WHERE StatusID=@id";
							sql.AddParam("@id", id);
							sql.CommandTrans(cmdTxt);
							sql.CommitTrans();
						}
						else
						{
							dr.Close();
							//既存ﾃﾞｰﾀが存在したら削除不可
							this.ButtonDelete.Enabled = false;
							return;
						}
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				Response.Redirect("./ListFrame.aspx");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 入力ﾁｪｯｸ
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
        //［SRC-02-03-01-005］private class CSect
        #region private bool InputCheck(CSql sql)
		private bool InputCheck(CSql sql)
		{
			if(Common.CnvToDB(this.TextBoxStatusName.Text) == string.Empty)
			{
				Common.Alert(this, Common.GetText(sql, this.mType, "AlertStatusInput"));
				return false;
			}
			return true;
		}
		#endregion

















	}
}
