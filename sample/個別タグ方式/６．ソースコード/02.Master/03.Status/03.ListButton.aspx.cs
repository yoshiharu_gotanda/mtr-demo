using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace EPSNet.Master.Status
{
	/// <summary>
	/// ListButton の概要の説明です。
	/// </summary>
	public class ListButton : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonAdd;

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		private Type mType = typeof(ListButton);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-02-03-03-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				//ﾃｷｽﾄ設定
				using(CSql sql = new CSql())
				{
					sql.Open();
					Common.SetText(sql, this.mType, this.ButtonAdd);
					this.ButtonAdd.Value += "...";

					//各ﾎﾞﾀﾝにｽｸﾘﾌﾟﾄ設定
					this.ButtonAdd.Attributes["onclick"] = "parent.location='Edit.aspx'";
				}
			}
		}
		#endregion

	}
}
