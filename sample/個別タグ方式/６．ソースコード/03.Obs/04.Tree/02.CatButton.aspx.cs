using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.IO;

namespace EPSNet.Obs.Tree
{
	/// <summary>
	/// CatButton の概要の説明です。
	/// </summary>
	public class CatButton : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonCancelMove;
		protected System.Web.UI.WebControls.Button ButtonFinishMove;
		protected System.Web.UI.WebControls.Button ButtonRight;
		protected System.Web.UI.WebControls.Button ButtonLeft;
		protected System.Web.UI.WebControls.Button ButtonUp;
		protected System.Web.UI.WebControls.Button ButtonDown;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.WebControls.Button ButtonEdit;
		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Button ButtonMove;

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonMove.Click += new System.EventHandler(this.ButtonMove_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.ButtonDown.Click += new System.EventHandler(this.ButtonDown_Click);
			this.ButtonUp.Click += new System.EventHandler(this.ButtonUp_Click);
			this.ButtonLeft.Click += new System.EventHandler(this.ButtonLeft_Click);
			this.ButtonRight.Click += new System.EventHandler(this.ButtonRight_Click);
			this.ButtonFinishMove.Click += new System.EventHandler(this.ButtonFinishMove_Click);
			this.ButtonCancelMove.Click += new System.EventHandler(this.ButtonCancelMove_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(CatButton);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾌｫｰﾑﾛｰﾄﾞ
		/// </summary>
        //［SRC-03-04-02-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					//削除確認ﾒｯｾｰｼﾞ設定
					this.ButtonDelete.Attributes["onclick"] = "return confirm('" + Common.GetText(sql, this.mType, "ConfirmDelete") + "')";
					//追加ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
					this.ButtonAdd.Attributes["onclick"] = "OpenAddWindow();return false;";
					//編集ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定
					this.ButtonEdit.Attributes["onclick"] = "OpenEditWindow();return false;";

					Common.SetText(sql, this.mType, this.ButtonAdd);
					Common.SetText(sql, this.mType, this.ButtonEdit);
					Common.SetText(sql, this.mType, this.ButtonMove);
					Common.SetText(sql, this.mType, this.ButtonDelete);
					Common.SetText(sql, this.mType, this.ButtonFinishMove);
					Common.SetText(sql, this.mType, this.ButtonCancelMove);
					//Common.SetText(sql, this.mType, this.ButtonRefresh);

					#endregion
				}
				//内部ﾌﾗｸﾞによりﾎﾞﾀﾝの状態を設定する
				#region
				string catMode = Common.GetSession(SessionID.Obs_Tree_CatTree_Mode).ToString();	//0 or 1
				object catSel = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID);			//null or catid
				string obsMode = Common.GetSession(SessionID.Obs_Tree_ObsTree_Mode).ToString();	//0 or 1
				object obsSel = Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID);			//null or obsid
				if(catSel == null)
				{
					//* ｶﾃｺﾞﾘ未選択
					#region
					//移動系ﾎﾞﾀﾝ不可視
					this.ButtonFinishMove.Visible = this.ButtonCancelMove.Visible = this.ButtonUp.Visible = this.ButtonDown.Visible = this.ButtonLeft.Visible = this.ButtonRight.Visible = false;
					//編集、移動、削除ﾎﾞﾀﾝ無効
					this.ButtonEdit.Enabled = this.ButtonMove.Enabled = this.ButtonDelete.Enabled = false;
					#endregion
				}
				else
				{
					//* ｶﾃｺﾞﾘ選択有り
					#region
					if(catMode == "0")
					{
						//* 選択ﾓｰﾄﾞ
						//移動系ﾎﾞﾀﾝ不可視
						this.ButtonFinishMove.Visible = this.ButtonCancelMove.Visible = this.ButtonUp.Visible = this.ButtonDown.Visible = this.ButtonLeft.Visible = this.ButtonRight.Visible = false;
						if(obsMode == "1")
						{
							//* 障害移動中
							//編集系ﾎﾞﾀﾝ不可視
							this.ButtonAdd.Visible = this.ButtonEdit.Visible = this.ButtonMove.Visible = this.ButtonDelete.Visible = false;
						}
						else
						{
							//子ﾂﾘｰ、子障害が存在したら削除不可
							#region
							ArrayList alCatID = new ArrayList();
							using(CSql sql = new CSql())
							{
								string cmdTxt;
								SqlDataReader dr;
								sql.Open();
								Common.GetChildCat(sql, alCatID, Convert.ToInt32(catSel));
								//ｶﾃｺﾞﾘに子ｶﾃｺﾞﾘが存在したら削除不可
								if(alCatID.Count > 1)
								{
									this.ButtonDelete.Enabled = false;
								}
								else
								{
									for(int i = 0; i < alCatID.Count; i++)
									{
										//ｶﾃｺﾞﾘに属する障害ID取得
										ArrayList alObsMgrID = new ArrayList();
										cmdTxt = "SELECT ObsMgrID FROM T_ObsTree WHERE CatID=@CatID";
										sql.AddParam("@CatID", alCatID[i].ToString());
										dr = sql.ReadTrans(cmdTxt);
										while(dr.Read() == true)
										{
											alObsMgrID.Add(dr["ObsMgrID"].ToString().Trim());
										}
										dr.Close();
										//ｶﾃｺﾞﾘに属する障害が存在したら削除不可
										if(alObsMgrID.Count > 0)
										{
											this.ButtonDelete.Enabled = false;
										}
									}
								}
							}
							#endregion
						}
					}
					else
					{
						//* 移動ﾓｰﾄﾞ
						//編集系ﾎﾞﾀﾝ不可視
						this.ButtonAdd.Visible = this.ButtonEdit.Visible = this.ButtonMove.Visible = this.ButtonDelete.Visible = false;
					}
					#endregion
				}
				#endregion
			}
		}
		#endregion

		/// <summary>
		/// 移動ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-002］private void ButtonMove_Click
        #region private void ButtonMove_Click(object sender, System.EventArgs e)
		private void ButtonMove_Click(object sender, System.EventArgs e)
		{
			try
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//ﾂﾘｰの状態をﾜｰｸﾃｰﾌﾞﾙにｺﾋﾟｰ
						cmdTxt = "DELETE FROM T_ObsCategoryTreeWk WHERE UserID=@UserID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.CommandTrans(cmdTxt);

						cmdTxt = "INSERT INTO T_ObsCategoryTreeWk(UserID,CatID,CatName,SortID,ParentID)";
						cmdTxt += "SELECT @UserID,CatID,CatName,SortID,ParentID FROM T_ObsCategoryTree";
						sql.AddParam("@UserID", Common.LoginID);
						sql.CommandTrans(cmdTxt);

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//移動ﾓｰﾄﾞにする
				this.SetMode(1);
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 削除ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-003］private void ButtonDelete_Click
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				//子ﾂﾘｰ、子障害存在ﾁｪｯｸ
				ArrayList alCatID = new ArrayList();
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					Common.GetChildCat(sql, alCatID, Convert.ToInt32(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID)));
					//ｶﾃｺﾞﾘに子ｶﾃｺﾞﾘが存在したら削除不可
					if(alCatID.Count > 1)
					{
						//alert表示後表示更新処理
						#region
						string mes = Common.GetText(this.mType, "AlertChildExist");
						mes = mes.Replace("'", string.Empty);
						mes = mes.Replace("\r", "\\r");
						mes = mes.Replace("\n", "\\n");
						string script = string.Empty;
						script += "<script>";
						script += "alert('" + mes + "');";
						script += "parent.lbutton.location='./CatButton.aspx';";
						script += "parent.rbutton.location='./ObsButton.aspx';";
						script += "</script>";
						this.RegisterStartupScript("script", script);
						#endregion
						return;
					}
					for(int i = 0; i < alCatID.Count; i++)
					{
						//ｶﾃｺﾞﾘに属する障害ID取得
						ArrayList alObsMgrID = new ArrayList();
						cmdTxt = "SELECT ObsMgrID FROM T_ObsTree WHERE CatID=@CatID";
						sql.AddParam("@CatID", alCatID[i].ToString());
						dr = sql.ReadTrans(cmdTxt);
						while(dr.Read() == true)
						{
							alObsMgrID.Add(dr["ObsMgrID"].ToString().Trim());
						}
						dr.Close();
						if(alObsMgrID.Count > 0)
						{
							//alert表示後表示更新処理
							#region
							string mes = Common.GetText(this.mType, "AlertChildExist");
							mes = mes.Replace("'", string.Empty);
							mes = mes.Replace("\r", "\\r");
							mes = mes.Replace("\n", "\\n");
							string script = string.Empty;
							script += "<script>";
							script += "alert('" + mes + "');";
							script += "parent.lbutton.location='./CatButton.aspx';";
							script += "parent.rbutton.location='./ObsButton.aspx';";
							script += "</script>";
							this.RegisterStartupScript("script", script);
							#endregion
							return;
						}
					}
				}

				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
//						ArrayList alCatID = new ArrayList();
//						Common.GetChildCat(sql, alCatID, Convert.ToInt32(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID)));
						for(int i = 0; i < alCatID.Count; i++)
						{
							//ｶﾃｺﾞﾘ情報削除
							cmdTxt = "DELETE FROM T_ObsCategoryTree WHERE CatID=@CatID";
							sql.AddParam("@CatID", alCatID[i].ToString());
							sql.CommandTrans(cmdTxt);
							//ﾂﾘｰ開閉情報削除
							cmdTxt = "DELETE FROM T_ObsCategoryTreeStatus WHERE CatID=@CatID";
							sql.AddParam("@CatID", alCatID[i].ToString());
							sql.CommandTrans(cmdTxt);
							//ｶﾃｺﾞﾘに属する障害ID取得
							ArrayList alObsMgrID = new ArrayList();
							cmdTxt = "SELECT ObsMgrID FROM T_ObsTree WHERE CatID=@CatID";
							sql.AddParam("@CatID", alCatID[i].ToString());
							dr = sql.ReadTrans(cmdTxt);
							while(dr.Read() == true)
							{
								alObsMgrID.Add(dr["ObsMgrID"].ToString().Trim());
							}
							dr.Close();
							//***
							//ｶﾃｺﾞﾘに障害が属する場合は、上ではじいているので実際にこのﾛｼﾞｯｸを通る事は無い
							//***
							//ｶﾃｺﾞﾘに属する障害情報削除
							for(int j = 0; j < alObsMgrID.Count; j++)
							{
								//障害情報削除
								cmdTxt = "DELETE FROM T_ObsMgr2 WHERE ObsMgrID=@ObsMgrID";
								sql.AddParam("@ObsMgrID", alObsMgrID[j].ToString());
								sql.CommandTrans(cmdTxt);
								//障害可変部ﾃﾞｰﾀ削除
								cmdTxt = "DELETE FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID";
								sql.AddParam("@ObsMgrID", alObsMgrID[j].ToString());
								sql.CommandTrans(cmdTxt);
								//添付ﾌｧｲﾙ情報削除
								cmdTxt = "DELETE FROM T_ObsMgr_Lib WHERE ObsMgrID=@ObsMgrID";
								sql.AddParam("@ObsMgrID", alObsMgrID[j].ToString());
								sql.CommandTrans(cmdTxt);
								//ﾘﾝｸ情報削除
								cmdTxt = "DELETE FROM T_ObsMgrLink WHERE ObsMgrID=@ObsMgrID";
								sql.AddParam("@ObsMgrID", alObsMgrID[j].ToString());
								sql.CommandTrans(cmdTxt);
								//承認履歴削除
								cmdTxt = "DELETE FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=@ObsMgrID";
								sql.AddParam("@ObsMgrID", alObsMgrID[j].ToString());
								sql.CommandTrans(cmdTxt);
								//添付ﾌｧｲﾙ格納ﾌｫﾙﾀﾞ削除
								//string path = Path.Combine(Server.MapPath(string.Empty), @"../Lib/" + alObsMgrID[j].ToString());
								string path = Common.GetCommonLib(alObsMgrID[j].ToString());
								if(Directory.Exists(path) == true) Directory.Delete(path, true);
							}
							//障害ﾂﾘｰ情報削除
							cmdTxt = "DELETE FROM T_ObsTree WHERE CatID=@CatID";
							sql.AddParam("@CatID", alCatID[i].ToString());
							sql.CommandTrans(cmdTxt);
						}
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ﾂﾘｰ選択情報ｸﾘｱ
				Common.SetSession(SessionID.Obs_Tree_CatTree_SelCatID, null);
				Common.SetSession(SessionID.Obs_Tree_ObsTree_SelObsID, null);
				//表示更新
				Common.ScriptStart();
				Common.ScriptOutput("parent.lbutton.location='./CatButton.aspx';");
				Common.ScriptOutput("parent.rbutton.location='./ObsButton.aspx';");
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 移動ｷｬﾝｾﾙﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-004］private void ButtonCancelMove_Click
        #region private void ButtonCancelMove_Click(object sender, System.EventArgs e)
		private void ButtonCancelMove_Click(object sender, System.EventArgs e)
		{
			//通常ﾓｰﾄﾞにする
			this.SetMode(0);
		}
		#endregion

		/// <summary>
		/// 移動終了ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-005］private void ButtonFinishMove_Click
        #region private void ButtonFinishMove_Click(object sender, System.EventArgs e)
		private void ButtonFinishMove_Click(object sender, System.EventArgs e)
		{
			try
			{
				//ﾜｰｸﾃｰﾌﾞﾙの状態をﾂﾘｰﾃｰﾌﾞﾙにｺﾋﾟｰ
				#region
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					sql.BeginTrans();
					try
					{
						cmdTxt = "DELETE FROM T_ObsCategoryTree";
						sql.CommandTrans(cmdTxt);

						cmdTxt = "INSERT INTO T_ObsCategoryTree(CatID,CatName,SortID,ParentID)";
						cmdTxt += "SELECT CatID,CatName,SortID,ParentID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.CommandTrans(cmdTxt);

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				#endregion

				//移動後がぶら下がる親ｶﾃｺﾞﾘが閉じていたら開く
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					//自分の親ｶﾃｺﾞﾘIDを全て取得する
					ArrayList alCatID = new ArrayList();
					string catID = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
					while(true)
					{
						cmdTxt = "SELECT ParentID FROM T_ObsCategoryTree WHERE CatID=@CatID";
						sql.AddParam("@CatID", catID);
						dr = sql.Read(cmdTxt);
						if(dr.Read() == true)
						{
							catID = dr["ParentID"].ToString();
						}
						else
						{
							catID = string.Empty;
						}
						dr.Close();
						//最上位または親が見つからない場合は終了
						if(catID == "-1" || catID == string.Empty)
						{
							break;
						}
						alCatID.Add	(catID);
					}
					//自分の親ｶﾃｺﾞﾘを開く
					if(alCatID.Count > 0)
					{
						cmdTxt = "DELETE FROM T_ObsCategoryTreeStatus WHERE UserID=@UserID AND (";
						for(int i = 0; i < alCatID.Count; i++)
						{
							if(i != 0) cmdTxt += " OR ";
							cmdTxt += "CatID='" + alCatID[i].ToString() + "'";
						}
						cmdTxt += ")";
						sql.AddParam("@UserID", Common.LoginID);
						sql.Command(cmdTxt);
					}
				}

				//通常ﾓｰﾄﾞにする
				this.SetMode(0);
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ↓ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-006］private void ButtonDown_Click
        #region private void ButtonDown_Click(object sender, System.EventArgs e)
		private void ButtonDown_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//自PrentID/SortIDを取得
						int sortId = -1;
						int parentId = -1;
						#region
						cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@CatID", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							sortId = Convert.ToInt32(dr["SortID"]);
							parentId = Convert.ToInt32(dr["ParentID"]);
						}
						dr.Close();
						if(sortId == -1)
						{
							sql.CommitTrans();
							return;
						}
						#endregion
						//自ParentIDと同じParentIDで自SortIDよりも大きいSortIDで一番小さいSortIDのCatIDを取得する
						#region
						cmdTxt = "SELECT CatID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID AND SortID > @SortID ORDER BY SortID ASC";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentId);
						sql.AddParam("@SortID", sortId);
						dr = sql.ReadTrans(cmdTxt);
						#endregion
						int catIdDst;
						int sortIdDst;
						if(dr.Read() == true && dr["CatID"] != DBNull.Value)
						{
							catIdDst = Convert.ToInt32(dr["CatID"]);
							sortIdDst = Convert.ToInt32(dr["SortID"]);
							dr.Close();
							//位置交換処理
							//下を上へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@SortID", sortId);
							sql.AddParam("@CatID", catIdDst);
							sql.CommandTrans(cmdTxt);
							#endregion
							//上を下へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@SortID", sortIdDst);
							sql.AddParam("@CatID", id);
							sql.CommandTrans(cmdTxt);
							#endregion
						}
						else
						{
							//自分の弟が存在しない場合
							dr.Close();
							//親の親のIDと親の順番を取得
							int parentParentId = -1;
							int parentSortId = -1;
							#region
							cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@CatID", parentId.ToString());
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								parentSortId = Convert.ToInt32(dr["SortID"]);
								parentParentId = Convert.ToInt32(dr["ParentID"]);
							}
							dr.Close();
							if(parentSortId == -1)
							{
								sql.CommitTrans();
								return;
							}
							#endregion
							//親と同一階層で、一つ下のﾂﾘｰの番号を取得
							int dstParentCatId = -1;
							#region
							cmdTxt = "SELECT CatID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID AND SortID > @SortID ORDER BY SortID ASC";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", parentParentId);
							sql.AddParam("@SortID", parentSortId);
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								dstParentCatId = Convert.ToInt32(dr["CatID"]);
								dr.Close();
							}
							else
							{
								dr.Close();
								sql.CommitTrans();
								return;
							}
							#endregion
							//一番先頭に移動
							int dstParentSortId = 0;
							//移動先のﾂﾘｰのSortIDを全て+1する
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=SortID+1 WHERE UserID=@UserID AND ParentID=@ParentID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", dstParentCatId);
							sql.CommandTrans(cmdTxt);
							#endregion
							//位置移動処理
							//上を下へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET ParentID=@ParentID, SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", dstParentCatId);
							sql.AddParam("@SortID", dstParentSortId);
							sql.AddParam("@CatID", id);
							sql.CommandTrans(cmdTxt);
							#endregion
						}
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ﾂﾘｰ表示更新
				Common.ScriptStart();
				Common.ScriptOutput("parent.ltree.location='./CatTree.aspx'");
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ↑ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-007］private void ButtonUp_Click
        #region private void ButtonUp_Click(object sender, System.EventArgs e)
		private void ButtonUp_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//自PrentID/SortIDを取得
						int sortId = -1;
						int parentId = -1;
						#region
						cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@CatID", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							sortId = Convert.ToInt32(dr["SortID"]);
							parentId = Convert.ToInt32(dr["ParentID"]);
						}
						dr.Close();
						if(sortId == -1)
						{
							sql.CommitTrans();
							return;
						}
						#endregion
						//自ParentIDと同じParentIDで自SortIDよりも小さいSortIDで一番大きいSortIDのCatIDを取得する
						#region
						cmdTxt = "SELECT CatID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID AND SortID < @SortID ORDER BY SortID DESC";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentId);
						sql.AddParam("@SortID", sortId);
						dr = sql.ReadTrans(cmdTxt);
						#endregion
						int catIdDst;
						int sortIdDst;
						if(dr.Read() == true && dr["CatID"] != DBNull.Value)
						{
							catIdDst = Convert.ToInt32(dr["CatID"]);
							sortIdDst = Convert.ToInt32(dr["SortID"]);
							dr.Close();

							//位置交換処理
							//上を下へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@SortID", sortId);
							sql.AddParam("@CatID", catIdDst);
							sql.CommandTrans(cmdTxt);
							#endregion
							//下を上へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@SortID", sortIdDst);
							sql.AddParam("@CatID", id);
							sql.CommandTrans(cmdTxt);
							#endregion
						}
						else
						{
							//自分の兄が存在しない場合
							dr.Close();
							//親の親のIDと親の順番を取得
							int parentParentId = -1;
							int parentSortId = -1;
							#region
							cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@CatID", parentId.ToString());
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								parentSortId = Convert.ToInt32(dr["SortID"]);
								parentParentId = Convert.ToInt32(dr["ParentID"]);
							}
							dr.Close();
							if(parentSortId == -1)
							{
								sql.CommitTrans();
								return;
							}
							#endregion
							//親と同一階層で、一つ上のﾂﾘｰの番号を取得
							int dstParentCatId = -1;
							#region
							cmdTxt = "SELECT CatID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID AND SortID < @SortID ORDER BY SortID DESC";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", parentParentId);
							sql.AddParam("@SortID", parentSortId);
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								dstParentCatId = Convert.ToInt32(dr["CatID"]);
								dr.Close();
							}
							else
							{
								dr.Close();
								sql.CommitTrans();
								return;

							}
							#endregion
							//そのﾂﾘｰの子ﾂﾘｰの最大SortID + 1を取得
							int dstParentSortId = -1;
							#region
							cmdTxt = "SELECT SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID ORDER BY SortID DESC";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", dstParentCatId);
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								dstParentSortId = Convert.ToInt32(dr["SortID"]) + 1;
								dr.Close();
							}
							else
							{
								dstParentSortId = 0;
								dr.Close();
							}
							#endregion
							//位置移動処理
							//下を上へ
							#region
							cmdTxt = "UPDATE T_ObsCategoryTreeWk SET ParentID=@ParentID, SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@ParentID", dstParentCatId);
							sql.AddParam("@SortID", dstParentSortId);
							sql.AddParam("@CatID", id);
							sql.CommandTrans(cmdTxt);
							#endregion
						}
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ﾂﾘｰ表示更新
				Common.ScriptStart();
				Common.ScriptOutput("parent.ltree.location='./CatTree.aspx'");
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion
		
		/// <summary>
		/// ←ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-008］private void ButtonLeft_Click
        #region private void ButtonLeft_Click(object sender, System.EventArgs e)
		private void ButtonLeft_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
				//自分の親のParentIDにつなげる。
				//位置は自分のParentIDの次。
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						int parentID = -9;			//選択行の親ID >= -1
						int parentParentID = -9;	//選択行の親の親ID
						int parentSortID = -9;		//選択行の親のSortID
						//自分のParentIDを取得
						#region
						cmdTxt = "SELECT ParentID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@CatID", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							parentID = Convert.ToInt32(dr["ParentID"]);
						}
						dr.Close();
						if(parentID == -9)
						{
							return;
						}
						#endregion

						//自分の親のParentIDとSortIDを取得
						#region
						cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@CatID", parentID);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							parentParentID = Convert.ToInt32(dr["ParentID"]);
							parentSortID = Convert.ToInt32(dr["SortID"]);
						}
						dr.Close();
						if(parentParentID == -9) return;
						#endregion

						//ParentIDがparentParentIDで、SortIDがSortIDより大きいﾃﾞｰﾀのSortIDを+1する
						#region
						cmdTxt = "UPDATE T_ObsCategoryTreeWk SET SortID=SortID+1 WHERE UserID=@UserID AND ParentID=@ParentID AND SortID>@SortID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentParentID);
						sql.AddParam("@SortID", parentSortID);
						sql.CommandTrans(cmdTxt);
						#endregion

						//選択行を上位に移動、SortIDはparentSortID + 1とする
						#region
						cmdTxt = "UPDATE T_ObsCategoryTreeWk SET ParentID=@ParentID, SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentParentID);
						sql.AddParam("@SortID", parentSortID + 1);
						sql.AddParam("@CatID", id);
						sql.CommandTrans(cmdTxt);
						#endregion

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ﾂﾘｰ表示更新
				Common.ScriptStart();
				Common.ScriptOutput("parent.ltree.location='./CatTree.aspx'");
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// →ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-009］private void ButtonRight_Click
        #region private void ButtonRight_Click(object sender, System.EventArgs e)
		private void ButtonRight_Click(object sender, System.EventArgs e)
		{
			try
			{
				string id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
				//自分と同じParentIDを持ち、SortIDが自分より小さい直近のﾂﾘｰにぶら下がる。
				//位置はぶら下がるﾂﾘｰを親とするﾂﾘｰたちの一番最後
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						int parentID = -9;			//選択行の親ID
						int sortID = -9;			//選択行のSortID
						int parentParentID = -9;	//選択行の親のParentID
						int dstSortID = 0;			//移動先で自分がつけるSortID
						//自分のParentID、SortIDを取得
						#region
						cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@CatID", id);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							parentID = Convert.ToInt32(dr["ParentID"]);
							sortID = Convert.ToInt32(dr["SortID"]);
						}
						dr.Close();
						if(parentID == -9)
						{
							return;
						}
						#endregion

						//自分と同じ親を持つﾂﾘｰで、SortIDが自分の直前のものを取得
						#region
						cmdTxt = "SELECT CatID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID AND SortID < @SortID ORDER BY SortID DESC";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentID);
						sql.AddParam("@SortID", sortID);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							parentParentID = Convert.ToInt32(dr["CatID"]);
						}
						dr.Close();
						if(parentParentID == -9) return;
						#endregion

						//今度親になるCatIDを親に持つﾂﾘｰのSortIDの最大値を取得する
						#region
						cmdTxt = "SELECT MAX(SortID) AS SortID FROM T_ObsCategoryTreeWk WHERE UserID=@UserID AND ParentID=@ParentID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentParentID);
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true && dr["SortID"] != DBNull.Value)
						{
							dstSortID = Convert.ToInt32(dr["SortID"]) + 1;
						}
						dr.Close();
						#endregion

						//選択行を下位に移動、SortIDはparentSortID + 1とする
						#region
						cmdTxt = "UPDATE T_ObsCategoryTreeWk SET ParentID=@ParentID, SortID=@SortID WHERE UserID=@UserID AND CatID=@CatID";
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ParentID", parentParentID);
						sql.AddParam("@SortID", dstSortID);
						sql.AddParam("@CatID", id);
						sql.CommandTrans(cmdTxt);
						#endregion

						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//ﾂﾘｰ表示更新
				Common.ScriptStart();
				Common.ScriptOutput("parent.ltree.location='./CatTree.aspx'");
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 更新ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-02-010］private void ButtonRefresh_Click
        #region private void ButtonRefresh_Click(object sender, System.EventArgs e)
		private void ButtonRefresh_Click(object sender, System.EventArgs e)
		{
			//ﾂﾘｰ表示更新
			Common.ScriptStart();
			Common.ScriptOutput("parent.lbutton.location='./CatButton.aspx';");
			Common.ScriptOutput("parent.rbutton.location='./ObsButton.aspx';");
			Common.ScriptEnd();
		}
		#endregion



		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================


		/// <summary>
		/// ﾓｰﾄﾞ設定
		/// </summary>
		/// <param name="mode">0:通常 1:移動処理</param>
        //［SRC-03-04-02-011］private void SetMode
        #region private void SetMode(int mode)
		private void SetMode(int mode)
		{
			//ｶﾃｺﾞﾘ移動ﾓｰﾄﾞ設定
			Common.SetSession(SessionID.Obs_Tree_CatTree_Mode, mode);
			//画面更新
			Common.ScriptStart();
			Common.ScriptOutput("parent.lbutton.location='./CatButton.aspx';");
			Common.ScriptOutput("parent.rbutton.location='./ObsButton.aspx';");
			Common.ScriptEnd();
		}
		#endregion





	













	}
}
