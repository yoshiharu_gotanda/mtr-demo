using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Obs.Tree
{
	/// <summary>
	/// CatEdit の概要の説明です。
	/// </summary>
	public class CatEdit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox TextBoxCatName;
		protected System.Web.UI.WebControls.Label LabelCatName;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;
		protected System.Web.UI.WebControls.Button ButtonOK;

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		private Type mType = typeof(CatEdit);



		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-03-04-03-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					SqlDataReader dr;
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelCatName);
					Common.SetText(sql, this.mType, this.ButtonOK);
					Common.SetText(sql, this.mType, this.ButtonCancel);

					#endregion

					//ｶﾃｺﾞﾘ名取得
					cmdTxt = "SELECT CatName FROM T_ObsCategoryTree WHERE CatID=@CatID";
					sql.AddParam("@CatID", Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID));
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true)
					{
						this.TextBoxCatName.Text = dr["CatName"].ToString();
					}
					dr.Close();
				}
			}
		}
		#endregion

		/// <summary>
		/// OKﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-03-002］private void ButtonOK_Click
        #region private void ButtonOK_Click(object sender, System.EventArgs e)
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(Common.CnvToDB(this.TextBoxCatName.Text) == string.Empty)
				{
					Common.Alert(this, Common.GetText(this.mType, "AlertCatName"));
					return;
				}
				using(CSql sql = new CSql())
				{
					string cmdTxt;
					sql.Open();
					cmdTxt = "UPDATE T_ObsCategoryTree SET CatName=@CatName WHERE CatID=@CatID";
					sql.AddParam("@CatID", Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID));
					sql.AddParam("@CatName", Common.CnvToDB(this.TextBoxCatName.Text));
					sql.Command(cmdTxt);
				}
				//ﾂﾘｰ表示更新
				Common.ScriptStart();
				Common.ScriptOutput("self.opener.parent.lbutton.location='./CatButton.aspx';");
				Common.ScriptOutput("self.opener.parent.rbutton.location='./ObsButton.aspx';");
				//ｳｨﾝﾄﾞｳを閉じる
				Common.ScriptOutput("self.close();");
				//ｽｸﾘﾌﾟﾄ出力終了
				Common.ScriptEnd();
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 指定CatIDにつながるCatIDを取得する
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="alCatID"></param>
		/// <param name="catID"></param>
        //［SRC-03-04-03-003］private void GetChildTree
        #region private void GetChildTree(CSql sql, ArrayList alCatID, int catID)
		private void GetChildTree(CSql sql, ArrayList alCatID, int catID)
		{
			alCatID.Add(catID);
			string cmdTxt;
			SqlDataReader dr;
			ArrayList al = new ArrayList();
			cmdTxt = "SELECT CatID FROM T_ObsCategoryTree WHERE ParentID=@ParentID";
			sql.AddParam("@ParentID", catID);
			dr = sql.ReadTrans(cmdTxt);
			while(dr.Read() == true)
			{
				al.Add(dr["CatID"].ToString());
			}
			dr.Close();
			for(int i = 0; i < al.Count; i++)
			{
				this.GetChildTree(sql, alCatID, Convert.ToInt32(al[i]));
			}
		}
		#endregion


































	}
}
