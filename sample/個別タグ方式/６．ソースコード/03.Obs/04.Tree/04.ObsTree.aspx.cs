using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;

namespace EPSNet.Obs.Tree
{
	/// <summary>
	/// ObsTree の概要の説明です。
	/// </summary>
	public class ObsTree : System.Web.UI.Page
	{
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄｸﾗｽ
		///================================================================

		/// <summary>
		/// ﾂﾘｰ作成用情報保持ｸﾗｽ
		/// </summary>
		#region private class CTree
		private class CTree
		{
			public string ObsMgrID;
			public string MgrNo;
			public string Subject;
			public int SortID;
			public string ParentID;
			public int LevelID;

			public bool IsTopAtSameLevel = false;
			public bool IsLastAtSameLevel = false;
			public bool HasChildTree = false;
			public bool Finished = false;
			public ArrayList alParents = new ArrayList();
			public ArrayList alIsLastParents = new ArrayList();
			public ArrayList alIcon = new ArrayList();
			public string RcgStatus = string.Empty;
			public string Status = string.Empty;
			public string RegDateTime = string.Empty;//登録日時
			public bool HasFile; //添付ﾌｧｲﾙ有り/無しﾌﾗｸﾞ
			public CTree(object ObsMgrID, object MgrNo, object CatID, object Subject, object SortID, object ParentID, object Finished, object Status, object EntDt)
			{
				this.ObsMgrID = ObsMgrID.ToString().Trim();
				this.MgrNo = MgrNo.ToString();
				this.Subject = Subject.ToString();
				this.SortID = Convert.ToInt32(SortID);
				this.ParentID = ParentID.ToString().Trim();
				this.Finished = Convert.ToBoolean(Finished);
				string tmp = EntDt.ToString();
				if(tmp.Length == 14)
				this.RegDateTime = tmp.Substring(0,4) + "/" + tmp.Substring(4, 2) + "/" + tmp.Substring(6, 2) + " " + tmp.Substring(8, 2) + ":" + tmp.Substring(10, 2) + ":" + tmp.Substring(12, 2);
				this.Status = Status.ToString();
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(ObsTree);
		private ArrayList alTree = new ArrayList();
		private string mTreeTag = string.Empty;


		private string BeginningParentheses = string.Empty;			//【
		private string EndPparentheses = string.Empty;			//】

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-03-04-04-001]private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID) == null)
			{
				//Response.Write("Select Category");
			}
			else
			{
				if(this.IsPostBack == false)
				{
					string ObsMgrID = Request.QueryString["id"];
					if(ObsMgrID != null) //障害IDがQueryStringで渡されている時は、その値をｾｯｼｮﾝに保存し、ﾎﾞﾀﾝﾍﾟｰｼﾞを更新する
					{
						//選択されている障害IDをｾｯｼｮﾝに保存
						Common.SetSession(SessionID.Obs_Tree_ObsTree_SelObsID, ObsMgrID);
						//画面更新
						Common.ScriptStart();
						Common.ScriptOutput("parent.lbutton.location='./CatButton.aspx';");
						Common.ScriptOutput("parent.rbutton.location='./ObsButton.aspx';");
						Common.ScriptEnd();
					}
					else
					{
						//ﾂﾘｰﾀｸﾞ作成
						this.CreateTree();
					}
				}
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// ﾂﾘｰﾀｸﾞ作成
		/// ﾂﾘｰ作成に必要な幾つかの関数を呼び出し、最終的にﾂﾘｰﾀｸﾞの作成までを行う
		/// </summary>
        //［SRC-03-04-04-001］private void CreateTree
        #region private void CreateTree()
		private void CreateTree()
		{
			using(CSql sql = new CSql())
			{
				SqlDataReader dr;
				string cmdTxt;
				sql.Open();
				//承認状態文字列取得
				string rcgStatus1 = Common.GetText(sql, typeof(Common), "RcgStatus1");
				string rcgStatus2 = Common.GetText(sql, typeof(Common), "RcgStatus2");
				string rcgStatus3 = Common.GetText(sql, typeof(Common), "RcgStatus3");

				//【】文字列取得
				BeginningParentheses = Common.GetText(sql, typeof(ObsTree), "BeginningParentheses");
				EndPparentheses = Common.GetText(sql, typeof(ObsTree), "EndPparentheses");
			
				//ｶﾃｺﾞﾘ名取得
				cmdTxt = "SELECT CatName FROM T_ObsCategoryTree WHERE CatID=@CatID";
				sql.AddParam("@CatID", Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID));
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true)
				{
					//ｶﾃｺﾞﾘ追加
					this.alTree.Add(new CTree("-1", string.Empty, -1, dr["CatName"], 0, -2, false, string.Empty, string.Empty));
				}
				else
				{
					//ｶﾃｺﾞﾘ追加
					this.alTree.Add(new CTree("-1", string.Empty, -1, "-", 0, -2, false, string.Empty, string.Empty));
				}
				dr.Close();
				//0:通常 1:移動
				int mode = Convert.ToInt32(Common.GetSession(SessionID.Obs_Tree_ObsTree_Mode));
				//全障害情報取得
				if(mode == 0) //通常ﾓｰﾄﾞ
				{
					cmdTxt = "SELECT T.*, M.*, S.Finished, S.Text" + Common.LangID.ToString() + " AS StatusName";
					cmdTxt += ",(SELECT COUNT(*) FROM T_ObsMgr_Lib WHERE ObsMgrID=T.ObsMgrID) AS CNT";
					cmdTxt += ",(SELECT TOP 1 M.FullName FROM T_ObsMgr_RcgUsr AS R LEFT JOIN M_users AS M ON M.UserID=R.RcgUserID WHERE ObsMgrID=T.ObsMgrID ORDER BY Seq DESC) AS FullName";
					cmdTxt += ",(SELECT TOP 1 R.RcgDateTime FROM T_ObsMgr_RcgUsr AS R LEFT JOIN M_users AS M ON M.UserID=R.RcgUserID WHERE ObsMgrID=T.ObsMgrID ORDER BY Seq DESC) AS RcgDateTime";
					cmdTxt += " FROM T_ObsTree AS T";
					cmdTxt += " LEFT JOIN T_ObsMgr2 AS M ON M.ObsMgrID=T.ObsMgrID";
					cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=M.Status";
					cmdTxt += " WHERE T.CatID=@CatID ORDER BY T.SortID";
				}
				else
				{
					//移動処理中はﾜｰｸﾃｰﾌﾞﾙから情報を取得
					cmdTxt = "SELECT T.*, M.*, S.Finished, S.Text" + Common.LangID.ToString() + " AS StatusName";
					cmdTxt += ",(SELECT COUNT(*) FROM T_ObsMgr_Lib WHERE ObsMgrID=T.ObsMgrID) AS CNT";
					cmdTxt += ",(SELECT TOP 1 M.FullName FROM T_ObsMgr_RcgUsr AS R LEFT JOIN M_users AS M ON M.UserID=R.RcgUserID WHERE ObsMgrID=T.ObsMgrID ORDER BY Seq DESC) AS FullName";
					cmdTxt += ",(SELECT TOP 1 R.RcgDateTime FROM T_ObsMgr_RcgUsr AS R LEFT JOIN M_users AS M ON M.UserID=R.RcgUserID WHERE ObsMgrID=T.ObsMgrID ORDER BY Seq DESC) AS RcgDateTime";
					cmdTxt += " FROM T_ObsTreeWk AS T";
					cmdTxt += " LEFT JOIN T_ObsMgr2 AS M ON M.ObsMgrID=T.ObsMgrID";
					cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=M.Status";
					cmdTxt += " WHERE T.CatID=@CatID AND T.UserID=@UserID ORDER BY T.SortID";
					sql.AddParam("@UserID", Common.LoginID);
				}
				sql.AddParam("@CatID", Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID));
				dr = sql.Read(cmdTxt);
				while(dr.Read() == true)
				{
					//障害情報追加
					this.alTree.Add(new CTree(dr["ObsMgrID"], dr["MgrNo"], dr["CatID"], dr["Sbj"], dr["SortID"], dr["ParentID"], dr["Finished"], dr["StatusName"], dr["EntDt"]));
					CTree tree = (CTree)this.alTree[this.alTree.Count - 1];
					//添付ﾌｧｲﾙ有無設定
					#region
					if(dr["CNT"] != DBNull.Value && Convert.ToInt32(dr["CNT"]) > 0)
					{
						tree.HasFile = true;
					}
					else
					{
						tree.HasFile = false;
					}
					#endregion
					//承認状態設定
					#region
					if(dr["FullName"] == DBNull.Value)
					{
						//承認設定無し
						tree.RcgStatus = rcgStatus1;
					}
					else if(dr["RcgDateTime"] == DBNull.Value)
					{
						//承認待ち
						tree.RcgStatus = rcgStatus2 + "(" + HttpUtility.HtmlEncode(dr["FullName"].ToString()) + ")";
					}
					else
					{
						//承認済み
						tree.RcgStatus = rcgStatus3 + "(" + HttpUtility.HtmlEncode(dr["FullName"].ToString()) + ")";
					}
					#endregion
				}
				dr.Close();
			}
			//各ﾂﾘｰの情報を生成
			this.SetTreeInfo("-2", 0, new ArrayList(), new ArrayList());
			//ｱｲｺﾝ情報設定
			this.SetIcon();
			StringBuilder sb = new StringBuilder();
			//ﾀｸﾞ作成
			this.AddRow(sb, this.alTree, "-2");
			this.mTreeTag = sb.ToString();

			//<br>を<tr><td nowrap>〜</td></tr>に置換処理
			string newTag = string.Empty;
			newTag = "<table border=0 cellpadding=0 cellspacing=0><tr><td nowrap>";
			newTag += this.mTreeTag.Replace("<br>", "</td></tr><tr><td nowrap>");
			newTag += "</td></tr></table>";
			
			this.mTreeTag = newTag;
		}
		#endregion

		/// <summary>
		/// 各ﾂﾘｰの情報を生成
		/// 主に階層が関係する情報を作成する
		/// </summary>
		/// <param name="parentID"></param>
		/// <param name="levelID"></param>
		/// <param name="alParents"></param>
		/// <param name="alIsLastParents"></param>
		/// <returns></returns>
        //［SRC-03-04-04-002］private int SetTreeInfo
        #region private int SetTreeInfo(string parentID, int levelID, ArrayList alParents, ArrayList alIsLastParents)
		private int SetTreeInfo(string parentID, int levelID, ArrayList alParents, ArrayList alIsLastParents)
		{
			ArrayList al = this.GetTree(this.alTree, parentID);
			for(int i = 0; i < al.Count; i++)
			{
				CTree tree = (CTree)al[i];
				//同一親内での最後のTreeであるか
				if(i == al.Count - 1) tree.IsLastAtSameLevel = true;
				if(i == 0) tree.IsTopAtSameLevel = true;
				//LevelID設定
				tree.LevelID = levelID;
				//Parents情報設定
				for(int j = 0; j < alParents.Count; j++)
				{
					tree.alParents.Add(alParents[j]);
				}
				tree.alParents.Add(i + 1);
				ArrayList newParents = (ArrayList)alParents.Clone();
				newParents.Add(i + 1);
				//IsLastParentsﾌﾗｸﾞ設定
				bool IsLast = (i == al.Count - 1);
				for(int j = 0; j < alIsLastParents.Count; j++)
				{
					tree.alIsLastParents.Add(alIsLastParents[j]);
				}
				tree.alIsLastParents.Add(IsLast);
				ArrayList newIsLastParents = (ArrayList)alIsLastParents.Clone();
				newIsLastParents.Add(IsLast);
				//
				int childCnt = this.SetTreeInfo(tree.ObsMgrID, levelID + 1, newParents, newIsLastParents);
				if(childCnt != 0) tree.HasChildTree = true;
			}
			return al.Count;
		}
		#endregion

		/// <summary>
		/// ｱｲｺﾝ設定
		/// 階層の状態によりｱｲｺﾝ情報を設定する
		/// </summary>
        //［SRC-03-04-04-003］private void SetIcon
        #region private void SetIcon()
		private void SetIcon()
		{
			//0:通常 1:移動
			int mode = Convert.ToInt32(Common.GetSession(SessionID.Obs_Tree_ObsTree_Mode));

			for(int i = 0; i < this.alTree.Count; i++)
			{
				CTree tree = (CTree)this.alTree[i];
				//各ﾚﾍﾞﾙのｱｲｺﾝを設定する
				for(int j = 0; j < tree.LevelID; j++)
				{
					int sort = Convert.ToInt32(tree.alParents[j]);
					if(j == tree.LevelID - 1)	//自分の所
					{
						
						if(tree.IsLastAtSameLevel == false) //下に存在する
						{
							tree.alIcon.Add("<img src='../../img/line02.gif' width=16px height=16px>");
						}
						else //自分で最後
						{
							tree.alIcon.Add("<img src='../../img/line03.gif' width=16px height=16px>");
						}
					}
					else
					{
						//親のﾚﾍﾞﾙ
						bool IsLast = Convert.ToBoolean(tree.alIsLastParents[j + 1]);
						if(IsLast == true)
						{
							tree.alIcon.Add("<img src='../../img/dot11.gif' width=16px height=16px>");
						}
						else
						{
							tree.alIcon.Add("<img src='../../img/line01.gif' width=16px height=16px>");
						}
					}
				}
			}
		}
		#endregion

		/// <summary>
		/// ﾀｸﾞ出力処理
		/// 実際にﾀｸﾞ文字列を作成する処理
		/// </summary>
		/// <param name="sb"></param>
		/// <param name="alTree"></param>
		/// <param name="parentID"></param>
        //［SRC-03-04-04-0014］private void AddRow
        #region private void AddRow(StringBuilder sb, ArrayList alTree, string parentID)
		private void AddRow(StringBuilder sb, ArrayList alTree, string parentID)
		{

			ArrayList al = this.GetTree(alTree, parentID);
			for(int i = 0; i < al.Count; i++)
			{
				CTree tree = (CTree)al[i];
				//ｱｲｺﾝ追加
				for(int j = 0; j < tree.alIcon.Count; j++)
				{
					sb.Append(tree.alIcon[j].ToString());
				}
				//0:通常 1:移動
				int mode = 0;
				if(Common.GetSession(SessionID.Obs_Tree_ObsTree_Mode).ToString() == "1" || Common.GetSession(SessionID.Obs_Tree_CatTree_Mode).ToString() == "1")
				{
					mode = 1;
				}
				object selection = Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID);
				//ｱｲｺﾝ設定
				string fileName;
				if(parentID == "-2")									//最上位
				{
					sb.Append("<img src='../../img/fold1.gif'/>");
				}
				else if(selection != null && selection.ToString() == tree.ObsMgrID.ToString())		//選択状態
				{
					//状態によりｱｲｺﾝを変える
					if(tree.Finished == true)	fileName = "finish2.gif";
					else						fileName = "subject2.gif";
					//移動処理中はﾌｫﾙﾀﾞ選択不可
					if(mode == 0)	sb.Append("<img src='../../img/" + fileName + "' onclick=\"SelectCat('" + tree.ObsMgrID.ToString() + "')\" class='Hand'/>");
					else			sb.Append("<img src='../../img/" + fileName + "'/>");
				}
				else
				{
					//状態によりｱｲｺﾝを変える
					if(tree.Finished == true)	fileName = "finish1.gif";
					else						fileName = "subject1.gif";
					//移動処理中はﾌｫﾙﾀﾞ選択不可
					if(mode == 0)	sb.Append("<img src='../../img/" + fileName + "' onclick=\"SelectCat('" + tree.ObsMgrID.ToString() + "')\" class='Hand'/>");
					else			sb.Append("<img src='../../img/" + fileName + "'/>");
				}
				//最上位はﾘﾝｸを張らない
				if(parentID == "-2")
				{
					sb.Append(HttpUtility.HtmlEncode(tree.Subject));
					sb.Append("<br>\r\n");
				}
				else
				{
					//(管理番号)件名【承認状態-ｽﾃｰﾀｽ-登録日時】
					//管理番号
					sb.Append("(");
					sb.Append(HttpUtility.HtmlEncode(tree.MgrNo));
					sb.Append(")");
					if(mode == 0)//通常ﾓｰﾄﾞ
					{
						//ﾘﾝｸ文字ｸﾘｯｸで障害詳細ｳｨﾝﾄﾞｳを表示
						sb.Append("<a href=\"javascript:OpenDetailWindow('" + tree.ObsMgrID.ToString() + "');\">");
						sb.Append(HttpUtility.HtmlEncode(tree.Subject) + "</a>");
						//承認状態-ｽﾃｰﾀｽ-登録日時
						sb.Append(BeginningParentheses + tree.RcgStatus + "-" + HttpUtility.HtmlEncode(tree.Status) + "-" + tree.RegDateTime + EndPparentheses);
					}
					else//移動ﾓｰﾄﾞ
					{
						sb.Append(HttpUtility.HtmlEncode(tree.Subject));
						//承認状態-ｽﾃｰﾀｽ-登録日時
						sb.Append(BeginningParentheses + tree.RcgStatus + "-" + HttpUtility.HtmlEncode(tree.Status) + "-" + tree.RegDateTime + EndPparentheses);
					}
					//添付ﾌｧｲﾙｱｲｺﾝ追加
					if(tree.HasFile == true)
					{
						sb.Append("<img src='../../img/ATTACH.GIF'>");
					}
					sb.Append("<br>\r\n");
				}
				this.AddRow(sb, alTree, tree.ObsMgrID);
			}
		}
		#endregion



		/// <summary>
		/// 指定した２つのCatIDが親子関係(孫、曾孫等も含む)にあたるかﾁｪｯｸ
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parentId"></param>
		/// <param name="childId"></param>
		/// <returns></returns>
        //［SRC-03-04-04-005］private bool IsSameTree
        #region private bool IsSameTree(CSql sql, string parentId, string childId)
		private bool IsSameTree(CSql sql, string parentId, string childId)
		{
			string cmdTxt;
			SqlDataReader dr;
			//parentIdにつながるﾂﾘｰを全て取得する
			ArrayList alTree = new ArrayList();
			cmdTxt = "SELECT CatID FROM T_ObsCategoryTree WHERE ParentID=@ParentID";
			sql.AddParam("@ParentID", parentId);
			dr = sql.ReadTrans(cmdTxt);
			while(dr.Read() == true)
			{
				string catId = dr["CatID"].ToString();
				if(catId == childId)
				{
					dr.Close();
					return true;
				}
				alTree.Add(catId);
			}
			dr.Close();
			for(int i = 0; i < alTree.Count; i++)
			{
				if(this.IsSameTree(sql, alTree[i].ToString(), childId) == true) return true;
			}
			return false;
		}
		#endregion

		/// <summary>
		/// 指定されたParentIDにぶら下がる全Treeを取得する
		/// </summary>
		/// <param name="alTree"></param>
		/// <param name="parentID"></param>
		/// <returns></returns>
        //［SRC-03-04-04-006］private ArrayList GetTree
        #region private ArrayList GetTree(ArrayList alTree, string parentID)
		private ArrayList GetTree(ArrayList alTree, string parentID)
		{
			ArrayList ret = new ArrayList();
			for(int i = 0; i < alTree.Count; i++)
			{
				CTree tree = (CTree)alTree[i];
				if(tree.ParentID == parentID)
				{
					ret.Add(tree);
				}
			}
			return ret;
		}
		#endregion



		///================================================================
		/// ﾌﾟﾛﾊﾟﾃｨ
		///================================================================

        //［SRC-03-04-04-007］protected string TreeTag
        #region protected string TreeTag
		protected string TreeTag
		{
			get
			{
				return this.mTreeTag;
			}
		}
		#endregion

















	}
}
