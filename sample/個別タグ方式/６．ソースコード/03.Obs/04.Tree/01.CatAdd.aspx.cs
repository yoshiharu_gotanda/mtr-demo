using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Obs.Tree
{
	/// <summary>
	/// CategoryDetail の概要の説明です。
	/// </summary>
	public class CatAdd : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox TextBoxCatName;
		protected System.Web.UI.WebControls.RadioButton RadioButtonChild;
		protected System.Web.UI.WebControls.RadioButton RadioButtonUp;
		protected System.Web.UI.WebControls.RadioButton RadioButtonDown;
		protected System.Web.UI.WebControls.Label LabelName;
		protected System.Web.UI.WebControls.Label LabelPos;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.HtmlControls.HtmlInputButton ButtonCancel;
		protected System.Web.UI.WebControls.Button ButtonOK;
	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================
		
		private Type mType = typeof(CatAdd);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-03-04-01-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack == false)
			{
				if(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID) == null)
				{
					this.RadioButtonUp.Enabled = this.RadioButtonDown.Enabled = this.RadioButtonChild.Enabled = false;
				}

				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelName);
					Common.SetText(sql, this.mType, this.LabelPos);
					Common.SetText(sql, this.mType, this.ButtonOK);
					Common.SetText(sql, this.mType, this.ButtonCancel);
					Common.SetText(sql, this.mType, this.RadioButtonUp);
					Common.SetText(sql, this.mType, this.RadioButtonDown);
					Common.SetText(sql, this.mType, this.RadioButtonChild);

					#endregion
				}
			}
		}
		#endregion

		/// <summary>
		/// OKﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-04-01-002］private void ButtonOK_Click
        #region private void ButtonOK_Click(object sender, System.EventArgs e)
		private void ButtonOK_Click(object sender, System.EventArgs e)
		{
			//ｶﾃｺﾞﾘ名入力ﾁｪｯｸ
			if(Common.CnvToDB(this.TextBoxCatName.Text) == string.Empty)
			{
				Common.Alert(this, Common.GetText(this.mType, "AlertCatName"));
				return;
			}
			//CatTree存在ﾁｪｯｸ
			try
			{
				using(CSql sql = new CSql())
				{
					sql.Open();
					sql.BeginTrans();
					try
					{
						if(this.RadioButtonDown.Checked == true)
						{
							this.AddDownPos(sql);
						}
						else if(this.RadioButtonUp.Checked == true)
						{
							this.AddUpPos(sql);
						}
						else
						{
							this.AddChild(sql);
						}
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
				return;
			}
			//ﾂﾘｰ表示更新
			Common.ScriptStart();
			Common.ScriptOutput("self.opener.parent.lbutton.location='./CatButton.aspx';");
			Common.ScriptOutput("self.opener.parent.rbutton.location='./ObsButton.aspx';");
			//ｳｨﾝﾄﾞｳを閉じる
			Common.ScriptOutput("self.close();");
			Common.ScriptEnd();
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 下に追加
		/// </summary>
        //［SRC-03-04-01-003］private void AddDownPos
        #region private void AddDownPos(CSql sql)
		private void AddDownPos(CSql sql)
		{
			string id = "-1";
			int parentID = -1;
			int sortID = 0;

			if(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID) != null) id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();

			string cmdTxt;
			SqlDataReader dr;
			int catID = 0;
			//追加用catID取得
			#region
			cmdTxt = "SELECT CatID FROM T_ObsCategoryTree ORDER BY CatID DESC";
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true) catID = Convert.ToInt32(dr["CatID"]) + 1;
			dr.Close();
			#endregion

			//ParentID,SortIDを取得
			#region
			if(id != "-1")
			{
				cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTree WHERE CatID=@CatID";
				sql.AddParam("@CatID", id);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					parentID = Convert.ToInt32(dr["ParentID"]);
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			else
			{
				cmdTxt = "SELECT SortID FROM T_ObsCategoryTree WHERE ParentID=-1 ORDER BY SortID DESC";
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			#endregion

			//同一で自分より後ろの全てのﾂﾘｰのSortIDを+1する
			#region
			cmdTxt = "UPDATE T_ObsCategoryTree SET SortID=SortID + 1 WHERE ParentID=@ParentID AND SortID>@SortID";
			sql.AddParam("@ParentID", parentID);
			sql.AddParam("@SortID", sortID);
			sql.CommandTrans(cmdTxt);
			#endregion

			//ｶﾃｺﾞﾘｰ追加
			#region
			cmdTxt = "INSERT INTO T_ObsCategoryTree(CatID,CatName,SortID,ParentID)VALUES(@CatID,@CatName,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@CatName", Common.CnvToDB(this.TextBoxCatName.Text));
			sql.AddParam("@SortID", sortID + 1);
			sql.AddParam("@ParentID", parentID);
			sql.CommandTrans(cmdTxt);
			#endregion

			sql.CommitTrans();
		}
		#endregion

		/// <summary>
		/// 上に追加
		/// </summary>
        //［SRC-03-04-01-004］private void AddUpPos
        #region private void AddUpPos(CSql sql)
		private void AddUpPos(CSql sql)
		{
			string id = "-1";
			int parentID = -1;
			int sortID = 0;

			if(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID) != null) id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();

			string cmdTxt;
			SqlDataReader dr;
			int catID = 0;
			//追加用catID取得
			#region
			cmdTxt = "SELECT CatID FROM T_ObsCategoryTree ORDER BY CatID DESC";
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true) catID = Convert.ToInt32(dr["CatID"]) + 1;
			dr.Close();
			#endregion

			//ParentID,SortIDを取得
			#region
			if(id != "-1")
			{
				cmdTxt = "SELECT ParentID, SortID FROM T_ObsCategoryTree WHERE CatID=@CatID";
				sql.AddParam("@CatID", id);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					parentID = Convert.ToInt32(dr["ParentID"]);
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			#endregion

			//同一の親を持つ全てのﾂﾘｰのSortIDを+1する
			#region
			cmdTxt = "UPDATE T_ObsCategoryTree SET SortID=SortID + 1 WHERE ParentID=@ParentID AND SortID >= @SortID";
			sql.AddParam("@ParentID", parentID);
			sql.AddParam("@SortID", sortID);
			sql.CommandTrans(cmdTxt);
			#endregion

			//ｶﾃｺﾞﾘｰ追加
			#region
			cmdTxt = "INSERT INTO T_ObsCategoryTree(CatID,CatName,SortID,ParentID)VALUES(@CatID,@CatName,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@CatName", Common.CnvToDB(this.TextBoxCatName.Text));
			sql.AddParam("@SortID", sortID);
			sql.AddParam("@ParentID", parentID);
			sql.CommandTrans(cmdTxt);
			#endregion

			sql.CommitTrans();
		}
		#endregion

		/// <summary>
		/// 子ﾂﾘｰとして追加
		/// </summary>
        //［SRC-03-04-01-005］private void AddChild
        #region private void AddChild(CSql sql)
		private void AddChild(CSql sql)
		{
			string id = "-1";
			if(Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID) != null) id = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();

			string cmdTxt;
			SqlDataReader dr;
			int catID = 0;
			//追加用catID取得
			#region
			cmdTxt = "SELECT CatID FROM T_ObsCategoryTree ORDER BY CatID DESC";
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true) catID = Convert.ToInt32(dr["CatID"]) + 1;
			dr.Close();
			#endregion

			//同一階層の最大SortIDを取得
			#region
			int sortID = 0;
			cmdTxt = "SELECT MAX(SortID) AS SortID FROM T_ObsCategoryTree WHERE ParentID=@ParentID";
			sql.AddParam("@ParentID", id);
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true && dr["SortID"] != DBNull.Value)
			{
				sortID = Convert.ToInt32(dr["SortID"]) + 1;
			}
			dr.Close();
			#endregion

			//ｶﾃｺﾞﾘｰ追加
			#region
			cmdTxt = "INSERT INTO T_ObsCategoryTree(CatID,CatName,SortID,ParentID)VALUES(@CatID,@CatName,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@CatName", Common.CnvToDB(this.TextBoxCatName.Text));
			sql.AddParam("@SortID", sortID);
			sql.AddParam("@ParentID", id);
			sql.CommandTrans(cmdTxt);
			#endregion

			sql.CommitTrans();
		}
		#endregion
















































	}
}
