using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;

namespace EPSNet.Obs.Search
{
	/// <summary>
	/// Condition の概要の説明です。
	/// </summary>
	public class List : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
	
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
			this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			this.DataGrid1.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemCreated);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.ButtonAddSort.Click += new System.EventHandler(this.ButtonAddSort_Click);
			this.ButtonDelSort.Click += new System.EventHandler(this.ButtonDelSort_Click);
			this.DataGrid2.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid2_ItemCreated);
			this.DataGrid2.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid2_ItemDataBound);
			this.DataGrid2.SelectedIndexChanged += new System.EventHandler(this.DataGrid2_SelectedIndexChanged);
			this.ButtonCSV.Click += new System.EventHandler(this.ButtonCSV_Click);
			this.ID = "Index2";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Button ButtonDelete;
		protected System.Web.UI.HtmlControls.HtmlInputHidden ServerName;
		protected System.Web.UI.WebControls.Button ButtonSearch;
		protected System.Web.UI.WebControls.DataGrid DataGrid2;
		protected System.Web.UI.WebControls.Button ButtonAddSort;
		protected System.Web.UI.WebControls.Button ButtonDelSort;
		protected System.Web.UI.WebControls.Label LabelSearch;
		protected System.Web.UI.WebControls.Label LabelSort;
		protected System.Web.UI.WebControls.Button ButtonCSV;
		protected System.Web.UI.WebControls.Label LabelTitle2;
		protected System.Web.UI.WebControls.DataGrid DataGridResult;
		protected System.Web.UI.WebControls.Label LabelTitle1;
		protected System.Web.UI.WebControls.Label LabelCnt;
		protected System.Web.UI.WebControls.Label LabelResultTitle;
		protected System.Web.UI.WebControls.Panel Panel1;



		//-------------------------------------------------------------
		// 【TargetObj】
		// 1:承認者 2:起票者 3:状態 4:件名 5:ﾌﾟﾛｼﾞｪｸﾄ 6:内容 7:日付(相対) 8:日付(絶対) 9:[ﾃｷｽﾄ型] 10:[ﾘｽﾄ型]
		// 11:[数値型] 12:[整数型] 13:[日付型](相対) 14:[日付型](絶対) 15:[ﾕｰｻﾞ型] 16:[部門型] 17:ﾒｰﾙ送信対象者 18:管理番号
		//-------------------------------------------------------------

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ定数
		///================================================================

		//検索結果一覧用
		private const int COL_MgrNo = 0;
		private const int COL_CatName = 1;
		private const int COL_Subject = 2;
		private const int COL_RcgStatus = 3;
		private const int COL_Status = 4;
		private const int COL_Date = 5;
		//検索条件一覧用
		private const int COL_Group = 1;
		private const int COL_Disabled = 6;


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(List);

		private DataTable mTable;
		private DataTable mSortTable;
		private ArrayList alAndOr;						//かつ/または
		private ArrayList alSearchItem;					//固定検索項目
		private ArrayList alSortItem;					//固定ｿｰﾄ項目
		private ArrayList alOrder;						//昇順・降順
		private ArrayList alYesNo;
		private ArrayList alDateOption;					//相対値/絶対値
		private string mGroup;							//ｸﾞﾙｰﾌﾟ文字列
		private string mCondition = string.Empty;		//検索条件文字列(WHERE句)
		private string mSortCondition = string.Empty;	//ｿｰﾄ条件文字列

		#region private enum ConditionItemValue
		private enum ConditionItemValue
		{
			承認者 = 10000,
			起票者,
			状態,
			件名,
			プロジェクト名,
			内容,
			日付,
			メール送信対象者,
			管理番号,
			カテゴリ名,
			ステータス,
		}
		#endregion



		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
        //［SRC-03-03-01-001］private void Page_Load
        #region private void Page_Load(object sender, System.EventArgs e)
		private void Page_Load(object sender, System.EventArgs e)
		{
			//LoginID取得
			string id = Request.QueryString["UserName"];
			if(id == null && Common.LoginID != null) id = Common.LoginID;
			if(id == null)
			{
				//ｴﾗｰﾍﾟｰｼﾞへ遷移
				Response.Write("<script>");
				Response.Write("document.location='../../Common/Message.aspx'");
				Response.Write("</script>");
			}
			else
			{
				Common.SetSession(SessionID.LoginID, id);
				Common.GetLanguage();
			}

			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					sql.Open();
					//ｻｰﾊﾞｰ名取得(ｶﾚﾝﾀﾞｰ表示の為)
					this.ServerName.Value = Common.GetHttpHost(sql);

					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle1);
					Common.SetText(sql, this.mType, this.LabelSearch);
					Common.SetText(sql, this.mType, this.LabelSort);
					Common.SetText(sql, this.mType, this.ButtonSearch);
					Common.SetText(sql, this.mType, this.ButtonAdd);
					Common.SetText(sql, this.mType, this.ButtonDelete);
					Common.SetText(sql, this.mType, this.ButtonAddSort);
					Common.SetText(sql, this.mType, this.ButtonDelSort);

					(this.DataGrid1.Columns[0] as ButtonColumn).Text = Common.GetText(sql, this.mType, "SelectString");
					(this.DataGrid2.Columns[0] as ButtonColumn).Text = Common.GetText(sql, this.mType, "SelectString");
					this.DataGrid1.Columns[COL_Disabled].HeaderText = Common.GetText(sql, this.mType, "DisabledString");
					//this.DataGrid1.Columns[COL_Group].HeaderText = Common.GetText(sql, this.mType, "StrGroup");
					
					Common.SetText(sql, this.mType, this.LabelTitle2);
					Common.SetText(sql, this.mType, this.LabelResultTitle);
					Common.SetText(sql, this.mType, this.ButtonCSV);

					this.DataGridResult.Columns[COL_MgrNo].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGridResult.Columns[COL_CatName].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGridResult.Columns[COL_Subject].HeaderText = Common.GetText(sql, this.mType, "Col3");
					this.DataGridResult.Columns[COL_RcgStatus].HeaderText = Common.GetText(sql, this.mType, "Col4");
					this.DataGridResult.Columns[COL_Status].HeaderText = Common.GetText(sql, this.mType, "Col5");
					this.DataGridResult.Columns[COL_Date].HeaderText = Common.GetText(sql, this.mType, "Col6");
					
					#endregion

					//検索条件をDBから取得する
					#region
					string cmdTxt;
					SqlDataReader dr;
					try
					{
						cmdTxt = "SELECT * FROM T_ObsSearchCondition WHERE UserID=@UserID ORDER BY ConditionID";
						sql.AddParam("@UserID", Common.LoginID);
						dr = sql.Read(cmdTxt);
						this.mTable = this.CreateTable();
						while(dr.Read() == true)
						{
							DataRow row = this.mTable.NewRow();
							switch(Convert.ToInt32(dr["TargetObj"]))
							{
								case 1://承認者
								{
									#region
									row["Item"] = (int)ConditionItemValue.承認者;
									row["Index"] = dr["ConditionData1"];
									break;
									#endregion
								}
								case 2://起票者
								{
									#region
									row["Item"] = (int)ConditionItemValue.起票者;
									row["Index"] = dr["ConditionData2"];
									break;
									#endregion
								}
								case 3://状態
								{
									#region
									row["Item"] = (int)ConditionItemValue.状態;
									row["Index"] = dr["ConditionData3"];
									break;
									#endregion
								}							
								case 4://件名
								{
									#region
									row["Item"] = (int)ConditionItemValue.件名;
									row["Text"] = dr["ConditionData4"];
									break;
									#endregion
								}							
								case 5://ﾌﾟﾛｼﾞｪｸﾄ名
								{
									#region
									row["Item"] = (int)ConditionItemValue.プロジェクト名;
									row["Index"] = dr["ConditionData5"];
									break;
									#endregion
								}							
								case 6://内容
								{
									#region
									row["Item"] = (int)ConditionItemValue.内容;
									row["Text"] = dr["ConditionData6"];
									break;
									#endregion
								}							
								case 7://日付(相対)
								{
									#region
									row["Item"] = (int)ConditionItemValue.日付;
									row["Index2"] = "0";
									DateTime dt = DateTime.Today;
									dt = dt.AddDays(Convert.ToInt32(dr["ConditionData7"]));
									row["Text"] = dt.ToString("yyyy/MM/dd");
									break;
									#endregion
								}
								case 8://日付(絶対)
								{
									#region
									row["Item"] = (int)ConditionItemValue.日付;
									row["Index2"] = "1";
									row["Text"] = Convert.ToDateTime(dr["ConditionData8"]).ToString("yyyy/MM/dd");
									break;
									#endregion
								}
								case 9://[ﾃｷｽﾄ型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Text"] = dr["ConditionData9"];
									break;
									#endregion
								}
								case 10://[ﾘｽﾄ型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Index"] = dr["ConditionData10"];
									break;
									#endregion
								}
								case 11://[数値型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Text"] = dr["ConditionData11"];
									break;
									#endregion
								}
								case 12://[整数型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Text"] = dr["ConditionData12"];
									break;
									#endregion
								}
								case 13://日付(相対)
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Index2"] = "0";
									DateTime dt = DateTime.Today;
									dt = dt.AddDays(Convert.ToInt32(dr["ConditionData13"]));
									row["Text"] = dt.ToString("yyyy/MM/dd");
									break;
									#endregion
								}
								case 14://日付(絶対)
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Index2"] = "1";
									row["Text"] = Convert.ToDateTime(dr["ConditionData14"]).ToString("yyyy/MM/dd");
									break;
									#endregion
								}
								case 15://[ﾕｰｻﾞ型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Index"] = dr["ConditionData15"];
									break;
									#endregion
								}									
								case 16://[部門型]
								{
									#region
									row["Item"] = dr["ItemID"];
									row["Index"] = dr["ConditionData16"];
									break;
									#endregion
								}
								case 17://メール送信対象者
								{
									#region
									row["Item"] = (int)ConditionItemValue.メール送信対象者;
									row["Index"] = dr["ConditionData17"];
									break;
									#endregion
								}
								case 18://管理番号
								{
									#region
									row["Item"] = (int)ConditionItemValue.管理番号;
									row["Text"] = dr["ConditionData18"];
									break;
									#endregion
								}
								case 19://ｶﾃｺﾞﾘ名
								{
									#region
									row["Item"] = (int)ConditionItemValue.カテゴリ名;
									row["Index"] = dr["ConditionData19"];
									break;
									#endregion
								}
								case 20://ｽﾃｰﾀｽ
								{
									#region
									row["Item"] = (int)ConditionItemValue.ステータス;
									row["Index"] = dr["ConditionData20"];
									break;
									#endregion
								}
							}
							//共通項目ﾃﾞｰﾀ設定
							row["Group"] = Convert.ToInt32(dr["Group"]);
							row["AndOr"] = Convert.ToInt32(dr["IsLogAnd"]);
							row["YesNo"] = Convert.ToInt32(dr["IsSame"]);
							row["IsValid"] = Convert.ToBoolean(dr["DisabledFlag"]);
							
							this.mTable.Rows.Add(row);
						}
						dr.Close();
					}
					catch(Exception ex)
					{
						Common.Alert(this, ex.Message);
					}
					#endregion

					this.DataGrid1.DataSource = this.mTable;
					this.DataGrid1.DataBind();
					this.SetItemControlVisible();
					if(this.mTable.Rows.Count >= 10) this.ButtonAdd.Enabled = false;
					if(this.DataGrid1.Items.Count == 0) this.DataGrid1.Visible = false;

					//ｿｰﾄ条件をDBから取得する
					#region
					try
					{
						cmdTxt = "SELECT * FROM T_ObsSearchSortCondition WHERE UserID=@UserID ORDER BY SortConditionID";
						sql.AddParam("@UserID", Common.LoginID);
						dr = sql.Read(cmdTxt);
						this.mSortTable = new DataTable();
						this.mSortTable.Columns.Add("SortItem", typeof(int));
						this.mSortTable.Columns.Add("Sort", typeof(int));
						while(dr.Read() == true)
						{
							DataRow row = this.mSortTable.NewRow();

							row["Sort"] = Convert.ToInt32(dr["IsDesc"]);

							switch(Convert.ToInt32(dr["TargetObj"]))
							{
								case 1://承認者
								{
									row["SortItem"] = (int)ConditionItemValue.承認者;
									break;
								}
								case 2://起票者
								{
									row["SortItem"] = (int)ConditionItemValue.起票者;
									break;
								}
								case 3://状態
								{
									row["SortItem"] = (int)ConditionItemValue.状態;
									break;
								}							
								case 4://件名
								{
									row["SortItem"] = (int)ConditionItemValue.件名;
									break;
								}							
								case 5://ﾌﾟﾛｼﾞｪｸﾄ名
								{
									row["SortItem"] = (int)ConditionItemValue.プロジェクト名;
									break;
								}							
								case 7://日付
								{
									row["SortItem"] = (int)ConditionItemValue.日付;
									break;
								}
								case 10://[ﾘｽﾄ型]
								case 11://[数値型]
								case 12://[整数型]
								case 13://[日付型]
								case 15://[ﾕｰｻﾞ型]
								case 16://[部門型]
								{
									row["SortItem"] = dr["ItemID"];
									break;
								}
								case 18://管理番号
								{
									row["SortItem"] = (int)ConditionItemValue.管理番号;
									break;
								}
								case 19://ｶﾃｺﾞﾘ名
								{
									row["SortItem"] = (int)ConditionItemValue.カテゴリ名;
									break;
								}
							}
							
							this.mSortTable.Rows.Add(row);
						}
						dr.Close();
					}
					catch(Exception ex)
					{
						Common.Alert(this, ex.Message);
					}

					#endregion
					this.DataGrid2.DataSource = this.mSortTable;
					this.DataGrid2.DataBind();
					if(this.mSortTable.Rows.Count >= 5) this.ButtonAddSort.Enabled = false;
					
				}
				//this.ButtonSearch_Click(null, null);
			}
			else
			{
				this.mTable = this.GetCurrentData();
				this.mSortTable = this.GetCurrentSortData();
			}
		}
		#endregion


		/// <summary>
		/// DataGrid1_ItemCreated
		/// </summary>
        //［SRC-03-03-01-002］private void DataGrid1_ItemCreated
        #region private void DataGrid1_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		private void DataGrid1_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				DropDownList AndOr = e.Item.FindControl("AndOr") as DropDownList;
				DropDownList Item = e.Item.FindControl("Item") as DropDownList;
				DropDownList Group = e.Item.FindControl("Group") as DropDownList;
				
				//条件項目が変更された時に発生するｲﾍﾞﾝﾄ
				switch(e.Item.ItemIndex)
				{
					case 0: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged1); break;
					case 1: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged2); break;
					case 2: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged3); break;
					case 3: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged4); break;
					case 4: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged5); break;
					case 5: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged6); break;
					case 6: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged7); break;
					case 7: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged8); break;
					case 8: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged9); break;
					case 9: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged10); break;
					case 10: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged11); break;
					case 11: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged12); break;
					case 12: Item.SelectedIndexChanged += new EventHandler(ConditionItem_SelectedIndexChanged13); break;
				}

				//検索条件一覧作成に必要なﾃｷｽﾄを取得する
				if(this.alAndOr == null) this.CreateLanguage();

				//固定項目のDropDownListﾃﾞｰﾀを設定する
				AndOr.Items.Add(new ListItem(this.alAndOr[0].ToString(), "0"));
				AndOr.Items.Add(new ListItem(this.alAndOr[1].ToString(), "1"));
				Item.Items.Add(new ListItem(this.alSearchItem[0].ToString(), ((int)ConditionItemValue.承認者).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[1].ToString(), ((int)ConditionItemValue.起票者).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[2].ToString(), ((int)ConditionItemValue.状態).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[3].ToString(), ((int)ConditionItemValue.件名).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[4].ToString(), ((int)ConditionItemValue.プロジェクト名).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[5].ToString(), ((int)ConditionItemValue.内容).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[6].ToString(), ((int)ConditionItemValue.日付).ToString()));
				Item.Items.Add(new ListItem(this.alSearchItem[8].ToString(), ((int)ConditionItemValue.管理番号).ToString()));//管理番号
				Item.Items.Add(new ListItem(this.alSearchItem[9].ToString(), ((int)ConditionItemValue.カテゴリ名).ToString()));//カテゴリ名
				Item.Items.Add(new ListItem(this.alSearchItem[10].ToString(), ((int)ConditionItemValue.ステータス).ToString()));//ステータス
				//ｸﾞﾙｰﾌﾟ
				for(int i = 1; i <= 10; i++)
				{
					Group.Items.Add(new ListItem(this.mGroup + i.ToString(), i.ToString()));
				}
				//可変項目ｱｲﾃﾑ追加
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();
					switch(Common.LangID)
					{
						case 1: cmdTxt = "SELECT ItemID, Japanese AS Text FROM M_ObsItem WHERE ItemID<>0 ORDER BY ItemID"; break;
						case 2: cmdTxt = "SELECT ItemID, English AS Text FROM M_ObsItem WHERE ItemID<>0 ORDER BY ItemID"; break;
					}
					dr = sql.Read(cmdTxt);
					while(dr.Read() == true)
					{
						Item.Items.Add(new ListItem(dr["Text"].ToString(), dr["ItemID"].ToString()));
					}
					dr.Close();
				}
				Item.Items.Add(new ListItem(this.alSearchItem[7].ToString(), ((int)ConditionItemValue.メール送信対象者).ToString()));
				//最初の行はAndOrは不要
				if(e.Item.ItemIndex == 0) AndOr.Visible = false;
			}
		}
		#endregion

		/// <summary>
		/// DataGrid2_ItemCreated
		/// </summary>
        //［SRC-03-03-01-003］private void DataGrid2_ItemCreated
        #region private void DataGrid2_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		private void DataGrid2_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				DropDownList SortItem = e.Item.FindControl("SortItem") as DropDownList;
				DropDownList Sort = e.Item.FindControl("Sort") as DropDownList;

				//検索条件一覧作成に必要なﾃｷｽﾄを取得する
				if(this.alAndOr == null) this.CreateLanguage();

				//固定項目は検索条件と少し異なる
				SortItem.Items.Add(new ListItem(this.alSortItem[0].ToString(), ((int)ConditionItemValue.承認者).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[1].ToString(), ((int)ConditionItemValue.起票者).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[2].ToString(), ((int)ConditionItemValue.状態).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[3].ToString(), ((int)ConditionItemValue.件名).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[4].ToString(), ((int)ConditionItemValue.プロジェクト名).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[5].ToString(), ((int)ConditionItemValue.日付).ToString()));
				SortItem.Items.Add(new ListItem(this.alSortItem[7].ToString(), ((int)ConditionItemValue.カテゴリ名).ToString()));//カテゴリ名でソート可能
				//可変項目ｱｲﾃﾑ追加
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();
					switch(Common.LangID)
					{
						case 1: cmdTxt = "SELECT ItemID, Japanese AS Text FROM M_ObsItem WHERE ItemID<>0 AND DataType<>1 ORDER BY ItemID"; break;
						case 2: cmdTxt = "SELECT ItemID, English AS Text FROM M_ObsItem WHERE ItemID<>0 AND DataType<>1  ORDER BY ItemID"; break;
					}
					dr = sql.Read(cmdTxt);
					while(dr.Read() == true)
					{
						SortItem.Items.Add(new ListItem(dr["Text"].ToString(), dr["ItemID"].ToString()));
					}
					dr.Close();
				}
				SortItem.Items.Add(new ListItem(this.alSortItem[6].ToString(), ((int)ConditionItemValue.管理番号).ToString()));

				Sort.Items.Add(new ListItem(this.alOrder[0].ToString(), "0"));
				Sort.Items.Add(new ListItem(this.alOrder[1].ToString(), "1"));
			}
		}
		#endregion

		/// <summary>
		/// DataGrid2_ItemDataBound
		/// </summary>
        //［SRC-03-03-01-004］private void DataGrid2_ItemDataBound
        #region private void DataGrid2_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		private void DataGrid2_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				//値を設定するｺﾝﾄﾛｰﾙを取得
				DropDownList SortItem = e.Item.FindControl("SortItem") as DropDownList;
				DropDownList Sort = e.Item.FindControl("Sort") as DropDownList;
				//値を取得
				DataRow row = this.mSortTable.Rows[e.Item.ItemIndex];
				//ｺﾝﾄﾛｰﾙに値を設定
				Common.SetValue(SortItem, row["SortItem"].ToString());
				Common.SetValue(Sort, row["Sort"].ToString());
			}
		}
		#endregion

		/// <summary>
		/// 検索条件追加ｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-005］private void ButtonAdd_Click
        #region private void ButtonAdd_Click(object sender, System.EventArgs e)
		private void ButtonAdd_Click(object sender, System.EventArgs e)
		{
			this.AddRow(this.mTable, 1, 0, ConditionItemValue.承認者, string.Empty, 0, 0, 0, false);
			this.DataGrid1.DataSource = this.mTable;
			this.DataGrid1.DataBind();
			this.SetItemControlVisible();
			//追加した行を選択する
			this.DataGrid1.SelectedIndex = this.DataGrid1.Items.Count - 1;
			//検索条件は10件まで
			if(this.DataGrid1.Items.Count == 10)	this.ButtonAdd.Enabled = false;
			else									this.ButtonAdd.Enabled = true;
			//追加直後は削除可能なはず
			this.ButtonDelete.Enabled = true;
			//DataGrid可視化
			this.DataGrid1.Visible = true;
		}
		#endregion

		/// <summary>
		/// 検索条件削除ｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-006］private void ButtonDelete_Click
        #region private void ButtonDelete_Click(object sender, System.EventArgs e)
		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			this.mTable.Rows.RemoveAt(this.DataGrid1.SelectedIndex);
			this.DataGrid1.DataSource = this.mTable;
			this.DataGrid1.DataBind();
			this.SetItemControlVisible();
			//削除後は選択状態をｸﾘｱ
			this.DataGrid1.SelectedIndex = -1;
			//未選択なので削除は不可
			this.ButtonDelete.Enabled = false;
			//削除直後は追加可能なはず
			this.ButtonAdd.Enabled = true;
			if(this.DataGrid1.Items.Count == 0) this.DataGrid1.Visible = false;
		}
		#endregion

		/// <summary>
		/// ｿｰﾄ条件追加ｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-007］private void ButtonAddSort_Click
        #region private void ButtonAddSort_Click(object sender, System.EventArgs e)
		private void ButtonAddSort_Click(object sender, System.EventArgs e)
		{
			DataRow row = this.mSortTable.NewRow();
			row["SortItem"] = ConditionItemValue.承認者;
			row["Sort"] = 0;
			this.mSortTable.Rows.Add(row);
			this.DataGrid2.DataSource = this.mSortTable;
			this.DataGrid2.DataBind();
			//追加したｱｲﾃﾑを選択する
			this.DataGrid2.SelectedIndex = this.DataGrid2.Items.Count - 1;
			//ｿｰﾄ条件は最大5件
			if(this.DataGrid2.Items.Count == 5)	this.ButtonAddSort.Enabled = false;
			else								this.ButtonAddSort.Enabled = true;
			//追加直後は削除可能なはず
			this.ButtonDelSort.Enabled = true;
		}
		#endregion

		/// <summary>
		/// ｿｰﾄ条件削除ｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-008］private void ButtonDelSort_Click
        #region private void ButtonDelSort_Click(object sender, System.EventArgs e)
		private void ButtonDelSort_Click(object sender, System.EventArgs e)
		{
			this.mSortTable.Rows.RemoveAt(this.DataGrid2.SelectedIndex);
			this.DataGrid2.DataSource = this.mSortTable;
			this.DataGrid2.DataBind();
			//削除後は選択状態をｸﾘｱする
			this.DataGrid2.SelectedIndex = -1;
			//未選択状態なので削除は不可
			this.ButtonDelSort.Enabled = false;
			//削除直後は追加可能なはず
			this.ButtonAddSort.Enabled = true;
		}
		#endregion

		/// <summary>
		/// 検索条件、ｱｲﾃﾑ種別変更ｲﾍﾞﾝﾄ
		/// </summary>
        //［SRC-03-03-01-009］private void ConditionItem_SelectedIndexChanged1
        #region private void ConditionItem_SelectedIndexChanged(object sender, EventArgs e)
		private void ConditionItem_SelectedIndexChanged1(object sender, EventArgs e)
		{
			this.SetItemControlVisible(0);
		}
		private void ConditionItem_SelectedIndexChanged2(object sender, EventArgs e)
		{
			this.SetItemControlVisible(1);
		}
		private void ConditionItem_SelectedIndexChanged3(object sender, EventArgs e)
		{
			this.SetItemControlVisible(2);
		}
		private void ConditionItem_SelectedIndexChanged4(object sender, EventArgs e)
		{
			this.SetItemControlVisible(3);
		}
		private void ConditionItem_SelectedIndexChanged5(object sender, EventArgs e)
		{
			this.SetItemControlVisible(4);
		}
		private void ConditionItem_SelectedIndexChanged6(object sender, EventArgs e)
		{
			this.SetItemControlVisible(5);
		}
		private void ConditionItem_SelectedIndexChanged7(object sender, EventArgs e)
		{
			this.SetItemControlVisible(6);
		}
		private void ConditionItem_SelectedIndexChanged8(object sender, EventArgs e)
		{
			this.SetItemControlVisible(7);
		}
		private void ConditionItem_SelectedIndexChanged9(object sender, EventArgs e)
		{
			this.SetItemControlVisible(8);
		}
		private void ConditionItem_SelectedIndexChanged10(object sender, EventArgs e)
		{
			this.SetItemControlVisible(9);
		}
		private void ConditionItem_SelectedIndexChanged11(object sender, EventArgs e)
		{
			this.SetItemControlVisible(10);
		}
		private void ConditionItem_SelectedIndexChanged12(object sender, EventArgs e)
		{
			this.SetItemControlVisible(11);
		}
		private void ConditionItem_SelectedIndexChanged13(object sender, EventArgs e)
		{
			this.SetItemControlVisible(12);
		}
		#endregion

		/// <summary>
		/// 行選択ｲﾍﾞﾝﾄ
		/// </summary>
        //［SRC-03-03-01-010］private void DataGrid1_SelectedIndexChanged
        #region private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.DataGrid1.SelectedIndex == -1 || this.DataGrid1.SelectedIndex >= this.DataGrid1.Items.Count)
			{
				this.ButtonDelete.Enabled = false;
			}
			else
			{
				this.ButtonDelete.Enabled = true;
			}
		}
		#endregion

		/// <summary>
		/// 行選択ｲﾍﾞﾝﾄ(ｿｰﾄ)
		/// </summary>
        //［SRC-03-03-01-011］private void DataGrid2_SelectedIndexChanged
        #region private void DataGrid2_SelectedIndexChanged(object sender, System.EventArgs e)
		private void DataGrid2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(this.DataGrid2.SelectedIndex == -1 || this.DataGrid2.SelectedIndex >= this.DataGrid2.Items.Count)
			{
				this.ButtonDelSort.Enabled = false;
			}
			else
			{
				this.ButtonDelSort.Enabled = true;
			}
		}
		#endregion

		/// <summary>
		/// 検索ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-012］private void ButtonSearch_Click
        #region private void ButtonSearch_Click(object sender, System.EventArgs e)
		private void ButtonSearch_Click(object sender, System.EventArgs e)
		{
			//入力ﾁｪｯｸ
			#region
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				for(int i = 0; i < this.mTable.Rows.Count; i++)
				{
					DataRow row = this.mTable.Rows[i];
					switch((ConditionItemValue)Convert.ToInt32(row["Item"]))
					{
						case ConditionItemValue.日付:
						{
							#region
							if(Common.IsDate(row["Text"].ToString()) == false)
							{
								Common.Alert(this, Common.GetText(sql, this.mType, "AlertDate"));
								return;
							}
							row["Text"] = Common.FormatDate(row["Text"].ToString());
							break;
							#endregion
						}
						default: //可変項目
						{
							#region
							cmdTxt = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
							sql.AddParam("@ItemID", row["Item"]);
							dr = sql.ReadTrans(cmdTxt);
							if(dr.Read() == true)
							{
								switch(Convert.ToInt32(dr["DataType"]))
								{
									case 3: //数値型
									{
										#region
										if(Common.IsNumber(row["Text"].ToString()) == false)
										{
											dr.Close();
											Common.Alert(this, Common.GetText(sql, this.mType, "AlertNumber"));
											return;
										}
										break;
										#endregion
									}
									case 4:	//整数型
									{
										#region
										if(Common.IsInteger(row["Text"].ToString()) == false)
										{
											dr.Close();
											Common.Alert(this, Common.GetText(sql, this.mType, "AlertInt"));
											return;
										}
										break;
										#endregion
									}
									case 5:	//日付型
									{
										#region
										if(Common.IsDate(row["Text"].ToString()) == false)
										{
											dr.Close();
											Common.Alert(this, Common.GetText(sql, this.mType, "AlertDate"));
											return;
										}
										row["Text"] = Common.FormatDate(row["Text"].ToString());
										break;
										#endregion
									}
								}
							}
							dr.Close();
							break;
							#endregion
						}
					}
				}
			}
			#endregion

			//条件文、ｿｰﾄ文を作成
			this.CreateSqlString();

			//検索条件をDBに保存する
			#region
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				sql.BeginTrans();
				try
				{
					//検索条件をDBに保存する
					#region
					//条件削除
					cmdTxt = "DELETE FROM T_ObsSearchCondition WHERE UserID=@UserID";
					sql.AddParam("@UserID", Common.LoginID);
					sql.CommandTrans(cmdTxt);
					for(int i = 0; i < this.mTable.Rows.Count; i++)
					{
						DataRow row = this.mTable.Rows[i];
						switch((ConditionItemValue)Convert.ToInt32(row["Item"]))
						{
							case ConditionItemValue.承認者:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData1,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData1,@DisabledFlag)";
								sql.AddParam("@TargetObj", 1);
								sql.AddParam("@ConditionData1", row["Index"]);
								break;
								#endregion
							}
							case ConditionItemValue.起票者:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData2,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData2,@DisabledFlag)";
								sql.AddParam("@TargetObj", 2);
								sql.AddParam("@ConditionData2", row["Index"]);
								break;
								#endregion
							}
							case ConditionItemValue.状態:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData3,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData3,@DisabledFlag)";
								sql.AddParam("@TargetObj", 3);
								sql.AddParam("@ConditionData3", row["Index"]);
								break;
								#endregion
							}							
							case ConditionItemValue.件名:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData4,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData4,@DisabledFlag)";
								sql.AddParam("@TargetObj", 4);
								sql.AddParam("@ConditionData4", row["Text"]);
								break;
								#endregion
							}							
							case ConditionItemValue.プロジェクト名:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData5,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData5,@DisabledFlag)";
								sql.AddParam("@TargetObj", 5);
								sql.AddParam("@ConditionData5", row["Index"]);
								break;
								#endregion
							}							
							case ConditionItemValue.内容:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData6,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData6,@DisabledFlag)";
								sql.AddParam("@TargetObj", 6);
								sql.AddParam("@ConditionData6", row["Text"]);
								break;
								#endregion
							}							
							case ConditionItemValue.日付:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								switch(Convert.ToInt32(row["Index2"]))
								{
									case 0://相対
									{
										cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData7,DisabledFlag)";
										cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData7,@DisabledFlag)";
										sql.AddParam("@TargetObj", 7);
										DateTime dt = Convert.ToDateTime(row["Text"]);
										TimeSpan ts = dt - DateTime.Today;
										int days = (int)ts.TotalDays;
										sql.AddParam("@ConditionData7", days);
										break;
									}
									case 1://絶対
									{
										cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData8,DisabledFlag)";
										cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData8,@DisabledFlag)";
										sql.AddParam("@TargetObj", 8);
										sql.AddParam("@ConditionData8", row["Text"]);
										break;
									}
								}
								break;
								#endregion
							}
							case ConditionItemValue.メール送信対象者:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData17,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData17,@DisabledFlag)";
								sql.AddParam("@TargetObj", 17);
								sql.AddParam("@ConditionData17", row["Index"]);
								break;
								#endregion
							}
							case ConditionItemValue.管理番号:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData18,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData18,@DisabledFlag)";
								sql.AddParam("@TargetObj", 18);
								sql.AddParam("@ConditionData18", row["Text"]);
								break;
								#endregion
							}
							case ConditionItemValue.カテゴリ名:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData19,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData19,@DisabledFlag)";
								sql.AddParam("@TargetObj", 19);
								sql.AddParam("@ConditionData19", row["Index"]);
								break;
								#endregion
							}
							case ConditionItemValue.ステータス:
							{
								#region
								cmdTxt = "INSERT INTO T_ObsSearchCondition";
								cmdTxt += "(UserID,ConditionID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData20,DisabledFlag)";
								cmdTxt += "VALUES(@UserID,@ConditioID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData20,@DisabledFlag)";
								sql.AddParam("@TargetObj", 20);
								sql.AddParam("@ConditionData20", row["Index"]);
								break;
								#endregion
							}
							default: //可変項目
							{
								#region
								cmdTxt = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
								sql.AddParam("@ItemID", row["Item"]);
								dr = sql.ReadTrans(cmdTxt);
								if(dr.Read() == true)
								{
									switch(Convert.ToInt32(dr["DataType"]))
									{
										case 1:	//ﾃｷｽﾄ型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData9,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData9,@DisabledFlag)";
											sql.AddParam("@TargetObj", 9);
											sql.AddParam("@ConditionData9", row["Text"]);
											break;
											#endregion
										}
										case 2:	//ﾘｽﾄ型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData10,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData10,@DisabledFlag)";
											sql.AddParam("@TargetObj", 10);
											sql.AddParam("@ConditionData10", row["Index"]);
											break;
											#endregion
										}
										case 3:	//数値型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData11,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData11,@DisabledFlag)";
											sql.AddParam("@TargetObj", 11);
											sql.AddParam("@ConditionData11", row["Text"]);
											break;
											#endregion
										}
										case 4:	//整数型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData12,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData12,@DisabledFlag)";
											sql.AddParam("@TargetObj", 12);
											sql.AddParam("@ConditionData12", row["Text"]);
											break;
											#endregion
										}
										case 5:	//日付型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											switch(Convert.ToInt32(row["Index2"]))
											{
												case 0://相対
												{
													cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData13,DisabledFlag)";
													cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData13,@DisabledFlag)";
													sql.AddParam("@TargetObj", 13);
													DateTime dt = Convert.ToDateTime(row["Text"]);
													TimeSpan ts = dt - DateTime.Today;
													int days = (int)ts.TotalDays;
													sql.AddParam("@ConditionData13", days);
													break;
												}
												case 1://絶対
												{
													cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData14,DisabledFlag)";
													cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData14,@DisabledFlag)";
													sql.AddParam("@TargetObj", 14);
													sql.AddParam("@ConditionData14", row["Text"]);
													break;
												}
											}
											break;
											#endregion
										}
										case 6:	//ﾕｰｻﾞ型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData15,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData15,@DisabledFlag)";
											sql.AddParam("@TargetObj", 15);
											sql.AddParam("@ConditionData15", row["Index"]);
											break;
											#endregion
										}
										case 7:	//部門型
										{
											#region
											cmdTxt = "INSERT INTO T_ObsSearchCondition";
											cmdTxt += "(UserID,ConditionID,ItemID,[Group],IsLogAnd,TargetObj,IsSame,ConditionData16,DisabledFlag)";
											cmdTxt += "VALUES(@UserID,@ConditioID,@ItemID,@Group,@IsLogAnd,@TargetObj,@IsSame,@ConditionData16,@DisabledFlag)";
											sql.AddParam("@TargetObj", 16);
											sql.AddParam("@ConditionData16", row["Index"]);
											break;
											#endregion
										}
									}
								}
								dr.Close();
								string item = row["Item"].ToString();
								sql.AddParam("@ItemID", row["Item"]);
								break;
								#endregion
							}
						}
						//共通項目ﾃﾞｰﾀ設定
						sql.AddParam("@UserID", Common.LoginID);
						sql.AddParam("@ConditioID", i);
						sql.AddParam("@Group", row["Group"]);
						sql.AddParam("@IsLogAnd", row["AndOr"]);
						sql.AddParam("@IsSame", row["YesNo"]);
						sql.AddParam("@DisabledFlag", row["IsValid"]);

						int g = Convert.ToInt32(row["Group"]);

						sql.CommandTrans(cmdTxt);
					}
					#endregion

					//Sort条件をDBに保存
					#region
					cmdTxt = "DELETE FROM T_ObsSearchSortCondition WHERE UserID=@UserID";
					sql.AddParam("@UserID", Common.LoginID);
					sql.CommandTrans(cmdTxt);
					using(CSql sql2 = new CSql())
					{
						sql2.Open();
						for(int i = 0; i < this.DataGrid2.Items.Count; i++)
						{
							DataRow row = this.mSortTable.Rows[i];
							DropDownList SortItem = this.DataGrid2.Items[i].FindControl("SortItem") as DropDownList;
							cmdTxt = "INSERT INTO T_ObsSearchSortCondition(UserID,SortConditionID,TargetObj,ItemID,IsDesc)";
							cmdTxt += "VALUES(@UserID,@SortConditionID,@TargetObj,@ItemID,@IsDesc)";
							sql.AddParam("@UserID", Common.LoginID);
							sql.AddParam("@SortConditionID", i);
							sql.AddParam("@IsDesc", Convert.ToInt32(row["Sort"]));

							switch((ConditionItemValue)Convert.ToInt32(row["SortItem"]))
							{
								case ConditionItemValue.承認者:
								{
									#region
									sql.AddParam("@TargetObj", 1);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.起票者:
								{
									#region
									sql.AddParam("@TargetObj", 2);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.状態:
								{
									#region
									sql.AddParam("@TargetObj", 3);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.件名:
								{
									#region
									sql.AddParam("@TargetObj", 4);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.プロジェクト名:
								{
									#region
									sql.AddParam("@TargetObj", 5);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.日付:
								{
									#region
									sql.AddParam("@TargetObj", 7);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.管理番号:
								{
									#region
									sql.AddParam("@TargetObj", 18);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								case ConditionItemValue.カテゴリ名:
								{
									#region
									sql.AddParam("@TargetObj", 19);
									sql.AddParam("@ItemID",  DBNull.Value);
									break;
									#endregion
								}
								default: //可変項目
								{
									#region
									string cmdTxt2 = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
									sql2.AddParam("@ItemID", row["SortItem"]);
									dr = sql2.ReadTrans(cmdTxt2);
									if(dr.Read() == true)
									{
										switch(Convert.ToInt32(dr["DataType"]))
										{
											case 2:	//ﾘｽﾄ型
											{
												sql.AddParam("@TargetObj", 10);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
											case 3:	//数値型
											{
												sql.AddParam("@TargetObj", 11);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
											case 4:	//整数型
											{
												sql.AddParam("@TargetObj", 12);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
											case 5:	//日付型
											{
												sql.AddParam("@TargetObj", 13);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
											case 6:	//ﾕｰｻﾞ型
											{
												sql.AddParam("@TargetObj", 15);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
											case 7:	//部門型
											{
												sql.AddParam("@TargetObj", 16);
												sql.AddParam("@ItemID",  SortItem.SelectedValue);
												break;
											}
										}
									}
									dr.Close();
									break;
									#endregion
								}
							}
							sql.CommandTrans(cmdTxt);
						}
					}
					#endregion

					sql.CommitTrans();
				}
				catch(Exception ex)
				{
					sql.RollbackTrans();
					Common.Alert(this, ex.Message);
					return;
				}
			}
			#endregion

			//検索実行
			this.CreateResultList();

			//ｸﾞﾙｰﾌﾟ順に検索条件を並び替える
			#region

			DataTable tableNew = this.CreateTable();
			for(int i = 1; i <= 10; i++)
			{
				for(int j = 0; j < this.mTable.Rows.Count; j++)
				{
					DataRow row = this.mTable.Rows[j];
					if(Convert.ToInt32(row["Group"]) != i) continue;
					DataRow rowNew = tableNew.NewRow();
					for(int k = 0; k < row.ItemArray.Length; k++)
					{
						rowNew[k] = row[k];
					}
					tableNew.Rows.Add(rowNew);
				}
			}
			this.mTable = tableNew;
			this.DataGrid1.DataSource = this.mTable;
			this.DataGrid1.DataBind();
			this.SetItemControlVisible();

			#endregion

			//結果を表示
			this.Panel1.Visible = true;
		}
		#endregion

		/// <summary>
		/// CSV出力ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
        //［SRC-03-03-01-013］private void ButtonCSV_Click
        #region private void ButtonCSV_Click(object sender, System.EventArgs e)
		private void ButtonCSV_Click(object sender, System.EventArgs e)
		{
			try
			{
				//条件文、ソート文作成
				this.CreateSqlString();

				StringBuilder sb = new StringBuilder();
				using(CSql sql = new CSql())
				{
					using(CSql sql2 = new CSql())
					{
						using(CSql sql3 = new CSql())
						{
							string cmdTxt = string.Empty;
							SqlDataReader dr;
							SqlDataReader dr2;
							SqlDataReader dr3;
							sql.Open();
							sql2.Open();
							sql3.Open();
							
							//ﾍｯﾀﾞ行出力
							#region
							string head = string.Empty;
							head += "\"" + Common.GetText(sql, this.mType, "Title1") + "\"";//管理番号
							head += ",\"" + Common.GetText(sql, this.mType, "Title11") + "\"";//ｶﾃｺﾞﾘ名
							head += ",\"" + Common.GetText(sql, this.mType, "Title2") + "\"";//件名
							head += ",\"" + Common.GetText(sql, this.mType, "Title3") + "\"";//承認状態
							head += ",\"" + Common.GetText(sql, this.mType, "Title4") + "\"";//ｽﾃｰﾀｽ
							head += ",\"" + Common.GetNumberingItemName(sql) + "\"";//採番対象項目
							//可変項目名取得
							switch(Common.LangID)
							{
								case 1: cmdTxt = "SELECT Japanese FROM M_ObsItem WHERE ItemID <> 0 ORDER BY ItemID"; break;
								case 2: cmdTxt = "SELECT English FROM M_ObsItem WHERE ItemID <> 0 ORDER BY ItemID"; break;
							}
							dr = sql.Read(cmdTxt);
							while(dr.Read() == true)
							{
								head += ",\"" + Common.CSVCnv(dr[0].ToString()) + "\"";//項目名
							}
							dr.Close();
							head += ",\"" + Common.GetText(sql, this.mType, "Title5") + "\"";//内容
							head += ",\"" + Common.GetText(sql, this.mType, "Title6") + "\"";//提起者
							head += ",\"" + Common.GetText(sql, this.mType, "Title7") + "\"";//提起日
							head += ",\"" + Common.GetText(sql, this.mType, "Title8") + "\"";//更新日
							head += ",\"" + Common.GetText(sql, this.mType, "Title9") + "\"";//添付ﾌｧｲﾙ
							head += ",\"" + Common.GetText(sql, this.mType, "Title10") + "\"";//URLﾘﾝｸ

							sb.Append(head + "\r\n");
							//Response.Write(HttpUtility.HtmlDecode(head) + "\r\n");
							#endregion

							//ﾃﾞｰﾀ取得SQL文
							#region
							cmdTxt = "SELECT T.ObsMgrID, T.MgrNo, T.Sbj, T.Cnt, T.EntDt, T.UpdDt, C.CatName";
							cmdTxt += ", U.FullName";
							cmdTxt += ", S.Text" + Common.LangID.ToString() + " AS Status";
							cmdTxt += ", P.Lang0" + Common.LangID.ToString() + " AS PName";
							cmdTxt += " FROM T_ObsMgr2 AS T";
							cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=T.Status";
							cmdTxt += " LEFT JOIN M_Prj_Typ AS P ON P.TypCD=T.PrjID";
							cmdTxt += " LEFT JOIN M_users AS U ON U.UserID=T.EntUsrID";
							cmdTxt += " LEFT JOIN (SELECT ObsMgrID,RcgDateTime,RcgUserID,ReqDateTime FROM T_ObsMgr_RcgUsr WHERE NOT Exists(SELECT * FROM T_ObsMgr_RcgUsr AS TR WHERE TR.ObsMgrID=T_ObsMgr_RcgUsr.ObsMgrID AND TR.Seq>T_ObsMgr_RcgUsr.Seq)) AS R ON R.ObsMgrID=T.ObsMgrID "; //承認状態取得用
							cmdTxt += " LEFT JOIN T_ObsTree AS TR ON TR.ObsMgrID=T.ObsMgrID";
							cmdTxt += " LEFT JOIN T_ObsCategoryTree AS C ON C.CatID=TR.CatID";
							#endregion

							//条件文が設定されていたらSQL文に追加
							if(this.mCondition!= string.Empty) cmdTxt += this.mCondition;
							//ｿｰﾄ順が設定されていたらSQL文に追加
							if(this.mSortCondition != string.Empty) cmdTxt += this.mSortCondition;

							dr = sql.Read(cmdTxt);

							while(dr.Read() == true)
							{
								//ﾃﾞｰﾀ出力
								string data = string.Empty;
								string id = dr["ObsMgrID"].ToString();
								data += "\"" + Common.CSVCnv(dr["MgrNo"].ToString()) + "\"";										//管理番号
								data += ",\"" + Common.CSVCnv(dr["CatName"].ToString()) + "\"";										//ｶﾃｺﾞﾘ名
								data += ",\"" + Common.CSVCnv(dr["Sbj"].ToString()) + "\"";											//件名
								data += ",\"" + Common.CSVCnv(Common.GetRcgStatusString(sql2, dr["ObsMgrID"].ToString())) + "\"";	//承認状態
								data += ",\"" + Common.CSVCnv(dr["Status"].ToString()) + "\"";										//ｽﾃｰﾀｽ
								data += ",\"" + Common.CSVCnv(dr["PName"].ToString()) + "\"";										//採番対象項目
								//可変項目ﾃﾞｰﾀ値
								switch(Common.LangID)
								{
									case 1: cmdTxt = "SELECT I.Japanese, I.DataType, I.ItemID, D.* FROM M_ObsItem AS I LEFT JOIN T_ObsMgrData AS D ON D.ObsMgrID=" + id + " AND D.ItemID=I.ItemID WHERE I.ItemID <> 0 ORDER BY I.ItemID"; break;
									case 2: cmdTxt = "SELECT I.English, I.DataType, I.ItemID, D.* FROM M_ObsItem AS I LEFT JOIN T_ObsMgrData AS D ON D.ObsMgrID=" + id + " AND D.ItemID=I.ItemID WHERE I.ItemID <> 0 ORDER BY I.ItemID"; break;
								}
								dr2 = sql2.Read(cmdTxt);
								while(dr2.Read() == true)
								{
									int type = Convert.ToInt32(dr2["DataType"]);
									string val = dr2["Data" + type.ToString()].ToString();
									if(val != string.Empty){
										switch(type)
										{
											case 2://ﾘｽﾄ型
											{
												#region
												cmdTxt = "SELECT Text" + Common.LangID.ToString() + " FROM M_ObsItemList WHERE ItemID=@ItemID AND ListID=@ListID";
												sql3.AddParam("@ItemID", dr2["ItemID"].ToString());
												sql3.AddParam("@ListID", val);
												dr3 = sql3.Read(cmdTxt);
												if(dr3.Read() == true)
												{
													val = dr3[0].ToString();
												}
												else
												{
													val = string.Empty;
												}
												dr3.Close();
												break;
												#endregion
											}
											case 5://日付型
											{
												#region
												//yyyy/MM/ddのみ使用する
												if(val.Length > 10) val = val.Substring(0,  10);
												break;
												#endregion
											}
											case 6://ﾕｰｻﾞ型
											{
												#region
												cmdTxt = "SELECT FullName FROM M_users WHERE UserID=@UserID";
												sql3.AddParam("@UserID", val);
												dr3 = sql3.Read(cmdTxt);
												if(dr3.Read() == true)
												{
													val = dr3[0].ToString();
												}
												else
												{
													val = string.Empty;
												}
												dr3.Close();
												break;
												#endregion
											}
											case 7://部門型
											{
												#region
												cmdTxt = "SELECT SectName FROM M_SECT WHERE SECT=@SECT";
												sql3.AddParam("@SECT", val);
												dr3 = sql3.Read(cmdTxt);
												if(dr3.Read() == true)
												{
													val = dr3[0].ToString();
												}
												else
												{
													val = string.Empty;
												}
												dr3.Close();
												break;
												#endregion
											}
										}
									}
									data += ",\"" + Common.CSVCnv(val) + "\"";//ﾃﾞｰﾀ
								}
								dr2.Close();
								data += ",\"" + Common.CSVCnv(dr["Cnt"].ToString()) + "\"";				//内容
								data += ",\"" + Common.CSVCnv(dr["FullName"].ToString()) + "\"";		//提起者
								data += ",\"" + Common.CSVCnv(this.DateTimeCnv(dr["EntDt"])) + "\"";	//提起日
								data += ",\"" + Common.CSVCnv(this.DateTimeCnv(dr["UpdDt"])) + "\"";	//更新日

								//添付ﾌｧｲﾙ
								string files = string.Empty;
								#region
								cmdTxt = "SELECT UserPath FROM T_ObsMgr_Lib WHERE ObsMgrID=@ObsMgrID ORDER BY UpdDt";
								sql2.AddParam("@ObsMgrID", dr["ObsMgrID"].ToString());
								dr2 = sql2.Read(cmdTxt);
								while(dr2.Read() == true)
								{
									if(files != string.Empty) files += ",";
									files += dr2["UserPath"].ToString();
								}
								dr2.Close();
								#endregion
								data += ",\"" + Common.CSVCnv(files) + "\"";

								//URLﾘﾝｸ
								string links = string.Empty;
								#region
								cmdTxt = "SELECT LinkName, LinkUrl FROM T_ObsMgrLink WHERE ObsMgrID=@ObsMgrID ORDER BY LinkID";
								sql2.AddParam("@ObsMgrID", dr["ObsMgrID"].ToString());
								dr2 = sql2.Read(cmdTxt);
								while(dr2.Read() == true)
								{
									if(links != string.Empty) links += ",";
									links += "[" + dr2["LinkName"].ToString() + "]" + dr2["LinkUrl"].ToString();
								}
								dr2.Close();
								#endregion
								data += ",\"" + Common.CSVCnv(links) + "\"";					//URLﾘﾝｸ
								sb.Append(data + "\r\n");
							}
							dr.Close();
						}//using(CSql sql3 = new CSql())
					}//using(CSql sql2 = new CSql())
				}//using(CSql sql = new CSql())
				//CSVﾌｧｲﾙ名
				Common.SetSession(SessionID.Common_CSVFileName, "SearchResult.csv");
				//CSVﾃﾞｰﾀを設定
				Common.SetSession(SessionID.Common_CSVData, sb.ToString());
				//CSV出力用ﾍﾟｰｼﾞを開く
				this.RegisterStartupScript("script", "<script>window.location='../../Common/CSVOutput.aspx';</script>");
				return;
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion




		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 画面で指定された条件に従って検索用のSQL文を作成する
		/// </summary>
        //［SRC-03-03-01-014］private void CreateSqlString
        #region private void CreateSqlString()
		private void CreateSqlString()
		{
			//条件文作成
			#region
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();
				string strCondition = string.Empty;
				//1〜10のｸﾞﾙｰﾌﾟ順に条件を取得していく
				for(int group = 1; group <= 10; group++)
				{
					bool groupStart = false;
					for(int i = 0; i < this.mTable.Rows.Count; i++)
					{
						DataRow row = this.mTable.Rows[i];
						//無効の場合は無視
						if(Convert.ToBoolean(row["IsValid"]) == true) continue;
						//ｸﾞﾙｰﾌﾟが違う場合は無視
						if(Convert.ToInt32(row["Group"]) != group) continue;
						//項目種別取得＆ﾁｪｯｸ
						int itemID = Convert.ToInt32(row["Item"]);	//項目種別
						int dataType = -1;							//可変項目種別
						bool textItem = false;						//ﾃｷｽﾄ項目?
						switch(itemID)
						{
							case (int)ConditionItemValue.承認者:
							case (int)ConditionItemValue.起票者:
							case (int)ConditionItemValue.状態:
							case (int)ConditionItemValue.プロジェクト名:
							case (int)ConditionItemValue.日付:
							case (int)ConditionItemValue.メール送信対象者:
								break;
							case (int)ConditionItemValue.件名:
							case (int)ConditionItemValue.内容:
								textItem=true;
								break;
							default:
								cmdTxt = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
								sql.AddParam("@ItemID", itemID);
								dr = sql.Read(cmdTxt);
								if(dr.Read() == true)
								{
									dataType = Convert.ToInt32(dr["DataType"]);
								}
								dr.Close();
								if(dataType == 1) textItem=true;
								break;
						}
						//ﾃｷｽﾄ項目で、文字列が空白の場合は無視
						if((textItem == true) && (row["Text"].ToString().Trim().Length <= 0)) continue;
						//管理番号で、文字列が空白の場合は無視
						if(itemID == (int)ConditionItemValue.管理番号 && (row["Text"].ToString().Trim().Length <= 0)) continue;

						if(strCondition == string.Empty)
						{
							groupStart = true;
							strCondition = " WHERE ";
							strCondition += "(";//ｸﾞﾙｰﾌﾟ開始括弧
						}
						else
						{
							if(groupStart == false) //ｸﾞﾙｰﾌﾟの先頭の条件
							{
								groupStart = true;
								strCondition += " AND ";	//ｸﾞﾙｰﾌﾟ間の条件をつなぐのは必ずAND
								strCondition += "(";		//ｸﾞﾙｰﾌﾟ開始括弧
							}
							else
							{
								if(row["AndOr"].ToString() == "0")	strCondition += " AND ";
								else								strCondition += " OR ";
							}
						}
						switch(itemID)
						{
							case (int)ConditionItemValue.承認者:
							{
								#region
	
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";
	
								strCondition += "EXISTS(SELECT * FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=T.ObsMgrID AND RcgUserID=" + row["Index"] + ")";
	
								strCondition += ")";
	
								break;
								#endregion
							}
							case (int)ConditionItemValue.起票者:
							{
								#region

								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "EntUsrID=" + row["Index"];

								strCondition += ")";

								break;
								#endregion
							}
							case (int)ConditionItemValue.状態:
							{
								#region
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								int status = Convert.ToInt32(row["Index"]);
								switch (status)
								{
									case 0://承認者未設定
										strCondition += "R.ObsMgrID IS NULL AND RcgDateTime IS NULL";
										break;
									case 1://承認待ち
										strCondition += "R.ObsMgrID IS NOT NULL AND RcgDateTime IS NULL";
										break;
									case 2://承認済み
										strCondition += "R.ObsMgrID IS NOT NULL AND RcgDateTime IS NOT NULL";
										break;
								}
								strCondition += ")";
								break;
								#endregion
							}
							case (int)ConditionItemValue.件名:
							{
								#region
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "T.Sbj LIKE '%" + Common.SQCnv(row["Text"]);

								strCondition += "%')";
								break;
								#endregion
							}
							case (int)ConditionItemValue.プロジェクト名:
							{
								#region
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "P.TypCD='" + Common.SQCnv(row["Index"]);

								strCondition += "')";
								break;
								#endregion
							}
							case (int)ConditionItemValue.内容:
							{
								#region
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "T.Cnt LIKE '%" + Common.SQCnv(row["Text"]);

								strCondition += "%')";
								break;
								#endregion
							}
							case (int)ConditionItemValue.日付:
							{
								#region
								if(row["YesNo"].ToString() == "0")
								{
									strCondition += "LEFT(T.EntDt, 8) <= '" + Convert.ToDateTime(row["Text"]).ToString("yyyyMMdd") + "'";
								}
								else
								{
									strCondition += "LEFT(T.EntDt, 8) >= '" + Convert.ToDateTime(row["Text"]).ToString("yyyyMMdd") + "'";
								}
								break;
								#endregion
							}
							case (int)ConditionItemValue.メール送信対象者:
							{
								#region

								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";
								//No.118対応
								//if(mail.Check1 == true) send += 2;
								//if(mail.Check2 == true) send += 8;
								//if(mail.Check3 == true) send += 1024;
								//ﾒｰﾙ送信対象者に設定されているか
								strCondition += "EXISTS(SELECT * FROM T_AutoMailAddress WHERE CD=T.PrjID AND UserID=" + row["Index"] + ")";
								strCondition += " OR ";
								//承認者であるか
								strCondition += "((P.Send=2 OR P.Send=10 OR P.Send=1026 OR P.Send=1034) AND R.RcgUserID=" + row["Index"] + ")";
								strCondition += " OR ";
								//追加・更新者であるか
								strCondition += "((P.Send=8 OR P.Send=10 OR P.Send=1032 OR P.Send=1034) AND (T.EntUsrID=" + row["Index"] + " OR T.ModUsrID=" + row["Index"] + "))";
								strCondition += " OR ";
								//承認者（承認済み）であるか
								strCondition += "((P.Send=1024 OR P.Send=1026 OR P.Send=1032 OR P.Send=1034) AND";
								strCondition += " (EXISTS(SELECT * FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=T.ObsMgrID AND RcgDateTime IS NOT NULL AND RcgUserID=" + row["Index"] + ")))";
								strCondition += ")";
								//No.118対応完
								break;
								#endregion
							}
							case (int)ConditionItemValue.管理番号:
							{
								#region
								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "T.MgrNo LIKE '%" + Common.SQCnv(row["Text"]);

								strCondition += "%')";
								break;
								#endregion
							}
							case (int)ConditionItemValue.カテゴリ名:
							{
								#region

								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "C.CatID=" + row["Index"];

								strCondition += ")";

								break;
								#endregion
							}
							case (int)ConditionItemValue.ステータス:
							{
								#region

								if(row["YesNo"].ToString() == "0")	strCondition += "(";
								else								strCondition += "NOT(";

								strCondition += "T.Status=" + row["Index"];

								strCondition += ")";

								break;
								#endregion
							}
							default:
							{
								//可変項目
								#region
								switch(dataType)
								{
									case 1://ﾃｷｽﾄ型
									{
										if(row["YesNo"].ToString() == "0")	strCondition += "(";
										else								strCondition += "NOT(";
										strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") LIKE '%" + Common.SQCnv(row["Text"]) + "%'";
										strCondition += ")";
										break;
									}
									case 2://ﾘｽﾄ型
									{
										if(row["YesNo"].ToString() == "0")	strCondition += "(";
										else								strCondition += "NOT(";
										strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ")='" + row["Index"] + "'";
										strCondition += ")";
										break;
									}
									case 3://数値型
									{
										if(row["YesNo"].ToString() == "0")
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") >='" + row["Text"] + "'";
										else
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") <='" + row["Text"] + "'";
										break;
									}
									case 4://整数型
									{
										if(row["YesNo"].ToString() == "0")
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") >=" + row["Text"];
										else
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") <=" + row["Text"];
										break;
									}
									case 5://日付型
									{
										if(row["YesNo"].ToString() == "0")
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") <='" + row["Text"] + "'";
										else
											strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ") >='" + row["Text"] + "'";
										break;
									}
									case 6://ﾕｰｻﾞ型
									{
										if(row["YesNo"].ToString() == "0")	strCondition += "(";
										else								strCondition += "NOT(";
										strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ")='" + row["Index"] + "'";
										strCondition += ")";
										break;
									}
									case 7://部門型
									{
										if(row["YesNo"].ToString() == "0")	strCondition += "(";
										else								strCondition += "NOT(";
										strCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ")='" + Common.SQCnv(row["Index"]) + "'";
										strCondition += ")";
										break;
									}
								}
								break;
								#endregion
							}
						}
					}//for(int i = 0; i < this.mTable.Rows.Count; i++)

					//ｸﾞﾙｰﾌﾟの条件が存在したら終わり括弧追加
					if(groupStart == true) strCondition += ")";

				}//for(int group = 1; group <= 10; group++)

				//ｸﾞﾙｰﾌﾟの最後に余計な文字列が存在する場合は削除する
				//if(strCondition.Substring(strCondition.Length - 4, 4) == "AND(") strCondition = strCondition.Substring(0, strCondition.Length - 4);

				this.mCondition = strCondition;
			}//using(CSql sql = new CSql())
			#endregion

			//ｿｰﾄ文作成
			#region
			using(CSql sql = new CSql())
			{
				string cmdTxt;
				SqlDataReader dr;
				sql.Open();

				ArrayList alSortedItem = new ArrayList();//ORDER BYに指定済みのSortItem(重複を避ける処理用)
				string strSortCondition = string.Empty;
				for(int i = 0; i < this.mSortTable.Rows.Count; i++)
				{
					DataRow row = this.mSortTable.Rows[i];

					//ORDER BYの重複指定を避ける
					int index = alSortedItem.IndexOf(row["SortItem"].ToString());
					if(index >= 0) continue;
					alSortedItem.Add(row["SortItem"].ToString());

					if(strSortCondition == string.Empty)
					{
						strSortCondition = " ORDER BY ";
					}
					else
					{
						strSortCondition += ", ";
					}
					switch(Convert.ToInt32(row["SortItem"]))
					{
						case (int)ConditionItemValue.承認者:
						{
							strSortCondition += "(SELECT TOP 1 RcgUserID FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=T.ObsMgrID ORDER BY Seq DESC)";
							break;
						}
						case (int)ConditionItemValue.起票者:
						{
							strSortCondition += " T.EntUsrID";
							break;
						}
						case (int)ConditionItemValue.状態:
						{
							strSortCondition += " R.RcgDateTime, R.RcgUserID, R.ReqDateTime";
							break;
						}
						case (int)ConditionItemValue.件名:
						{
							strSortCondition += " T.Sbj";
							break;
						}
						case (int)ConditionItemValue.プロジェクト名:
						{
							strSortCondition += " P.Lang0" + Common.LangID.ToString();
							break;
						}
						case (int)ConditionItemValue.日付:
						{
							strSortCondition += " T.EntDt";
							break;
						}
						case (int)ConditionItemValue.管理番号:
						{
							strSortCondition += " T.MgrNo";
							break;
						}
						case (int)ConditionItemValue.カテゴリ名:
						{
							strSortCondition += " C.CatName";
							break;
						}
						default:
						{
							//可変項目
							int itemID = Convert.ToInt32(row["SortItem"]);
							int dataType = 1;
							cmdTxt = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
							sql.AddParam("@ItemID", itemID);
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								dataType = Convert.ToInt32(dr["DataType"]);
							}
							dr.Close();
							strSortCondition += "(SELECT Data" + dataType.ToString() + " FROM T_ObsMgrData WHERE ObsMgrID=T.ObsMgrID AND ItemID=" + itemID.ToString() + ")";
							break;
						}
					}
					if(row["Sort"].ToString() == "1") strSortCondition += " DESC ";
					this.mSortCondition = strSortCondition;
				}//for(int i = 0; i < this.mSortTable.Rows.Count; i++)
			}//using(CSql sql = new CSql())
			
			#endregion
		}
		#endregion

		/// <summary>
		/// 指定されたIndexの検索条件項目の値を設定する
		/// (mTableの値を各ｺﾝﾄﾛｰﾙに設定する)
		/// </summary>
		/// <param name="index"></param>
        //［SRC-03-03-01-015］private void SetItemControlVisible
        #region private void SetItemControlVisible(int index)
		private void SetItemControlVisible(int index)
		{
			using(CSql sql = new CSql())
			{
				sql.Open();
				DataGridItem item = this.DataGrid1.Items[index];
				DropDownList Group = item.FindControl("Group") as DropDownList;
				DropDownList AndOr = item.FindControl("AndOr") as DropDownList;
				DropDownList Item = item.FindControl("Item") as DropDownList;
				DropDownList Index = item.FindControl("Index") as DropDownList;
				TextBox Text = item.FindControl("Text") as TextBox;
				DropDownList Index2 = item.FindControl("Index2") as DropDownList;
				Label Link = item.FindControl("Link") as Label;
				DropDownList YesNo = item.FindControl("YesNo") as DropDownList;
				CheckBox IsValid = item.FindControl("IsValid") as CheckBox;
				//選択項目が変わったらﾃﾞｰﾀｸﾘｱ
				this.mTable.Rows[index]["AndOr"] = 0;
				this.mTable.Rows[index]["Index"] = string.Empty;
				this.mTable.Rows[index]["Text"] = string.Empty;
				this.mTable.Rows[index]["Index2"] = string.Empty;
				this.mTable.Rows[index]["YesNo"] = 0;
				this.mTable.Rows[index]["IsValid"] = false;
				//値設定
				this.CreateRowControl(sql, index, Item, Group, AndOr, Index, Text, Link, Index2, YesNo, IsValid);
			}
		}
		#endregion

		/// <summary>
		/// 検索条件の全ての行の値を設定する
		/// (mTableの値を各ｺﾝﾄﾛｰﾙに設定する)
		/// </summary>
        //［SRC-03-03-01-016］private void SetItemControlVisible
        #region private void SetItemControlVisible()
		private void SetItemControlVisible()
		{
			DataTable table = this.GetCurrentData();
			using(CSql sql = new CSql())
			{
				sql.Open();
				for(int i = 0; i < this.DataGrid1.Items.Count; i++)
				{
					DataGridItem item = this.DataGrid1.Items[i];
					DropDownList Group = item.FindControl("Group") as DropDownList;
					DropDownList AndOr = item.FindControl("AndOr") as DropDownList;
					DropDownList Item = item.FindControl("Item") as DropDownList;
					DropDownList Index = item.FindControl("Index") as DropDownList;
					TextBox Text = item.FindControl("Text") as TextBox;
					DropDownList Index2 = item.FindControl("Index2") as DropDownList;
					Label Link = item.FindControl("Link") as Label;
					DropDownList YesNo = item.FindControl("YesNo") as DropDownList;
					CheckBox IsValid = item.FindControl("IsValid") as CheckBox;
					//値設定
					this.CreateRowControl(sql, i, Item, Group, AndOr, Index, Text, Link, Index2, YesNo, IsValid);
				}
			}
		}
		#endregion

        //［SRC-03-03-01-017］private void CreateRowControl
        #region private void CreateRowControl(CSql sql, int index, DropDownList Item, DropDownList Group, DropDownList AndOr, DropDownList Index, TextBox Text, Label Link, DropDownList Index2, DropDownList YesNo, CheckBox IsValid)
		private void CreateRowControl(CSql sql, int index, DropDownList Item, DropDownList Group, DropDownList AndOr, DropDownList Index, TextBox Text, Label Link, DropDownList Index2, DropDownList YesNo, CheckBox IsValid)
		{
			Index.Items.Clear();
			YesNo.Items.Clear();
			Index2.Items.Clear();

			Link.Text = string.Empty;

			int iItem = Convert.ToInt32(this.mTable.Rows[index]["Item"]);

			//ｺﾝﾄﾛｰﾙ作成
			switch((ConditionItemValue)iItem)
			{
				case ConditionItemValue.承認者:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ﾕｰｻﾞ一覧作成
					this.CreateUserList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.起票者:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ﾕｰｻﾞ一覧作成
					this.CreateUserList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));
					
					break;
					#endregion
				}
				case ConditionItemValue.状態:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;

					//承認状態一覧作成
					this.CreateRcgStatusList(sql, Index);

					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.件名:
				{
					#region

					Index.Visible = false;
					Text.Visible = true;
					Index2.Visible = false;

					Text.MaxLength = 255;
					Text.Columns = 50;

					//含む/含まない
					YesNo.Items.Add(new ListItem(this.alYesNo[2].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[3].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.プロジェクト名:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ﾌﾟﾛｼﾞｪｸﾄ一覧作成
					this.CreateProjectList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.内容:
				{
					#region

					Index.Visible = false;
					Text.Visible = true;
					Index2.Visible = false;

					Text.MaxLength = 255;
					Text.Columns = 50;

					//YesNo一覧(含む/含まない)
					YesNo.Items.Add(new ListItem(this.alYesNo[2].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[3].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.日付:
				{
					#region

					Index.Visible = false;
					Text.Visible = true;
					Index2.Visible = true;

					Text.MaxLength = 10;
					Text.Columns = 12;

					Text.Attributes["onDblClick"]="SetToday(this)";

					Link.Text = "<a href=\"javascript:void(0)\" onclick=\"javascript:OpenCalendar(event,'" + Text.UniqueID.Replace(':', '_') + "');\">▼</a>";

					//YesNo一覧(以前/以降)
					YesNo.Items.Add(new ListItem(this.alYesNo[4].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[5].ToString(), "1"));
					//ｶﾚﾝﾀﾞｰ条件(相対値/絶対値)
					Index2.Items.Add(new ListItem(this.alDateOption[0].ToString(), "0"));
					Index2.Items.Add(new ListItem(this.alDateOption[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.メール送信対象者:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ﾕｰｻﾞ一覧作成
					this.CreateUserList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.管理番号:
				{
					#region

					Index.Visible = false;
					Text.Visible = true;
					Index2.Visible = false;

					Text.MaxLength = 255;
					Text.Columns = 50;

					//YesNo一覧(含む/含まない)
					YesNo.Items.Add(new ListItem(this.alYesNo[2].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[3].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.カテゴリ名:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ｶﾃｺﾞﾘ一覧作成
					this.CreateCategoryList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				case ConditionItemValue.ステータス:
				{
					#region

					Index.Visible = true;
					Text.Visible = false;
					Index2.Visible = false;
					//ｽﾃｰﾀｽ一覧作成
					this.CreateStatusList(sql, Index);
					//YesNo一覧
					YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
					YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

					break;
					#endregion
				}
				default:	//可変項目
				{
					#region
					string cmdTxt;
					SqlDataReader dr;
					cmdTxt = "SELECT DataType FROM M_ObsItem WHERE ItemID=@ItemID";
					sql.AddParam("@ItemID", iItem);
					dr = sql.Read(cmdTxt);
					if(dr.Read() == true)
					{
						switch(Convert.ToInt32(dr["DataType"]))
						{
							case 1:	//ﾃｷｽﾄ型
							{
								#region
								Index.Visible = false;
								Text.Visible = true;
								Index2.Visible = false;

								Text.MaxLength = 255;
								Text.Columns = 50;

								//YesNo一覧(含む/含まない)
								YesNo.Items.Add(new ListItem(this.alYesNo[2].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[3].ToString(), "1"));

								break;
								#endregion
							}
							case 2:	//ﾘｽﾄ型
							{
								#region
								Index.Visible = true;
								Text.Visible = false;
								Index2.Visible = false;

								using(CSql sql2 = new CSql())
								{
									sql2.Open();
									SqlDataReader dr2;
									cmdTxt = "SELECT ListID, Text" + Common.LangID.ToString() + " AS Text FROM M_ObsItemList WHERE ItemID=@ItemID AND DisabledFlag=0 ORDER BY SortID";
									sql2.AddParam("@ItemID", iItem);
									dr2 = sql2.Read(cmdTxt);
									while(dr2.Read() == true)
									{
										Index.Items.Add(new ListItem(dr2["Text"].ToString(), dr2["ListID"].ToString()));
									}
									dr2.Close();
								}
								//YesNo一覧
								YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

								break;
								#endregion
							}
							case 3:	//数値型
							{
								#region
								Index.Visible = false;
								Text.Visible = true;
								Index2.Visible = false;

								Text.MaxLength = 10;
								Text.Columns = 13;

								//以上/以下
								YesNo.Items.Add(new ListItem(this.alYesNo[6].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[7].ToString(), "1"));

								break;
								#endregion
							}
							case 4:	//整数型
							{
								#region
								Index.Visible = false;
								Text.Visible = true;
								Index2.Visible = false;

								Text.MaxLength = 11;
								Text.Columns = 14;

								//以上/以下
								YesNo.Items.Add(new ListItem(this.alYesNo[6].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[7].ToString(), "1"));

								break;
								#endregion
							}
							case 5:	//日付型
							{
								#region
								Index.Visible = false;
								Text.Visible = true;
								Index2.Visible = true;

								Text.MaxLength = 10;
								Text.Columns = 12;
								Text.Attributes["onDblClick"]="SetToday(this)";

								Link.Text = "<a href=\"javascript:void(0)\" onclick=\"javascript:OpenCalendar(event,'" + Text.UniqueID.Replace(':', '_') + "');\">▼</a>";
								//YesNo一覧(以前/以降)
								YesNo.Items.Add(new ListItem(this.alYesNo[4].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[5].ToString(), "1"));
								//ｶﾚﾝﾀﾞｰ条件(相対値/絶対値)
								Index2.Items.Add(new ListItem(this.alDateOption[0].ToString(), "0"));
								Index2.Items.Add(new ListItem(this.alDateOption[1].ToString(), "1"));

								break;
								#endregion
							}
							case 6:	//ﾕｰｻﾞ型
							{
								#region
								Index.Visible = true;
								Text.Visible = false;
								Index2.Visible = false;
								using(CSql sql2 = new CSql())
								{
									sql2.Open();
									//ﾕｰｻﾞ一覧作成
									this.CreateUserList(sql2, Index);
								}
								//YesNo一覧
								YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

								break;
								#endregion
							}
							case 7:	//部門型
							{
								#region

								Index.Visible = true;
								Text.Visible = false;
								Index2.Visible = false;
								
								using(CSql sql2 = new CSql())
								{
									sql2.Open();
									//部門一覧作成
									this.CreateSectList(sql2, Index);
								}
								//YesNo一覧
								YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
								YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));

								break;
								#endregion
							}
						}
						dr.Close();
					}
					else
					{
						dr.Close();
						//ｱｲﾃﾑが削除されている場合は、「承認者」が選択された状態にする
						Index.Visible = true;
						Text.Visible = false;
						Index2.Visible = false;
						//ﾕｰｻﾞ一覧作成
						this.CreateUserList(sql, Index);
						//YesNo一覧
						YesNo.Items.Add(new ListItem(this.alYesNo[0].ToString(), "0"));
						YesNo.Items.Add(new ListItem(this.alYesNo[1].ToString(), "1"));
					}
					break;
					#endregion
				}
			}

			//値設定
			Common.SetValue(Group, this.mTable.Rows[index]["Group"].ToString());
			Common.SetValue(AndOr, this.mTable.Rows[index]["AndOr"].ToString());
			Common.SetValue(Item, this.mTable.Rows[index]["Item"].ToString());
			Common.SetValue(Index, this.mTable.Rows[index]["Index"].ToString());
			Text.Text = this.mTable.Rows[index]["Text"].ToString();
			Common.SetValue(Index2, this.mTable.Rows[index]["Index2"].ToString());
			Common.SetValue(YesNo, this.mTable.Rows[index]["YesNo"].ToString());
			IsValid.Checked = Convert.ToBoolean(this.mTable.Rows[index]["IsValid"]);
			
		}
		#endregion

		/// <summary>
		/// ﾌｫｰﾑから現在入力されているﾃﾞｰﾀを取得する
		/// </summary>
		/// <returns>全ﾃﾞｰﾀを格納したﾃｰﾌﾞﾙ</returns>
        //［SRC-03-03-01-018］private DataTable GetCurrentData
        #region private DataTable GetCurrentData()
		private DataTable GetCurrentData()
		{
			DataTable table = this.CreateTable();
			for(int i = 0; i < this.DataGrid1.Items.Count; i++)
			{
				DataGridItem item = this.DataGrid1.Items[i];
				DropDownList Group = item.FindControl("Group") as DropDownList;
				DropDownList AndOr = item.FindControl("AndOr") as DropDownList;
				DropDownList Item = item.FindControl("Item") as DropDownList;
				DropDownList Index = item.FindControl("Index") as DropDownList;
				TextBox Text = item.FindControl("Text") as TextBox;
				DropDownList Index2 = item.FindControl("Index2") as DropDownList;
				DropDownList YesNo = item.FindControl("YesNo") as DropDownList;
				CheckBox IsValid = item.FindControl("IsValid") as CheckBox;

				DataRow row = table.NewRow();

				row["Group"] = Convert.ToInt32(Group.SelectedValue);
				row["AndOr"] = AndOr.SelectedValue;
				row["Item"] = Item.SelectedValue;
				if(Index.SelectedValue != string.Empty)		row["Index"] = Index.SelectedValue;
				row["Text"] = Text.Text;
				if(Index2.SelectedValue != string.Empty)	row["Index2"] = Index2.SelectedValue;
				if(YesNo.SelectedValue != string.Empty)		row["YesNo"] = YesNo.SelectedValue;
				row["IsValid"] = IsValid.Checked;

				table.Rows.Add(row);
			}
			return table;
		}
		#endregion

		/// <summary>
		/// ﾌｫｰﾑから現在入力されているﾃﾞｰﾀを取得する
		/// </summary>
		/// <returns>全ﾃﾞｰﾀを格納したﾃｰﾌﾞﾙ</returns>
        //［SRC-03-03-01-019］private DataTable GetCurrentSortData
        #region private DataTable GetCurrentSortData()
		private DataTable GetCurrentSortData()
		{
			DataTable table = new DataTable();

			table.Columns.Add("SortItem", typeof(int));
			table.Columns.Add("Sort", typeof(int));

			for(int i = 0; i < this.DataGrid2.Items.Count; i++)
			{
				DataGridItem item = this.DataGrid2.Items[i];

				DropDownList SortItem = item.FindControl("SortItem") as DropDownList;
				DropDownList Sort = item.FindControl("Sort") as DropDownList;

				DataRow row = table.NewRow();

				row["SortItem"] = SortItem.SelectedValue;
				row["Sort"] = Sort.SelectedValue;

				table.Rows.Add(row);
			}
			return table;
		}
		#endregion

		/// <summary>
		/// 空ﾃｰﾌﾞﾙ作成
		/// </summary>
		/// <returns></returns>
        //［SRC-03-03-01-020］private DataTable CreateTable
        #region private DataTable CreateTable()
		private DataTable CreateTable()
		{
			DataTable table = new DataTable();
			table.Columns.Add("Group", typeof(int));
			table.Columns.Add("AndOr", typeof(int));
			table.Columns.Add("Item", typeof(int));
			table.Columns.Add("Index", typeof(string));
			table.Columns.Add("Text", typeof(string));
			table.Columns.Add("Index2", typeof(string));
			table.Columns.Add("YesNo", typeof(int));
			table.Columns.Add("IsValid", typeof(bool));
			return table;
		}
		#endregion

		/// <summary>
		/// 条件行追加
		/// </summary>
		/// <param name="table"></param>
		/// <param name="AndOr"></param>
		/// <param name="AndOr"></param>
		/// <param name="ConditionItem"></param>
		/// <param name="ConditionText"></param>
		/// <param name="ConditionIndex"></param>
		/// <param name="ConditionOptionIndex"></param>
		/// <param name="YesNo"></param>
		/// <param name="Valid"></param>
        //［SRC-03-03-01-021］private void DataTable
        #region private void AddRow(DataTable table, object Group, object AndOr, object ConditionItem, object ConditionText, object ConditionIndex, object ConditionOptionIndex, object YesNo, object IsValid)
		private void AddRow(DataTable table, object Group, object AndOr, object ConditionItem, object ConditionText, object ConditionIndex, object ConditionOptionIndex, object YesNo, object IsValid)
		{
			DataRow row = table.NewRow();

			row["Group"] = Convert.ToInt32(Group);
			row["AndOr"] = Convert.ToInt32(AndOr);
			row["Item"] = Convert.ToInt32(ConditionItem);
			row["Index"] = ConditionIndex.ToString();
			row["Text"] = ConditionText.ToString();
			row["Index2"] = ConditionOptionIndex.ToString();
			row["YesNo"] = Convert.ToInt32(YesNo);
			row["IsValid"] = Convert.ToBoolean(IsValid);

			table.Rows.Add(row);
		}
		#endregion

		/// <summary>
		/// 検索条件一覧作成に必要なﾃｷｽﾄを取得する
		/// </summary>
        //［SRC-03-03-01-022］private void CreateLanguage
        #region private void CreateLanguage()
		private void CreateLanguage()
		{
			using(CSql sql = new CSql())
			{
				sql.Open();

				this.alAndOr = new ArrayList();
				this.alAndOr.Add(Common.GetText(sql, this.mType, "AndOr1"));
				this.alAndOr.Add(Common.GetText(sql, this.mType, "AndOr2"));
				
				this.alSearchItem = new ArrayList();
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem1"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem2"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem3"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem4"));
				this.alSearchItem.Add(Common.GetNumberingItemName());
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem5"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem6"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem7"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem8"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem9"));
				this.alSearchItem.Add(Common.GetText(sql, this.mType, "SearchItem10"));

				this.alSortItem = new ArrayList();
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem1"));
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem2"));
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem3"));
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem4"));
				this.alSortItem.Add(Common.GetNumberingItemName());
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem5"));
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem6"));
				this.alSortItem.Add(Common.GetText(sql, this.mType, "SortItem7"));

				this.alOrder = new ArrayList();
				this.alOrder.Add(Common.GetText(sql, this.mType, "Order1"));
				this.alOrder.Add(Common.GetText(sql, this.mType, "Order2"));

				//である、でない／含む、含まない／以前、以降／以上、以下
				this.alYesNo = new ArrayList();
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo1"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo2"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo3"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo4"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo5"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo6"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo7"));
				this.alYesNo.Add(Common.GetText(sql, this.mType, "YesNo8"));

				this.alDateOption = new ArrayList();
				this.alDateOption.Add(Common.GetText(sql, this.mType, "DateOption1"));
				this.alDateOption.Add(Common.GetText(sql, this.mType, "DateOption2"));

				//ｸﾞﾙｰﾌﾟ文字列
				this.mGroup = Common.GetText(sql, this.mType, "StrGroup");
			}
		}
		#endregion


		/// <summary>
		/// ﾕｰｻﾞ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-023］private void CreateUserList
        #region private void CreateUserList(CSql sql, DropDownList ddl)
		private void CreateUserList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT UserID, FullName FROM M_users ORDER BY UserID";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["FullName"].ToString(), dr["UserID"].ToString()));
			}
			dr.Close();
		}
		#endregion

		/// <summary>
		/// 部署一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-024］private void CreateSectList
        #region private void CreateSectList(CSql sql, DropDownList ddl)
		private void CreateSectList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT Sect, SectName FROM M_SECT WHERE SectLevel<>0 ORDER BY Sect ";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["SectName"].ToString(), dr["Sect"].ToString()));
			}
			dr.Close();
		}
		#endregion

		/// <summary>
		/// 承認状態一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-025］private void CreateRcgStatusList
        #region private void CreateRcgStatusList(CSql sql, DropDownList ddl)
		private void CreateRcgStatusList(CSql sql, DropDownList ddl)
		{
			ddl.Items.Clear();
			ddl.Items.Add(new ListItem(Common.GetText(sql, typeof(Common), "RcgStatus1"), "0"));
			ddl.Items.Add(new ListItem(Common.GetText(sql, typeof(Common), "RcgStatus2"), "1"));
			ddl.Items.Add(new ListItem(Common.GetText(sql, typeof(Common), "RcgStatus3"), "2"));
		}
		#endregion

		/// <summary>
		/// ﾌﾟﾛｼﾞｪｸﾄ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-026］private void CreateProjectList
        #region private void CreateProjectList(CSql sql, DropDownList ddl)
		private void CreateProjectList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT TypCD, Lang0" + Common.LangID.ToString() + " AS Text FROM M_Prj_Typ ORDER BY TypCD";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["Text"].ToString(), dr["TypCD"].ToString()));
			}
			dr.Close();
		}
		#endregion

		/// <summary>
		/// ｽﾃｰﾀｽ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-027］private void CreateStatusList
        #region private void CreateStatusList(CSql sql, DropDownList ddl)
		private void CreateStatusList(CSql sql, DropDownList ddl)
		{
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT StatusID, Text" + Common.LangID.ToString() + " FROM M_ObsStatus ORDER BY SortID";
			dr = sql.Read(cmdTxt);
			ddl.Items.Clear();
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["Text" + Common.LangID.ToString()].ToString(), dr["StatusID"].ToString()));
			}
			dr.Close();
		}
		#endregion

		/// <summary>
		/// ｶﾃｺﾞﾘ一覧作成用ｸﾗｽ
		/// </summary>
        //［SRC-03-03-01-028］private class CCategory
        #region private class CCategory
		private class CCategory
		{
			public int CatID;
			public string CatName;
			public int ParentID;
			public CCategory(int CatID, string CatName, int ParentID)
			{
				this.CatID = CatID;
				this.CatName = CatName;
				this.ParentID = ParentID;
			}
		}
		#endregion

		/// <summary>
		/// ｶﾃｺﾞﾘ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ddl"></param>
        //［SRC-03-03-01-029］private void CreateCategoryList
        #region private void CreateCategoryList(CSql sql, DropDownList ddl)
		private void CreateCategoryList(CSql sql, DropDownList ddl)
		{
			ArrayList alCat = new ArrayList();
			string cmdTxt;
			SqlDataReader dr;
			cmdTxt = "SELECT CatID, CatName, ParentID FROM T_ObsCategoryTree ORDER BY ParentID, SortID";
			dr = sql.Read(cmdTxt);
			while(dr.Read() == true)
			{
				ddl.Items.Add(new ListItem(dr["CatName"].ToString(), dr["CatID"].ToString()));
				alCat.Add(new CCategory(Convert.ToInt32(dr["CatID"]), dr["CatName"].ToString(), Convert.ToInt32(dr["ParentID"])));
			}
			dr.Close();
			ddl.Items.Clear();
			ArrayList al = this.GetCategory(alCat, -1);
			string strSpace = Common.GetText(typeof(Common), "str5");
			for(int i = 0; i < al.Count; i++)
			{
				CCategory cat = (CCategory)al[i];
				this.CreateCategoryListSub(ddl, alCat, cat, 0, strSpace);
			}										  
//			cmdTxt = "SELECT CatID, CatName FROM T_ObsCategoryTree ORDER BY CatID";
//			dr = sql.Read(cmdTxt);
//			ddl.Items.Clear();
//			while(dr.Read() == true)
//			{
//				ddl.Items.Add(new ListItem(dr["CatName"].ToString(), dr["CatID"].ToString()));
//			}
//			dr.Close();
		}
		#endregion

        //［SRC-03-03-01-030］private void CreateCategoryListSub
        #region private void CreateCategoryListSub(DropDownList ddl, ArrayList alCat, CCategory cat, int indent, string strSpace)
		private void CreateCategoryListSub(DropDownList ddl, ArrayList alCat, CCategory cat, int indent, string strSpace)
		{
			//字下げ処理
			string sp = string.Empty;
			for(int i = 0; i < indent; i++) sp += strSpace;
			//ｶﾃｺﾞﾘ追加
			ddl.Items.Add(new ListItem(sp + cat.CatName, cat.CatID.ToString()));
			//自分の子を取得
			ArrayList al = this.GetCategory(alCat, cat.CatID);
			for(int i = 0; i < al.Count; i++)
			{
				CCategory c = (CCategory)al[i];
				this.CreateCategoryListSub(ddl, alCat, c, indent + 1, strSpace);
			}
	/*
	EPSNet.Common	str1	┃	|		
	EPSNet.Common	str2	┣	|-		
	EPSNet.Common	str3	┗	+-		
	EPSNet.Common	str4	■	[]		
	EPSNet.Common	str5
	*/
		}
		#endregion

		/// <summary>
		/// 指定されたParentIDを親に持つｶﾃｺﾞﾘを取得する
		/// </summary>
		/// <param name="parentId"></param>
		/// <param name="alCat"></param>
		/// <returns></returns>
        //［SRC-03-03-01-031］private ArrayList GetCategory
        #region private ArrayList GetCategory(ArrayList alCat, int parentId)
		private ArrayList GetCategory(ArrayList alCat, int parentId)
		{
			ArrayList al = new ArrayList();
			for(int i = 0; i < alCat.Count; i++)
			{
				CCategory cat = (CCategory)alCat[i];
				if(cat.ParentID == parentId)
				{
					al.Add(cat);
				}
			}
			return al;
		}
		#endregion

		/// <summary>
		/// 検索結果一覧作成
		/// </summary>
		/// <param name="sql"></param>
        //［SRC-03-03-01-032］private void CreateResultList
        #region private void CreateResultList()
		private void CreateResultList()
		{
			DataTable table = new DataTable();
			table.Columns.Add("MgrNo");
			table.Columns.Add("CatName");
			table.Columns.Add("Subject");
			table.Columns.Add("ApprovalStatus");
			table.Columns.Add("Status");
			table.Columns.Add("DateTime");
			table.Columns.Add("ObsMgrID");

			using(CSql sql = new CSql())
			{
				sql.Open();
				string cmdTxt;
				SqlDataReader dr;
				//SELECT文作成
				#region
				cmdTxt = "SELECT T.ObsMgrID, C.CatName, T.MgrNo, T.Sbj, T.EntDt";
				cmdTxt += ",S.Text" + Common.LangID.ToString() + " AS Status, S.Finished AS Finished";
				cmdTxt += " FROM T_ObsMgr2 AS T";
				cmdTxt += " LEFT JOIN T_ObsTree AS O ON O.ObsMgrID=T.ObsMgrID";
				cmdTxt += " LEFT JOIN T_ObsCategoryTree AS C ON C.CatID=O.CatID";
				cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=T.Status";
				cmdTxt += " LEFT JOIN M_Prj_Typ AS P ON P.TypCD=T.PrjID";
				cmdTxt += " LEFT JOIN (SELECT ObsMgrID,RcgDateTime,RcgUserID,ReqDateTime FROM T_ObsMgr_RcgUsr WHERE NOT Exists(SELECT * FROM T_ObsMgr_RcgUsr AS TR WHERE TR.ObsMgrID=T_ObsMgr_RcgUsr.ObsMgrID AND TR.Seq>T_ObsMgr_RcgUsr.Seq)) AS R ON R.ObsMgrID=T.ObsMgrID "; //承認状態取得用
				#endregion

				//条件文が設定されていたらSQL文に追加
				if(this.mCondition!= string.Empty) cmdTxt += this.mCondition;
				//ｿｰﾄ順が設定されていたらSQL文に追加
				if(this.mSortCondition != string.Empty) cmdTxt += this.mSortCondition;

				//for debug (SQL文を画面表示)
				//Response.Write(cmdTxt);
				
				dr = sql.Read(cmdTxt);
				using(CSql sql2 = new CSql())
				{
					sql2.Open();
					while(dr.Read() == true)
					{
						DataRow row = table.NewRow();
						row["ObsMgrID"] = dr["ObsMgrID"];
						row["CatName"] = HttpUtility.HtmlEncode(dr["CatName"].ToString());
						row["MgrNo"] = "<a href=javascript:OpenDetailWindow('" + dr["ObsMgrID"] + "')>" + HttpUtility.HtmlEncode(dr["MgrNo"].ToString()) + "</a>";
						//状態によりｱｲｺﾝを変える
						string fileName;
						if(Convert.ToBoolean(dr["Finished"]) == true)	fileName = "finish1.gif";
						else											fileName = "subject1.gif";
						row["Subject"] = "<img src='../../img/" + fileName + "'></img>&nbsp;" + "<a href=javascript:OpenDetailWindow('" + dr["ObsMgrID"] + "')>" + HttpUtility.HtmlEncode(dr["Sbj"].ToString()) + "</a>";
						row["Status"] = HttpUtility.HtmlEncode(dr["Status"].ToString());
						//EntDtの形式はyyyyMMddHHmmssで登録されているので多少変更する
						row["DateTime"] = this.DateTimeCnv(dr["EntDt"]);
						//承認状態設定
						row["ApprovalStatus"] = Common.GetRcgStatusString(sql2, dr["ObsMgrID"].ToString());
						table.Rows.Add(row);
					}
					dr.Close();
				}
			}
			this.DataGridResult.DataSource = table;
			this.DataGridResult.DataBind();

			this.LabelCnt.Text = table.Rows.Count.ToString();
		}
		#endregion

		/// <summary>
		/// yyyyMMddHHmmssをyyyy/MM/dd HH:mm:ssに変換
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
        //［SRC-03-03-01-033］private string DateTimeCnv
        #region private string DateTimeCnv(object src)
		private string DateTimeCnv(object src)
		{
			string str = src.ToString();
			if(str.Length == 14)
			{
				return str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2) + " " + str.Substring(8, 2) + ":" + str.Substring(10, 2) + ":" + str.Substring(12, 2);
			}
			else
			{
				return str;
			}
		}
		#endregion
































	}
}
