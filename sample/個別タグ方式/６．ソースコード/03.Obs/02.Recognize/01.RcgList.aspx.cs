using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;

namespace EPSNet.Obs.Recognize
{
	/// <summary>
	/// RcgList の概要の説明です。
	/// </summary>
	public class RcgList : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGridResult;
	
		#region Web フォーム デザイナで生成されたコード 
		//［SRC-03-02-01-001］override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//［SRC-03-02-01-002］private void InitializeComponent
		private void InitializeComponent()
		{    
			this.ButtonSearch.Click += new System.EventHandler(this.ButtonRefresh_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ定数
		///================================================================

		private Type mType = typeof(RcgList);
		
		private const int COL_MgrNo = 0;
		private const int COL_CatName = 1;
		private const int COL_Subject = 2;
		private const int COL_Status = 3;
		private const int COL_Date = 4;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Button ButtonSearch;
		protected System.Web.UI.WebControls.Label LabelCnt;
		protected System.Web.UI.WebControls.Label LabelResultTitle;
		private const int COL_Disabled = 5;

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//［SRC-03-02-01-003］private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			//LoginID取得
			string id = Request.QueryString["UserName"];
			if(id == null && Common.LoginID != null) id = Common.LoginID;
			if(id == null)
			{
				//ｴﾗｰﾍﾟｰｼﾞへ遷移
				Response.Write("<script>");
				Response.Write("document.location='../../Common/Message.aspx'");
				Response.Write("</script>");
			}
			else
			{
				Common.SetSession(SessionID.LoginID, id);
				Common.GetLanguage();
			}

			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					sql.Open();

					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle);
					Common.SetText(sql, this.mType, this.LabelResultTitle);
					this.DataGridResult.Columns[COL_MgrNo].HeaderText = Common.GetText(sql, this.mType, "Col1");
					this.DataGridResult.Columns[COL_CatName].HeaderText = Common.GetText(sql, this.mType, "Col2");
					this.DataGridResult.Columns[COL_Subject].HeaderText = Common.GetText(sql, this.mType, "Col3");
					this.DataGridResult.Columns[COL_Status].HeaderText = Common.GetText(sql, this.mType, "Col4");
					this.DataGridResult.Columns[COL_Date].HeaderText = Common.GetText(sql, this.mType, "Col5");
					#endregion
				}
			}

			//承認待ち一覧作成
			this.CreateResultList();
		}
		#endregion

		/// <summary>
		/// Refreshﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonRefresh_Click(object sender, System.EventArgs e)
		//［SRC-03-02-01-004］private void ButtonRefresh_Click
		private void ButtonRefresh_Click(object sender, System.EventArgs e)
		{
			this.CreateResultList();
		}
		#endregion




		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================

		/// <summary>
		/// 検索結果一覧作成
		/// </summary>
		/// <param name="sql"></param>
		#region private void CreateResultList()
		//［SRC-03-02-01-005］private void CreateResultList
		private void CreateResultList()
		{
			DataTable table = new DataTable();
			table.Columns.Add("MgrNo");
			table.Columns.Add("CatName");
			table.Columns.Add("Subject");
			table.Columns.Add("Status");
			table.Columns.Add("DateTime");
			table.Columns.Add("ObsMgrID");

			using(CSql sql = new CSql())
			{
				sql.Open();
				string cmdTxt;
				SqlDataReader dr;

				cmdTxt = "SELECT T.ObsMgrID, C.CatName, T.MgrNo, T.Sbj, T.EntDt, S.Text" + Common.LangID.ToString() + " AS Status FROM T_ObsMgr2 AS T";
				cmdTxt += " LEFT JOIN T_ObsTree AS O ON O.ObsMgrID=T.ObsMgrID";
				cmdTxt += " LEFT JOIN T_ObsCategoryTree AS C ON C.CatID=O.CatID";
				cmdTxt += " LEFT JOIN M_ObsStatus AS S ON S.StatusID=T.Status";
				cmdTxt += " LEFT JOIN T_ObsMgr_RcgUsr AS R ON R.ObsMgrID=T.ObsMgrID";
				cmdTxt += " WHERE R.RcgUserID=@UserID";
				cmdTxt += " AND R.RcgDateTime IS NULL";
				cmdTxt += " ORDER BY R.ReqDateTime desc";
				sql.AddParam("@UserID", Common.LoginID);
				//for debug
				//Response.Write(cmdTxt);
				dr = sql.Read(cmdTxt);

				using(CSql sql2 = new CSql())
				{
					sql2.Open();
					while(dr.Read() == true)
					{
						DataRow row = table.NewRow();
						row["ObsMgrID"] = dr["ObsMgrID"];
						row["CatName"] = HttpUtility.HtmlEncode(dr["CatName"].ToString());
						row["MgrNo"] = "<a href=javascript:OpenDetailWindow('" + dr["ObsMgrID"] + "')>" + HttpUtility.HtmlEncode(dr["MgrNo"].ToString()) + "</a>";
						row["Subject"] = "<a href=javascript:OpenDetailWindow('" + dr["ObsMgrID"] + "')>" + HttpUtility.HtmlEncode(dr["Sbj"].ToString()) + "</a>";
						row["Status"] = HttpUtility.HtmlEncode(dr["Status"].ToString());
						//EntDtの形式はyyyyMMddHHmmssで登録されているので多少変更する
						string ent = dr["EntDt"].ToString();
						row["DateTime"] = ent.Substring(0, 4) + "/" + ent.Substring(4, 2) + "/" + ent.Substring(6, 2) + " " + ent.Substring(8, 2) + ":" + ent.Substring(10, 2) + ":" + ent.Substring(12, 2);
						table.Rows.Add(row);
					}
					dr.Close();
				}
			}
			this.DataGridResult.DataSource = table;
			this.DataGridResult.DataBind();

			this.LabelCnt.Text = table.Rows.Count.ToString();
		}
		#endregion




	}
}
