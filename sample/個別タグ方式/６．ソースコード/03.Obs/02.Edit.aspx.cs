using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Text;
using System.IO;

namespace EPSNet.Obs
{
	/// <summary>
	/// Edit の概要の説明です。
	/// </summary>
	public class Edit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonUpdate;
		protected System.Web.UI.WebControls.Button ButtonAdd;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelMgrNo;
		protected System.Web.UI.WebControls.DropDownList DropDownListProject;
		protected System.Web.UI.WebControls.DropDownList DropDownListStatus;
		protected System.Web.UI.WebControls.Label LabelHelp2;
		protected System.Web.UI.WebControls.Label LabelHelp1;
		protected System.Web.UI.WebControls.Label LabelMgrNoValue;
		protected System.Web.UI.WebControls.Label LabelHelp3;
		protected System.Web.UI.WebControls.TextBox TextBoxSubject;
		protected System.Web.UI.WebControls.Button ButtonCancel;
		protected System.Web.UI.WebControls.DataGrid DataGridLib;
		protected System.Web.UI.WebControls.DataGrid DataGridLink;
		protected System.Web.UI.HtmlControls.HtmlTableRow TRAddPosition;
		protected System.Web.UI.WebControls.RadioButton RadioButtonUp;
		protected System.Web.UI.WebControls.RadioButton RadioButtonDown;
		protected System.Web.UI.WebControls.RadioButton RadioButtonChild;
		protected System.Web.UI.WebControls.Button ButtonRefresh;
		protected System.Web.UI.WebControls.TextBox TextBoxAppUser;
		protected System.Web.UI.WebControls.Button ButtonClear;
		protected System.Web.UI.WebControls.Panel PanelAdd;
		protected System.Web.UI.WebControls.Panel PanelEdit;
		protected System.Web.UI.WebControls.Button ButtonHistory;
		protected System.Web.UI.WebControls.Button ButtonBack;
		protected System.Web.UI.WebControls.Label LabelSbj;
		protected System.Web.UI.WebControls.Label LabelStatus;
		protected System.Web.UI.WebControls.Label LabelProject;
		protected System.Web.UI.WebControls.Label LabelPos;
		protected System.Web.UI.WebControls.Label LabelBody;
		protected System.Web.UI.WebControls.HyperLink HyperLinkFiles;
		protected System.Web.UI.WebControls.HyperLink HyperLinkLinks;
		protected System.Web.UI.WebControls.HyperLink HyperLinkRcg;
		protected System.Web.UI.HtmlControls.HtmlInputHidden ServerName;
		protected System.Web.UI.HtmlControls.HtmlInputHidden oldProjectID;
		protected System.Web.UI.WebControls.Button ButtonClose;
		protected System.Web.UI.WebControls.TextBox TextBoxContent;
	
		#region Web フォーム デザイナで生成されたコード 
		//［SRC-03-02-001］override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
			this.ButtonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
			this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
			this.DropDownListProject.SelectedIndexChanged += new System.EventHandler(this.DropDownListProject_SelectedIndexChanged);
			this.DataGridLib.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGridLib_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄｸﾗｽ
		///================================================================

		/// <summary>
		/// ﾘﾝｸ情報ｸﾗｽ
		/// </summary>
		#region private class CLink
		//［SRC-03-02-002］private class CLink
		private class CLink
		{
			public string LinkName;
			public string LinkUrl;
			public string LinkID;
			public CLink(string LinkName, string LinkUrl, string LinkID)
			{
				this.LinkName = LinkName;
				this.LinkUrl = LinkUrl;
				this.LinkID = LinkID;
			}
			public CLink(string LinkName, string LinkUrl)
			{
				this.LinkName = LinkName;
				this.LinkUrl = LinkUrl;
				this.LinkID = string.Empty;
			}
		}
		#endregion

		/// <summary>
		/// 添付ﾌｧｲﾙ情報ｸﾗｽ
		/// </summary>
		#region private class CLib
		//［SRC-03-02-003］private class CLib
		private class CLib
		{
			public string UserPath;
			public string ServerPath;
			public string ServerName;
			public string UpdDt;
			public string Comment;
			public CLib(string UserPath, string ServerPath, string ServerName, string Comment)
			{
				this.UserPath = UserPath;
				this.ServerPath = ServerPath;
				this.ServerName = ServerName;
				this.Comment = Comment;
			}
			public CLib(string UserPath, string ServerPath, string ServerName, string Comment, string UpdDt)
			{
				this.UserPath = UserPath;
				this.ServerPath = ServerPath;
				this.ServerName = ServerName;
				this.Comment = Comment;
				this.UpdDt = UpdDt;
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Edit);
		

		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//［SRC-03-02-004］private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				this.ButtonCancel.Attributes["onclick"] = "window.close();";

				if(this.IsPostBack == false)
				{
					this.ButtonClose.Visible = false;

					string mode = Request.QueryString["mode"];
					if(mode == "Rcg")
					{
						//承認画面から開かれた場合、戻るﾎﾞﾀﾝ非表示・閉じるﾎﾞﾀﾝ表示
						this.ButtonBack.Visible = false;
						this.ButtonClose.Visible = true;
						//閉じられた場合に、承認一覧を更新
						this.ButtonClose.Attributes["onclick"] = "self.opener.location='./Recognize/RcgList.aspx';self.window.close();";
					}

					string id = Request.QueryString["id"];

					//ﾘﾝｸ情報ｸﾘｱ
					Common.SetSession(SessionID.Obs_Edit_Link, null);
					//添付ﾌｧｲﾙ情報ｸﾘｱ
					Common.SetSession(SessionID.Obs_Edit_Lib, null);
					//一時用の添付ﾌｫﾙﾀﾞを削除
					string filePath = Path.Combine(Server.MapPath(string.Empty), @"Lib\Tmp");
					filePath = Path.Combine(filePath, Common.LoginID);	//ﾕｰｻﾞID
					if(Directory.Exists(filePath) == true) Directory.Delete(filePath, true);
					//承認者ｾｯｼｮﾝ情報ｸﾘｱ
					Common.SetSession(SessionID.Obs_Edit_RcgUser, null);

					//承認履歴ﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定(ｻｰﾊﾞｰにいかないようにreturn falseする)
					this.ButtonHistory.Attributes["onclick"] = "OpenHistory('" + id + "');return false;";
					//戻るﾎﾞﾀﾝｽｸﾘﾌﾟﾄ設定(ｻｰﾊﾞｰにいかないようにreturn falseする)
					this.ButtonBack.Attributes["onclick"] = "document.location='./Detail.aspx?id=" + id + "';return false;";

					using(CSql sql = new CSql())
					{
						sql.Open();
						//ｻｰﾊﾞｰ名取得(ｶﾚﾝﾀﾞｰ表示の為)
						this.ServerName.Value = Common.GetHttpHost(sql);
						//ﾌﾟﾛｼﾞｪｸﾄ一覧作成
						this.CreateProjectList(sql);
						//ｽﾃｰﾀｽ一覧作成
						Common.CreateStatusDropDownList(sql, this.DropDownListStatus);
						//多国語対応
						#region
						Common.SetText(sql, this.mType, this.LabelHelp1);
						Common.SetText(sql, this.mType, this.LabelHelp2);
						Common.SetText(sql, this.mType, this.LabelHelp3);
						Common.SetText(sql, this.mType, this.ButtonCancel);
						Common.SetText(sql, this.mType, this.ButtonClear);
						Common.SetText(sql, this.mType, this.ButtonBack);
						Common.SetText(sql, this.mType, this.ButtonHistory);
						Common.SetText(sql, this.mType, this.ButtonAdd);
						Common.SetText(sql, this.mType, this.ButtonUpdate);
						Common.SetText(sql, this.mType, this.ButtonClose);
						Common.SetText(sql, this.mType, this.LabelMgrNo);
						Common.SetText(sql, this.mType, this.HyperLinkRcg);
						Common.SetText(sql, this.mType, this.LabelSbj);
						Common.SetText(sql, this.mType, this.LabelStatus);
						Common.SetText(sql, this.mType, this.LabelPos);
						Common.SetText(sql, this.mType, this.LabelBody);
						Common.SetText(sql, this.mType, this.HyperLinkFiles);
						Common.SetText(sql, this.mType, this.HyperLinkLinks);
						Common.SetText(sql, this.mType, this.TextBoxSubject);
						Common.SetText(sql, this.mType, this.TextBoxContent);
						Common.SetText(sql, this.mType, this.TextBoxAppUser);
						this.LabelProject.Text = HttpUtility.HtmlEncode(Common.GetNumberingItemName(sql)) + "<sup>*</sup>";
						//固定項目tips設定
//						this.TextBoxSubject.ToolTip = Common.GetTips(sql, this.mType, "LabelSbj");//件名
//						this.TextBoxContent.ToolTip = Common.GetTips(sql, this.mType, "LabelBody");//内容
//						this.TextBoxAppUser.ToolTip = Common.GetTips(sql, this.mType, "HyperLinkRcg");//承認者

						#endregion
						
						if(id == null)
						{
							//追加時
							this.PanelAdd.Visible = true;
							this.PanelEdit.Visible = false;
							//ﾀｲﾄﾙ設定
							this.LabelTitle.Text = Common.GetText(sql, this.mType, "LabelTitleNew");
							//未採番
							this.LabelMgrNoValue.Text = Common.GetText(sql, this.mType, "NewMgrNo");
							//追加位置表示
							this.TRAddPosition.Visible = true;
							//選択されている障害が存在しない場合は、追加位置指定は無効
							if(Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID) == null)
							{
								this.RadioButtonChild.Enabled = this.RadioButtonDown.Enabled = this.RadioButtonUp.Enabled = false;
							}
							//登録済み添付ﾌｧｲﾙ一覧作成
							this.CreateLibList(sql, id);
							//登録済みﾘﾝｸ一覧作成
							this.CreateLinkList(sql, id);
						}
						else
						{
							//更新時
							this.PanelAdd.Visible = false;
							this.PanelEdit.Visible = true;
							//ﾀｲﾄﾙ設定
							this.LabelTitle.Text = Common.GetText(sql, this.mType, "LabelTitleUpdate");
							//追加位置非表示
							this.TRAddPosition.Visible = false;
							//ﾃﾞｰﾀ表示
							#region
							this.ButtonUpdate.Visible = true;
							this.ButtonHistory.Visible = true;
							//this.DropDownListProject.Enabled = false;
							SqlDataReader dr;
							string cmdTxt;
							cmdTxt = "SELECT O.MgrNo, O.Sbj AS Subject, O.PrjID AS TypCD, O.Cnt AS Body, O.Status, M.Lang0" + Common.LangID.ToString() + " AS Lang FROM T_ObsMgr2 AS O";
							cmdTxt += " LEFT JOIN M_Prj_Typ AS M ON M.TypCD=O.PrjID WHERE O.ObsMgrID=@ObsMgrID";
							sql.AddParam("@ObsMgrID", id);
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								this.LabelMgrNoValue.Text = HttpUtility.HtmlEncode(dr["MgrNo"].ToString());
								this.TextBoxSubject.Text = dr["Subject"].ToString();
								Common.SetText(this.DropDownListProject, dr["Lang"].ToString());
								Common.SetValue(this.DropDownListStatus, dr["Status"].ToString());
								this.TextBoxContent.Text = dr["Body"].ToString();
								this.oldProjectID.Value = dr["TypCD"].ToString();
							}
							dr.Close();
							#endregion
							//承認ﾕｰｻﾞ表示
							#region
							//未承認のﾕｰｻﾞがいれば表示
							cmdTxt = "SELECT U.UserID, U.FullName FROM T_ObsMgr_RcgUsr AS T";
							cmdTxt += " LEFT JOIN M_users AS U ON U.UserID=T.RcgUserID";
							cmdTxt += " WHERE ObsMgrID=@ObsMgrID AND RcgDateTime IS NULL";
							sql.AddParam("@ObsMgrID", id);
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								this.TextBoxAppUser.Text = dr["FullName"].ToString();
								Common.SetSession(SessionID.Obs_Edit_RcgUser, dr["UserID"].ToString());
								this.ButtonClear.Visible = true;
							}
							dr.Close();
							#endregion
							//登録済み添付ﾌｧｲﾙ一覧作成
							this.CreateLibList(sql, id);
							//登録済みﾘﾝｸ一覧作成
							this.CreateLinkList(sql, id);
						}
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ｷｬﾝｾﾙﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonCancel_Click(object sender, System.EventArgs e)
		//［SRC-03-02-005］private void ButtonCancel_Click
		private void ButtonCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("./Search/List.aspx");
		}
		#endregion

		/// <summary>
		/// 承認履歴ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonHistory_Click(object sender, System.EventArgs e)
		//［SRC-03-02-006］private void ButtonHistory_Click
		private void ButtonHistory_Click(object sender, System.EventArgs e)
		{
		
		}
		#endregion

		/// <summary>
		/// 追加ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonAdd_Click(object sender, System.EventArgs e)
		//［SRC-03-02-007］private void ButtonAdd_Click
		private void ButtonAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				string ObsMgrID;
				//追加処理
				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//入力ﾃﾞｰﾀﾁｪｯｸ
						if(this.DataCheck(sql) == false)
						{
							sql.RollbackTrans();
							return;
						}
						//ﾌﾟﾛｼﾞｪｸﾄのﾃﾝﾌﾟﾚｰﾄで使用されている項目一覧を取得する
						ArrayList ItemID = new ArrayList();
						ArrayList DataType = new ArrayList();
						#region
						string TemplateID = this.DropDownListProject.SelectedValue.Split(':')[1];
						cmdTxt = "SELECT I.ItemID, I.DataType FROM M_ObsTemplateItem AS T";
						cmdTxt += " LEFT JOIN M_ObsItem AS I ON I.ItemID=T.ItemID WHERE T.TemplateID=@TemplateID ORDER BY T.SortID";
						sql.AddParam("@TemplateID", TemplateID);
						dr = sql.ReadTrans(cmdTxt);
						while(dr.Read() == true)
						{
							ItemID.Add(dr["ItemID"]);
							DataType.Add(dr["DataType"]);
						}
						dr.Close();
						#endregion
						//障害IDを作成
						ObsMgrID = DateTime.Now.ToString("yyMMddHHmmss");
						#region
						cmdTxt = "SELECT ObsMgrID FROM T_ObsMgr2 WHERE ObsMgrID like @ObsMgrID ORDER BY ObsMgrID DESC";
						sql.AddParam("@ObsMgrID", ObsMgrID + "%");
						dr = sql.ReadTrans(cmdTxt);
						if(dr.Read() == true)
						{
							int index = Convert.ToInt32(dr["ObsMgrID"].ToString().Substring(12, 2));
							ObsMgrID += (index + 1).ToString("00");
						}
						else
						{
							ObsMgrID += "00";
						}
						dr.Close();
						#endregion
						//管理番号を作成
						string mgrNo = this.CreateMgrNo(sql);
						//管理ﾃｰﾌﾞﾙに追加
						#region
						cmdTxt = "INSERT INTO T_ObsMgr2(ObsMgrID, Status, Sbj, PrjID, MgrNo, Cnt, EntUsrID, EntDt, UpdDt, ModUsrID)";
						cmdTxt += " VALUES(@ObsMgrID, @Status, @Subject, @TypCD, @MgrNo, @Body, @UserID, @CreateDateTime, @UpdateDateTime, @UserID)";
						sql.AddParam("@ObsMgrID", ObsMgrID);
						sql.AddParam("@MgrNo", mgrNo);
						sql.AddParam("@Subject", Common.CnvToDB(this.TextBoxSubject.Text));
						sql.AddParam("@TypCD", this.DropDownListProject.SelectedValue.Split(':')[0]);
						sql.AddParam("@Status", this.DropDownListStatus.SelectedValue);
						sql.AddParam("@Body", Common.CnvToDB(this.TextBoxContent.Text));
						sql.AddParam("@CreateDateTime", DateTime.Now.ToString("yyyyMMddHHmmss"));
						sql.AddParam("@UpdateDateTime", DateTime.Now.ToString("yyyyMMddHHmmss"));
						sql.AddParam("@UserID", Common.CnvToDB(Common.LoginID));
						sql.CommandTrans(cmdTxt);
						#endregion
						//各ｱｲﾃﾑﾃﾞｰﾀ追加
						#region
						for(int i = 0; i < ItemID.Count; i++)
						{
							int dataType = Convert.ToInt32(DataType[i]);
							string itemId = ItemID[i].ToString();
							cmdTxt = "INSERT INTO T_ObsMgrData(ObsMgrID, ItemID, Data" + dataType.ToString() + ")";
							cmdTxt += " VALUES(@ObsMgrID, @ItemID, @Data)";
							sql.AddParam("@ObsMgrID", ObsMgrID);
							sql.AddParam("@ItemID", itemId);
							switch(dataType)
							{
								case 1:
								case 3:
								case 4:
								case 5:
									//ﾃｷｽﾄ型
									string data = Common.CnvToDB(Request.Form["TextBox" + itemId]);
									if(data == string.Empty)	sql.AddParam("@Data", DBNull.Value);
									else						sql.AddParam("@Data", data);
									break;
								case 2:
								case 6:
								case 7:
									//ﾘｽﾄ型
									sql.AddParam("@Data", Common.CnvToDB(Request.Form["SelectList" + itemId]));
									break;
							}
							sql.CommandTrans(cmdTxt);
						}
						#endregion
						//障害ﾂﾘｰに追加
						if(Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID) == null)	this.AddTopPos(sql, ObsMgrID);	//障害未選択で追加場合
						else if(this.RadioButtonDown.Checked == true)						this.AddDownPos(sql, ObsMgrID);	//下へ追加
						else if(this.RadioButtonUp.Checked == true)							this.AddUpPos(sql, ObsMgrID);	//上へ追加
						else																this.AddChild(sql, ObsMgrID);	//子階層へ追加
						//ﾘﾝｸ情報保存
						this.SaveLinkInfo(sql, ObsMgrID);
						//添付ﾌｧｲﾙ情報保存
						this.SaveLibInfo(sql, ObsMgrID);
						//承認者情報保存
						this.SaveAppUserInfo(sql, ObsMgrID);

						//ﾄﾗﾝｻﾞｸｼｮﾝｺﾐｯﾄ
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//自動ﾒｰﾙ送信ﾌｫｰﾑ呼び出し&更新完了画面へ遷移のｽｸﾘﾌﾟﾄを作成
				string script = "loadSendMail('" + this.DropDownListProject.SelectedValue.Split(':')[0] + "','" + ObsMgrID + "','11');\n";
				script += "self.location='EditFinish.aspx?id=" + ObsMgrID + "';";
				this.RegisterStartupScript("script", "<script>"  + script + "</script>");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// 更新ﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonUpdate_Click(object sender, System.EventArgs e)
		//［SRC-03-02-008］private void ButtonUpdate_Click
		private void ButtonUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				//障害ID
				string ObsMgrID = Request.QueryString["id"];

				using(CSql sql = new CSql())
				{
					string cmdTxt = string.Empty;
					SqlDataReader dr;
					sql.Open();
					sql.BeginTrans();
					try
					{
						//入力ﾃﾞｰﾀﾁｪｯｸ
						if(this.DataCheck(sql) == false)
						{
							sql.RollbackTrans();
							return;
						}

						//ﾌﾟﾛｼﾞｪｸﾄのﾃﾝﾌﾟﾚｰﾄで使用されている項目一覧を取得する
						ArrayList ItemID = new ArrayList();
						ArrayList DataType = new ArrayList();
						#region
						string TemplateID = this.DropDownListProject.SelectedValue.Split(':')[1];
						cmdTxt = "SELECT I.ItemID, I.DataType FROM M_ObsTemplateItem AS T";
						cmdTxt += " LEFT JOIN M_ObsItem AS I ON I.ItemID=T.ItemID WHERE T.TemplateID=@TemplateID ORDER BY T.SortID";
						sql.AddParam("@TemplateID", TemplateID);
						dr = sql.ReadTrans(cmdTxt);
						while(dr.Read() == true)
						{
							ItemID.Add(dr["ItemID"]);
							DataType.Add(dr["DataType"]);
						}
						dr.Close();
						#endregion

						//管理番号を作成
						string mgrNo = HttpUtility.HtmlDecode(this.LabelMgrNoValue.Text);
						if(this.DropDownListProject.SelectedValue.Split(':')[0] != this.oldProjectID.Value)
							mgrNo = this.CreateMgrNo(sql);
						//管理ﾃｰﾌﾞﾙを更新
						#region
						cmdTxt = "UPDATE T_ObsMgr2 SET Sbj=@Subject, MgrNo=@MgrNo, PrjID=@TypCD, Status=@Status, Cnt=@Body, UpdDt=@UpdDt, ModUsrID=@UserID WHERE ObsMgrID=@ObsMgrID";
						sql.AddParam("@ObsMgrID", ObsMgrID);
						sql.AddParam("@Subject", HttpUtility.HtmlDecode(this.TextBoxSubject.Text));
						sql.AddParam("@MgrNo", mgrNo);
						sql.AddParam("@TypCD", this.DropDownListProject.SelectedValue.Split(':')[0]);
						sql.AddParam("@Status", this.DropDownListStatus.SelectedValue);
						sql.AddParam("@Body", Common.CnvFromDB(this.TextBoxContent.Text));
						sql.AddParam("@UpdDt", Common.CnvFromDB(DateTime.Now.ToString("yyyyMMddHHmmss")));
						sql.AddParam("@UserID", Common.CnvFromDB(Common.LoginID));
						sql.CommandTrans(cmdTxt);

						#endregion

						//** 更新ではﾃﾝﾌﾟﾚｰﾄにｱｲﾃﾑが追加された場合に対応出来ないのでDelete/Insertを行う
//						//ｱｲﾃﾑﾃﾞｰﾀ更新
//						#region
//						for(int i = 0; i < ItemID.Count; i++)
//						{
//							int dataType = Convert.ToInt32(DataType[i]);
//							string itemId = ItemID[i].ToString();
//							cmdTxt = "UPDATE T_ObsMgrData SET Data" + dataType.ToString() + "=@Data WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
//							sql.AddParam("@ObsMgrID", ObsMgrID);
//							sql.AddParam("@ItemID", itemId);
//							switch(dataType)
//							{
//								case 1:
//								case 3:
//								case 4:
//								case 5:
//									string data = Common.CnvToDB(Request.Form["TextBox" + itemId]);
//									if(data == string.Empty)	sql.AddParam("@Data", DBNull.Value);
//									else						sql.AddParam("@Data", data);
//									break;
//								case 2:
//								case 6:
//								case 7:
//									sql.AddParam("@Data", Request.Form["SelectList" + itemId].Trim());
//									break;
//							}
//							sql.CommandTrans(cmdTxt);
//						}
//						#endregion

						//各ｱｲﾃﾑﾃﾞｰﾀ更新
						#region
						//削除処理
						cmdTxt = "DELETE FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID";
						sql.AddParam("@ObsMgrID", ObsMgrID);
						sql.CommandTrans(cmdTxt);

						for(int i = 0; i < ItemID.Count; i++)
						{
							int dataType = Convert.ToInt32(DataType[i]);
							string itemId = ItemID[i].ToString();
							//追加
							cmdTxt = "INSERT INTO T_ObsMgrData(ObsMgrID, ItemID, Data" + dataType.ToString() + ")";
							cmdTxt += " VALUES(@ObsMgrID, @ItemID, @Data)";
							sql.AddParam("@ObsMgrID", ObsMgrID);
							sql.AddParam("@ItemID", itemId);
							switch(dataType)
							{
								case 1:
								case 3:
								case 4:
								case 5:
									//ﾃｷｽﾄ型
									string data = HttpUtility.HtmlDecode(Request.Form["TextBox" + itemId]);
									if(data == string.Empty)	sql.AddParam("@Data", DBNull.Value);
									else						sql.AddParam("@Data", data);
									break;
								case 2:
								case 6:
								case 7:
									//ﾘｽﾄ型
									sql.AddParam("@Data", Common.CnvToDB(Request.Form["SelectList" + itemId]));
									break;
							}
							sql.CommandTrans(cmdTxt);
						}
						#endregion

						//ﾘﾝｸ情報保存
						this.SaveLinkInfo(sql, ObsMgrID);
						//添付ﾌｧｲﾙ情報保存
						this.SaveLibInfo(sql, ObsMgrID);
						//承認者情報保存
						this.SaveAppUserInfo(sql, ObsMgrID);

						//未使用の障害ﾃﾞｰﾀを削除する
						Common.DeleteNoUseItemData(sql, ObsMgrID);

						//ﾄﾗﾝｻﾞｸｼｮﾝｺﾐｯﾄ
						sql.CommitTrans();
					}
					catch(Exception ex)
					{
						sql.RollbackTrans();
						Common.Alert(this, ex.Message);
						return;
					}
				}
				//自動ﾒｰﾙ送信ﾌｫｰﾑ呼び出し&更新完了画面へ遷移のｽｸﾘﾌﾟﾄを作成
				string script = "loadSendMail('" + this.DropDownListProject.SelectedValue.Split(':')[0] + "','" + ObsMgrID + "','11');\n";
				script += "self.location='EditFinish.aspx?id=" + Request.QueryString["id"] + "';";
				this.RegisterStartupScript("script", "<script language=\"javascript\" type=\"text/javascript\">"  + script + "</script>");
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion

		/// <summary>
		/// ﾌﾟﾛｼﾞｪｸﾄ選択ｲﾍﾞﾝﾄ
		/// </summary>
		#region private void DropDownListProject_SelectedIndexChanged(object sender, System.EventArgs e)
		//［SRC-03-02-009］private void DropDownListProject_SelectedIndexChanged
		private void DropDownListProject_SelectedIndexChanged(object sender, System.EventArgs e)
		{


		
		}
		#endregion

		/// <summary>
		/// Refreshﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonRefresh_Click(object sender, System.EventArgs e)
		//［SRC-03-02-010］private void ButtonRefresh_Click
		private void ButtonRefresh_Click(object sender, System.EventArgs e)
		{
			using(CSql sql = new CSql())
			{
				sql.Open();
				//承認者表示
				this.DispAppUser(sql);
				//ﾘﾝｸ一覧作成
				this.CreateLinkList(sql, string.Empty);
				//添付ﾌｧｲﾙ一覧作成
				this.CreateLibList(sql, string.Empty);
			}
		}
		#endregion

		/// <summary>
		/// 承認者設定ﾕｰｻﾞｸﾘｱﾎﾞﾀﾝｸﾘｯｸ
		/// </summary>
		#region private void ButtonClear_Click(object sender, System.EventArgs e)
		//［SRC-03-02-011］private void ButtonClear_Click
		private void ButtonClear_Click(object sender, System.EventArgs e)
		{
			this.ButtonClear.Visible = false;
			Common.SetSession(SessionID.Obs_Edit_RcgUser, null);
			this.TextBoxAppUser.Text = string.Empty;
		}
		#endregion

		/// <summary>
		/// ItemDataBoundｲﾍﾞﾝﾄ
		/// </summary>
		#region private void DataGridLib_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		//［SRC-03-02-012］private void DataGridLib_ItemDataBound
		private void DataGridLib_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1) return;

			//ﾘﾝｸURLが設定されていない添付ﾌｧｲﾙは、赤表示する
			HyperLink hl = e.Item.Cells[0].Controls[0] as HyperLink;
			if(hl.NavigateUrl == string.Empty)
			{
				e.Item.Cells[0].ForeColor = Color.Red;
			}
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ関数
		///================================================================


		/// <summary>
		/// 最上位階層の一番上に追加
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ObsMgrID"></param>
		#region private void AddTopPos(CSql sql, string ObsMgrID)
		//［SRC-03-02-013］private void AddTopPos
		private void AddTopPos(CSql sql, string ObsMgrID)
		{
			string catID = "-1";
			catID = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();

			string cmdTxt;

			//ParentID=-1のSortIDを+1する
			#region
			cmdTxt = "UPDATE T_ObsTree SET SortID=SortID + 1 WHERE ParentID='-1'";
			sql.CommandTrans(cmdTxt);
			#endregion

			//障害ﾂﾘｰに追加
			#region
			cmdTxt = "INSERT INTO T_ObsTree(CatID,ObsMgrID,SortID,ParentID)VALUES(@CatID,@ObsMgrID,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.AddParam("@SortID", 1);
			sql.AddParam("@ParentID", "-1");
			sql.CommandTrans(cmdTxt);
			#endregion

		}
		#endregion

		/// <summary>
		/// 下に追加
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ObsMgrID"></param>
		#region private void AddDownPos(CSql sql, string ObsMgrID)
		//［SRC-03-02-014］private void AddDownPos
		private void AddDownPos(CSql sql, string ObsMgrID)
		{
			string id = "-1";
			string catID = "-1";
			string parentID = "-1";
			int sortID = 0;

			catID = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
			if(Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID) != null) id = Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID).ToString();

			string cmdTxt;
			SqlDataReader dr;

			//ParentID, SortIDを取得
			#region
			if(id != "-1")	//選択されている場合その下に挿入
			{
				cmdTxt = "SELECT ParentID, SortID FROM T_ObsTree WHERE ObsMgrID=@ObsMgrID";
				sql.AddParam("@ObsMgrID", id);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					parentID = dr["ParentID"].ToString().Trim();
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			else			//選択されていない場合は、一番最後に追加
			{
				cmdTxt = "SELECT SortID FROM T_ObsTree WHERE ParentID='-1' AND CatID=@CatID ORDER BY SortID DESC";
				sql.AddParam("@CatID", catID);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			#endregion

			//同一階層で自分より後ろの全てのﾂﾘｰのSortIDを+1する
			#region
			cmdTxt = "UPDATE T_ObsTree SET SortID=SortID + 1 WHERE ParentID=@ParentID AND SortID > @SortID";
			sql.AddParam("@ParentID", parentID);
			sql.AddParam("@SortID", sortID);
			sql.CommandTrans(cmdTxt);
			#endregion

			//障害ﾂﾘｰに追加
			#region
			cmdTxt = "INSERT INTO T_ObsTree(CatID,ObsMgrID,SortID,ParentID)VALUES(@CatID,@ObsMgrID,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.AddParam("@SortID", sortID + 1);
			sql.AddParam("@ParentID", parentID);
			sql.CommandTrans(cmdTxt);
			#endregion

		}
		#endregion


		/// <summary>
		/// 上に追加
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ObsMgrID"></param>
		#region private void AddUpPos(CSql sql, string ObsMgrID)
		//［SRC-03-02-015］private void AddUpPos
		private void AddUpPos(CSql sql, string ObsMgrID)
		{
			string id = "-1";
			string catID = "-1";
			string parentID = "-1";
			int sortID = 0;

			catID = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
			if(Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID) != null) id = Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID).ToString();

			string cmdTxt;
			SqlDataReader dr;

			//ParentID,SortIDを取得
			#region
			if(id != "-1") //選択されている場合、その上に追加
			{
				cmdTxt = "SELECT ParentID, SortID FROM T_ObsTree WHERE ObsMgrID=@ObsMgrID";
				sql.AddParam("@ObsMgrID", id);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					parentID = dr["ParentID"].ToString().Trim();
					sortID = Convert.ToInt32(dr["SortID"]);
				}
				dr.Close();
			}
			#endregion

			//同一の親を持ち、指定ﾂﾘｰ以降のSortIDを+1する
			#region
			cmdTxt = "UPDATE T_ObsTree SET SortID=SortID + 1 WHERE ParentID=@ParentID AND SortID >= @SortID";
			sql.AddParam("@ParentID", parentID);
			sql.AddParam("@SortID", sortID);
			sql.CommandTrans(cmdTxt);
			#endregion

			//障害ﾂﾘｰに追加
			#region
			cmdTxt = "INSERT INTO T_ObsTree(CatID,ObsMgrID,SortID,ParentID)VALUES(@CatID,@ObsMgrID,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.AddParam("@SortID", sortID);
			sql.AddParam("@ParentID", parentID);
			sql.CommandTrans(cmdTxt);
			#endregion

		}
		#endregion

		/// <summary>
		/// 子ﾂﾘｰとして追加
		/// 子障害の一番上に追加する
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="ObsMgrID"></param>
		#region private void AddChild(CSql sql, string ObsMgrID)
		//［SRC-03-02-016］private void AddChild
		private void AddChild(CSql sql, string ObsMgrID)
		{
			string id = "-1";
			string catID = "-1";

			//選択されているｶﾃｺﾞﾘID取得
			catID = Common.GetSession(SessionID.Obs_Tree_CatTree_SelCatID).ToString();
			//選択されている障害ID取得
			if(Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID) != null) id = Common.GetSession(SessionID.Obs_Tree_ObsTree_SelObsID).ToString();

			string cmdTxt;

			//子階層のSortIDを全て+1する
			cmdTxt = "UPDATE T_ObsTree SET SortID = SortID + 1 WHERE ParentID= @ParentID";
			sql.AddParam("@ParentID", id);
			sql.CommandTrans(cmdTxt);

			//障害ﾂﾘｰに追加
			cmdTxt = "INSERT INTO T_ObsTree(CatID,ObsMgrID,SortID,ParentID)VALUES(@CatID,@ObsMgrID,@SortID,@ParentID)";
			sql.AddParam("@CatID", catID);
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.AddParam("@SortID", 1);
			sql.AddParam("@ParentID", id);
			sql.CommandTrans(cmdTxt);

		}
		#endregion

		/// <summary>
		/// 添付ﾌｧｲﾙ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="id"></param>
		#region private void CreateLibList(CSql sql, string id)
		//［SRC-03-02-017］private void CreateLibList
		private void CreateLibList(CSql sql, string id)
		{
			//編集中のﾌｧｲﾙ情報が存在しない場合
			if(Common.GetSession(SessionID.Obs_Edit_Lib) == null)
			{
				DataTable table = new DataTable();
				table.Columns.Add("UserPath");
				table.Columns.Add("ServerPath");
				table.Columns.Add("ServerName");
				table.Columns.Add("FullPath");
				table.Columns.Add("Comment");
				table.Columns.Add("CommentDisp");
				if(id != null) //既存ﾃﾞｰﾀの場合
				{
					//添付ﾌｧｲﾙの絶対ﾊﾟｽを取得
					string filePath = Common.GetCommonLib(id);

					string cmdTxt;
					SqlDataReader dr;
					cmdTxt = "SELECT UserPath, ServerPath, ServerName, Comment FROM T_ObsMgr_Lib WHERE ObsMgrID=@ObsMgrID ORDER BY UpdDt";
					sql.AddParam("@ObsMgrID", id);
					dr = sql.Read(cmdTxt);
					while(dr.Read() == true)
					{
						DataRow row = table.NewRow();
						row["UserPath"] = Common.CnvFromDB(dr["UserPath"].ToString());
						row["ServerPath"] = Common.CnvFromDB(dr["ServerPath"].ToString());
						row["ServerName"] = Common.CnvFromDB(dr["ServerName"].ToString());
						row["Comment"] = dr["Comment"].ToString();
						row["CommentDisp"] = HttpUtility.HtmlEncode(dr["Comment"].ToString());
						//ﾌｧｲﾙ存在ﾁｪｯｸ
						string fileName = Path.Combine(filePath, Common.CnvFromDB(dr["ServerName"].ToString()));
						if(File.Exists(fileName) == true)
						{
							row["FullPath"] = "../Common/GetFile.aspx?ObsMgrID=" + id + "&ServerName=" + row["ServerName"].ToString();
						}
						else
						{
							//URLをｾｯﾄしない(ﾘﾝｸを張らない)
							row["FullPath"] = "";
						}
						table.Rows.Add(row);
					}
					dr.Close();
				}
				Common.SetSession(SessionID.Obs_Edit_Lib, table);
				this.DataGridLib.DataSource = table;
			}
			else //編集中のﾌｧｲﾙ情報が存在する場合
			{
				//ﾌｧｲﾙ情報が編集された後は、ｾｯｼｮﾝの内容を表示する
				this.DataGridLib.DataSource = (DataTable)Common.GetSession(SessionID.Obs_Edit_Lib);
			}
			this.DataGridLib.DataBind();
		}
		#endregion

		/// <summary>
		/// ﾘﾝｸ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="id"></param>
		#region private void CreateLinkList(CSql sql, string id)
		//［SRC-03-02-018］private void CreateLinkList
		private void CreateLinkList(CSql sql, string id)
		{
			//編集中のﾘﾝｸ情報が存在しない場合
			if(Common.GetSession(SessionID.Obs_Edit_Link) == null)
			{
				DataTable table = new DataTable();
				table.Columns.Add("LinkName");
				table.Columns.Add("LinkUrl");
				table.Columns.Add("Checked");
				if(id != null) //既存ﾃﾞｰﾀの場合
				{
					string cmdTxt;
					SqlDataReader dr;
					cmdTxt = "SELECT LinkName, LinkUrl FROM T_ObsMgrLink WHERE ObsMgrID=@ObsMgrID ORDER BY LinkID";
					sql.AddParam("@ObsMgrID", id);
					dr = sql.Read(cmdTxt);
					while(dr.Read() == true)
					{
						DataRow row = table.NewRow();
						row["LinkName"] = HttpUtility.HtmlEncode(dr["LinkName"].ToString());
						row["LinkUrl"] = dr["LinkUrl"];
						row["Checked"] = false;
						table.Rows.Add(row);
					}
					dr.Close();
				}
				Common.SetSession(SessionID.Obs_Edit_Link, table);
				this.DataGridLink.DataSource = table;
			}
			else //編集中のﾘﾝｸ情報が存在する場合
			{
				//ﾘﾝｸ内容が編集された後は、ｾｯｼｮﾝの内容を表示する
				this.DataGridLink.DataSource = (DataTable)Common.GetSession(SessionID.Obs_Edit_Link);
			}
			this.DataGridLink.DataBind();
		}
		#endregion

		/// <summary>
		/// ﾌﾟﾛｼﾞｪｸﾄ一覧作成
		/// </summary>
		/// <param name="sql"></param>
		#region private void CreateProjectList(CSql sql)
		//［SRC-03-02-019］private void CreateProjectList
		private void CreateProjectList(CSql sql)
		{
			string cmdTxt;
			SqlDataReader dr;
			this.DropDownListProject.Items.Clear();

			cmdTxt = "SELECT TypCD, Lang0" + Common.LangID.ToString() + " AS Text, TemplateID FROM M_Prj_Typ ORDER BY TypCD";
			dr = sql.Read(cmdTxt);
			while(dr.Read() == true)
			{
				this.DropDownListProject.Items.Add(new ListItem(dr["Text"].ToString(), dr["TypCD"].ToString() + ":" + dr["TemplateID"].ToString()));
			}
			if(this.DropDownListProject.Items.Count > 0) this.DropDownListProject.SelectedIndex = 0;
			dr.Close();
		}
		#endregion

		/// <summary>
		/// 障害管理番号を生成する
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		#region private string CreateMgrNo(CSql sql)
		//［SRC-03-02-020］private string CreateMgrNo
		private string CreateMgrNo(CSql sql)
		{
			string cmdTxt;
			SqlDataReader dr;
			//採番対象項目の情報を取得する
			int unit = 0;					//採番単位 0:全体 1:個別
			int seqNum = 1;					//採番単位が個別の場合の、現在の番号
			string format = string.Empty;	//管理番号の書式
			#region
			cmdTxt = "SELECT Unit, SeqNum, Format FROM M_Prj_Typ WHERE TypCD=@TypCD";
			sql.AddParam("@TypCD", this.DropDownListProject.SelectedValue.Split(':')[0]);
			dr = sql.ReadTrans(cmdTxt);
			if(dr.Read() == true)
			{
				unit = Convert.ToInt32(dr["Unit"]);
				format = dr["Format"].ToString();
				if(dr["SeqNum"] == DBNull.Value)	seqNum = 1;
				else								seqNum = Convert.ToInt32(dr["SeqNum"]) + 1;
			}
			dr.Close();
			#endregion

			if(unit == 0)
			{
				//全体の場合
				int id = 0;
				#region
				//全体での管理番号作成のための情報を取得する
				cmdTxt = "SELECT ID, SeqNum, AllFormat FROM M_Defect";
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true)
				{
					seqNum = Convert.ToInt32(dr["SeqNum"]) + 1;
					format = dr["AllFormat"].ToString();
					id = Convert.ToInt32(dr["ID"]);
				}
				else
				{
					seqNum = 1;
				}
				dr.Close();
				//全体でのｼｰｹﾝｽ番号を更新
				cmdTxt = "UPDATE M_Defect SET SeqNum=@SeqNum WHERE ID=@ID";
				sql.AddParam("@SeqNum", seqNum);
				sql.AddParam("@ID", id);
				sql.CommandTrans(cmdTxt);
				#endregion
			}
			else
			{
				//個別の場合
				#region
				//seqNum更新
				cmdTxt = "UPDATE M_Prj_Typ SET SeqNum=@SeqNum WHERE TypCD=@TypCD";
				sql.AddParam("@SeqNum", seqNum);
				sql.AddParam("@TypCD", this.DropDownListProject.SelectedValue.Split(':')[0]);
				sql.CommandTrans(cmdTxt);
				#endregion
			}
			//管理番号生成
			return Common.CreateMngNo(this.DropDownListProject.SelectedItem.Text, format, seqNum);
		}
		#endregion

		/// <summary>
		/// 入力ﾃﾞｰﾀﾁｪｯｸ
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		#region private bool DataCheck(CSql sql)
		//［SRC-03-02-021］private bool DataCheck
		private bool DataCheck(CSql sql)
		{
			try
			{
				//件名入力ﾁｪｯｸ
				if(Common.CnvToDB(this.TextBoxSubject.Text) == string.Empty)
				{
					Common.Alert(this, Common.GetText(sql, this.mType, "AlertSubject", true));
					return false;
				}
				//ｽﾃｰﾀｽﾁｪｯｸ
				if(this.DropDownListStatus.SelectedIndex == -1)
				{
					Common.Alert(this, Common.GetText(sql, this.mType, "AlertStatus", true));
					return false;
				}
				//ﾌﾟﾛｼﾞｪｸﾄﾁｪｯｸ
				if(this.DropDownListProject.SelectedIndex == -1)
				{
					Common.Alert(this, Common.GetText(sql, this.mType, "AlertProject", true));
					return false;
				}
				//内容
				int cnt = Common.UserEncode.GetByteCount(this.TextBoxContent.Text);
				if(cnt > 2000)
				{
					Common.Alert(this, Common.GetText(sql, this.mType, "AlertContent", true));
					return false;
				}
				//可変項目入力ﾁｪｯｸ
				string cmdTxt = string.Empty;
				SqlDataReader dr;
				//ﾌﾟﾛｼﾞｪｸﾄのﾃﾝﾌﾟﾚｰﾄで使用されている項目一覧を取得する
				ArrayList ItemID = new ArrayList();
				ArrayList DataType = new ArrayList();
				#region
				string TemplateID = this.DropDownListProject.SelectedValue.Split(':')[1];
				cmdTxt = "SELECT I.ItemID, I.DataType FROM M_ObsTemplateItem AS T";
				cmdTxt += " LEFT JOIN M_ObsItem AS I ON I.ItemID=T.ItemID WHERE T.TemplateID=@TemplateID ORDER BY T.SortID";
				sql.AddParam("@TemplateID", TemplateID);
				dr = sql.ReadTrans(cmdTxt);
				while(dr.Read() == true)
				{
					ItemID.Add(dr["ItemID"]);
					DataType.Add(dr["DataType"]);
				}
				dr.Close();
				#endregion
				//各ｱｲﾃﾑﾃﾞｰﾀ追加
				for(int i = 0; i < ItemID.Count; i++)
				{
					int dataType = Convert.ToInt32(DataType[i]);
					switch(dataType)
					{
						case 1:	//ﾃｷｽﾄ型
						{
							#region
							string str = Common.CnvToDB(Request.Form["TextBox" + ItemID[i].ToString()]);
							if(str.Length > 512)
							{
								Common.OutputScript(this, "document.Form1.TextBox" + ItemID[i].ToString() + ".focus();" +
									"alert(\"" + Common.GetText(sql, this.mType, "AlertText", true) + "\");");
								return false;
							}
							break;
							#endregion
						}
						case 2:	//ﾘｽﾄ型
						{
							#region
							if(Request.Form["SelectList" + ItemID[i].ToString()] == null)
							{
								Common.Alert(this, Common.GetText(sql, this.mType, "AlertList", true));
								return false;
							}
							break;
							#endregion
						}
						case 3:	//数値型
						{
							#region
							string str = Common.CnvToDB(Request.Form["TextBox" + ItemID[i].ToString()]);
							if(str != string.Empty && Common.IsNumber(str) == false)
							{
								Common.OutputScript(this, "document.Form1.TextBox" + ItemID[i].ToString() + ".focus();" +
									"alert(\"" + Common.GetText(sql, this.mType, "AlertNumber", true) + "\");");
								return false;
							}
							break;
							#endregion
						}
						case 4:	//整数型
						{
							#region
							string str = Common.CnvToDB(Request.Form["TextBox" + ItemID[i].ToString()]);
							if(str != string.Empty && Common.IsInteger(str) == false)
							{
								Common.OutputScript(this, "document.Form1.TextBox" + ItemID[i].ToString() + ".focus();" +
									"alert(\"" + Common.GetText(sql, this.mType, "AlertInteger", true) + "\");");
								return false;
							}
							break;
							#endregion
						}
						case 5:	//日付型
						{
							#region
							string str = Common.CnvToDB(Request.Form["TextBox" + ItemID[i].ToString()]);
							if(str != string.Empty && Common.IsDate(str) == false)
							{
								Common.OutputScript(this, "document.Form1.TextBox" + ItemID[i].ToString() + ".focus();" +
									"alert(\"" + Common.GetText(sql, this.mType, "AlertDate", true) + "\");");
								return false;
							}
							break;
							#endregion
						}
						case 6:	//ﾕｰｻﾞ型
						{
							#region
							if(Request.Form["SelectList" + ItemID[i].ToString()] == null)
							{
								Common.Alert(this, Common.GetText(sql, this.mType, "AlertUser", true));
								return false;
							}
							break;
							#endregion
						}
						case 7:	//部門型
						{
							#region
							if(Request.Form["SelectList" + ItemID[i].ToString()] == null)
							{
								Common.Alert(this, Common.GetText(sql, this.mType, "AlertSect", true));
								return false;
							}
							break;
							#endregion
						}
					}
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
				return false;
			}
			return true;
		}
		#endregion

		/// <summary>
		/// ﾘﾝｸ情報を保存
		/// </summary>
		/// <param name="sql"></param>
		#region private void SaveLinkInfo(CSql sql, string ObsMgrID)
		//［SRC-03-02-022］private void SaveLinkInfo
		private void SaveLinkInfo(CSql sql, string ObsMgrID)
		{
			string cmdTxt;
			DataTable table = (DataTable)Common.GetSession(SessionID.Obs_Edit_Link);
			//既存ﾘﾝｸ情報削除
			cmdTxt = "DELETE FROM T_ObsMgrLink WHERE ObsMgrID=@ObsMgrID";
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.CommandTrans(cmdTxt);
			//ﾘﾝｸ情報追加
			for(int i = 0; i < table.Rows.Count; i++)
			{
				cmdTxt = "INSERT INTO T_ObsMgrLink(ObsMgrID,LinkID,LinkName,LinkUrl)VALUES(@ObsMgrID,@LinkID,@LinkName,@LinkUrl)";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				sql.AddParam("@LinkID", i);
				sql.AddParam("@LinkName", HttpUtility.HtmlDecode(table.Rows[i]["LinkName"].ToString()));
				sql.AddParam("@LinkUrl", table.Rows[i]["LinkUrl"]);
				sql.CommandTrans(cmdTxt);
			}
		}
		#endregion

		/// <summary>
		/// 添付ﾌｧｲﾙを保存
		/// </summary>
		/// <param name="sql"></param>
		#region private void SaveLibInfo(CSql sql, string ObsMgrID)
		//［SRC-03-02-023］private void SaveLibInfo
		private void SaveLibInfo(CSql sql, string ObsMgrID)
		{
			string cmdTxt;
			DataTable table = (DataTable)Common.GetSession(SessionID.Obs_Edit_Lib);
			//obstacle/lib/ObsMgrIDﾌｫﾙﾀﾞ以下に存在する不要なﾌｧｲﾙを削除する
			string path = Common.GetCommonLib(ObsMgrID);
			DirectoryInfo di = new DirectoryInfo(path);
			if(di.Exists == true)
			{
				FileInfo[] fis = di.GetFiles();
				foreach(FileInfo fi in fis)
				{
					if(this.IsValidFile(table, fi.Name) == false)
					{
						fi.Delete();
					}
				}
			}
			//obstacle/tmp/userIDﾌｫﾙﾀﾞ以下の必要なﾌｧｲﾙをｺﾋﾟｰする
			string pathTmp = Common.GetCommonLibTmp();
			di = new DirectoryInfo(pathTmp);
			if(di.Exists == true)
			{
				FileInfo[] fis = di.GetFiles();
				foreach(FileInfo fi in fis)
				{
					if(this.IsValidFile(table, fi.Name) == true)
					{
						string dst = Path.Combine(path, fi.Name);
						if(Directory.Exists(path) == false) Directory.CreateDirectory(path);
						fi.CopyTo(dst, true);
					}
				}
				//obstacle/lib/tmp//userIDﾌｫﾙﾀﾞを削除する
				di.Delete(true);
			}
			//情報更新

			//既存添付ﾌｧｲﾙ情報削除
			cmdTxt = "DELETE FROM T_ObsMgr_Lib WHERE ObsMgrID=@ObsMgrID";
			sql.AddParam("@ObsMgrID", ObsMgrID);
			sql.CommandTrans(cmdTxt);
			//添付ﾌｧｲﾙ情報追加
			for(int i = 0; i < table.Rows.Count; i++)
			{
				DataRow row = table.Rows[i];
				cmdTxt = "INSERT INTO T_ObsMgr_Lib(ObsMgrID,UserPath,ServerPath,ServerName,UpdDt,Comment)VALUES(@ObsMgrID,@UserPath,@ServerPath,@ServerName,@UpdDt,@Comment)";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				sql.AddParam("@UserPath", row["UserPath"]);
				sql.AddParam("@ServerPath", string.Empty);//ServerPathはﾌﾟﾛｸﾞﾗﾑでは使用していないので空白をｾｯﾄ
				//sql.AddParam("@ServerPath", path);
				sql.AddParam("@ServerName", row["ServerName"]);
				sql.AddParam("@UpdDt", DateTime.Now.ToString("yyyyMMddHHmmss"));
				sql.AddParam("@Comment", HttpUtility.HtmlDecode(row["Comment"].ToString()));
				sql.CommandTrans(cmdTxt);
			}
		}
		#endregion

		/// <summary>
		/// 指定したﾌｧｲﾙ名(ｻｰﾊﾞｰ上での)が保存対象かﾁｪｯｸ
		/// </summary>
		/// <param name="table"></param>
		/// <param name="ServerName"></param>
		/// <returns></returns>
		#region private bool IsValidFile(DataTable table, string ServerName)
		//［SRC-03-02-024］private bool IsValidFile
		private bool IsValidFile(DataTable table, string ServerName)
		{
			for(int i = 0; i < table.Rows.Count; i++)
			{
				if(table.Rows[i]["ServerName"].ToString().ToUpper() == ServerName.ToUpper())
				{
					return true;
				}
			}
			return false;
		}
		#endregion

		/// <summary>
		/// 承認者名表示
		/// </summary>
		/// <param name="sql"></param>
		#region private void DispAppUser(CSql sql)
		//［SRC-03-02-025］private void DispAppUser
		private void DispAppUser(CSql sql)
		{
			if(Common.GetSession(SessionID.Obs_Edit_RcgUser) != null)
			{
				//承認者表示
				string cmdTxt;
				SqlDataReader dr;
				cmdTxt = "SELECT FullName FROM M_users WHERE UserID=@UserID";
				sql.AddParam("@UserID", Common.GetSession(SessionID.Obs_Edit_RcgUser));
				dr = sql.Read(cmdTxt);
				if(dr.Read() == true & dr["FullName"] != DBNull.Value)
				{
					this.TextBoxAppUser.Text = dr["FullName"].ToString();
				}
				dr.Close();
				//承認者設定ｸﾘｱﾎﾞﾀﾝ表示
				this.ButtonClear.Visible = true;
			}
			else
			{
				this.TextBoxAppUser.Text = string.Empty;
			}
		}
		#endregion

		/// <summary>
		/// 承認者情報を保存
		/// </summary>
		/// <param name="sql">BeginTransをしているCSql変数</param>
		/// <param name="ObsMgrID"></param>
		#region private void SaveAppUserInfo(CSql sql, string ObsMgrID)
		//［SRC-03-02-026］private void SaveAppUserInfo
		private void SaveAppUserInfo(CSql sql, string ObsMgrID)
		{
			string cmdTxt;
			SqlDataReader dr;
			if(Common.GetSession(SessionID.Obs_Edit_RcgUser) == null)
			{
				//承認者が未設定の場合は、未承認の依頼を(存在すれば)削除
				cmdTxt = "DELETE FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=@ObsMgrID AND RcgDateTime IS NULL";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				sql.CommandTrans(cmdTxt);
			}
			else
			{
				//承認者が設定済みの場合も、未承認の依頼を(存在すれば)削除(Delete/Insertを行う為)
				cmdTxt = "DELETE FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=@ObsMgrID AND RcgDateTime IS NULL";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				sql.CommandTrans(cmdTxt);
				//該当障害の承認履歴SEQ最大値を求める
				int seq = 0;
				cmdTxt = "SELECT MAX(Seq) AS Seq FROM T_ObsMgr_RcgUsr WHERE ObsMgrID=@ObsMgrID ORDER BY Seq DESC";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				dr = sql.ReadTrans(cmdTxt);
				if(dr.Read() == true && dr["Seq"] != DBNull.Value)
				{
					seq = Convert.ToInt32(dr["Seq"]) + 1;
				}
				dr.Close();
				//承認情報を追加
				cmdTxt = "INSERT INTO T_ObsMgr_RcgUsr(ObsMgrID,Seq,RcgUserID,RcgTyp,ReqUserID,ReqDateTime,UpdDt)VALUES";
				cmdTxt += "(@ObsMgrID,@Seq,@AppUserID,@RcgTyp,@ReqUserID,GETDATE(),@UpdDt)";
				sql.AddParam("@ObsMgrID", ObsMgrID);
				sql.AddParam("@Seq", seq);
				sql.AddParam("@AppUserID", Common.GetSession(SessionID.Obs_Edit_RcgUser));
				sql.AddParam("@RcgTyp", "RC01");//承認依頼の場合はこの値でOK?
				sql.AddParam("@ReqUserID", Common.LoginID);
				sql.AddParam("@UpdDt", DateTime.Now.ToString("yyyyMMddHHmmss"));
				sql.CommandTrans(cmdTxt);
			}
		}
		#endregion








		///================================================================
		/// ﾌﾟﾛﾊﾟﾃｨ
		///================================================================

		/// <summary>
		/// 可変項目ﾀｸﾞ作成
		/// </summary>
		#region protected string ItemTag
		//［SRC-03-02-027］protected string ItemTag
		protected string ItemTag
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				if(this.DropDownListProject.SelectedIndex == -1) return sb.ToString();

				using(CSql sql = new CSql())
				{
					using(CSql sql2 = new CSql())
					{
						string cmdTxt = string.Empty;
						SqlDataReader dr;
						SqlDataReader dr2;
						sql.Open();
						sql2.Open();

						//ﾃﾝﾌﾟﾚｰﾄID取得
						string TemplateID = string.Empty;
						#region
						//ObsMgrID
						string id = Request.QueryString["id"];

						//採番対象項目の変更を可能にする
						TemplateID = this.DropDownListProject.SelectedValue.Split(':')[1];
						/*
						if(id == null)
						{
							//新規
							TemplateID = this.DropDownListProject.SelectedValue.Split(':')[1];
						}
						else
						{
							//更新
							cmdTxt = "SELECT P.TemplateID FROM T_ObsMgr2 AS O";
							cmdTxt += " LEFT JOIN M_Prj_Typ AS P ON P.TypCD=O.PrjID";
							cmdTxt += " WHERE O.ObsMgrID=@ObsMgrID";
							sql.AddParam("@ObsMgrID", id);
							dr = sql.Read(cmdTxt);
							if(dr.Read() == true)
							{
								TemplateID = dr["TemplateID"].ToString();
							}
							dr.Close();
						}
						*/
						#endregion

						//ﾌﾟﾛｼﾞｪｸﾄのﾃﾝﾌﾟﾚｰﾄから使用されている項目一覧を取得する
						#region
						switch(Common.LangID)
						{
							case 1: cmdTxt = "SELECT I.ItemID, I.Japanese AS Text, I.DataType FROM M_ObsTemplateItem AS T"; break;
							case 2: cmdTxt = "SELECT I.ItemID, I.English AS Text, I.DataType FROM M_ObsTemplateItem AS T"; break;
						}
						cmdTxt += " LEFT JOIN M_ObsItem AS I ON I.ItemID=T.ItemID WHERE T.TemplateID=@TemplateID ORDER BY T.SortID";
						sql.AddParam("@TemplateID", TemplateID);
						dr = sql.Read(cmdTxt);
						#endregion

						while(dr.Read() == true)
						{
							sb.Append("<tr class=Content><td>");
							sb.Append(HttpUtility.HtmlEncode(dr["Text"].ToString()));		//項目名称
							sb.Append("</td><td>");
							int type = Convert.ToInt32(dr["DataType"]);
							string itemID = dr["ItemID"].ToString();
							//tips取得
							string tips = string.Empty;
							#region
							switch(Common.LangID)
							{
								case 1: cmdTxt = "SELECT Tips FROM M_ObsItem WHERE ItemID=@ItemID"; break;
								case 2: cmdTxt = "SELECT Tips2 FROM M_ObsItem WHERE ItemID=@ItemID"; break;
							}
							sql2.AddParam("@ItemID", itemID);
							dr2 = sql2.Read(cmdTxt);
							//if(dr2.Read() == true) tips = HttpUtility.HtmlEncode(dr2[0].ToString());
							if(dr2.Read() == true) tips = HttpUtility.HtmlEncode(dr2[0].ToString());
							dr2.Close();
							#endregion

							switch(type)
							{
								case 1:	//ﾃｷｽﾄ型
								{
									#region
									if(this.IsPostBack == true)
									{
										string str = string.Empty;
										if(Request.Form["TextBox" + itemID] != null)
											str = HttpUtility.HtmlEncode(Request.Form["TextBox" + itemID].ToString());
										sb.Append("<input type=text name=TextBox" + itemID + " maxlength=512 size=50" +
											" value=\"" + str + "\" title=\"" + tips + "\">" +
											Common.GetText(sql2, this.mType, "HelpText1"));
									}
									else
									{
										if(id == null)
										{
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=512 size=50 title=\"" + tips + "\">" + 
												Common.GetText(sql2, this.mType, "HelpText1"));
										}
										else
										{
											cmdTxt = "SELECT Data1 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
											sql2.AddParam("@ObsMgrID", id);
											sql2.AddParam("@ItemID", itemID);
											dr2 = sql2.Read(cmdTxt);
											string str = string.Empty;
											if(dr2.Read() == true && dr2["Data1"] != DBNull.Value) str = HttpUtility.HtmlEncode(dr2["Data1"].ToString());
											dr2.Close();
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=512 size=50 value=\"" + str + "\" title=\"" + tips + "\">" +
												Common.GetText(sql2, this.mType, "HelpText1"));
										}
									}
									break;
									#endregion
								}
								case 2:	//ﾘｽﾄ型
								{
									#region
									string selectedValue = string.Empty;
									if(this.IsPostBack == true)
									{
										selectedValue = Request.Form["SelectList" + itemID];
									}
									else if(id != null)
									{
										cmdTxt = "SELECT Data2 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
										sql2.AddParam("@ObsMgrID", id);
										sql2.AddParam("@ItemID", itemID);
										dr2 = sql2.Read(cmdTxt);
										if(dr2.Read() == true) selectedValue = dr2["Data2"].ToString();
										dr2.Close();
									}
									cmdTxt = "SELECT Text" + Common.LangID.ToString() +" AS Text, ListID FROM M_ObsItemList WHERE ItemID=@ItemID AND DisabledFlag=0 ORDER BY SortID";
									sql2.AddParam("@ItemID", itemID);
									dr2 = sql2.Read(cmdTxt);
									sb.Append("<select name=SelectList" + itemID + ">\r\n");
									//空白ｱｲﾃﾑ追加
									if(selectedValue == "-1")
									{
										sb.Append("<option value=-1 selected>");
										sb.Append("</option>\r\n");
									}
									else
									{
										sb.Append("<option value=-1>");
										sb.Append("</option>\r\n");
									}
									while(dr2.Read() == true)
									{
										if(dr2["ListID"].ToString() == selectedValue)
										{
											sb.Append("<option value=" + dr2["ListID"].ToString() + " selected>");
											sb.Append(HttpUtility.HtmlEncode(dr2["Text"].ToString()));
											sb.Append("</option>\r\n");
										}
										else
										{
											sb.Append("<option value=" + dr2["ListID"].ToString() + ">");
											sb.Append(HttpUtility.HtmlEncode(dr2["Text"].ToString()));

											sb.Append("</option>\r\n");
										}
									}
									sb.Append("</select>\r\n");
									dr2.Close();
									break;
									#endregion
								}
								case 3:	//数値型
								{
									#region
									if(this.IsPostBack == true)
									{
										sb.Append("<input type=text name=TextBox" + itemID + " maxlength=10 size=13 value=\"" + Request.Form["TextBox" + itemID] + "\" title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText2"));
									}
									else
									{
										if(id == null)
										{
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=10 size=13 title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText2"));
										}
										else
										{
											cmdTxt = "SELECT Data3 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
											sql2.AddParam("@ObsMgrID", id);
											sql2.AddParam("@ItemID", itemID);
											dr2 = sql2.Read(cmdTxt);
											string str = string.Empty;
											if(dr2.Read() == true && dr2["Data3"] != DBNull.Value) str = dr2["Data3"].ToString();
											dr2.Close();
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=10 size=13 value=\"" + str + "\" title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText2"));
										}
									}
									break;
									#endregion
								}								
								case 4:	//整数型
								{
									#region
									if(this.IsPostBack == true)
									{
										sb.Append("<input type=text name=TextBox" + itemID + " maxlength=11 size=14 value=\"" + Request.Form["TextBox" + itemID] + "\" title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText3"));
									}
									else
									{
										if(id == null)
										{
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=11 size=14 title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText3"));
										}
										else
										{
											cmdTxt = "SELECT Data4 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
											sql2.AddParam("@ObsMgrID", id);
											sql2.AddParam("@ItemID", itemID);
											dr2 = sql2.Read(cmdTxt);
											string str = string.Empty;
											if(dr2.Read() == true && dr2["Data4"] != DBNull.Value) str = dr2["Data4"].ToString();
											dr2.Close();
											sb.Append("<input type=text name=TextBox" + itemID + " maxlength=11 size=14 value=\"" + str + "\" title=\"" + tips + "\">" + Common.GetText(sql2, this.mType, "HelpText3"));
										}
									}
									break;
									#endregion
								}
								case 5:	//日付型
								{
									#region
									if(this.IsPostBack == true)
									{
										sb.Append("<input type=text name=TextBox" + itemID);
										sb.Append(" onDblClick='SetToday(this)'");
										sb.Append(" maxlength=10 size=13 value=\"" + Request["TextBox" + itemID] + "\" title=\"" + tips + "\">");
									}
									else
									{
										if(id == null)
										{
											sb.Append("<input type=text name=TextBox" + itemID);
											sb.Append(" onDblClick='SetToday(this)'");
											sb.Append(" maxlength=10 size=13 title=\"" + tips + "\">");
										}
										else
										{
											cmdTxt = "SELECT Data5 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
											sql2.AddParam("@ObsMgrID", id);
											sql2.AddParam("@ItemID", itemID);
											dr2 = sql2.Read(cmdTxt);
											string str = string.Empty;
											if(dr2.Read() == true && dr2["Data5"] != DBNull.Value) str = Convert.ToDateTime(dr2["Data5"]).ToString("yyyy/MM/dd");
											dr2.Close();
											sb.Append("<input type=text name=TextBox" + itemID + "  onDblClick='SetToday(this)' maxlength=10 size=13 value=\"" + str + "\" title=\"" + tips + "\">");
										}
									}
									sb.Append("<a href=\"javascript:void(0)\" onclick=\"javascript:OpenCalendar(event,'TextBox" + itemID + "')\">▼</a>");
									break;
									#endregion
								}								
								case 6:	//ﾕｰｻﾞ型
								{
									#region
									string selectedValue = string.Empty;
									if(this.IsPostBack == true)
									{
										selectedValue = Request.Form["SelectList" + itemID];
									}
									else if(id != null)
									{
										cmdTxt = "SELECT Data6 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
										sql2.AddParam("@ObsMgrID", id);
										sql2.AddParam("@ItemID", itemID);
										dr2 = sql2.Read(cmdTxt);
										if(dr2.Read() == true) selectedValue = dr2["Data6"].ToString();
										dr2.Close();
									}
									cmdTxt = "SELECT UserID, FullName FROM M_users ORDER BY UserID";
									dr2 = sql2.Read(cmdTxt);
									sb.Append("<select name=SelectList" + itemID + ">\r\n");
									//空白ｱｲﾃﾑ追加
									if(selectedValue == "-1")
									{
										sb.Append("<option value=-1 selected>");
										sb.Append("</option>\r\n");
									}
									else
									{
										sb.Append("<option value=-1>");
										sb.Append("</option>\r\n");
									}
									while(dr2.Read() == true)
									{
										if(dr2["UserID"].ToString() == selectedValue)
										{
											sb.Append("<option value=" + dr2["UserID"].ToString() + " selected>");
											sb.Append(HttpUtility.HtmlEncode(dr2["FullName"].ToString()));
											sb.Append("</option>\r\n");
										}
										else
										{
											sb.Append("<option value=" + dr2["UserID"].ToString() + ">");
											sb.Append(HttpUtility.HtmlEncode(dr2["FullName"].ToString()));
											sb.Append("</option>\r\n");
										}
									}
									sb.Append("</select>\r\n");
									dr2.Close();
									break;
									#endregion
								}								
								case 7:	//部門型
								{
									#region
									string selectedValue = string.Empty;
									if(this.IsPostBack == true)
									{
										selectedValue = Request.Form["SelectList" + itemID];
									}
									else if(id != null)
									{
										cmdTxt = "SELECT Data7 FROM T_ObsMgrData WHERE ObsMgrID=@ObsMgrID AND ItemID=@ItemID";
										sql2.AddParam("@ObsMgrID", id);
										sql2.AddParam("@ItemID", itemID);
										dr2 = sql2.Read(cmdTxt);
										if(dr2.Read() == true) selectedValue = dr2["Data7"].ToString();
										dr2.Close();
									}
									cmdTxt = "SELECT Sect, SectName FROM M_SECT WHERE SectLevel<>0 ORDER BY Sect";
									dr2 = sql2.Read(cmdTxt);
									sb.Append("<select name=SelectList" + itemID + ">\r\n");
									//空白ｱｲﾃﾑ追加
									if(selectedValue == "-1")
									{
										sb.Append("<option value=-1 selected>");
										sb.Append("</option>\r\n");
									}
									else
									{
										sb.Append("<option value=-1>");
										sb.Append("</option>\r\n");
									}
									while(dr2.Read() == true)
									{
										if(dr2["Sect"].ToString() == selectedValue)
										{
											sb.Append("<option value=" + dr2["Sect"].ToString() + " selected>");
											sb.Append(HttpUtility.HtmlEncode(dr2["SectName"].ToString()));
											sb.Append("</option>\r\n");
										}
										else
										{
											sb.Append("<option value=" + dr2["Sect"].ToString() + ">");
											sb.Append(HttpUtility.HtmlEncode(dr2["SectName"].ToString()));
											sb.Append("</option>\r\n");
										}
									}
									sb.Append("</select>\r\n");
									dr2.Close();
									break;
									#endregion
								}
							}
							sb.Append("</td></tr>\r\n");
						}
						dr.Close();
					}
				}
				return sb.ToString();
			}
		}
		#endregion
















































	}
}
