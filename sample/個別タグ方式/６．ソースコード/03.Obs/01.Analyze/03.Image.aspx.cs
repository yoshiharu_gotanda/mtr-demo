using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


using System.IO;
using System.Drawing.Imaging;

namespace EPSNet.Obs.Analyze
{
	/// <summary>
	/// Image の概要の説明です。
	/// </summary>
	public class Image : System.Web.UI.Page
	{

		#region Web フォーム デザイナで生成されたコード
		//［SRC-03-01-03-001］override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//［SRC-03-01-03-002］private void InitializeComponent
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================
		
		private Type mType = typeof(Graph1);//Graph1のTextを使用(共有)する


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================
		

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//［SRC-03-01-03-003］private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			using(Bitmap bmp = new Bitmap(640, 480))
			{
				//画像作成
				using(Graphics g = Graphics.FromImage(bmp))
				{
					#region

					string id = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Kind);		//集計種別
					string item = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Item);	//集計項目
					string unit = (string)Common.GetSession(SessionID.Obs_Analyze_Main_Unit);	//集計単位
					string from = (string)Common.GetSession(SessionID.Obs_Analyze_Main_From);	//集計期間From
					string to = (string)Common.GetSession(SessionID.Obs_Analyze_Main_To);		//集計期間To
					string[] items = item.Split(',');

					//棒ｸﾞﾗﾌ色設定
					Color[] cols = new Color[10];
					#region
					cols[0] = Color.Gray;
					cols[1] = Color.Blue;
					cols[2] = Color.LightGreen;
					cols[3] = Color.Brown;
					cols[4] = Color.Pink;
					cols[5] = Color.Violet;
					cols[6] = Color.Purple;
					cols[7] = Color.YellowGreen;
					cols[8] = Color.GreenYellow;
					cols[9] = Color.FromArgb(0, 0, 0);
					#endregion

					//ﾏｰｼﾞﾝ値
					int topMargin = 10;
					int leftMargin = 50;
					int rightMargin = 20;

					//ｸﾞﾗﾌ背景色設定
					//g.Clear(Color.Ivory);
					g.Clear(Color.White);

					using(CSql sql = new CSql())
					{
						string cmdTxt = string.Empty;
						sql.Open();

						//選択された集計種別の、集計項目名称／DataTypeを取得
						string ItemName;
						int DataType;
						if(Common.GetItemDataInfo(sql, id, out ItemName, out DataType) == false) return;

						//「ﾀｲﾄﾙ」描画
						#region
						using(Font font = new Font("ＭＳ ゴシック", 12, FontStyle.Bold))
						{
							SizeF size = g.MeasureString(ItemName, font);
							g.DrawString(ItemName, font, Brushes.Black, new PointF((bmp.Width - size.Width) / 2, topMargin));
						}
						#endregion
						//「発生件数」描画
						#region
						using(Font font = new Font("ＭＳ ゴシック", 9, FontStyle.Bold))
						{
							
							string tmp = Common.GetText(sql, this.mType, "StrCnt");
							//縦書き
							StringFormat sf = new StringFormat(StringFormatFlags.DirectionVertical);
							SizeF size = g.MeasureString(tmp, font, int.MaxValue, sf);
							g.DrawString(tmp, font, Brushes.Black, 10, (bmp.Height - size.Height) / 2, sf);
						}
						#endregion
						//集計対象の年月日等一覧を取得
						ArrayList alSpan;
						#region
						if(unit == "m")
						{
							//月単位
							alSpan = Common.GetRangeYM(from, to);
						}
						else if(unit == "d")
						{
							//日単位
							alSpan = Common.GetRangeYMD(from, to);
						}
						else
						{
							//時単位
							alSpan = new ArrayList();
							for(int i = 0; i < 24; i++)
							{
								alSpan.Add(from.Replace("/", string.Empty) + i.ToString("00"));
							}
						}
						#endregion

						//集計処理
						ArrayList alCode;
						ArrayList alText;
						ArrayList alCount;
						Common.Analyze(sql, id, items, alSpan, DataType, out alCode, out alText, out alCount);

						//ｸﾞﾗﾌを描画するBottom位置
						float graphBottom = bmp.Height;
						//ｸﾞﾗﾌを描画するTop位置
						float graphTop = 0;

						//対象ｱｲﾃﾑ名をｸﾞﾗﾌ上部に描画
						#region
					{
						int rowHeight = 20;
						int sampleWidth = 20;
						float itemSpace = 10;
						float tmpX = 20;
						int tmpY = 40;
						using(Font font = new Font("MS UI Gothic", 9))
						{
							for(int i = 0; i < alText.Count; i++)
							{
								string txt = alText[i].ToString();
								SizeF size = g.MeasureString(txt, font);
								if(tmpX != 20)//一番左に出力される名称はX位置調整しない
								{
									//名称出力後の右端の位置がはみ出していたら改行する
									float right = tmpX + size.Width + sampleWidth;
									if(right > bmp.Width)
									{
										tmpX = 20;
										tmpY += rowHeight;
									}
								}
								g.FillRectangle(new SolidBrush(cols[i]), tmpX, tmpY, sampleWidth, size.Height);
								g.DrawString(txt, font, Brushes.Black, tmpX + sampleWidth, tmpY);
								tmpX += size.Width + sampleWidth + itemSpace;
							}
							graphTop = tmpY + rowHeight;
						}
						graphTop += 10; //項目名とｸﾞﾗﾌの間を少し空ける
					}
						#endregion

						//1期間の幅
						float spanWidth = (bmp.Width - leftMargin - rightMargin) / (float)alSpan.Count;

						//期間文字列
						#region
						using(Font font = new Font("MS UI Gothic", 9))
						{
							if(unit == "m")
							{
								//月単位の場合は横
								#region
								float drawableX = int.MaxValue; //次に日付を描画可能なx座標
								//期間ﾀｸﾞ出力
								float spanMaxHeight = g.MeasureString(alSpan[0].ToString(), font, int.MaxValue).Height;
								//期間の終わりから描画する
								for(int i = alSpan.Count - 1; i >= 0; i--)
								{
									//年月出力
									string tmp = alSpan[i].ToString();
									tmp = Common.DateTimeConvert(sql, tmp);
									SizeF size = g.MeasureString(tmp, font, int.MaxValue);
									float drawx = leftMargin + spanWidth * i + (spanWidth - size.Width) / 2;
									if(drawx <= drawableX)
									{
										g.DrawString(tmp, font, Brushes.Black, drawx, bmp.Height - spanMaxHeight);
										drawableX = drawx - size.Width;
										float tmpx = leftMargin + spanWidth * i + spanWidth / 2;
										float tmpy = bmp.Height - spanMaxHeight;
										g.DrawLine(Pens.Black, tmpx, tmpy, tmpx, tmpy - 5);
										tmpy -= 5;
										if(graphBottom > tmpy) graphBottom = tmpy;
									}
								}
								#endregion
							}
							else
							{
								//日、時間単位の場合は縦
								#region
								float drawableX = int.MaxValue; //次に日付を描画可能なx座標
								//期間ﾀｸﾞ出力
								//期間文字列の高さの最大を取得する
								float spanMaxHeight = 0;
								for(int i = 0; i < alSpan.Count; i++)
								{
									string tmp = alSpan[i].ToString();
									tmp = Common.DateTimeConvert(sql, tmp);
									StringFormat sf = new StringFormat(StringFormatFlags.DirectionVertical);
									SizeF size = g.MeasureString(tmp, font, int.MaxValue, sf);
									if(size.Height > spanMaxHeight)  spanMaxHeight = size.Height;
								}
								//期間の終わりから描画する
								for(int i = alSpan.Count - 1; i >= 0; i--)
								{
									//日付出力
									string tmp = alSpan[i].ToString();
									tmp = Common.DateTimeConvert(sql, tmp);
									StringFormat sf = new StringFormat(StringFormatFlags.DirectionVertical);
									SizeF size = g.MeasureString(tmp, font, int.MaxValue, sf);
									//SizeF size = g.MeasureString(tmp, font, int.MaxValue);
									float drawx = leftMargin + spanWidth * i + (spanWidth - size.Width) / 2;
									if(drawx <= drawableX)
									{
										//g.DrawString(tmp, font, Brushes.Black, drawx, bmp.Height - spanMaxHeight, sf);
										drawableX = drawx - size.Width;
										//drawableX = drawx - size.Height;
										g.RotateTransform(-90);
										g.DrawString(tmp, font, Brushes.Black, -(bmp.Height - spanMaxHeight) - size.Height, drawx);
										g.RotateTransform(90);

										float tmpx = leftMargin + spanWidth * i + spanWidth / 2;
										float tmpy = bmp.Height - spanMaxHeight;
										g.DrawLine(Pens.Black, tmpx, tmpy, tmpx, tmpy - 5);
										tmpy -= 5;
										if(graphBottom > tmpy) graphBottom = tmpy;
									}
								}
								#endregion
							}
						}
						#endregion

						//目盛り出力
						#region
						int max = 0;
						for(int i = 0; i < alCount.Count; i++)
						{
							int cnt = Convert.ToInt32(alCount[i]);
							if(cnt >= max) max = cnt;
						}
						if(max == 0) max = 1;
						float per = (graphBottom - graphTop) / max;	//1件の目盛り
						using(Font font = new Font("MS UI Gothic", 9))
						{
							int maxStr = 15;			//描画する目盛りの数
							int inc = max / maxStr + 1;	//目盛りを描画する間隔数
							for(int i = 0; i <= max; i += inc)
							{
								//線描画位置
								float tmpy = graphBottom - per * i;
								//線描画
								if(i == 0 || i == max)	g.DrawLine(Pens.Black, leftMargin, tmpy, bmp.Width - rightMargin, tmpy);
								else					g.DrawLine(Pens.Silver, leftMargin, tmpy, bmp.Width - rightMargin, tmpy);
								//目盛り文字列出力
								//目盛り値最大の文字列を重なる場合は文字列描画はしない
								SizeF sizeTop = g.MeasureString(max.ToString(), font);
								float yBottom = graphBottom - per * max + sizeTop.Height;//最大値目盛りののBottom位置
								SizeF size = g.MeasureString(i.ToString(), font);
								float  yTop = graphBottom - per * i;//現在の目盛りのTop位置
								if(yTop > yBottom)
								{
									g.DrawString(i.ToString(), font, Brushes.Black, leftMargin - size.Width, tmpy - size.Height / 2);
								}
							}
							//y方向最大目盛り描画
							if(true)
							{
								//線描画位置
								float tmpy = graphBottom - per * max;
								//線描画
								g.DrawLine(Pens.Black, leftMargin, tmpy, bmp.Width - rightMargin, tmpy);
								//目盛り文字列出力
								SizeF size = g.MeasureString(max.ToString(), font);
								g.DrawString(max.ToString(), font, Brushes.Black, leftMargin - size.Width, tmpy - size.Height / 2);
							}
						}
						#endregion

						//ｸﾞﾗﾌの囲い描画
						g.DrawLine(Pens.Black, leftMargin, graphBottom, leftMargin, graphTop);
						g.DrawLine(Pens.Black, bmp.Width - rightMargin, graphBottom, bmp.Width - rightMargin, graphTop);
						//棒ｸﾞﾗﾌの棒の最大の太さ
						float lineMaxWidth = 10;
						//1本のｸﾞﾗﾌの幅(1期間の幅を、表示するｱｲﾃﾑ数で割った値)
						float lineWidth = (float)spanWidth / (float)alText.Count;
						//棒ｸﾞﾗﾌ描画
						#region
						int loopCnt = 0;
						for(int j = 0; j < alText.Count; j++)
						{
							for(int i = 0; i < alSpan.Count; i++)
							{
								float tmpx = leftMargin + i * spanWidth + j * lineWidth;
								int cnt = Convert.ToInt32(alCount[loopCnt]);
								float height = per * cnt;
								float drawLineWidth = lineWidth;
								if(drawLineWidth < 1) drawLineWidth = 1;	//ｸﾞﾗﾌの太さは1未満にはしない
								if(drawLineWidth > lineMaxWidth)			//ｸﾞﾗﾌが太い場合は細くして、表示位置を中央に寄せる
								{
									tmpx += (drawLineWidth - lineMaxWidth) / 2;
									drawLineWidth = lineMaxWidth;
								}
								g.FillRectangle(new SolidBrush(cols[j]), tmpx, graphBottom - height, drawLineWidth, height);
								loopCnt++;
							}
						}
						#endregion
					}
					#endregion
				}
				//画像ﾃﾞｰﾀ出力
				Response.ContentType = "image/png";
				using(MemoryStream ms = new MemoryStream())
				{
					bmp.Save(ms, ImageFormat.Png);
					byte[] b = ms.GetBuffer();
					Response.BinaryWrite(b);
				}
				Response.End();
			}
		}
		#endregion


































	}
}
