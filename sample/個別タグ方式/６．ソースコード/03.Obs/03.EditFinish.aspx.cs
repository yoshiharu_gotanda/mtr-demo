using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;


namespace EPSNet.Obs
{
	/// <summary>
	/// EditFinish の概要の説明です。
	/// </summary>
	public class EditFinish : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button ButtonClose;
		protected System.Web.UI.WebControls.Label LabelTitle;
		protected System.Web.UI.WebControls.Label LabelMessage;
		protected System.Web.UI.WebControls.Button ButtonMail;
	
		#region Web フォーム デザイナで生成されたコード 
		//［SRC-03-03-001］override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//［SRC-03-03-002］private void InitializeComponent
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(EditFinish);
		


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//［SRC-03-03-003］private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(this.IsPostBack == false)
				{
					string id = Request.QueryString["id"];
					
					this.ButtonClose.Attributes["onclick"] = "Unload();";

					this.ButtonMail.Attributes["onclick"] = "document.location='SendMail.aspx?id=" + id + "';return false;";

					using(CSql sql = new CSql())
					{
						string cmdTxt;
						SqlDataReader dr;
						sql.Open();
						//管理番号取得
						string mgrNo = string.Empty;
						cmdTxt = "SELECT MgrNo FROM T_ObsMgr2 WHERE ObsMgrID=@ObsMgrID";
						sql.AddParam("@ObsMgrID", id);
						dr = sql.Read(cmdTxt);
						if(dr.Read() == true)
						{
							mgrNo = Common.CnvFromDB(dr["MgrNo"].ToString());
						}
						dr.Close();
						//多国語対応
						#region
						Common.SetText(sql, this.mType, this.ButtonClose);
						Common.SetText(sql, this.mType, this.ButtonMail);
						Common.SetText(sql, this.mType, this.LabelTitle);
						this.LabelMessage.Text = Common.GetText(sql, this.mType, "LabelMessage").Replace("%[MgrNo]", HttpUtility.HtmlEncode(mgrNo));
						#endregion
					}
					
				}
			}
			catch(Exception ex)
			{
				Common.Alert(this, ex.Message);
			}
		}
		#endregion


































	}
}
