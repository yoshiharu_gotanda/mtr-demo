using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace EPSNet.Obs
{
	/// <summary>
	/// Menu の概要の説明です。
	/// </summary>
	public class Menu : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label LabelTitle1;
		protected System.Web.UI.WebControls.Label LabelTitle2;
		protected System.Web.UI.WebControls.Label LabelTitle3;
		protected System.Web.UI.WebControls.LinkButton LinkButtonMaster1;
		protected System.Web.UI.WebControls.LinkButton LinkButtonMaster2;
		protected System.Web.UI.WebControls.LinkButton LinkButtonMaster3;
		protected System.Web.UI.WebControls.LinkButton LinkButtonSearch;
		protected System.Web.UI.WebControls.LinkButton LinkButtonNoProject;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdSearch;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdAnalyze;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdMaster1;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdMaster2;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdMaster3;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdTree;
		protected System.Web.UI.WebControls.LinkButton LinkButtonAnalyze;
	
		#region Web フォーム デザイナで生成されたコード
		//［SRC-03-07-001］override protected void OnInit
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		//［SRC-03-07-002］private void InitializeComponent
		private void InitializeComponent()
		{    
			this.LinkButtonSearch.Click += new System.EventHandler(this.LinkButtonSearch_Click);
			this.LinkButtonAnalyze.Click += new System.EventHandler(this.LinkButtonAnalyze_Click);
			this.LinkButtonMaster1.Click += new System.EventHandler(this.LinkButtonMaster1_Click);
			this.LinkButtonMaster2.Click += new System.EventHandler(this.LinkButtonMaster2_Click);
			this.LinkButtonMaster3.Click += new System.EventHandler(this.LinkButtonMaster3_Click);
			this.LinkButtonNoProject.Click += new System.EventHandler(this.LinkButtonNoProject_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		///================================================================
		/// ﾌﾟﾗｲﾍﾞｰﾄ変数
		///================================================================

		private Type mType = typeof(Menu);


		///================================================================
		/// ｲﾍﾞﾝﾄ
		///================================================================

		/// <summary>
		/// ﾍﾟｰｼﾞﾛｰﾄﾞ
		/// </summary>
		#region private void Page_Load(object sender, System.EventArgs e)
		//［SRC-03-07-003］private void Page_Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			//LoginID取得
			string id = Request.QueryString["UserName"];
			if(id == null && Common.LoginID != null) id = Common.LoginID;
			if(id == null)
			{
				//ｴﾗｰﾍﾟｰｼﾞへ遷移
				Response.Write("<script>");
				Response.Write("document.location='../../Common/Message.aspx'");
				Response.Write("</script>");
			}
			if(this.IsPostBack == false)
			{
				using(CSql sql = new CSql())
				{
					sql.Open();
					//多国語対応
					#region
					Common.SetText(sql, this.mType, this.LabelTitle1);
					Common.SetText(sql, this.mType, this.LabelTitle2);
					Common.SetText(sql, this.mType, this.LabelTitle3);
					Common.SetText(sql, this.mType, this.LinkButtonSearch);
					Common.SetText(sql, this.mType, this.LinkButtonAnalyze);
					Common.SetText(sql, this.mType, this.LinkButtonNoProject);
					Common.SetText(sql, this.mType, this.LinkButtonMaster1);
					Common.SetText(sql, this.mType, this.LinkButtonMaster2);
					this.LinkButtonMaster3.Text = Common.GetText(this.mType, this.LinkButtonMaster3.ID).Replace("%[Numbering]", Common.GetNumberingItemName(sql));
					#endregion
				}
			}
		}
		#endregion

		#region private void LinkButtonMaster1_Click(object sender, System.EventArgs e)
		//［SRC-03-07-004］private void LinkButtonMaster1_Click
		private void LinkButtonMaster1_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "silver";
			this.tdMaster2.BgColor = "white";
			this.tdMaster3.BgColor = "white";
			this.tdAnalyze.BgColor = "white";
			this.tdSearch.BgColor = "white";
			this.tdTree.BgColor = "white";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./PasswordConfirm.aspx?mode=1\";");
			Common.ScriptEnd();
		}
		#endregion

		#region private void LinkButtonMaster2_Click(object sender, System.EventArgs e)
		//［SRC-03-07-005］private void LinkButtonMaster2_Click
		private void LinkButtonMaster2_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "white";
			this.tdMaster2.BgColor = "silver";
			this.tdMaster3.BgColor = "white";
			this.tdAnalyze.BgColor = "white";
			this.tdSearch.BgColor = "white";
			this.tdTree.BgColor = "white";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./PasswordConfirm.aspx?mode=2\";");
			Common.ScriptEnd();
		}
		#endregion

		#region private void LinkButtonMaster3_Click(object sender, System.EventArgs e)
		//［SRC-03-07-006］private void LinkButtonMaster3_Click
		private void LinkButtonMaster3_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "white";
			this.tdMaster2.BgColor = "white";
			this.tdMaster3.BgColor = "silver";
			this.tdAnalyze.BgColor = "white";
			this.tdSearch.BgColor = "white";
			this.tdTree.BgColor = "white";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./PasswordConfirm.aspx?mode=3\";");
			Common.ScriptEnd();
		}
		#endregion

		#region private void LinkButtonSearch_Click(object sender, System.EventArgs e)
		//［SRC-03-07-007］private void LinkButtonSearch_Click
		private void LinkButtonSearch_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "white";
			this.tdMaster2.BgColor = "white";
			this.tdMaster3.BgColor = "white";
			this.tdAnalyze.BgColor = "white";
			this.tdSearch.BgColor = "silver";
			this.tdTree.BgColor = "white";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./Search/List.aspx\";");
			Common.ScriptEnd();
		}
		#endregion

		#region private void LinkButtonAnalyze_Click(object sender, System.EventArgs e)
		//［SRC-03-07-008］private void LinkButtonAnalyze_Click
		private void LinkButtonAnalyze_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "white";
			this.tdMaster2.BgColor = "white";
			this.tdMaster3.BgColor = "white";
			this.tdAnalyze.BgColor = "silver";
			this.tdSearch.BgColor = "white";
			this.tdTree.BgColor = "white";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./Analyze/Main.aspx\";");
			Common.ScriptEnd();
		}
		#endregion

		#region private void LinkButtonNoProject_Click(object sender, System.EventArgs e)
		//［SRC-03-07-009］private void LinkButtonNoProject_Click
		private void LinkButtonNoProject_Click(object sender, System.EventArgs e)
		{
			this.tdMaster1.BgColor = "white";
			this.tdMaster2.BgColor = "white";
			this.tdMaster3.BgColor = "white";
			this.tdAnalyze.BgColor = "white";
			this.tdSearch.BgColor = "white";
			this.tdTree.BgColor = "silver";

			Common.ScriptStart();
			Common.ScriptOutput("parent.ObsMain.location=\"./Tree/Frame.aspx\";");
			Common.ScriptEnd();
		}
		#endregion



































	
	}
}
