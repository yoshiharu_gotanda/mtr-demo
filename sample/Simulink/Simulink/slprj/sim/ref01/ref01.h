#include "__cf_ref01.h"
#ifndef RTW_HEADER_ref01_h_
#define RTW_HEADER_ref01_h_
#include "rtw_modelmap.h"
#ifndef ref01_COMMON_INCLUDES_
#define ref01_COMMON_INCLUDES_
#include <stddef.h>
#include <string.h>
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "rt_nonfinite.h"
#endif
#include "ref01_types.h"
struct aylnaks1vr { struct SimStruct_tag * _mdlRefSfcnS ; struct {
rtwCAPI_ModelMappingInfo mmi ; rtwCAPI_ModelMapLoggingInstanceInfo
mmiLogInstanceInfo ; sysRanDType * systemRan [ 2 ] ; int_T systemTid [ 2 ] ;
} DataMapInfo ; } ; typedef struct { h4cidq24a2 rtm ; } m5nsvwtchyo ; extern
void jajmzx4vxz ( SimStruct * _mdlRefSfcnS , int_T mdlref_TID0 , h4cidq24a2 *
const kcgyl3elef , void * sysRanPtr , int contextTid ,
rtwCAPI_ModelMappingInfo * rt_ParentMMI , const char_T * rt_ChildPath , int_T
rt_ChildMMIIdx , int_T rt_CSTATEIdx ) ; extern void mr_ref01_MdlInfoRegFcn (
SimStruct * mdlRefSfcnS , char_T * modelName , int_T * retVal ) ; extern
const rtwCAPI_ModelMappingStaticInfo * ref01_GetCAPIStaticMap ( void ) ;
#endif
