#include "__cf_ref01.h"
#include "ref01.h"
#include "rtw_capi.h"
#include "ref01_private.h"
static rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 0 , ( NULL ) , ( NULL ) ,
0 , 0 , 0 , 0 , 0 } } ; static rtwCAPI_LoggingBusElement rtBusElements [ ] =
{ { 0 , rtwCAPI_signal } } ; static rtwCAPI_LoggingBusSignals rtBusSignals [
] = { { ( NULL ) , ( NULL ) , 0 , 0 , ( NULL ) } } ; static rtwCAPI_States
rtBlockStates [ ] = { { 0 , - 1 , ( NULL ) , ( NULL ) , ( NULL ) , 0 , 0 , 0
, 0 , 0 , 0 } } ; static rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "" , ""
, 0 , 0 , 0 , 0 , 0 , 0 } } ; static rtwCAPI_ElementMap rtElementMap [ ] = {
{ ( NULL ) , 0 , 0 , 0 , 0 } , } ; static rtwCAPI_DimensionMap rtDimensionMap
[ ] = { { rtwCAPI_SCALAR , 0 , 0 , 0 } } ; static uint_T rtDimensionArray [ ]
= { 0 } ; static rtwCAPI_FixPtMap rtFixPtMap [ ] = { { ( NULL ) , ( NULL ) ,
rtwCAPI_FIX_RESERVED , 0 , 0 , 0 } , } ; static rtwCAPI_SampleTimeMap
rtSampleTimeMap [ ] = { { ( NULL ) , ( NULL ) , 0 , 0 } } ; static int_T
rtContextSystems [ 2 ] ; static rtwCAPI_LoggingMetaInfo loggingMetaInfo [ ] =
{ { 0 , 0 , "" , 0 } } ; static rtwCAPI_ModelMapLoggingStaticInfo
mmiStaticInfoLogging = { 2 , rtContextSystems , loggingMetaInfo , 0 ,
rtBusSignals , { 0 , NULL , NULL } , 0 , ( NULL ) } ; static
rtwCAPI_ModelMappingStaticInfo mmiStatic = { { rtBlockSignals , 0 , ( NULL )
, 0 , ( NULL ) , 0 } , { ( NULL ) , 0 , ( NULL ) , 0 } , { rtBlockStates , 0
} , { rtDataTypeMap , rtDimensionMap , rtFixPtMap , rtElementMap ,
rtSampleTimeMap , rtDimensionArray } , "float" , & mmiStaticInfoLogging , 0 ,
} ; const rtwCAPI_ModelMappingStaticInfo * ref01_GetCAPIStaticMap ( ) {
return & mmiStatic ; } static void ref01_InitializeSystemRan ( h4cidq24a2 *
const kcgyl3elef , sysRanDType * systemRan [ ] , int_T systemTid [ ] , void *
rootSysRanPtr , int rootTid ) { systemRan [ 0 ] = ( sysRanDType * )
rootSysRanPtr ; systemRan [ 1 ] = ( NULL ) ; systemTid [ 1 ] = jren5tcwu0 [ 0
] ; systemTid [ 0 ] = rootTid ; rtContextSystems [ 0 ] = 0 ; rtContextSystems
[ 1 ] = 0 ; } void ref01_InitializeDataMapInfo ( h4cidq24a2 * const
kcgyl3elef , void * sysRanPtr , int contextTid ) { rtwCAPI_SetVersion (
kcgyl3elef -> DataMapInfo . mmi , 1 ) ; rtwCAPI_SetStaticMap ( kcgyl3elef ->
DataMapInfo . mmi , & mmiStatic ) ; rtwCAPI_SetLoggingStaticMap ( kcgyl3elef
-> DataMapInfo . mmi , & mmiStaticInfoLogging ) ; rtwCAPI_SetPath (
kcgyl3elef -> DataMapInfo . mmi , ( NULL ) ) ; rtwCAPI_SetFullPath (
kcgyl3elef -> DataMapInfo . mmi , ( NULL ) ) ; rtwCAPI_SetInstanceLoggingInfo
( kcgyl3elef -> DataMapInfo . mmi , & kcgyl3elef -> DataMapInfo .
mmiLogInstanceInfo ) ; rtwCAPI_SetChildMMIArray ( kcgyl3elef -> DataMapInfo .
mmi , ( NULL ) ) ; rtwCAPI_SetChildMMIArrayLen ( kcgyl3elef -> DataMapInfo .
mmi , 0 ) ; ref01_InitializeSystemRan ( kcgyl3elef , kcgyl3elef ->
DataMapInfo . systemRan , kcgyl3elef -> DataMapInfo . systemTid , sysRanPtr ,
contextTid ) ; rtwCAPI_SetSystemRan ( kcgyl3elef -> DataMapInfo . mmi ,
kcgyl3elef -> DataMapInfo . systemRan ) ; rtwCAPI_SetSystemTid ( kcgyl3elef
-> DataMapInfo . mmi , kcgyl3elef -> DataMapInfo . systemTid ) ;
rtwCAPI_SetGlobalTIDMap ( kcgyl3elef -> DataMapInfo . mmi , & jren5tcwu0 [ 0
] ) ; }
