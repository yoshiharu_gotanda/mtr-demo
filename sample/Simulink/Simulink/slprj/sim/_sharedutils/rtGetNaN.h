/*
 * rtGetNaN.h
 *
 * Code generation for model "ref01".
 *
 * Model version              : 1.16
 * Simulink Coder version : 8.1 (R2013a) 13-Feb-2013
 * C source code generated on : Thu Dec 04 16:23:00 2014
 */
#ifndef RTW_HEADER_rtGetNaN_h_
#define RTW_HEADER_rtGetNaN_h_
#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"

extern real_T rtGetNaN(void);
extern real32_T rtGetNaNF(void);

#endif                                 /* RTW_HEADER_rtGetNaN_h_ */
