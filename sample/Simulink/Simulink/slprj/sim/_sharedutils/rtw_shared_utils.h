/*
 * File: rtw_shared_utils.h
 *
 * Code generation for model "ref01".
 *
 * Model version              : 1.16
 * Simulink Coder version : 8.1 (R2013a) 13-Feb-2013
 * C source code generated on : Thu Dec 04 16:23:00 2014
 */
#ifndef RTW_HEADER_rtw_shared_utils_h_
#define RTW_HEADER_rtw_shared_utils_h_
#ifndef rtw_shared_utils_h_
# define rtw_shared_utils_h_

/* Shared utilities general include header file.*/
#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#endif                                 /* rtw_shared_utils_h_ */
#endif                                 /* RTW_HEADER_rtw_shared_utils_h_ */
